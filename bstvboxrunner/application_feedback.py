"""

    blueprint to handle http feedback from devices to test station



    /restop/api/v1/tvbox_agents/feedback/ardom/1
    /restop/api/v1/tvbox_agents/feedback/stats/1
    /restop/api/v1/tvbox_agents/feedback/lineup/1


"""

from flask import Blueprint,request

from restop_components.stats_scheduler import StatSchedulerClient
from models import BsTvboxAgents

ALL_METHODS= ['GET', 'POST','PUT','PATCH','DELETE','HEAD','OPTIONS']

feedback= Blueprint('feedback','feedback')



#
# serving feedback dynamic urls ( catch all urls like /feedback/*
#
# @feedback.route('/feedback/', defaults={'path': ''}, methods=ALL_METHODS)
# @feedback.route('/feedback/<path:path>' , methods=ALL_METHODS)
# def feedback_catch_all(path):
#     """
#
#
#     :param path:
#     :return:
#     """
#     method= request.method
#
#     return "feedback url for %s" % path





# #@feedback.route('/PostArDom' ,methods=['GET', 'POST'])
# def post_ardom():
#     """
#
#     The stb do a GET to this url to send a Dump
#
#     the stb send Dom to the address stored in file:
#          /flash/Resources/resources/test_params
#
#         eg: http://192.168.12/PostArDom
#
#     we store it in a file dump.html
#
#
#     :return:
#     """
#     print "receive an Ar Dom"
#     with open('./dump.html','w') as fh:
#         fh.write(request.data)
#     #print request.content
#     return ""


# #@feedback.route('/feedback/ardom/<device>' ,methods=['GET', 'POST'])
# @feedback.route('/tvbox_agents/feedback-ardom/<device>' ,methods=['GET', 'POST'])
# def post_tvbox_ardom(device='TV'):
#     """
#
#     The stb do a GET to this url to send a Dump
#
#     the stb send Dom to the address stored in file:
#          /flash/Resources/resources/test_params
#
#         eg: http://192.168.12/feedback/ardom/tv
#
#     we store it in a file ardom-tv.html
#
#
#     :return:
#     """
#     filename= "ardom-%s.html" % device
#     print "receive an ArDom for device: %s -> %s" % (device,filename)
#     data = request.get_data()
#     print "ArDom length=%d" % len(data)
#     with open( filename,'w') as fh:
#         fh.write(data)
#     return ""


@feedback.route('/tvbox_Agents/feedback-lineup/<device>' ,methods=['GET', 'POST'])
def lineup(device='TV'):
    """

        the stb call /lineup to post it file /flash/Resources/lineup/lineup.json


    :return:
    """
    print "receive lineup for device: %s" % device
    data= request.get_data()
    print "lineup length=%d" % len(data)
    with open('./lineup-%s.json' % device ,'w') as fh:
        fh.write(data)
    return ""


@feedback.route('/tvbox_Agents/feedback-stats/<device>' ,methods=['GET', 'POST'])
def stat_scheduler(device='TV'):
    """

        the stb call /stat_scheduler to post stat result

    :return:
    """
    print "receive stats for device %s" % device
    data= request.get_data()
    filename= "./stats-%s.txt" % device
    print "stats length: %d , file is %s" % (len(data),filename)
    with open( filename ,'w') as fh:
        fh.write(data)
    data=data.split('\n')
    #scheduler= StatSchedulerClient('dummy')
    scheduler= StatSchedulerClient.push_to_graphite(data,device=device,configfile='collectors.ini')
    return ""