
class Dummy(object):
    """
        TVBOXRUNNER : the Orange Set Top Box
    """



    def _close_session(self,  **kwargs):
        """
                    close a local session
        
            POST /hub/samples/-/close_session
        
                parameters:  members
        
        :return:
        """


    def _open_session(self,  **kwargs):
        """
                    open a local session
        
            POST /hub/samples/-/open_session
        
        :return:
        """


    def detect_motion(self, alias, **kwargs):
        """
                    check if there is a movement
        :return:
        """


    def expect(self, alias, **kwargs):
        """
                    read serial to find the pattern
        
        :param pattern:
        :param timeout:
        :return:
        """


    def get_tv_esp(self, alias, **kwargs):
        """
                    replacement TvEsp(channel )
        
        
        :param channel:
        :return:
        """


    def menu_select(self, alias, **kwargs):
        """
                :param path: string like 0/2/1
        :return:
        """


    def dummy(self, alias, **kwargs):
        """
                 POST /restop/api/v1/hub/samples/1/dummy
        
        :param item:
        :param kwargs:
        :return:
        """


    def feedback_ardom(self, alias, **kwargs):
        """
                    call back for ardom
        
            the stb performs a curl POST on
        
            https://<internal_ip>/restop/upload/tvbox_agents/<device>/feedback_ardom
        
            the master redirects here with
        
            localhost:5005/restop/api/v1/tvbox_agents/<device>/feedback_ardom
        
        :return:
        """


    def feedback_scheduler(self, alias, **kwargs):
        """
                call back for stats scheduler
        
         the stb performs a curl POST on
        
         https://<internal_ip>/restop/upload/tvbox_agents/<device>/feedback_scheduler
        
         the master redirects here with
        
         localhost:5005/restop/api/v1/tvbox_agents/<device>/feedback_feedback_schduler
        """


    def page_actionInstantRecord(self, alias, **kwargs):
        """
        
        """


    def page_actionScheduleRecord(self, alias, **kwargs):
        """
        
        """


    def page_actionSelectInFipPolaris(self, alias, **kwargs):
        """
        
        """


    def page_actionSelectInLiveBannerPolaris(self, alias, **kwargs):
        """
        
        """


    def page_actionSelectInToolboxPolaris(self, alias, **kwargs):
        """
        
        """


    def page_actionSelectInVodMenuPolaris(self, alias, **kwargs):
        """
        
        """


    def page_action_desk_move_to(self, alias, **kwargs):
        """
                    on menu page move to selected text
        
        :param text:
        :return:
        """


    def page_action_desk_select(self, alias, **kwargs):
        """
                    on menu page , move to selected text and press OK
        
        :param text:
        :return:
        """


    def page_action_select(self, alias, **kwargs):
        """
                :param t: string eg epgNOW
        :param partial:
        :return:
        """


    def page_action_select_in_live_banner(self, alias, **kwargs):
        """
        
        """


    def page_findPvrAdvancedConflictPicto(self, alias, **kwargs):
        """
        
        """


    def page_find_in_Dialog_box(self, alias, **kwargs):
        """
        
        """


    def page_find_in_list(self, alias, **kwargs):
        """
        
        """


    def page_find_in_page(self, alias, **kwargs):
        """
        
        """


    def page_find_progress_bar(self, alias, **kwargs):
        """
        
        """


    def page_getAmountFromPrepaidAccount(self, alias, **kwargs):
        """
        
        """


    def page_getInfoFromLiveBannerPolaris(self, alias, **kwargs):
        """
        
        """


    def page_getInfoFromPvrBanner(self, alias, **kwargs):
        """
        
        """


    def page_getInfoFromRecordFocus(self, alias, **kwargs):
        """
        
        """


    def page_getInfoFromRecordPage(self, alias, **kwargs):
        """
        
        """


    def page_getInfoFromToolboxPolaris(self, alias, **kwargs):
        """
        
        """


    def page_getInfoFromVodBaner(self, alias, **kwargs):
        """
        
        """


    def page_getInfoFromVodPage(self, alias, **kwargs):
        """
        
        """


    def page_getLabelFromDeskFocus(self, alias, **kwargs):
        """
        
        """


    def page_getManualRecordingSchedulerStatus(self, alias, **kwargs):
        """
        
        """


    def page_getRecordInfoFromFip(self, alias, **kwargs):
        """
        
        """


    def page_getTextFromDialogBox(self, alias, **kwargs):
        """
        
        """


    def page_getTitleFromVodFocus(self, alias, **kwargs):
        """
        
        """


    def page_get_info_from_epg_focus(self, alias, **kwargs):
        """
        
        """


    def page_get_info_from_live_banner(self, alias, **kwargs):
        """
        
        """


    def page_get_info_from_mosaic_focus(self, alias, **kwargs):
        """
        
        """


    def page_get_label_from_list_focus(self, alias, **kwargs):
        """
        
        """


    def page_get_list(self, alias, **kwargs):
        """
        
        """


    def page_get_status(self, alias, **kwargs):
        """
                :param key: string , one of
                stbStatus,noRightPanel,scene,dialog,frontPanel,miniLive,redCross,mosaicAdBanner
        :return:
        """


    def page_resolvePvrAdvancedConflict(self, alias, **kwargs):
        """
        
        """


    def power_off(self, alias, **kwargs):
        """
                :return:
        """


    def power_on(self, alias, **kwargs):
        """
                :return:
        """


    def scheduler_close(self, alias, **kwargs):
        """
                :param clear:
        :return:
        """


    def scheduler_init(self, alias, **kwargs):
        """
                    ask scheduler send stat requests to stb
        :return:
        """


    def scheduler_sync(self, alias, **kwargs):
        """
                    sync the trace:
                stop scheduler
                curl trace file
                rm trace file
                restart scheduler
        
        :return:
        """


    def send(self, alias, **kwargs):
        """
                    send a command to console via serial port
        :param cmd:
        :return:
        """


    def send_cmd(self, alias, **kwargs):
        """
                    send a command to stb via serial port
        :param cmd:
        :return:
        """


    def send_key(self, alias, **kwargs):
        """
                   send a key with remote ir via http
           wait key reading serial port
        
        :param key:
        :return:
        """


    def serial_expect(self, alias, **kwargs):
        """
                    read serial to find the pattern
        
        :param pattern:
        :param timeout:
        :return:
        """


    def serial_synch(self, alias, **kwargs):
        """
                    empty the serial log
        :param timeout:
        :return:
        """


    def serial_watch(self, alias, **kwargs):
        """
                    watch serial log for a duration
        :param timeout:
        :return:
        """


    def start(self, alias, **kwargs):
        """
                    start the loop proxy between redis queues and tv serial port
        
        :return:
        """


    def stb_get_env(self, alias, **kwargs):
        """
        
        """


    def stb_get_lineup(self, alias, **kwargs):
        """
                    fetch the file
        
        :return:
        """


    def stb_info(self, alias, **kwargs):
        """
                send command pcb_cli "DeviceInfo.AdditionalSoftwareVersion.?"
        
            and get
                DeviceInfo.AdditionalSoftwareVersion=01.02.22,01.28.20
        
        :param kwargs:
        :return:
        """


    def stb_pcb_cli(self, alias, **kwargs):
        """
                :param command:
        :return:
        """


    def stb_send_dump(self, alias, **kwargs):
        """
                :return:
        """


    def stb_status(self, alias, **kwargs):
        """
                :return:
        """


    def stb_toolbox_list(self, alias, **kwargs):
        """
                :return:
        """


    def stb_toolbox_select(self, alias, **kwargs):
        """
                :return:
        """


    def stb_zap(self, alias, **kwargs):
        """
                :param channel: string eg 'tv1
        :return:
        """


    def stop(self, alias, **kwargs):
        """
                :return:
        """


    def sync(self, alias, **kwargs):
        """
                :param timeout:
        :return:
        """


    def tv_data_get(self, alias, **kwargs):
        """
        
        """


    def tv_get_first_channel_Dtt_off(self, alias, **kwargs):
        """
                :param channel: string eg tv2 )
        :return:
        """


    def tv_get_first_channel_Dtt_on(self, alias, **kwargs):
        """
                :param channel: string eg tv2 )
        :return:
        """


    def tv_get_lcn(self, alias, **kwargs):
        """
                :param channel:
        :return:
        """


    def video_screenshot(self, alias, **kwargs):
        """
                    check if there is a movement
        :return: False or encoded image
        """


    def watch(self, alias, **kwargs):
        """
                    watch serial log for a duration
        :param timeout:
        :return:
        """


    def wording_get(self, alias, **kwargs):
        """
        
        """
