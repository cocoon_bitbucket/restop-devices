import requests
from restop_tests.mockserver.mockclient import MockClient

from bstvboxrunner.tvbox_device.remote_control import StbRemoteControl

response_ok= {
	"result" : {
		"responseCode" : "0",
		"message" : "ok",
		"data" : {}
	}
}


def test_mockserver(remote_stb):

    #c= get_mock_client()


    r= remote_stb.check_mockserver()
    r= remote_stb.create_mock_url('/my/url', 'POST', status=200, data= {'name': 'name'})

    r= requests.post('http://localhost:5016/my/url',json={'new':'new'})
    assert r.status_code==200
    data=r.json()
    assert data['name'] == 'name'

    r= remote_stb.mock_url_usage(clear=False)
    assert r['my/url']['POST']['new'] == 'new'

    r = remote_stb.mock_url_usage(url='/my/url', method='POST', clear=True)
    assert r['new'] == 'new'

    r= remote_stb.mock_url_usage(url='/my/url')
    assert len(r) == 0


    #client.shutdown_mockserver()

    return


def test_remote_stb(mock_stb_remote):
    """

    :param client:
    :return:
    """
    r = mock_stb_remote.check_mockserver()

    remote= StbRemoteControl(base_url='http://localhost:5016')

    # set a mock response for send key
    url= '/remoteControl/cmd'
    r = mock_stb_remote.create_mock_url(url, 'GET', status=200, data=response_ok)

    rc= remote.send_key("1")
    data=rc.json()
    assert data['result']['responseCode'] == '0'

    return





if __name__=="__main__":


    def remote_stb():
        """


        :return:
        """
        host = 'localhost'
        port = 5016
        client = MockClient(host=host, port=port)
        client.start_mockserver(threaded=True)
        return client


    # client= remote_stb()
    # test_mockserver(client)
    # client.shutdown_mockserver()

    client= remote_stb()
    test_remote_stb(client)
    client.shutdown_mockserver()





    print "Done"