"""


    test acces to usbhub server on master via the usbhub client


    prerequisite
        a usbhub server must be started


"""
import time

from walrus import Database
from wbackend.model import Model

from restop_usb.models import UsbQueueHub
from restop_usb.controllers import ProcessUsbQueueHubServer

from restop_usb.client  import UsbControllerClient


#REDIS_HOST= '192.168.1.21'
REDIS_HOST= 'localhost'
REDIS_PORT= 6379
REDIS_DB= 0


redis_db= Database(host=REDIS_HOST,port=REDIS_PORT,db=REDIS_DB)
redis_db.flushdb()
Model.bind(redis_db)

hub_model= UsbQueueHub.create()
hub_server= ProcessUsbQueueHubServer(hub_model)
hub_server.create_queue_server('_loop',device="loop://?logging=debug")
hub_server.create_queue_server('usb0',device='/dev/ttyUSB0')
hub_server.start()


def test_loop():
    """

    :return:
    """

    client= UsbControllerClient(redis_db=redis_db)

    usb= client.start_queue_server('_loop')

    time.sleep(1)
    usb.clear()

    usb.write('\n')
    usb.write('ls -l /tmp\n')

    for i in xrange(0,10):
        res = usb.read()
        print res
        time.sleep(1)
    usb.exit()

    return




def test_usb0():
    """

    :return:
    """

    client = UsbControllerClient(redis_db=redis_db)

    usb = client.start_queue_server('usb0')

    time.sleep(1)
    usb.clear()

    usb.write('\n')
    usb.write('ls -l /tmp\n')

    for i in xrange(0, 10):
        res = usb.read()
        print res
        time.sleep(2)
    usb.exit()

    return


if __name__=="__main__":

    test_loop()
    test_usb0()

    print "Done"


