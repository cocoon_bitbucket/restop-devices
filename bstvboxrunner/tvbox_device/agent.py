"""
    native agent

    use NewTvTesting with a

    * built-in Remote Controller
        ( drive stb http remote controller instead of IR )
    * or a local IR controller
        ( yet not available )



"""
import os
import time
import datetime
import codecs

from restop.application import ApplicationResponse,ApplicationError

#from restop_components.some_agents.pipe_agents import SerialAgent
from restop_components.some_agents.usb_agent import UsbAgent

from restop_components.powerswitch_device import PowerswitchPlug
from restop_components.feedback import FeedBackSender
from restop_components.video_capture.video_detection import StickTvVideoDetection

from remote_control import StbRemoteControl

from stb_menu import  StbMenuHandler

from ntvt_interface import Pager
from ntvt_interface import  Wording
#from ntvt_interface import  TvData, VodData
#from ntvt_interface import TvEsp,TvItem , ChannelDetail

from stbdom.keyinfo import Keyinfo
from stbdom.arviews import ToolBoxLive,LiveBanner,LiveBannerPolaris
from stbdom.armenu import StbMenu


from pcb_cli import PcbCliManager


ardom_callback_pattern="https://%s/restop/upload/%s/%s/feedback_ardom"

base = os.getcwd()
DUMP_FILENAME= os.path.join(base,"dump.html")


#CALL_BACK_ARDOM= "https://192.168.1.21/PostArDom"

default_metrics= ['df', 'cpu','meminfo','loadavg']

default_powerswitch= 'powerswitch'


#class Agent(BaseAgent):
#class Agent(SerialAgent):
class Agent(UsbAgent):
    """

        tvbox agent
        -----------

        an agent to drive the orange tv set top box

        * emulate the remote control to send keystroke to the tvbox
        * send commands and read logs via serial port


    """
    collection= 'bstvbox_agents'

    SYNCHRO_TAG= 'SYNCHRO TAG'
    SYNCHRO_KEYS= ['OK','HOME','VOD']

    _default_parameters= dict (
        external_url= 'localhost',
        grafana_url= "localhost:3000/dashboard/db/restop" ,
        graphite_address= "localhost:8000",
        default_metrics= ['df', 'cpu','meminfo','loadavg'],

        scheduler_feedback_url='https//localhost/feedback/stats',

        lineup_filename='/flash/Resources/lineup/lineup.json',
        lineup_feedback_url = '%s/feedback/lineup/%s',
    )

    # others
    def _setup(self):
        """
            create
                a remote: remote controller fot sending ir keys
                a serial: serial interface with stb
        :return:
        """
        super(Agent,self)._setup()

        # add powerswitch driver
        if 'powerswitch_device' in self.model.parameters:
            if 'powerswitch_port' in self.model.parameters:
                self._powerswitch= PowerswitchPlug.get(
                    self.model.parameters['powerswitch_device'],
                    channel= self.model.parameters['powerswitch_port']
                )
            else:
                raise ApplicationError('no powerswitch_port declared in platform for device: %s' % self.alias)
        else:
            self._powerswitch=None


        # SETUP ardom
        #self.ardom_callback= "https://192.168.1.21/restop/hub/tvbox_agents/tv/feedback_Ardom
        self.ardom_callback = ardom_callback_pattern % (self._station_peer_address,self.collection,self.alias)
        #self.ardom_callback = ardom_callback_pattern % (self.device_data['peer_address'], self.alias)
        self.ardom_filename = "ardom-%s.html" % self.alias

        # start remote control for stb
        #remote_control_url= '%s:%s' % (self.device_data['peer_address'],self.device_data['peer_port'])
        remote_control_url = self.device_data['remote_url']
        self.log.debug('set Stb Remote controller to [http://%s]' %  remote_control_url)
        self.remote= StbRemoteControl(remote_control_url,log=self.log)

        self._page= None

        self.Wording= Wording
        self.TvData= None
        self.VodData= None

        self._video_detection = StickTvVideoDetection()

        return True

    def start(self):
        """
            start the loop proxy between redis queues and tv serial port

        :return:
        """
        rc= super(Agent,self).start()

        self._set_ardom_callback_url(self.ardom_callback)
        message= "%s Ar Dom call back Url set to %s" % (self.agent_id,self.ardom_callback)
        self.log.info(message)

        return rc

    @property
    def page(self):
        """
            lazy access to pager (webkit)

        :return:
        """
        if not self._page:

            # lazzy launch pager (webkit)
            # http call back eg http://localhost:5005
            http_callback= "http://%s:%s" % ( self.dashboard.main['hub_ip'],self.dashboard.main['hub_port'])
            # selenium_host eg localhost:4444
            #selenium_host= self.device_data.get('selenium_host','localhost:4444')
            self._page = Pager(self.remote,device=self.alias,http_callback=http_callback)

            #time.sleep(2)
        return self._page



    def stop(self):
        """
            stop the device agent
        :return:
        """
        rc= super(Agent,self).stop()

        # shutdown webkit driver
        if self._page:
            self._page.close()

        return rc


    def _to_dict(self,obj):
        """

        :param obj:
        :return:
        """
        dic= {}
        for attr_name in dir(obj):
            if attr_name.startswith('_'):
                continue
            attr_value= getattr(obj,attr_name)
            if isinstance(attr_value,(basestring,int,bool)):
                dic[attr_name]=attr_value
            elif isinstance(attr_value,datetime.datetime):
                dic[attr_name]= str(attr_value)
            elif isinstance(attr_value,datetime.timedelta):
                dic[attr_name]=str(attr_value)
            elif attr_value is None:
                dic[attr_name]=None
            else:
                pass
        return dic


    def _remote_send_key(self,key):
        """

            low level send key via remote command

        :param self:
        :param keys:
        :return:
        """
        #print self.serial.trace("send key: [%s]" % key)
        #print self.client.trace("send key: [%s]" % key)
        self.remote.send_key(key)

    def _serial_wait_key(self,key,timeout= 5):
        """

        low level watch a stb Event key in serial output

        :param key:
        :return:
        """
        #pattern= r'app.controller\> stb event \[%s\] \(count\: 1\)' % key
        # 'root: [1;37m[DEBUG] app.controller> stb event [VOD] (count: 1)...[0m\n'
        pattern= "app.controller\> stb event \[%s\]" % key
        #pattern= r'stb event'
        r= self.expect(pattern,timeout=timeout)

        return r

    def send_key(self,key,timeout=5,wait=False):
         """
            send a key with remote ir via http

         :param key: string , the key code to send to remote control (eg KEY_OK )
         :return:
         """
         result= ["======= send key [%s] =====\n" % str(key),]
         self.log.info(result)
         self._remote_send_key(key)

         if wait:
             # check effective wait ( can wait only on SYNCHRO KEYS )
            if not key.upper() in self.SYNCHRO_KEYS:
                wait=False

         # to flush buffer
         if wait:
            #time.sleep(1)
            #self.serial.send("echo SYNCRO TAG %s" % str(time.time()))
            # wait for serial feed back
            r= self._serial_wait_key(key,timeout=timeout)
         else:
            # do not wait for serial feed back just watch log
            #r= self._serial_watch(timeout=timeout)
            # DO NOTHING
            r=""

         result.extend(r)
         #print "".join(result)
         return result

    def send_keys(self,keys):
        """

        :param keys:
        :return:
        """
        result= ["======= send keys [%s] =====\n" % str(keys),]
        self.log.info(result)
        self.remote.send_keys(keys)
        return result

    def send_long_key(self,key,timer=None,hold_time=0.8):
        """

        :param key:
        :return:
        """
        result= ["======= send long key [%s] =====\n" % str(key),]
        self.log.info(result)
        self.remote.send_long_key(key,timer=timer,hold_time=hold_time)
        return result


    def stb_send_dump(self,mode=0):
        """
            ask the stb to send the ardom status

        :param:mode: int
                mode =0 => press Blue / wait / release Blue
                mode= 1 => press ZOOM / press Record
        :return: True
        """
        r= self.remote.stb_send_dump(mode=mode)
        return r


    def stb_fetch_dump(self):
        """
            return the content of the last ardom.html
        :return:
        """
        content= u""
        if os.path.isfile(self.ardom_filename):
            # dom = BeautifulSoup(codecs.open( self.ardom_filename ,"r", "utf8"),'lxml')
            with codecs.open(self.ardom_filename, "r", "utf8") as fh:
                content=fh.read()
        return content




    # def stb_zap(self,channel):
    #     """
    #
    #
    #     :param channel: string eg 'tv1
    #     :return:
    #     """
    #     #tv_esp= TvEsp(TvData.T(channel))
    #     tv_esp= self.tv_data_get(channel)
    #     #lcn =tv_esp.lcn
    #     lcn= tv_esp
    #     r= self.remote.stb_zap(lcn)
    #     return r


    def serial_watch(self,timeout=5):
        """
            watch the device serial log for a duration of <timeout>


        :param timeout: integer , nb of seconds to wait (default=5)
        :return:
        """
        return self.watch(timeout=timeout)

    def serial_expect(self,pattern,timeout=5,cancel_on=None, regex='no', **kwargs ):
        """
            read serial to find a pattern

        :param pattern: string , the pattern to search for
        :param timeout: integer , number of second to wait before timeout
        """
        return self.expect(pattern,timeout=5,cancel_on=cancel_on, regex=regex, **kwargs )


    def serial_synch(self,timeout=10):
        """
            empty the device serial log

        :param timeout: integer , number of second to wait before timeout
        """
        return self.sync(timeout=timeout)


    def stb_status(self):
        """
            get the status of the stb
        :return: dictionary
        """
        r= self.remote.stb_status()
        #print r.content
        return r


    def send_cmd(self,cmd):
        """
            send a command to stb via serial port
        :param cmd: string
        :return:
        """
        return self.send(cmd)

        # r= self.serial.send(cmd)
        # return r

    def _set_ardom_callback_url(self,url):
        """
            write the station callback url for ardom dump

            echo "https://192.168.12/PostArDom"  >  /flash/Resources/resources/test_params


        :return:
        """
        self.log.debug("set ardom callback to %s" % url)
        # send newline to wake up serial
        self.send("\n")
        time.sleep(0.1)
        # echo on
        #self.send('echo on')
        # create directory if not exists
        self.send('mkdir -p /flash/Resources/resources/')
        # send command to create the call back
        command_line= 'echo "%s" > /flash/Resources/resources/test_params' % url
        r= self.send(command_line)
        return r


    def menu_select(self,path):
        """
            select an option within the MENU screen

        :param path: string like 0/2/1
        """
        menu= StbMenuHandler(self,delay=2)
        try:
            menu.select(path)
        except Exception as e:
            self.log.error("menu select has failed %s" % e.message)
            return False
        return True


    def stb_get_lineup(self):
        """

           ask the stb to post the lineup file to the test station

        :return:
        """

        """
            ask the stb to post the lineup file to the test station

            lineup file: /flash/Resources/lineup/lineup.json

            curl -i -X POST --data-binary @/flash/Resources/lineup/lineup.json https://192.168.1.21/feedback/lineup


        :return:
        """

        #host= "https://192.168.1.21"
        host= self._platform_info['internal_ip']
        #filename= '/flash/Resources/lineup/lineup.json'
        #feedback_url= '%s/feedback/lineup/%s' % (host,self.alias)

        fd = FeedBackSender(self.agent_id, log=self.log,redis_db=self.redis_db)
        feedback_url= self._default_parameters['lineup_feedback_url'] % (host,self.alias)
        filename= self._default_parameters['lineup_filename']
        r= fd.get_by_curl(filename,feedback_url)

        time.sleep(5)

        # #curl_line= "curl -i -X POST --data-binary @/flash/Resources/lineup/lineup.json https://192.168.1.21/feedback/lineup"
        #
        # host= "https://192.168.1.21"
        # filename= '/flash/Resources/lineup/lineup.json'
        # feedback_url= '%s/feedback/lineup' % host
        #
        # cmd= '/usr/bin/curl -i -vv -X POST --data-binary @%s %s' % (filename,feedback_url)
        #
        # self.log.debug("send lineup command to stb serial: %s" % cmd)
        # self.serial.send(cmd)
        #
        # time.sleep(15)

        return True


    #
    # interface to wording
    #
    def wording_get(self,tag):
        """
            get the tag in the wording config

        :param tag: string
        :return:
        """
        return self.Wording.W[tag]

    #
    # interface to tv data
    #
    def tv_data_get(self,tag):
        return self.TvData.T[tag]

    #
    # interface to config.ENV
    #
    def stb_get_env(self,key=None):
        data= self._to_dict(Env)
        if key:
            return data[key]
        return data


    #
    # pager methods
    #

    # def page_actionSelectInToolboxPolaris(self, category, choice1=None, choice2=None):
    #     raise NotImplemented

    # def page_action_select_in_live_banner(self,**kwargs):
    #     return self.page_action_select_in_live_banner(**kwargs)
    #
    # def page_actionSelectInLiveBannerPolaris(self, lcn, up=True, maxValue=10):
    #     raise NotImplemented
    #
    # def page_actionSelectInFipPolaris(self, text, level=1):
    #     raise NotImplemented

    def stb_channel_info(self):
        """
            read live banner corresponding to KEY_INFO

        :return: dict ProogramInfo
        """
        # try the other way
        dom = self.page._get_dom()
        page = Keyinfo(dom)
        data = page.feed()
        data= page.export(data)

        # close webkit
        self.log.debug("stb_channel_info() got: %s" % str(data))
        self.page.close()

        return data





    def page_action_desk_move_to(self, text):
        """
            on menu screen move to selected text

        :param text: string to search for
        """
        page = StbMenu.from_file(self.ardom_filename)
        page.feed()
        # get the path for this text in menu page
        path = page.get_slug_path(text)
        self.log.debug("path= %s" % path)
        menu = StbMenuHandler(self)
        # send keys to select the item
        rc= menu.select(path)
        return rc

    def page_action_desk_select(self,text):
        """
            on menu screen , move to selected text and press OK

        :param text: string
        """
        self.page_action_desk_move_to(text)
        rc= self._remote_send_key("KEY_OK")
        return rc



    # def page_actionInstantRecord(self, length=5):
    #     raise NotImplemented
    #
    # def page_actionScheduleRecord(self, package, lcn, date, length, recurrence=0, idWord=None):
    #     raise NotImplemented
    #
    # def page_actionSelectInVodMenuPolaris(self, category, name):
    #     raise NotImplemented
    #
    # def page_getLabelFromDeskFocus(self):
    #     raise NotImplemented


    def page_getInfoFromPvrBanner(self):
        data=None
        # get a ProgramInfoItem object
        r= self.page.getInfoFromPvrBanner()
        # close webkit
        self.page.close()
        if r:
            data = self._to_dict(r)
        return data

    def page_get_info_from_live_banner(self,**kwargs):
        """

            get the data contained in the live banner
        :return: a dictionary with data
        """
        # Try the native way
        #program_info=self.page.getInfoFromLiveBanner()
        program_info=None
        if program_info:
            data= {}
            if program_info:
                data = self._to_dict(program_info)
            return data

        # try the other way
        dom = self.page._get_dom()
        page = LiveBanner(dom)
        data = page.feed()
        data= page.export(data)
        # return program_info

        # close webkit
        self.log.debug("getInfoFromLiveBanner() got: %s" % str(data))
        self.page.close()

        return data


        # #filename = os.path.join( os.getcwd(), self.ardom_filename)
        # #dom_file= 'file://%s' % filename
        # dom_file= self.ardom_filename
        # page=  LiveBanner.from_file(dom_file)
        # data = page.feed()
        # program_info= page.export(data)
        # return program_info


    # def page_get_info_from_live_banner_polaris(self):
    #     """
    #         info live banner for stbplay
    #     :return:
    #     """
    #     data = {}
    #     #program_info = self.page.getInfoFromLiveBanner()
    #     program_info=None
    #     if program_info:
    #         if program_info:
    #             data = self._to_dict(program_info)
    #
    #     else:
    #         # try the other way
    #         dom = self.page._get_dom()
    #         page = LiveBannerPolaris(dom)
    #         data = page.feed()
    #         data = page.export(data)
    #
    #     # close webkit
    #     self.log.debug("getInfoFromLiveBannerPolaris() got: %s" % str(data))
    #     self.page.close()
    #
    #     return data


    #
    # def page_getInfoFromVodBaner(self):
    #     raise NotImplemented

    def page_get_info_from_epg_focus(self):
        """
            get the info in epg screen

        :return: dictionary
        """
        info=self.page.getInfoFromEpgFocus()
        # info is a ProgramInfoItem object
        self.page.close()
        data = self._to_dict(info)
        return data

    def page_get_info_from_mosaic_focus(self, **kwargs):
        """
            get the info of the mosaic

        :return: dictionary
        """
        # get a ProgramInfoItem object
        r=self.page_get_info_from_mosaic_focus(**kwargs)
        # close webkit
        self.page.close()
        r = self._to_dict(r)
        return r

    # def page_getInfoFromRecordFocus(self):
    #     raise NotImplemented
    #
    # def page_getInfoFromRecordPage(self):
    #     raise NotImplemented
    #
    # def page_getRecordInfoFromFip(self):
    #     raise NotImplemented
    #
    # def page_getInfoFromVodPage(self):
    #     raise NotImplemented
    #
    # def page_getTitleFromVodFocus(self):
    #     raise NotImplemented
    #
    # def page_getInfoFromToolboxPolaris(self, category, parameter=1, focus=False):
    #     raise NotImplemented

    def page_get_status(self,key=None):
        """
            get the status of a screen

        :param key: string , one of
                stbStatus,noRightPanel,scene,dialog,frontPanel,miniLive,redCross,mosaicAdBanner
        :return:
        """
        status = self.page.getStatus()
        # status is a Containers.Status,object
        data= self._to_dict(status)
        if key:
            return data[key]
        return data

    # def page_getManualRecordingSchedulerStatus(self):
    #     raise NotImplemented
    #
    # def page_getAmountFromPrepaidAccount(self):
    #     raise NotImplemented
    #
    # def page_getTextFromDialogBox(self):
    #     raise NotImplemented

    # def page_find_in_Dialog_box(self,txt,regex=False):
    #     """
    #         in dialog box get
    #     :param txt:
    #     :param regex:
    #     :return:
    #     """
    #     return self.page.findInDialogBox(txt,regex=regex)
    #
    # def page_find_in_page(self, **kwargs):
    #     return self.page.findInPage(**kwargs)
    #
    # def page_find_progress_bar(self):
    #     return self.page.findProgressBar()
    #
    # def page_findPvrAdvancedConflictPicto(self):
    #     raise NotImplemented
    #
    # def page_resolvePvrAdvancedConflict(self):
    #     raise NotImplemented

    #
    # helpers
    #
    # def get_tv_esp(self,channel_code):
    #     """
    #
    #         replacement TvEsp(channel )
    #
    #
    #     :param channel:
    #     :return:
    #     """
    #     legacy= False
    #     if legacy:
    #         tv_esp = TvEsp(channel_code)
    #     else:
    #         # get data from platform.config
    #         package = Env.DEFAULT_PACKAGE
    #         #platform_data= self.backend.item_get('platforms','1')
    #         data= self._platform.data['profiles']['tvbox']['configuration']
    #
    #         universe_data= data['tv_channels'][package]
    #         channel_data= data['tv_channels'][package][str(channel_code)]
    #
    #         tv_esp= TvItem(channel_code,[])
    #         for c in channel_data:
    #             channel_detail = ChannelDetail(c[0],c[1],c[2],c[3])
    #             tv_esp.channels.append(channel_detail)
    #
    #     return tv_esp



    # def tv_get_lcn(self,channel):
    #     """
    #
    #     :param channel:
    #     :return:
    #     """
    #     #tv_esp=TvEsp(self.tv_data_get(channel))
    #     tv_esp=self.get_tv_esp(self.tv_data_get(channel))
    #     lcn= tv_esp.lcn
    #     return lcn
    #
    # def tv_get_first_channel_Dtt_off(self,channel):
    #     """
    #
    #     :param channel: string eg tv2 )
    #     :return:
    #     """
    #     tv_esp= TvEsp(self.tv_data_get(channel))
    #     obj= tv_esp.getFirstChannel_DttOff()
    #     return obj.name
    #
    #
    # def tv_get_first_channel_Dtt_on(self, channel):
    #     """
    #
    #     :param channel: string eg tv2 )
    #     :return:
    #     """
    #     tv_esp = TvEsp(self.tv_data_get(channel))
    #     obj = tv_esp.getFirstChannel_DttOn()
    #     return obj.name

    #
    #  handle the ToolboxLive screen
    #
    #   a menu with  multicam, a_la_demande, resume , chaines , langue

    def page_action_select(self, t, partial=False):
        """
            select an action on the toolbox

        :param t: string eg epgNOW
        :param partial:
        :return:
        """
        # Wording.W[t]
        t = self.wording_get(t)
        r = self.page.actionSelect(t, partial=partial)
        # return boolean
        return r

    def page_get_list(self):
        """
            get info on a list of items
        :return: a list of dictionary (text=,selected= ,active= )
        """
        data= self.page.getList()
        self.page.close()
        l =[]
        for item in data:
            l.append( dict(text=item.text,selected=item.selected,active=item.active))
        self.model.logs.extend(l)
        #response = ApplicationResponse(result=200, message=l, logs=[])
        return l

    def page_get_label_from_list_focus(self):
        """

        :return:
        """
        item= self.page.getLabelFromListFocus()
        return dict(text=item.text,selected=item.selected,active=item.active)


    def page_find_in_list(self, t, regex=False):
        """
            find an item in a list

        :param t: string to find
        :param regex: boolean , whether or not t is a regex (default is False)
        :return:
        """
        return self.page.findInList(t, regex=regex)


    #
    # toolbox using bs4
    #
    def stb_toolbox_list(self):
        """
            get list of labels in toolbox

        :return: list of labels
        """
        #tb= ToolBoxLive.from_file(DUMP_FILENAME)
        tb = ToolBoxLive.from_file(self.ardom_filename)
        items= tb.list_active_items()
        labels= [item.text for item in items]
        #response= ApplicationResponse(message=labels)
        return labels


    def stb_toolbox_select(self,text,confirm=True):
        """
            select an element in a toolbox

        :param text: string , the text to search for
        :param confirm: boolean , if True send 'KEY_OK' to confirm the selection
        """
        logs=[]
        #tb = ToolBoxLive.from_file(DUMP_FILENAME)
        tb = ToolBoxLive.from_file(self.ardom_filename)
        delta= tb.get_delta_index_for(text)
        if delta is None:
            self.log.info('cannot find choice: %s' % text)
            return 'KO'
            #return ApplicationResponse(message='KO',logs=['cannot find choice: %s' % text])
        if delta > 0:
            key= 'DOWN'
        elif delta < 0:
            key= 'UP'
        else:
            # assume is 0
            key= None
            self.log.info('already selected')

        if key:
            for i in xrange(0,delta):
                r= self._remote_send_key(key)
                time.sleep(1)
            self.log.info('moved %d * %s' % (delta, key))

        if confirm:
            r= self._remote_send_key('OK')
        return 'OK'


    #
    # new keywords
    #
    def stb_pcb_cli(self,commands):
        """
            send a pcb_cli command to the stb

        :param commands: list
        :return: True
        """
        if not isinstance(commands,list or tuple):
            commands= [commands]
        pcb_parser = PcbCliManager(self, commands)

        # send command to serial
        pcb_parser.send()
        return True


    def stb_info(self,**kwargs):
        """ send command pcb_cli "DeviceInfo.AdditionalSoftwareVersion.?"

            and get
                DeviceInfo.AdditionalSoftwareVersion=01.02.22,01.28.20

        :return: dictionary
        """
        commands= ['AdditionalSoftwareVersion',]
        pcb_parser= PcbCliManager(self,commands)

        try:
            results= pcb_parser.run()
            return results
        except Exception ,e:
            self.log.error(e.message)
            response= self.model.make_response({})
            raise ApplicationError(**response)


    #
    #   video
    #
    def detect_motion(self):
        """
            check if there is a movement
        :return: boolean True or False
        """
        r = self._video_detection.motionDetection()
        return r

    def video_screenshot(self):
        """
            check if there is a movement
        :return: encoded image or False
        """
        r = False
        try:
            os.unlink('stickimage.png')
        except OSError:
            pass
        self._video_detection.screenshot("stickimage.png")
        with open("stickimage.png", "rb") as fh:
            data = fh.read()
            encoded = data.encode("base64")
            os.unlink("stickimage.png")
            # print("flash:\n")
            # print encoded
            # print("----\n")
            return encoded


            #
            #  powerswitch interface
            #

    def power_on(self):
        """
            switch power on for the device

        :return: True
        """
        if self._powerswitch:
            return self._powerswitch.switch_on()
        else:
            raise ApplicationError('no powerswitch declared in platform')

    def power_off(self):
        """
            switch power off for the device
        :return:
        """
        if self._powerswitch:
            return self._powerswitch.switch_off(self.model.parameters['powerswitch_port'])
        else:
            raise ApplicationError('no powerswitch declared in platform')