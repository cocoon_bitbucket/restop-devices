"""


    handle log output from stb serial port

    filters, formatters, ...


    timestamp, level, emiter, message



    on line regex tester: https://regex101.com/#python



"""





import time
import re

samples= """\
[INFO]  >> LAUNCH FIRMWARE <<
>>>>> [FORCEDMODE] Hit a key to start downloader
[    0.000000] Booting Linux on physical CPU 0x0
Binding /tmp/var to /var ...
root: Starting PRE LOADER SERVICE: preloader ...done.
Jan  1 01:00:15 (none) user.notice root: Starting services ...
root: [1;37m[DEBUG] app.controller> stb event [OK] (count: 1)...[0m
root: [1;37m[DEBUG] app.vod> RightvMyvideosCatalog > launch getMovie() on id : LESCOWBOYSXW0109961_S_ESTTVQW_1, name : Les cowboys[0m
root: [1;37m[DEBUG] ROOT> filterTickets >> 0 >> current ticket :{"advisories":"none","type":"transaction","id":1458481845,"windowEnd":1617815468550,"assetId":1391643349,"videoExternalId":"LESCOWBOYSXW0109961_S_ESTTVQW_1","videoPackageName":"","name":"Les cowboys","videoId":7632279,"assetExternalId":"LESCOWBOYSXW0109961","rentalPeriod":1827,"allowedEstDownloadingDevices":"","prLevel":1,"prName":"CSA_1","downloadWindowEnd":"","template":"VOD_SERVICE","currentEstDownloadingDevices":"","windowStart":1459962668550,"status":"active","deliveryMode":"streaming","downloadWindowStart":"","downloadPeriodUnit":0,"attachment":{},"rentalPeriodUnit":4,"videoPackageExternalId":"","allowedViews":"","downloadPeriod":""}[0m
"""





# categories={
#
#     'app': ['root: [1;37m[DEBUG] app.controller> stb event [OK] (count: 1)...[0m'],
#     'ts' : []
#
# }

rules= [

    ['^root\:.*?(\[\w+?\])\s+(.*?)\>(.*)$',
                                'app','root: [1;37m[DEBUG] app.controller> stb event [OK] (count: 1)...[0m'],

    ['^\[([\s]*[\d]+\.[\d]+)\](.*)$',
                                'ts', '[    0.000000] Booting Linux on physical CPU 0x0'],

    ['^(\w\w\w [\s\d]\d \d\d\:\d\d\:\d\d) (.*)$',
                                'ts2', 'Jan  1 01:00:15 (none) user.notice root: Starting services ...'],


]


compiled_rules= []

for rule in rules:
    entry=[re.compile(rule[0]),rule[1]]
    compiled_rules.append(entry)



class LogParser(object):
    """



    """






def find_rule(line):

    for rule in compiled_rules:
        regex= rule[0]
        name= rule[1]
        m= regex.match(line)
        if m :
            return name,m

    return 'wild',None


def rule_app(m):

        debug= m.group(1).strip().upper()
        tag= m.group(2).strip().lower()
        message= m.group(3).strip()

        print '.',debug,tag,'-->',message

def rule_ts(m):
    timestamp= m.group(1)
    message= m.group(2)
    print timestamp,'.', 'ts','-->', message

def rule_ts2(m):
    timestamp= m.group(1)
    message= m.group(2)
    print timestamp,'.', 'ts2','-->', message



if __name__=="__main__":

    compiled_rules= []

    for rule in rules:
        entry=[re.compile(rule[0]),rule[1]]
        compiled_rules.append(entry)


    lines= samples.split('\n')
    for line in lines:
        name,regex= find_rule(line)

        print name,line
        pass


    with open('draft/cold_start.txt','r') as fh:
        for line in fh.readlines():
            name,regex= find_rule(line)

            if name is not 'wild':
                print name,line

                if name == "app":
                    rule_app(regex)

                if name == 'ts':
                    rule_ts(regex)

                if name == 'ts2':
                    rule_ts2(regex)

    # r= re.compile(rules[0][0])
    #
    # m= r.match('root: [1;37m[DEBUG] app.controller> stb event [OK] (count: 1)...[0m')
    # assert m
    # if m:
    #     debug= m.group(1).strip().upper()
    #     tag= m.group(2).strip().lower()
    #     message= m.group(3).strip()
    #
    # m= r.match('root: Starting PRE LOADER SERVICE: preloader ...done.')
    #
    #
    # r= re.compile(rules[1][0])
    # m =r.match('[    0.000000] Booting Linux on physical CPU 0x0')
    # assert m
    # if m:
    #     timestamp= m.group(1)
    #     message= m.group(2)
    #
    # m= r.match('[INFO]  >> LAUNCH FIRMWARE <<')
    #
    #
    # assert not m
    #
    #
    # print "Done"