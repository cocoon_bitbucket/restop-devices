import os
import sys
import time
import codecs
from bs4 import BeautifulSoup

import logging
log= logging.getLogger('StbTesting')


from restop.application import ApplicationError


try:
    from stb_testing import WebKitPage
    from stb_testing.uiprocessing.resident_app_page import ARDOMException
except ImportError,e:
    #NewTvTesting= None
    WebKitPage= None
    log.error('StbTesting is not installed')
    sys.exit(1)


from stb_testing import Wording, TVList, VodList

#from selenium.webdriver.common.desired_capabilities import DesiredCapabilities
#from NewTvTesting.Config import ConfAR, getDeskDataObject
#from NewTvTesting.Config import Env, Wording, TvData, VodData
#from NewTvTesting.Containers import RecordItem, Status, ProgramInfoItem, ListItem, VodItem, pvrItem,TvEsp,TvItem,ChannelDetail

#from RemoteControl import RpiRemoteControl


#from NewTvTesting.Config import Rpi as RpiBase


#from NewTvTesting.Config import Rpi

#base= os.path.dirname(os.path.realpath(__file__))
base = os.getcwd()
DUMP_FILENAME= os.path.join(base,"dump.html")
#DUMP_FILENAME= "/Users/cocoon/Documents/projects/restop/applications/tests/dump_sample.html"

# patch NewTvTesting.Config.Rpi
#global Rpi
#Rpi.DUMP="file://%s" % DUMP_FILENAME

#from NewTvTesting.ResidentAppPage import WebKitPage

#from stbtools.config.configprocess import LocalConfig

#from stbtools.serial import StbSerialPort
#from stbtools.remotecontrol import RemoteControl as BaseRemoteControl
#from stbtools.uiprocessing import WebKitPage as BaseWebKitPage




class Pager(WebKitPage):
    """

        redefine WebKitPAge : add remote_control argument


    """

    def __init__(self,remote_control,device='tv',http_callback=None):
        """

        :param remote_control: instance of RpiRemoteControl or equivalent
        :return:
        """
        self.device_alias= device
        self.http_callback= http_callback
        self.ardom_filename = "ardom-%s.html" % self.device_alias

        self._auto_fetch= True

        self.logger = logging.getLogger('StbTesting.WebKitPage')

        self.remote=remote_control
        self.rc= remote_control


    def close(self):
        """

        :return:
        """
        pass

    def _get_dom(self):
        dom = None

        if self._auto_fetch:
            if os.path.isfile(self.ardom_filename):
                os.rename( self.ardom_filename, "%s.old" % self.ardom_filename)
            #self.rc.send_long_key('KEY_BLUE')
            r = self.rc.stb_send_dump()

        for i in range(10):
            if os.path.isfile(self.ardom_filename):
                #dom = BeautifulSoup(codecs.open( self.ardom_filename ,"r", "utf8"),'lxml')
                dom = BeautifulSoup(codecs.open(self.ardom_filename, "r", "utf8"),'html.parser')
                break
            time.sleep(1)
        if dom is None:
            raise ARDOMException("Unable to retrieve DOM from STB")
        else:
            return dom

