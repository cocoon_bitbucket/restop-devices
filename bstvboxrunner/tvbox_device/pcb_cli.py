"""


    parser for pcb_cli


"""

import time

patterns= dict(

    AdditionalSoftwareVersion = dict(
        command= "DeviceInfo.AdditionalSoftwareVersion.?",
        pattern= "DeviceInfo.AdditionalSoftwareVersion=(.*)"

        )

)


class PcbCliManager(object):
    """


    """
    def __init__(self,agent,commands):
        """

        :param agent: instance of tvbox_device.agent.Agent
        :param commands:
        """
        self.agent=agent
        self.commands= commands or []

        self._send= agent.send
        self._expect= agent.expect
        self.results={}

    @property
    def patterns(self):
        """

        :return:
        """
        return patterns


    def send(self):
        """

        :return:
        """
        for command in self.commands:
            try:
                data= self.patterns[command]
                cmd= data['command']
            except:
                raise KeyError("pcb_cli invalid pattern: %s" % command)

            # send individual pcb_cli command to stb serial
            cmd = 'pcb_cli "%s"\n' % cmd
            self._send(cmd)
            time.sleep(0.2)
            return True

    def scan(self):
        """

        :return:
        """
        parser = PcbCliParser(patterns)
        for command in self.commands:

            try:
                data = self.patterns[command]
                pattern = data['pattern']

            except:
                raise KeyError("pcb_cli invalid pattern: %s" % command)

            # read serial response
            lines = self._expect(pattern, timeout=3, regex=True)
            if '=== found' in lines[-1]:
                # OK
                # load response parser
                self.results.update(parser.parse(lines[-2],command))
            else :
                # not found
                pass

        return self.results

    def run(self):
        """

        :return:
        """


        self.send()
        results= self.scan()

        return results


class PcbCliParser(object):
    """

        a parser for stb_cli commands


    """
    def __init__(self,patterns):
        """

        :param pcb_line:
        """
        self.patterns= patterns


    def parse(self,line,command):
        """


        :return:
        """
        try:
            func= getattr(self,command)
        except:
            raise RuntimeError('pcb_cli parse: command %s not implemented' % command)

        result= func(line,command)
        return result

    #
    # parser functions
    #

    def AdditionalSoftwareVersion(self,line,command):
        """
            parse result line like:
                DeviceInfo.AdditionalSoftwareVersion=01.02.22,01.28.20
        :param line:
        :return:
        """
        result={}
        result[command]= line.strip()
        return result