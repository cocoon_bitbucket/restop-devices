


import os
import sys




class ConsoleBase(object):

    def __init__(self):
        if sys.version_info >= (3, 0):
            self.byte_output = sys.stdout.buffer
        else:
            self.byte_output = sys.stdout
        self.output = sys.stdout

    def setup(self):
        pass

    def cleanup(self):
        pass

    def getkey(self):
        return None

    def write_bytes(self, s):
        self.byte_output.write(s)
        self.byte_output.flush()

    def write(self, s):
        self.output.write(s)
        self.output.flush()

    #  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -
    # context manager:
    # switch terminal temporary to normal mode (e.g. to get user input)

    def __enter__(self):
        self.cleanup()
        return self

    def __exit__(self, *args, **kwargs):
        self.setup()


import atexit
import termios
import codecs


class RedisConsole(object):
    """

        a console for serial.tools.miniterm

        get_key(): read a key from redis queue stdin
        write(data):  write line to redis queue stdout


        usage:

        import serial
        from serial.tools.miniterm import Miniterm

        serial_instance = serial.serial_for_url(
            args.port,
            args.baudrate,
            parity=args.parity,
            rtscts=args.rtscts,
            xonxoff=args.xonxoff,
            timeout=1,
            do_not_open=True)

        exit_char= =0x1d
        console= RedisConsole()
        console.unichr(exit_char)

        term= Miniterm( serial_instance, echo=False, eol='crlf', filters=()) )
        term.console= console
        term.exit_character = unichr(exit_char)

        term.start()



    """
    def __init__(self):

        # if sys.version_info >= (3, 0):
        #     self.byte_output = sys.stdout.buffer
        # else:
        #     self.byte_output = sys.stdout
        # self.output = sys.stdout


        # self.fd = sys.stdin.fileno()
        # self.old = termios.tcgetattr(self.fd)
        # atexit.register(self.cleanup)
        # if sys.version_info < (3, 0):
        #     self.enc_stdin = codecs.getreader(sys.stdin.encoding)(sys.stdin)
        # else:
        #     self.enc_stdin = sys.stdin
        pass

    #  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -
    # context manager:
    # switch terminal temporary to normal mode (e.g. to get user input)

    def __enter__(self):
        self.cleanup()
        return self

    def __exit__(self, *args, **kwargs):
        self.setup()


    def setup(self):
        # new = termios.tcgetattr(self.fd)
        # new[3] = new[3] & ~termios.ICANON & ~termios.ECHO & ~termios.ISIG
        # new[6][termios.VMIN] = 1
        # new[6][termios.VTIME] = 0
        # termios.tcsetattr(self.fd, termios.TCSANOW, new)
        pass

    def getkey(self):
        """
            get key from stdout redis queue

        :return:
        """
        # c = self.enc_stdin.read(1)
        # if c == unichr(0x7f):
        #     c = unichr(8)    # map the BS key (which yields DEL) to backspace
        # return c
        pass

    def cleanup(self):
        #termios.tcsetattr(self.fd, termios.TCSAFLUSH, self.old)
        pass

    def write_bytes(self, s):
        #self.byte_output.write(s)
        #self.byte_output.flush()
        raise NotImplementedError

    def write(self, s):
        """
            write line to stdin queue

        :param s:
        :return:
        """
        #self.output.write(s)
        #self.output.flush()
        pass

if __name__=="__main__":

    from qadapters.adapter import ThreadedAdapter,ClientAdapter

    #os.environ['PYTHONPATH'] = '/users/cocoon/Documents/projects/restop/v-restop/lib/python2.7/site-packages'
    os.environ['PYTHONPATH'] = '/usr/local/lib/device_proxy'

    #command_line = "python -m serial.tools.miniterm /dev/cu.usbserial-FTVE89ZND 115200"
    #command_line = "python -m serial.tools.miniterm /dev/tty.usbserial 115200"
    command_line = "python -m serial.tools.miniterm /dev/ttyUSB0 115200"

    agent_id= "tvboxes:1"


    a= ThreadedAdapter(agent_id,command_line)
    a.start()


    #client= ClientAdapter(agent_id)
    client= a


    n=100
    while n > 0:
            line = client.read()
            print line
            n -= 1
    client.stop()

    print "Done"


