"""
    legacy agent
        based on NewTvTesting and Rpi Remote


"""
from base_agent import BaseAgent


from NewTvTesting.RemoteControl import RpiRemoteControl
from NewTvTesting.ResidentAppPage import WebKitPage

from NewTvTesting.Config import Env, Wording, TvData, VodData


import logging

class Agent(BaseAgent):
    """

        tvbox agent
        -----------

        an agent to drive the orange tv set top box via Rpi

        use


    """

    def setup(self):
        """

        :return:
        """
        super(Agent,self).setup()

        formatter = logging.Formatter(u'%(asctime)s :: %(levelname)s :: %(message)s')
        self.handler = logging.StreamHandler()
        self.handler.setFormatter(formatter)
        self.logger = logging.getLogger('NewTvTesting')
        self.logger.setLevel(logging.INFO)
        self.logger.addHandler(self.handler)


        self.rc= RpiRemoteControl()
        self.page = WebKitPage()

        self.Wording= Wording
        self.TvData= TvData
        self.VodData= VodData


        # status = self.page.getStatus()
        # if status.getStbStatus() == "KO":
        #     self.logger.warning("Hard Reset")
        #     self.rc.hardReset()
        #     time.sleep(200)
        #     status = self.page.getStatus()
        #
        # scene = status.getScene()
        # if scene == "DESK":
        #     self.rc.sendKey("KEY_CHANNELUP")
        # elif scene == "TOOLBOX" or scene == "ZAPPING_BANNER":
        #     self.rc.sendKey("KEY_BACK")
        # elif scene == "PORTAL" or scene == "UNKNOWN":
        #     self.rc.sendKeys(["KEY_BACK","KEY_BACK","KEY_CHANNELUP"])
        # if status.findDialogBox():
        #     self.rc.sendKeys(["KEY_BACK","KEY_BACK","KEY_CHANNELUP"])

        return

    #
    # remote control rpi interface
    #
    def send_key(self,key,tempo=None):
        r=self.rc.sendKey(key,tempo=tempo)
        return r

    def send_keys(self,keys): return self.rc.sendKeys(keys)
    def zap(self,channel): return self.rc.zap(channel)
    def send_word(self,word): return self.rc.sendWord(word)
    def send_date_hour_min(self,date): return self.rc.sendDateHourMin(date)
    def hard_reset(self): return self.rc.hardReset()
    def get_stb_status(self): return self.rc.getStbStatus()
    def get_data_model_value(self,key): return self.rc.getStbDataModelValue(key)
    def set_data_model_value(self,key,value): return self.rc.setStbDataModelValue()


    #
    # page interface
    #
    def page_action_select(self,t,partial=False):
        r= self.page.actionSelect(t,partial=partial)

    def page_action_desk_select(self,text): return self.page.actionDeskSelect(text)
    def page_find_in_Dialog_box(self,txt,regex=False): return self.page.findInDialogBox(txt,regex=regex)
    def page_get_info_from_live_banner(self): return self.page.getInfoFromLiveBanner()
    def page_action_select_in_live_banner(self,lcn,up=True,maxValue=10): return self.page.actionSelectInLiveBanner(lcn,up=up,maxValue=maxValue)
    def page_get_info_from_epg_focus(self): return self.page.getInfoFromEpgFocus()
    def page_find_in_page(self,txt,regex=False): return self.page.findInPage(txt,regex=regex)
    def page_get_info_from_mosaic_focus(self): return self.page.getInfoFromMosaicFocus()


    #
    # interface to wording
    #
    def wording_get(self,tag):
        return self.Wording.W[tag]

    #
    # interface to tv data
    #
    def tv_data_get(self,tag):
        return self.TvData.T[tag]




if __name__ == "__main__":

    import time
    from plugable_backend.backends import get_backend

    Backend= get_backend()
    bk= Backend("")

    r= bk.item_new('tvbox_agents',1, {'alias':'tv'})
    r= bk.item_new('platforms:1:devices','tv', { 'peer_address':'localhost','peer_port':5000})

    # root: [DEBUG] app.controller> stb event [VOD] (count: 1)...

    # at open session
    tv= Agent(1,backend=bk)

    word= tv.wording_get('epgWeek')

    tv5= tv.tv_data_get('tv5')

    # sample usage
    #'''zapping p+'''
    tv.send_key(key="KEY_CHANNELUP")
    time.sleep(15)
    tv.send_key("KEY_INFO")

    pg= tv.page_get_info_from_live_banner()
