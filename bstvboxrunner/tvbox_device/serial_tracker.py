
import time
import random
import re
import serial
import sys

from restop_adapters.serial_port_adapter import SerialportThreadedAdapter
from restop_adapters.client import ClientAdapter



class SerialController(SerialportThreadedAdapter):
    """

        live link betwwen redis queues  and the tv serial port

    """

    def hook_on_readlines(self,lines):
        """

            called by _read_stdout , a hook to modify lines red from serial port

        :param lines: list of cr/lf separated lines
        :return:
        """
        return lines






class SerialControllerClient(ClientAdapter):
    """

        client to read write into redis queue to serial ports

    """


    def sync(self,timeout=10):
        """

        send an echo SYNCHRO on serial stdin and wait for SYNCHRO on stdout

        :param timeout:
        :return:
        """
        t0=time.time()

        self.send('\n')
        sync_message= "echo SYNCHRO TAG %s" % str(t0)
        self.send(sync_message)
        time.sleep(2)
        r= self.expect("^SYNCHRO TAG %s" % str(t0),timeout=timeout)
        return r




class LogCatcher(object):
    """



    """
    def __init__(self,port,filename=None):
        """


        :param port:
        :return:
        """
        self.port=port
        self.filename=filename

        self._out=None


    def __enter__(self):
        """


        :return:
        """
        s= serial.serial_for_url(self.port)
        #s=serial.Serial(self.port,)
        s.baudrate=115200
        s.timeout=1
        s.writeTimeout = 2
        s.xonxoff = False     #disable software flow control
        s.rtscts = False     #disable hardware (RTS/CTS) flow control
        s.dsrdtr = False       #disable hardware (DSR/DTR) flow control
        s.close()
        s.open()
        print s.portstr

        self.serial= s

        if self.filename:
            self._out=file(self.filename,"w")


        return self


    def __exit__(self,*args,**kwargs):
        """

        :return:
        """
        self.serial.close()
        if self._out:
            self._out.close()

    def capture(self):
        """


        :return:
        """
        try:
            while 1:
                line = self.serial.readline()
                if line :
                    self.out(line)
                else:
                    pass
                    #print "."
        except KeyboardInterrupt:
            print "!!! Interrupted by keyboard"
            pass

        finally:
            self.serial.close()


    def out(self,line):
        """

        :return:
        """
        print line
        if self._out:
            self._out.write(line)


    def inject(self,data):
        """


        :param data:
        :return:
        """
        self.serial.write(data)

    def inject_file(self,filename):
        """

        :param filename:
        :return:
        """
        with open(filename,"r") as fh:
            data= fh.read()
            self.inject(data)
        return data

class SerialTracker(object):
    """

        a process to

        * read order line from redis queue and write it to a serial port
        * read line from a serial port and write it to a redis queue


        parameters:
            id  identifier of the device
            channel name  ( to obtain queuenames for writing and listening redis queues
            serial port name ( to connect to the serial port


    """
    def __init__(self,device_id,serial_port,channel_name=None):
        """

        :param device_id:
        :param serial_port:
        :param channel_name:
        :return:
        """
        self.device_id=device_id
        self.serial_port= serial_port
        self.channel_name= channel_name



if __name__=="__main__":


    serial_port= "/dev/ttyUSB0"


    def test_pyserial():
        """

        """
        #port= "/dev/cu.usbserial-FTVE89ZND"
        port = serial_port

        s=serial.Serial(port,baudrate=115200)
        s.timeout=1
        s.writeTimeout = 20


        s.xonxoff = False     #disable software flow control

        s.rtscts = False     #disable hardware (RTS/CTS) flow control

        s.dsrdtr = False       #disable hardware (DSR/DTR) flow control



        s.close()
        s.open()
        print s.portstr

        n =10000

        try:
            while n > 0:
                line = s.readline()
                if line :
                    print line
                else:
                    print "."
                n -= 1
        except KeyboardInterrupt:
            print "!!! Interupted by keyboard"
            pass

        finally:
            s.close()


    def test_pyserial2():
        """

        """
        #port= "/dev/cu.usbserial-FTVE89ZND"
        #port= "/dev/tty.usbserial"
        port = serial_port
        baudrate= 115200


        # serial_instance = serial.serial_for_url(
        #         port,
        #         baudrate,
        #         parity=args.parity,
        #         rtscts=args.rtscts,
        #         xonxoff=args.xonxoff,
        #         timeout=1,
        #         do_not_open=True)


        try:
            serial_instance = serial.serial_for_url(
                    port,
                    baudrate,
                    timeout=1,
                    do_not_open=True)

            serial_instance.open()

        except serial.SerialException as e:
            sys.stderr.write('could not open port {}: {}\n'.format(repr(port), e))
            raise

        print serial_instance.portstr


        serial_instance.write("\n")

        n =10000
        try:
            while n > 0:
                line = serial_instance.readline()
                if line :
                    print line
                else:
                    print "."
                n -= 1
        except KeyboardInterrupt:
            print "!!! Interrupted by keyboard"
            pass

        finally:
            serial_instance.close()


    def test_capture():


        #port = "/dev/cu.usbserial-FTVE89ZNA"
        #port = "/dev/cu.usbserial-FTVE89ZNB"
        #port = "/dev/cu.usbserial-FTVE89ZNC"
        #port = "/dev/cu.usbserial-FTVE89ZND"
        #port = "/dev/tty.usbserial"
        port= serial_port


        fake_port= "loop://?logging=debug"


        with LogCatcher(fake_port) as c:

            c.inject_file("./tv_watch.txt")
            c.capture()


        return




    test_pyserial2()
    #test_capture()
    print "Done"