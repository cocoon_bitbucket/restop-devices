#! /usr/bin/python3
# -*- coding: utf-8 -*-


"""
    Module to manage configuration for TV.
    This parser supports gettting unexisting key and allow
    no value in configuration file.
"""

#_TV_CONFIG = {}

from ConfigParser import SafeConfigParser


class TVConfig(object):

    def __init__(self, config):
        self._config = config

    def __getattr__(self, option):
        """
            Override attribute get
            :param option: name of option to get value
            :type optionconfig_file: str
            :return the value of the given option if it exists, None otherwise
            :rtype str
        """
        if option == "set_config":
            super(TVConfig, self).__getattribute__(option)
        elif option in self._config:
            return self._config[option]
        else:
            return None

    def __setattr__(self, option, value):
        """
            Override attribute set
            :param option: name of option to get value
            :param value: the value to set at the option
            :type option: str
            :type value: str
        """
        if option.startswith("_"):
            super(TVConfig, self).__setattr__(option, value)
        else:
            self._config[option] = value

    def set_config(self, option, value):
        """
            Override static configuration
        """
        self._config[option] = value


class TVConfigParser(object):

    """
        Main class managing configuration for TV.
    """

    def __init__(self):
        """
            Constructor
            :param config_file: path of config to parser
            :type config_file: str
        """
        self.parser = SafeConfigParser(allow_no_value=True)

    def _is_int(self, s):
        try:
            int(s)
            return True
        except ValueError:
            return False

    def _is_float(self, s):
        try:
            float(s)
            return True
        except ValueError:
            return False

    def _parse(self):
        """
            Parse all the options contained in the loaded
            configuration files
        """
        config = {}
        for section in self.parser.sections():
            for option in self.parser.options(section):
                try:
                    value = self.parser.getboolean(section, option)
                except ValueError:
                    try:
                        value = self.parser.getint(section, option)
                    except ValueError:
                        try:
                            value = self.parser.getfloat(section, option)
                        except ValueError:
                            value = self.parser.get(section, option)
                config[option] = value
        return config

    def load_dict_from_file(self, config_file):
        """
            Load new configuration file. Content will be merge
            with current configuration.
            :param config_file: path of config file to load
            :type config_file: str
        """
        self.parser.read(config_file)
        config = self._parse()
        return config

    def load_config_from_file(self, config_file):
        """
            Load new configuration file. Content will be merge
            with current configuration.
            :param config_file: path of config file to load
            :type config_file: str
        """
        self.parser.read(config_file)
        config = self._parse()
        return TVConfig(config)
