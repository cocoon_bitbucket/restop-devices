#! /usr/bin/python3
# -*- coding: utf-8 -*-

"""
    Module to process the global configuration for a given test campaign
"""

from ConfigParser import Error
from uuid import uuid4
from subprocess import Popen, PIPE
from .configparser import TVConfigParser, TVConfig
from .core import Singleton


class LocalConfigException(Exception):
    """
       Define an exception class to manage configuration error
    """
    pass


#class LocalConfig(TVConfig, metaclass=Singleton):
class LocalConfig(TVConfig):
    """
       Define the global configuration manager for
       a test campaign, make all configuration
       accessible for any tests
    """

    __metaclass__ = Singleton


    def __init__(self, config_file="config.ini"):
        """
            Load given configuration file (default is config.ini in python
            current directory), and parse to a pythonic object for later use
            :param config_file: is the path of configuration to load
            :type config_file: str
        """
        self._uuid = str(uuid4())
        try:
            parser = TVConfigParser()
            config = parser.load_dict_from_file(config_file)
        except Error as err:
            raise LocalConfigException(
                "Unable to load configuration file: {}".format(err.message))
        else:
            super(LocalConfig, self).__init__(config)
        self._init_server()
        '''
        Esp data
        '''
        self.json_esp_data = None

        '''
        Desk data
        '''
        self.json_desk_data = None
        if self.suites is None:
            pass

        elif isinstance(self.suites, str):
            self.tests_suite = self.suites.split(',')

        elif not isinstance(self.suites, list):
            raise TypeError("The test list must be a string or a list ")
        else:
            self.tests_suite = self.suites

        '''
        Tv codes
        '''
        if "parental_code" not in self._config:
            self.set_config("parental_code", "1111")

        if "confidential_code" not in self._config:
            self.set_config("confidential_code", "1234")

    def _init_server(self):
        """
            get robot IP
        """
        try:
            ip_route = Popen(
                ["ip", "route", "get", "8.8.8.8"],
                stdout=PIPE)
            if ip_route.stdout:
                #result = str(ip_route.stdout.readline(), "utf-8")
                result = str(ip_route.stdout.readline())
                # override configuration if we're able to dynamically retrieve
                # stb_ip
                self.set_config("server_ip", result.split(" ")[-2].strip())
            ip_route.terminate()
        except OSError,e:
            print(str(e))
        except Exception ,e:
            print( str(e))
        return