# -*- coding: utf-8 -*-

import codecs
from . import configprocess
from ConfigParser import SafeConfigParser


class KeyMap(object):

    __metaclass__ = configprocess.Singleton

    def __init__(self):
        parser = SafeConfigParser()
        parser.optionxform = str
        parser.readfp(codecs.open("resources/lanapikeymap.ini", "r", "utf8"))
        self.__dict__.update(parser.items("keys"))
        self._setremotekeyboard()
        self._keymappingadded = True

    def _setremotekeyboard(self):
        self.keyboard = {"a": [2, 1]}
        self.keyboard["b"] = [2, 2]
        self.keyboard["c"] = [2, 3]
        self.keyboard["d"] = [3, 1]
        self.keyboard["e"] = [3, 2]
        self.keyboard["f"] = [3, 3]
        self.keyboard["g"] = [4, 1]
        self.keyboard["h"] = [4, 2]
        self.keyboard["i"] = [4, 3]
        self.keyboard["j"] = [5, 1]
        self.keyboard["k"] = [5, 2]
        self.keyboard["l"] = [5, 3]
        self.keyboard["m"] = [6, 1]
        self.keyboard["n"] = [6, 2]
        self.keyboard["o"] = [6, 3]
        self.keyboard["p"] = [7, 1]
        self.keyboard["q"] = [7, 2]
        self.keyboard["r"] = [7, 3]
        self.keyboard["s"] = [7, 4]
        self.keyboard["t"] = [8, 1]
        self.keyboard["u"] = [8, 2]
        self.keyboard["v"] = [8, 3]
        self.keyboard["w"] = [9, 1]
        self.keyboard["x"] = [9, 2]
        self.keyboard["y"] = [9, 3]
        self.keyboard["z"] = [9, 4]

        self.keyboard["0"] = [0, 1]
        self.keyboard["1"] = [1, 1]
        self.keyboard["2"] = [2, 4]
        self.keyboard["3"] = [3, 4]
        self.keyboard["4"] = [4, 4]
        self.keyboard["5"] = [5, 4]
        self.keyboard["6"] = [6, 4]
        self.keyboard["7"] = [7, 5]
        self.keyboard["8"] = [8, 4]
        self.keyboard["9"] = [9, 5]

        self.keyboard[" "] = [0, 2]
        self.keyboard["@"] = [1, 7]
