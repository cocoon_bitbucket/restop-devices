'''
Created on 8 avr. 2016

@author: jtembuyser
'''
import codecs
import random
from . import configprocess
from ConfigParser import SafeConfigParser


class TVList(object):
    """
    The TVList class is a singleton class (only one instance of the class
    can be created).
    The TVList class loads the dataset defined in the tv file (cf. config.ini).
    The TVList() object attributes are mapped to tv lists keys.
    """
    __metaclass__ = configprocess.Singleton

    def __init__(self):
        parser = SafeConfigParser()
        parser.read("resources/" + configprocess.LocalConfig().tv_file)

        for section in parser.sections():
            # Case of channels list
            if section == "channels_list":
                self.channels_list = {}
                # For each value in the section channel list
                for key, value in parser.items(section):
                    # Create the list of channels
                    self.channels_list[key] = [
                        int(i) for i in value.split(',')]
            else:
                self.__dict__.update(parser.items(section))

        self._channels_added = True

    def _joint_channel_group(self, condition, channel_group):
        # =====================================================================
        # Build the list of channels corresponding to groups pass in parameter.
        # ======================================================================
        i = 0
        if not channel_group:
            return None
        if len(channel_group) == 1:
            final_list = self.channels_list[channel_group[0]]
        else:
            if condition == 'AND':
                # If there is more than 1 group
                for group in channel_group:
                    if i > 0:
                        final_list = [int(i)
                                      for i in self.channels_list[channel_group[0]] if i in self.channels_list[group]]
                    i += 1
            elif condition == 'OR':
                final_list = []
                for group in channel_group:
                    for channel in self.channels_list[group]:
                        final_list.append(channel)
            else:
                return None
        return final_list

    def get_random_channel_from_group(self, channel_group):
        """
        Get a random channel from a group

        :param channel_group: group of channels name
        :type channel_group: string

        :return: channel number
        :rtype: int
        """
        return random.choice(self.channels_list[channel_group])

    def get_random_channels_from_group(self, nb, channel_group):
        """
        Get several random channels from a group

        :param nb: number of channels
        :type nb: int
        :param channel_group: group of channels name
        :type channel_group: string

        :return: channel numbers
        :rtype: list of int
        """
        if nb == 1:
            return random.choice(self.channels_list[channel_group])
        else:
            return random.sample(self.channels_list[channel_group], nb)

    def get_channels(self, condition, *channel_group):
        """
        Get the complete list of channels of the group(s).
        A logical operator must be given to precise how to join groups.
        (AND : tv channels that belong to all groups ; OR : tv channels of all groups)

        :param condition: logical operator (AND, OR)
        :param channel_group: group(s) of channels name(s)
        :type condition: str
        :type channel_group: multiple args of string

        :return: list of channels
        :rtype: list of int
        """
        if self._channels_added:
            return self._joint_channel_group(condition, channel_group)

    def get_random_channels(self, count, condition, *channel_group):
        """
        Select randomly one or several channel(s) from channel group(s).\n

        This method allow you to specify multiple channel groups (see example)
        and specify the condition of joint between the different group of channels.
        In this case the return channel(s) are channel(s) which
        are inside all different channel groups pass in parameter.\n

        :param count: number of channels you want to randomly get.
        :type count: int
        :param channel_group: channel group(s)
        :type channel_group: string (can be multiple args).
        :param condition: logical operator (AND, OR)
        :type condition: string

        :return: a random channel number or list of channel numbers
        :rtype: int, list of int

        :Example:

            **Get randomly a channel I can watch: a free channel or a scrambled channel whit rights**

                .. code-block:: python

                    list_available_channels = TVList().get_random_channels(1, 'OR', 'free_channels', 'scrambled_channels_with_rights')
        """
        result = None

        # ======================================================================
        # Join the different group of channels
        # ======================================================================
        final_list = self._joint_channel_group(condition, channel_group)

        # ======================================================================
        # Select the channel inside the list of channels
        # ======================================================================
        if final_list:
            if len(final_list) >= count:
                if count == 1:
                    result = random.choice(final_list)
                elif count > 1:
                    result = random.sample(final_list, count)
        return result

    def get_default_channel(self):
        """
        Return the channel defined in the default_channel key of tv.ini file

        :return: channel number
        :rtype: int
        """
        if self._channels_added:
            return self.channels_list['default_channel'][0]
