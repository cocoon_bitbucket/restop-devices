# -*- coding: utf-8 -*-

# from ConfigParser import SafeConfigParser
import codecs
from ConfigParser import RawConfigParser
from . import configprocess
from ..uiprocessing.containers import VodPath, PathItem


class VodList(object):
    """
    The VodList class is a singleton class (only one instance of the class
    can be created).
    The VodList class loads the dataset defined in the vod file (cf. config.ini).
    The VodList() object attributes are mapped to the vod list keys and contain
    VodPath

    :Example:

        **Finding a Vod page**

            .. code-block:: python

                from stbtools import VodList, RemoteControl, WebkitPage, Wording

                rc = RemoteControl()
                page = WebKitPage()

                v1 = VodList().vod_1
                rc.send_key("KEY_MENU")
                page.actionDeskSelect(wording().deskVideoOnDemand)
                for p in v1.path:
                    page.actionSelectInVodMenuPolaris(p.category, p.tile)
                vod = page.getInfoFromVodPage()
    """
    __metaclass__ = configprocess.Singleton

    def __init__(self):
        parser = RawConfigParser()
        parser.optionxform = str
        parser.readfp(codecs.open("resources/" +
                                  configprocess.LocalConfig().vod_file, "r", "utf8"))

        for section in parser.sections():
            vod_dict = parser.items(section)
            new_vod_dict = {}
            for key, value in vod_dict:
                tab_path = value.split(";")
                index = 0
                list_path = []
                while index < len(tab_path):
                    list_path.append(
                        PathItem(tab_path[index], tab_path[index + 1]))
                    index = index + 2
                new_value = VodPath(tab_path[len(tab_path) - 1], list_path)
                new_vod_dict[key] = new_value
            self.__dict__.update(new_vod_dict)

        self._vodadded = True
