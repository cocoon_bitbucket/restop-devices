# -*- coding: utf-8 -*-

# from ConfigParser import SafeConfigParser
from ConfigParser import RawConfigParser
from .configprocess import LocalConfig
from .core import Singleton


class Wording(object):
    """
    The Wording class is a singleton class (only one instance of the class
    can be created).
    The Wording class loads the dataset defined in the wording file (cf.config.ini).
    The Wording() object attributes are mapped to wording keys.

    :Example:
        **Using the Wording class**
            .. code-block:: python

                from stbtools import WebKitPage, Wording

                page = WebKitPage
                page.actionDeskSelect(Wording().deskVideoOnDemand)
    """
    __metaclass__ = Singleton

    def __init__(self):
        parser = RawConfigParser()
        parser.optionxform = str
        parser.read("resources/" + LocalConfig().wording_file)

        for section in parser.sections():
            self.__dict__.update(parser.items(section))

        self._wordingadded = True
