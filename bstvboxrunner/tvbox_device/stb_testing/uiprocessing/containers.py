#! /usr/bin/python3
#-*- coding: utf-8 -*-

#from ConfigParser import SafeConfigParser
import logging
import shutil
import json
import tarfile
import re
from ..config.configprocess import LocalConfig
#from ..core.serialmanager import SerialManager


class ProgramInfoItem(object):
    """
    The ProgramInfoItem class is a generic representation of a TV program

    :ivar lcn: logical TV channel number
    :ivar channelName: TV channel name
    :ivar favorite: True if the channel is in the favorites list
    :ivar program: name of the program
    :ivar start: start date of the program
    :ivar end: end date of the program
    :ivar length: length of the program
    :ivar genre: genre of the program
    :ivar nextProgram: next program name
    :ivar pictoCsa: True if Csa picto is displayed
    :ivar pictoHD: True if HD picto is displayed
    :ivar pictoRatio: True if 16/9 ratio picto is displayed
    :ivar pictoAudioDesc: True if audio description picto is displayed
    :ivar pictoVM: True if VM picto is displayed
    :ivar pictoDts: True if DTS picto is displayed
    :ivar pictoDolby: True if Dolby picto is displayed
    :ivar pictoEar: True if impaired hearing picto is displayed
    :ivar picto3D: True if 3D picto is displayed
    :ivar ongoingRec: True if a record is on going
    :ivar TS_start: position of the time shifting rank starting point (%)
    :ivar TS_width: time shifting rank width (%)
    :ivar TS_cursor: position of the time shifting cursor (%)

    :vartype lcn: int
    :vartype channelName: str
    :vartype favorite: boolean
    :vartype program: str
    :vartype start: datetime
    :vartype end: datetime
    :vartype length: timedelta
    :vartype genre: str
    :vartype nextProgram: str
    :vartype pictoCsa: boolean
    :vartype pictoHD: boolean
    :vartype pictoRatio: boolean
    :vartype pictoAudioDesc: boolean
    :vartype pictoVM: boolean
    :vartype pictoDts: boolean
    :vartype pictoDolby: boolean
    :vartype pictoEar: boolean
    :vartype picto3D: boolean
    :vartype ongoingRec: boolean
    :vartype TS_start: int
    :vartype TS_width: int
    :vartype TS_cursor: int

    """

    def __init__(self, lcn, channelName, program, start, end, length, genre,
                 favorite, ongoingRec, nextProgram, pictoCsa, pictoHD,
                 pictoRatio, pictoAudioDesc, pictoContentVersion, pictoDts,
                 pictoDolby, pictoEar, picto3D=None, pictoVO=None,
                 TS_start=None, TS_width=None, TS_cursor=None):
        self.lcn = lcn
        self.channelName = channelName
        self.favorite = favorite
        self.ongoingRec = ongoingRec
        self.program = program
        self.start = start
        self.end = end
        self.length = length
        self.genre = genre
        self.nextProgram = nextProgram
        self.logger = logging.getLogger(__name__)
        self.pictoCsa = pictoCsa
        self.pictoHD = pictoHD
        self.pictoRatio = pictoRatio
        self.pictoAudioDesc = pictoAudioDesc
        self.pictoContentVersion = pictoContentVersion
        self.pictoDts = pictoDts
        self.pictoDolby = pictoDolby
        self.pictoEar = pictoEar
        self.picto3D = picto3D
        self.pictoVO = pictoVO
        self.TS_width = TS_width
        self.TS_start = TS_start
        self.TS_cursor = TS_cursor

    def getLcn(self):
        return self.lcn

    def getChannelName(self):
        return self.channelName

    def getProgram(self):
        return self.program

    def getStart(self):
        return self.start

    def getEnd(self):
        return self.end

    def getGenre(self):
        return self.genre

    def getLength(self):
        return self.length

    def getNextProgram(self):
        return self.nextProgram

    def getFavorite(self):
        return self.favorite

    def getOngoingRec(self):
        return self.ongoingRec

    def display(self):
        self.logger.info("-------- program details ------")
        self.logger.info("lcn : " + str(self.lcn))
        self.logger.info("channel name : " + self.channelName)
        self.logger.info("program : " + self.program)
        self.logger.info("start : " + str(self.start))
        self.logger.info("end : " + str(self.end))
        self.logger.info("length : " + str(self.length))
        self.logger.info("genre : " + self.genre)
        self.logger.info("next program : " + self.nextProgram)
        self.logger.info("favorite : " + str(self.favorite))
        self.logger.info("recPicto : " + str(self.ongoingRec))
        self.logger.info("pictograms")
        self.logger.info("     csa : " + str(self.pictoCsa))
        self.logger.info("     HD : " + str(self.pictoHD))
        self.logger.info("     ratio : " + str(self.pictoRatio))
        self.logger.info("     audio description : " + str(
            self.pictoAudioDesc))
        self.logger.info("     VM : " + str(self.pictoContentVersion))
        self.logger.info("     dts mode : " + str(self.pictoDts))
        self.logger.info("     dolby mode : " + str(self.pictoDolby))
        self.logger.info("     hearing impaired : " + str(self.pictoEar))
        self.logger.info("     3D : " + str(self.picto3D))
        self.logger.info("     VO : " + str(self.pictoVO))
        self.logger.info("time shifting")
        self.logger.info("      cursor : " + str(self.TS_cursor))
        self.logger.info("      start : " + str(self.TS_start))
        self.logger.info("      width : " + str(self.TS_width))
        self.logger.info("-------------------------------")


class Channel(object):
    """
    The Channel class is a generic representation of a TV channel

    :ivar name: name of the TV channel
    :ivar lcn: logical channel number of the channel
    :ivar definition: video format of the channel (SD/HD/4K)
    :ivar source: source type of the channel (IP/DTT)
    :ivar slogan: slogan of the channel

    :vartype name: str
    :vartype lcn: int
    :vartype definition: str
    :vartype source: str
    :vartype slogan: str

    """

    def __init__(self, name, definition="HD", source="IP", slogan=None):
        self.name = name
        self.definition = definition
        self.source = source
        self.slogan = slogan

    def getName(self):
        return self.name

    def getDefinition(self):
        return self.definition

    def getSource(self):
        return self.source

    def getSlogan(self):
        return self.slogan


class TvItem(object):

    def __init__(self, lcn, channels):
        self.lcn = lcn
        self.channels = channels
        self.logger = logging.getLogger(__name__)

    def getLcn(self):
        return self.lcn

    def getChannels(self):
        return self.channels

    def getChannel(self, n):
        return self.channels[n]

    def getFirstChannel_DttOff(self):
        for c in self.channels:
            if c.getSource() == "IP":
                if c.getDefinition() == "HD" and LocalConfig().hd:
                    return c
                if c.getDefinition() == "SD":
                    return c

    def getFirstChannel_DttOn(self):
        if self.channels[0].getSource() == "DTT":
            return self.channels[0]
        return self.getFirstChannel_DttOff()

    def display(self):
        self.logger.info("-------- Tv details ------")
        self.logger.info(str(self.lcn))
        for c in self.channels:
            try:
                self.logger.info(
                    c.name + " / " + c.definition + " / " + c.source + " / " + c.slogan)
            except Exception:
                pass
        self.logger.info("--------------------------")


class TvEsp(TvItem):

    def __init__(self, lcn, package=int(LocalConfig().live_package)):
        find = False
        lcn_tab = LocalConfig().service_plan["UNIVERS_LIST"][package][
            "Bqt_List"][0]["Chnl_List"]
        for index, l in enumerate(lcn_tab):
            if l["LCN"] == lcn:
                find = True
                break
        channels = []
        if find:
            try:
                for c in LocalConfig().service_plan["UNIVERS_LIST"][package][
                        "Bqt_List"][0]["Chnl_List"][index]["Srv_List"]:
                    try:
                        channels.append(
                            Channel(c["Lng_nme"], c["ResT"],
                                    c["Src"], c["Slg"].encode("utf-8")))
                    except KeyError:
                        channels.append(
                            Channel(c["Lng_nme"], c["ResT"], c["Src"], None))

            except Exception as e:
                self.logger.debug(" >>> framework debug >>> %s", str(e))
        TvItem.__init__(self, lcn, channels)


class ListItem(object):
    """
    The ListItem class is a representation of a menu item

    :ivar text: text of the menu item
    :ivar selected: True if the menu item is selected (radio button)
    :ivar active: True if the menu item is selectable

    :vartype text: str
    :vartype selected: boulean
    :vartype active: boolean
    """

    def __init__(self, text, selected=False, active=True):
        self.text = text
        self.selected = selected
        self.active = active

    def __str__(self):
        return "Text : {0},\nSelected : {1},\nActive : {2}".format(self.text,
                                                                   self.selected,
                                                                   self.active)


class RecordItem(object):
    """
    The RecordItem class is a representation of a record item

    :ivar title: title of the record item
    :ivar date: date
    :ivar length: length of the record
    :ivar begin: the start time
    :ivar end: the end time
    :ivar channel: the channel name of the current record
    :ivar onGoing: the current record

    :vartype title: str
    :vartype date: int
    :vartype length: int
    :vartype begin: int
    :vartype end: int
    :vartype channel: str
    :vartype onGoing: str
    """

    def __init__(self, title, date, length, begin, end, channel, onGoing, status="OK", channelDetail=None):
        self.title = title
        self.date = date
        self.length = length
        self.begin = begin
        self.end = end
        self.channel = channel
        self.onGoing = onGoing
        self.status = status
        self.channelDetail = channelDetail
        self.logger = logging.getLogger(__name__)

    def getTitle(self):
        return self.title

    def getDate(self):
        return self.date

    def getLength(self):
        return self.length

    def getBegin(self):
        return self.begin

    def getEnd(self):
        return self.end

    def getChannel(self):
        return self.channel

    def getChannelDetail(self):
        return self.channelDetail

    def getStatus(self):
        return self.status

    def getOnGoing(self):
        return self.onGoing

    def display(self):
        self.logger.info("-------- record details ------")
        self.logger.info("title : " + str(self.title))
        self.logger.info("date : " + str(self.date))
        self.logger.info("on going: " + str(self.onGoing))
        self.logger.info("begin: " + str(self.begin))
        self.logger.info("end: " + str(self.end))
        self.logger.info("length: " + str(self.length))
        self.logger.info("channel name: " + str(self.channel))
        if self.channelDetail is not None:
            self.logger.info("channel detail: " + self.channelDetail.getName() + " " +
                             self.channelDetail.getSource() + " " + self.channelDetail.getDefinition())
        self.logger.info("status: " + str(self.status))
        self.logger.info("------------------------------")


class Status(object):
    """
    The Status class is a representation of the status of the STB

    :ivar stbStatus: global status of the device 'READY' 'STANDBY' 'DOWN'
    :ivar scene: Resident application section 'DESK' 'LIVE' 'EPG' 'VOD' 'MYACCOUNT'
    :ivar noRightPanel: True if a no right panel is displayed on the screen
    :ivar dialog: True if a dialog box is displayed on the screen

    :vartype stbStatus: str
    :vartype scene: str
    :vartype noRightPanel: boolean
    :vartype dialog: boulean
    """

    def __init__(self, stbStatus, noRightPanel=None, scene=None, dialog=None,
                 miniLive=None):
        self.stbStatus = stbStatus
        self.noRightPanel = noRightPanel
        self.scene = scene
        self.dialog = dialog
        self.miniLive = miniLive
        self.logger = logging.getLogger(__name__)

    def getStbStatus(self):
        return self.stbStatus

    def findNoRightPanel(self):
        return self.noRightPanel

    def getScene(self):
        return self.scene

    def getMiniLive(self):
        return self.miniLive

    def findDialogBox(self):
        return self.dialog

    def display(self):
        self.logger.info("-------- status ------")
        self.logger.info(" " + str(self.stbStatus))
        self.logger.info(" scene : " + str(self.scene))
        self.logger.info(" no right screen : " + str(self.noRightPanel))
        self.logger.info(" dialog box : " + str(self.dialog))
        self.logger.info("----------------------")


class VodItem(object):
    """
    The VodItem class is a generic representation of a Video on Demand

    :ivar title: Title of the VOD
    :ivar genre: genre of the VOD.
    :ivar length: duration of the VOD.
    :ivar endDate: Date when the VOD will not be available to watch.
    :ivar price: price of the VOD for renting.
    :ivar pictoCsa: VOD CSA.
    :ivar pictoHD: True if icto HD is displayed.
    :ivar pictoAudioDesc: True if picto audio description is displayed.
    :ivar pictoContentVersion: True if picto multi language is displayed.
    :ivar pictoDts: True if picto DTS is displayed.
    :ivar pictoDolby: True if picto Dolby is displayed.
    :ivar pictoEar: True if picto Ear is display.

    :vartype title: str
    :vartype genre: str
    :vartype length: datetime
    :vartype endDate: datetime
    :vartype price: float
    :vartype pictoCsa: str
    :vartype pictoHD: boolean
    :vartype pictoAudioDesc: boolean
    :vartype pictoContentVersion: boolean
    :vartype pictoDts: boolean
    :vartype pictoDolby: boolean
    :vartype pictoEar: boolean
    """

    def __init__(self, title, genre, length, endDate, price, pictoCsa,
                 pictoHD, pictoRatio, pictoAudioDesc,
                 pictoContentVersion, pictoDts, pictoDolby, pictoEar):
        self.title = title
        self.genre = genre
        self.length = length
        self.endDate = endDate
        self.price = price
        self.pictoCsa = pictoCsa
        self.pictoHD = pictoHD
        self.pictoRatio = pictoRatio
        self.pictoAudioDesc = pictoAudioDesc
        self.pictoContentVersion = pictoContentVersion
        self.pictoDts = pictoDts
        self.pictoDolby = pictoDolby
        self.pictoEar = pictoEar
        self.logger = logging.getLogger(__name__)
        self.cover = None
        self.rating = None

    def setCover(self, a_cover):
        self.cover = a_cover

    def getCover(self):
        return self.cover

    def setRating(self, a_rating):
        self.rating = a_rating

    def getRating(self):
        return self.rating

    def getTitle(self):
        return self.title

    def getGenre(self):
        return self.genre

    def getLength(self):
        return self.length

    def getEndDate(self):
        return self.endDate

    def getPrice(self):
        return self.price

    def getPictoCsa(self):
        return self.pictoCsa

    def display(self):
        self.logger.info("-------- Vod details ------")
        self.logger.info("title : " + self.title)
        self.logger.info("genre : " + self.genre)
        self.logger.info("length : " + str(self.length))
        self.logger.info("movie rental end date: " + str(self.endDate))
        self.logger.info("price : " + str(self.price))
        self.logger.info("cover : {0}".format(self.cover))
        self.logger.info("rating : {0}".format(self.rating))
        self.logger.info("Pictograms")
        self.logger.info("     csa : " + str(self.pictoCsa))
        self.logger.info("     hd : " + str(self.pictoHD))
        self.logger.info("     ratio : " + str(self.pictoRatio))
        self.logger.info("     audio description : " +
                         str(self.pictoAudioDesc))
        self.logger.info("     content version multilingual : " +
                         str(self.pictoContentVersion))
        self.logger.info("     dts mode : " + str(self.pictoDts))
        self.logger.info("     dolby mode : " + str(self.pictoDolby))
        self.logger.info("     hearing impaired : " + str(self.pictoEar))
        self.logger.info("---------------------------")


class VodRatingDetail(object):

    def __init__(self, rate=None, display=False):
        self._is_display = display
        self._rate = rate

    def is_display(self):
        return self._is_display

    def get_rate(self):
        return self._rate

    def __str__(self):
        return "Rate : {0},\nDisplayed : {1}".format(self._rate, self._is_display)


class VodRating(object):

    def __init__(self, vod_rating_press=None, vod_rating_users=None):
        self.vod_rating_press = vod_rating_press
        self.vod_rating_users = vod_rating_users

    def get_vod_rating_users(self):
        return self.vod_rating_users

    def get_vod_rating_press(self):
        return self.vod_rating_press

    def __str__(self):
        return "Press rating : {0},\nUsers rating : {1}".format(self.vod_rating_press,
                                                                self.vod_rating_users)


class VodPath(object):
    """
    The VodPath class is a representation of the location of a Vod in the Portal

    :ivar title: name of the Vod
    :ivar path: list of category/name elements

    :vartype title: str
    :vartype path: list of PathItem
    """

    def __init__(self, title, path):
        self.title = title
        self.path = path
        self.logger = logging.getLogger(__name__)

    def getName(self):
        return self.title

    def getPath(self):
        return self.path

    def display(self):
        self.logger.info("-------- Vod from dataset details ------")
        self.logger.info("title : " + self.title)
        for item in self.path:
            self.logger.info("category : " + item.category +
                             " / tile : " + item.tile)
        self.logger.info("----------------------------------------")


class PathItem(object):
    """
    The PathItem class is a representation of a path in the Vod Portal

    :ivar category: name of the category (it can be 'None' or 'TOP_BANNER')
    :ivar tile: name of the tile

    :vartype category: str
    :vartype tile: str
    """

    def __init__(self, category, tile):
        self.category = category
        self.tile = tile


class Remote(object):
    LETTERS = {}
    LETTERS["a"] = [2, 1]
    LETTERS["b"] = [2, 2]
    LETTERS["c"] = [2, 3]
    LETTERS["d"] = [3, 1]
    LETTERS["e"] = [3, 2]
    LETTERS["f"] = [3, 3]
    LETTERS["g"] = [4, 1]
    LETTERS["h"] = [4, 2]
    LETTERS["i"] = [4, 3]
    LETTERS["j"] = [5, 1]
    LETTERS["k"] = [5, 2]
    LETTERS["l"] = [5, 3]
    LETTERS["m"] = [6, 1]
    LETTERS["n"] = [6, 2]
    LETTERS["o"] = [6, 3]
    LETTERS["p"] = [7, 1]
    LETTERS["q"] = [7, 2]
    LETTERS["r"] = [7, 3]
    LETTERS["s"] = [7, 4]
    LETTERS["t"] = [8, 1]
    LETTERS["u"] = [8, 2]
    LETTERS["v"] = [8, 3]
    LETTERS["w"] = [9, 1]
    LETTERS["x"] = [9, 2]
    LETTERS["y"] = [9, 3]
    LETTERS["z"] = [9, 4]

    LETTERS["0"] = [0, 1]
    LETTERS["1"] = [1, 1]
    LETTERS["2"] = [2, 4]
    LETTERS["3"] = [3, 4]
    LETTERS["4"] = [4, 4]
    LETTERS["5"] = [5, 4]
    LETTERS["6"] = [6, 4]
    LETTERS["7"] = [7, 5]
    LETTERS["8"] = [8, 4]
    LETTERS["9"] = [9, 5]

    LETTERS[" "] = [0, 2]
    LETTERS["@"] = [1, 7]
