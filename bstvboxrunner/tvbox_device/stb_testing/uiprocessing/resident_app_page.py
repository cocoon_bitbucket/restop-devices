# -*- coding: utf-8 -*-

from datetime import datetime, timedelta
from bs4 import BeautifulSoup
import logging
import time
import os
import shutil
import json
import codecs
import re
from attrdict import AttrDict
from random import randint


from ConfigParser import SafeConfigParser

#from ..core.serialmanager import SerialManager

from ..config.configprocess import LocalConfig
from ..config.wording import Wording
from .containers import (ProgramInfoItem, ListItem, Status,
                         VodRatingDetail, VodRating, VodItem, RecordItem)

#from ..remotecontrol.remotecontrol import RemoteControl


class ARDOMException(Exception):

    def __init__(self, message):
        self.message = message


class ParsingException(Exception):

    def __init__(self, message):
        self.message = message


class WebKitPage(object):
    """Class to be used to interact with the STB UI.

    :Example:
        **Instanciate a WebKitPage**

            .. code-block:: python

                from stbtools import WebKitPage

                page = WebKitPage()
    """

    def __init__(self,remote_control):
        """

        """
        self.logger = logging.getLogger(__name__)
        self.rc=remote_control
        #self.rc = RemoteControl()

    def _get_dom(self):
        dom = None
        if os.path.isfile(LocalConfig().ar_dom_folder + "ar_dom.html"):
            os.rename(LocalConfig().ar_dom_folder + "ar_dom.html",
                      LocalConfig().ar_dom_folder + "old_ar_dom.html")
        self.rc.send_long_key('KEY_BLUE')
        for i in range(10):
            if os.path.isfile(LocalConfig().ar_dom_folder + "ar_dom.html"):
                dom = BeautifulSoup(codecs.open(LocalConfig().ar_dom_folder +
                                                "ar_dom.html", "r", "utf8"),
                                    'lxml')
                break
            time.sleep(1)
        if dom is None:
            raise ARDOMException("Unable to retrieve DOM from STB")
        else:
            return dom

    def _get_text_from_selectors(self, soup, selectors):
        elements = soup.select(selectors)
        n = len(elements) - 1
        if n >= 0:
            e = elements[n]
            return e.get_text()
        else:
            return None

    def _get_classes_from_selectors(self, soup, selectors):
        elements = soup.select(selectors)
        n = len(elements) - 1
        if n >= 0:
            e = elements[n]
            return e.attrs['class']
        else:
            return None

    def _get_style_from_selectors(self, soup, selectors):
        elements = soup.select(selectors)
        n = len(elements) - 1
        if n >= 0:
            e = elements[n]
            return e.attrs['style']
        else:
            return None

    def _find_class_from_selectors(self, soup, selectors, c):
        elements = soup.select(selectors)
        n = len(elements) - 1
        if n >= 0:
            e = elements[n]
            for i in e.attrs['class']:
                if c == i:
                    return True
            return False
        else:
            return False

    def _not_find_class_from_selectors(self, soup, selectors, c):
        elements = soup.select(selectors)
        n = len(elements) - 1
        if n >= 0:
            e = elements[n]
            for i in e.attrs['class']:
                if c == i:
                    return False
            return True
        else:
            return False

    def actionSelect(self, text, partial=False):
        """
        Moves the focus on a menu item and send KEY_OK
        Works with Toolbox and Portal menus

        :param text: text of the menu item
        :type text: str
        :param partial: True if the matching criteria is 'partial'
        :type partial: boolean
        :returns: True if the menu item has been selected
        :rtype: boolean
        """
        self.logger.info("  >>   selecting text >" + text + "<")
        highlight, activeHighlight, activeItems, items = self._loadPageList()
        find = False
        index = 0

        try:
            if partial is True:
                while text not in activeItems[index].text and index < (len(activeItems) - 1):
                    index = index + 1
                if text in activeItems[index].text:
                    find = True
            else:
                while activeItems[index].text != text and index < (len(activeItems) - 1):
                    index = index + 1
                if activeItems[index].text == text:
                    find = True

            if find:
                gap = index - activeHighlight

                if gap >= 0:
                    keyGap = "KEY_DOWN"
                else:
                    keyGap = "KEY_UP"

                self.rc.send_keys([keyGap] * abs(gap))
                self.rc.send_key("KEY_OK")

                return True

            else:
                soup = self._get_dom()
                if len(soup.select(".ToolBoxLive")) > 0:
                    maxItems = ConfAR.MAX_ITEMS_TOOLBOX
                else:
                    maxItems = ConfAR.MAX_ITEMS
                if len(items) == maxItems:

                    end = False
                    endText = activeItems[-1].text

                    while not end:
                        self.rc.send_keys(
                            ["KEY_UP"] * int(activeHighlight + 1))
                        highlight, activeHighlight, activeItems, items = self._loadPageList()
                        find = False
                        index = 0

                        if partial is True:
                            while text not in activeItems[index].text and index < (len(activeItems) - 1):
                                index = index + 1
                                if activeItems[index].text == endText:
                                    end = True

                            if text in activeItems[index].text:
                                find = True
                                end = True
                        else:
                            while activeItems[index].text != text and index < (len(activeItems) - 1):
                                index = index + 1
                                if activeItems[index].text == endText:
                                    end = True
                            if activeItems[index].text == text:
                                find = True
                                end = True
                    if find:
                        gap = index - activeHighlight

                        if gap >= 0:
                            keyGap = "KEY_DOWN"
                        else:
                            keyGap = "KEY_UP"

                        self.rc.send_keys([keyGap] * abs(gap))
                        self.rc.send_key("KEY_OK")
                        return True

                    else:
                        return False
                else:
                    return False
        except Exception as e:
            self.logger.debug(" >>> framework debug >>> %s", str(e))
            return False

    def actionDeskSelect(self, text):
        """
        Moves the focus on a tile item and send KEY_OK.
        The matching criteria is 'partial'.
        Works with desk menu only.

        :param text: text of the tile item
        :type text: str
        :returns: True if the tile item has been selected
        :rtype: boolean
        """
        self.logger.info("  <<   Desk Select " + str(text))
        select = False

        if self.actionDeskMoveTo(text):
            self.rc.send_key("KEY_OK")
            select = True

        return select

    def actionDeskMoveTo(self, text):
        """
        Moves the focus to a tile item.
        the matching criteria is 'partial'.
        Works with desk menu only.

        :param text: text of the tile item
        :type text: str
        :returns: True if the tile item has been reached
        :rtype: boolean
        """
        try:
            currentPosition, isHeader, currentTile = \
                self._getBlockPositionFromDeskFocus()
            currentX = currentPosition[0]
            self.logger.info("  >>   selecting text >" + text + "<")
            desk = getDeskDataObject()
            try:
                header = desk.getHeader()
                if header.button_name == text and not isHeader:
                    self._moveToTile(currentTile, currentX, 0)
                    self.rc.send_key("KEY_UP")
                    find = True
                elif header.button_name == text and isHeader:
                    find = True
                else:
                    tiles = desk.getTiles()
                    for i, tile in enumerate(tiles):
                        for block in tile.blocks:
                            if block.label == text:
                                self._moveToTile(currentTile, currentX, i)
                                self.rc.send_keys(["KEY_RIGHT"] * int(block.x))
                                if int(block.y) == 0 and int(block.h) == 1:
                                    self.rc.send_key("KEY_DOWN")
                                find = True
                                break

                return find
            except Exception as e:
                self.logger.debug(" >>> framework debug >>> %s", str(e))
                return False
        except Exception as e:
            self.logger.debug(" >>> framework debug >>> %s", str(e))
            return False

    def actionSelectInLiveBanner(self, lcn, up=True, maxValue=10):
        self.logger.info("  >>   select channel " + str(lcn))

        soup = self._get_dom()
        try:
            alltxt_list = self._get_text_from_selectors(
                soup, ".programInformation .nameOfChannel ").split(u' ')
            lcn_live_banner = int(alltxt_list[0])
        except Exception as e:
            raise ParsingException("Error while processing DOM: %s", str(e))

        m = 0
        if up:
            key = "KEY_UP"
        else:
            key = "KEY_DOWN"

        while (lcn_live_banner != lcn and m < maxValue):
            self.rc.send_key(key)

            time.sleep(0.2)
            soup = self._get_dom()
            try:
                alltxt_list = self._get_text_from_selectors(
                    soup, ".programInformation .nameOfChannel ").split(u' ')
                lcn_live_banner = int(alltxt_list[0])
            except Exception as e:
                raise ParsingException("Error while processing DOM: %s", str(e))
            m = m + 1
        if (lcn_live_banner == lcn):
            self.rc.send_key("KEY_OK")
            return True
        else:
            self.rc.send_key("KEY_BACK")
            return False

    def actionSelectInVodMenuPolaris(self, category, name):
        """
        Moves the focus on a tile of a category and send KEY_OK.
        The search matching criteria is 'partial'.
        Works only with Polaris VOD portal.

        :param category: text of the category name or 'TOP_BANNER' or 'None'
        :type category: str
        :param name: text of the tile to select
        :type name: str
        :returns: True if the tile item has been selected
        :rtype: boolean
        """
        self.logger.info("  >>   selecting " + category + " " + name)
        soup = self._get_dom()
        try:
            if category == u'TOP_BANNER':
                if self._get_text_from_selectors(soup, ".VodHeaderSearchElt").find(name) != -1:
                    self.rc.send_keys(["KEY_UP", "KEY_OK"])
                    return True
                elements = soup.select(".carouselItem .edito")
                find = False
                for index, element in enumerate(elements):
                    if element.get_text().find(name) != -1:
                        find = True
                        break
                if find:
                    for i in range(index):
                        self.rc.send_key('KEY_RIGHT')
                    self.rc.send_key('KEY_OK')
                    return True
                else:
                    return False

            elif category == 'None':

                if self._get_text_from_selectors(soup, ".highlight").find(name) != -1:
                    self.rc.send_key('KEY_OK')
                    return True
                else:
                    startLabel = self._get_text_from_selectors(soup, ".highlight")
                    self.rc.send_key('KEY_RIGHT')
                    soup = self._get_dom()
                    nb_items = 0
                    while self._get_text_from_selectors(soup, ".highlight") != startLabel and nb_items < 100:
                        if self._get_text_from_selectors(soup, ".highlight").find(name) != -1:
                            self.rc.send_key('KEY_OK')
                            return True
                        else:
                            self.rc.send_key('KEY_RIGHT')
                            nb_items = nb_items + 1
                            soup = self._get_dom()
                    soup = self._get_dom()
                    return False
            else:
                elements = soup.select(".VodHorizontalWidget .carouselTitle .txt_title2")
                find = False
                for index, element in enumerate(elements):
                    if element.get_text().find(category) != -1:
                        find = True
                        break
                if find:
                    if len(soup.select(".headerContainer .herozone")) > 0:
                        self.rc.send_key('KEY_DOWN')
                    for i in range(index):
                        self.rc.send_key('KEY_DOWN')
                else:
                    return False

                soup = self._get_dom()
                time.sleep(2)
                if self._get_text_from_selectors(soup, ".highlight").find(name) != -1:
                    self.rc.send_key('KEY_OK')
                    return True
                else:
                    startLabel = self._get_text_from_selectors(soup, ".highlight")
                    self.rc.send_key('KEY_RIGHT')
                    soup = self._get_dom()
                    nb_items = 0
                    while self._get_text_from_selectors(soup, ".highlight") != startLabel and nb_items < 100:
                        if self._get_text_from_selectors(soup, ".highlight").find(name) != -1:
                            self.rc.send_key('KEY_OK')
                            return True
                        else:
                            self.rc.send_key('KEY_RIGHT')
                            nb_items = nb_items + 1
                            soup = self._get_dom()
                    soup = self._get_dom()
                    return False
        except Exception as e:
            self.logger.debug(" >>> framework debug >>> %s", str(e))
            return False

    def actionInstantRecord(self, length=5):
        """
        Click on record button.
        Select duration of the record.

        :param hourTxt: end record time
        :type hourTxt: int
        :returns: True if the record has been launched
        :rtype: boolean
        """
        self.logger.info("  >>   instant recording")

        try:
            soup = self._get_dom()
            if len(soup.select(".dialog.irDialog")) < 1:
                raise ParsingException('Not expected RA page')
            currentDate = datetime.now()
            endDate = currentDate + timedelta(minutes=length)
            endHourTxt = str(endDate.hour)
            endMinTxt = str(endDate.minute)
            if len(endHourTxt) == 2:
                self.rc.send_key("KEY_LEFT")
                self.rc.send_keys(["KEY_" + endHourTxt[0], "KEY_" + endHourTxt[1]])
            else:
                self.rc.send_keys(["KEY_0", "KEY_" + endHourTxt[0]])
            time.sleep(1)
            if len(endMinTxt) == 2:
                self.rc.send_keys(["KEY_" + endMinTxt[0], "KEY_" + endMinTxt[1]])
            else:
                self.rc.send_keys(["KEY_0", "KEY_" + endHourTxt[0]])
            time.sleep(1)
            self.rc.send_key("KEY_OK")
            return True
        except Exception as e:
            self.logger.debug(" >>> framework debug >>> %s", str(e))
            return False

    def actionScheduleRecord(self, package, lcn, date, length, recurrence=0, idWord=None):
        self.logger.info("  >>   scheduled recording")

        try:
            soup = self._get_dom()
            if len(soup.select(".dialog.pvrScheduling")) < 1:
                raise ParsingException('Not expected RA page')

            """ title """
            if idWord is None:
                idWord = str(randint(1000, 9999)).replace("0", "1")
            self.rc.send_word(idWord)

            """ bouquet """
            self.rc.send_key("KEY_DOWN")
            self.rc.send_key("KEY_OK")
            self.rc.send_keys(["KEY_DOWN"] * int(package))
            self.rc.send_key("KEY_OK")

            """ channel """
            self.rc.send_key("KEY_DOWN")
            self.rc.send_key("KEY_OK")
            self.rc.zap(lcn)
            self.rc.send_key("KEY_OK")

            """ date """
            self.rc.send_key("KEY_DOWN")

            if len(str(date.day)) == 2:
                self.rc.send_keys(["KEY_" + str(date.day)[0],
                                  "KEY_" + str(date.day)[1]])
            else:
                self.rc.send_keys(["KEY_0", "KEY_" + str(date.day)[0]])

            if len(str(date.month)) == 2:
                self.rc.send_keys(["KEY_" + str(date.month)[0],
                                  "KEY_" + str(date.month)[1]])
            else:
                self.rc.send_keys(["KEY_0", "KEY_" + str(date.month)[0]])

            self.rc.send_keys(["KEY_" + str(date.year)[2], "KEY_" + str(date.year)[3]])

            if len(str(date.hour)) == 2:
                self.rc.send_keys(["KEY_" + str(date.hour)[0], "KEY_" + str(date.hour)[1]])
            else:
                self.rc.send_keys(["KEY_0", "KEY_" + str(date.hour)[0]])

            if len(str(date.minute)) == 2:
                self.rc.send_keys(["KEY_" + str(date.minute)[0], "KEY_" + str(date.minute)[1]])
            else:
                self.rc.send_keys(["KEY_0", "KEY_" + str(date.minute)[0]])

            endDate = date + timedelta(minutes=length)

            if len(str(endDate.hour)) == 2:
                self.rc.send_keys(["KEY_" + str(endDate.hour)[0],
                                  "KEY_" + str(endDate.hour)[1]])
            else:
                self.rc.send_keys(["KEY_0", "KEY_" + str(endDate.hour)[0]])

            if len(str(endDate.minute)) == 2:
                self.rc.send_keys(["KEY_" + str(endDate.minute)[0],
                                  "KEY_" + str(endDate.minute)[1]])
            else:
                self.rc.send_keys(["KEY_0", "KEY_" + str(endDate.minute)[0]])

            """ recurrence """
            self.rc.send_key("KEY_OK")
            self.rc.send_keys(["KEY_DOWN"] * int(recurrence))
            self.rc.send_key("KEY_OK")

            """ validation """
            self.rc.send_key("KEY_DOWN")
            self.rc.send_key("KEY_OK")

            """pop up checking """
            time.sleep(10)
            soup = self._get_dom()
            if len(soup.select(".dialog.message.pvrScheduling")) > 0:
                self.rc.send_key("KEY_OK")
                return idWord
            else:
                raise ParsingException('Dialog Box not found')
        except Exception as e:
            self.logger.warning(" >>> framework debug >>> %s", str(e))
            return False

    def findInPage(self, text):
        """
        Returns True if the text can be found anywhere in
        the page displayed on the screen.

        :param text: text
        :type category: str
        :returns: True if text has been found
        :rtype: boolean
        """
        soup = self._get_dom()
        if soup:
            return (soup.get_text().find(text) > -1)
        else:
            return False

    def findInList(self, text, partial=False):
        """
        Returns True if the text can be found in the menu
        list displayed on the screen.

        :param text: text
        :type category: str
        :returns: True if text has been found
        :rtype: boolean
        """
        highlight, activeHighlight, activeItems, items = self._loadPageList()
        if partial:
            for item in items:
                if text in item.text:
                    return True
        else:
            for item in items:
                if text == item.text:
                    return True

        # searching in hidden items of the list
        soup = self._get_dom()
        if len(soup.select(".ToolBoxLive")) > 0:
            maxItems = ConfAR.MAX_ITEMS_TOOLBOX
        else:
            maxItems = ConfAR.MAX_ITEMS
        if len(items) == maxItems:
            end = False
            endText = activeItems[-1].text

            while not end:
                self.rc.send_keys(["KEY_UP"] * int(activeHighlight + 1))
                highlight, activeHighlight, activeItems, items = self._loadPageList()
                find = False
                index = 0

                if partial is True:
                    while text not in activeItems[index].text and index < (len(activeItems) - 1):
                        index = index + 1
                        if activeItems[index].text == endText:
                            end = True

                    if text in activeItems[index].text:
                        find = True
                        end = True
                else:
                    while activeItems[index].text != text and index < (len(activeItems) - 1):
                        index = index + 1
                        if activeItems[index].text == endText:
                            end = True
                    if activeItems[index].text == text:
                        find = True
                        end = True
            return find
        else:
            return False

    def findInDialogBox(self, text):
        """
        Returns True if the text can be found in a dialog
        box displayed on the screen.

        :param text: text
        :type category: str
        :returns: True if text has been found
        :rtype: boolean
        """
        soup = self._get_dom()
        find = False
        try:
            if len(soup.select(".dialog")) > 0:
                element = self._get_text_from_selectors(soup, ".box")
                if element.find(text) > -1:
                    find = True
            return find
        except Exception:
            return find

    def getInfoFromLiveBanner(self):
        """
        Returns a ProgramInfoItem object if a Live banner is displayed on the
        screen

        :returns: details of a TV program
        :rtype: ProgramInfoItem
        """
        try:
            self.logger.info("  <<   Zapping Banner Get program info")
            soup = self._get_dom()
            if (len(soup.select('.virtualZappingBanner')) < 1 and
                    len(soup.select('.ToolBoxLive .zappingBanner')) < 1):
                return None

            alltxt_list = self._get_text_from_selectors(
                soup, ".programInformation .nameOfChannel ").split(u' ')

            num = int(alltxt_list[0])
            txt = ""
            inter = ""
            for index in range(2, len(alltxt_list)):
                txt = txt + inter + alltxt_list[index]
                inter = " "

            currentProgram = self._get_text_from_selectors(
                soup, ".programInformation .program")

            element = self._get_text_from_selectors(
                soup, ".programInformation .start")
            start = None
            end = None
            length = None

            if len(element) == 5:
                hour = element.split(":")[0]
                minutes = element.split(":")[1]
                now = datetime.now()
                start = datetime(
                    now.year, now.month, now.day, int(hour), int(minutes))

                element = self._get_text_from_selectors(
                    soup, ".programInformation .end")
                if len(element) == 5:
                    hour = element.split(":")[0]
                    minutes = element.split(":")[1]
                    now = datetime.now()
                    end = datetime(
                        now.year, now.month, now.day, int(hour),
                        int(minutes))
                    if end < start:
                        end = end + timedelta(days=1)
                    length = end - start
            genre = self._get_text_from_selectors(
                soup, ".programInformation .kind")

            favorite = self._not_find_class_from_selectors(
                soup, ".programInformation .favorite", "hidden")

            nextProgram = self._get_text_from_selectors(
                soup, ".programInformation .next")

            if len(soup.select(".programInformation .pictoCsa")) > 0:
                pictoCsa = int(self._get_classes_from_selectors(
                    soup, ".programInformation .pictoCsa")[1].lstrip("csa0"))
            else:
                pictoCsa = None

            pictoDefinition = self._not_find_class_from_selectors(
                soup, ".programInformation .moviePictos .pictoHD",
                "hidden")

            pictoRatio = self._not_find_class_from_selectors(
                soup, ".programInformation .moviePictos .picto16_9",
                "hidden")

            pictoAudioDesc = self._not_find_class_from_selectors(
                soup, ".programInformation .moviePictos .pictoAD",
                "hidden")

            pictoContentVersion = self._not_find_class_from_selectors(
                soup, ".programInformation .moviePictos .pictoVM",
                "hidden")

            pictoDts = self._not_find_class_from_selectors(
                soup, ".programInformation .moviePictos .pictoDTS",
                "hidden")

            pictoDolby = self._not_find_class_from_selectors(
                soup, ".programInformation .moviePictos .pictoDolby",
                "hidden")

            pictoEar = self._not_find_class_from_selectors(
                soup, ".programInformation .moviePictos .pictoEar",
                "hidden")

            try:
                TS_elements = self._get_style_from_selectors(
                    soup, ".programInformation .scrollerBar").split(';')
                TS_start = int(
                    TS_elements[0].replace('left: ', '').replace('%', ''))
                TS_width = int(
                    TS_elements[1].replace('width: ', '').replace('%', ''))
                TS_cursor = int(self._get_style_from_selectors(
                    soup, ".programInformation .tsCursor")
                    .replace('left: ', '').replace('%;', ''))
            except Exception:
                TS_start = None
                TS_width = None
                TS_cursor = None
            program_info = ProgramInfoItem(
                num, txt, currentProgram, start, end, length, genre,
                favorite, None, nextProgram, pictoCsa, pictoDefinition,
                pictoRatio, pictoAudioDesc, pictoContentVersion, pictoDts,
                pictoDolby, pictoEar, None, None, TS_start, TS_width,
                TS_cursor)
            program_info.display()
            return program_info
        except Exception as e:
            self.logger.debug(" >>> framework debug >>> %s", str(e))
            return None
        else:
            return None

    def getList(self):
        """
        Returns a list of ListItem objects if a menu is displayed on the
        screen

        :returns: list of menu items
        :rtype: ListItem list
        """
        highlight, activeHighlight, activeItems, items = self._loadPageList()
        return items

    def getLabelFromListFocus(self):
        """
        Returns a ListItem object corresponding to the focussed menu item

        :returns: menu item
        :rtype: ListItem
        """
        highlight, activeHighlight, activeItems, items = self._loadPageList()
        return items[highlight]

    def getInfoFromEpgFocus(self):
        """
        Returns a ProgramInfoItem object if an EPG grid is displayed on the
        screen

        :returns: details of a TV program
        :rtype: ProgramInfoItem
        """
        self.logger.info("  <<   Epg Get program info")
        wording = Wording()
        soup = self._get_dom()
        if soup:

            if len(soup.select(".epg .grid .content")) > 0:

                alltxt_list = self._get_text_from_selectors(
                    soup,
                    ".epg .grid .content .row.selected .channelText")

                alltxt_list = alltxt_list.replace(u'\xa0', u' ')
                alltxt_list = alltxt_list.split(u' ')
                lcn = int(alltxt_list[0].replace(".", ""))

                channelName = ""
                inter = ""
                for index in range(1, len(alltxt_list)):
                    channelName = channelName + inter + alltxt_list[index]
                    inter = " "

                program = self._get_text_from_selectors(
                    soup, ".epg .grid .description .title")
                genre = self._get_text_from_selectors(
                    soup, " .epg .grid .description .genre")
                nextProgram = ""

                try:
                    start = None
                    end = None
                    length = None
                    date = self._get_text_from_selectors(
                        soup,
                        ".epg .grid .timeLine .middleHour")
                    if date == wording.today:
                        now = datetime.now()
                        day = now.day
                        month = now.month
                    elif date.find(':') > -1:
                        now = datetime.now()
                        day = now.day
                        month = now.month
                    elif date == wording.tomorrow:
                        now = datetime.now() + timedelta(days=1)
                        day = now.day
                        month = now.month
                    elif date == wording.yesterday:
                        now = datetime.now() - timedelta(days=1)
                        day = now.day
                        month = now.month
                    else:
                        now = datetime.now()
                        day = date.split(" ")[1]
                        month = self._getmonth(date.split(" ")[2])

                    hours = self._get_text_from_selectors(
                        soup,
                        ".epg .grid .description .date")
                    if len(hours) > 10:
                        startTxt = hours.split(" ")[0]
                        hour = startTxt.split(":")[0]
                        minutes = startTxt.split(":")[1]
                        start = datetime(now.year, month, int(day),
                                         int(hour), int(minutes))

                        endTxt = hours.split(" ")[2]
                        hour = endTxt.split(":")[0]
                        minutes = endTxt.split(":")[1]
                        now = datetime.now()
                        end = datetime(now.year, month, int(day),
                                       int(hour), int(minutes))
                        if end < start:
                            end = end + timedelta(days=1)
                        length = end - start
                except Exception:
                    start = None
                    end = None
                    length = None

                if(self._get_style_from_selectors(
                        soup,
                        ".epg .grid .content .row.selected .epgHeart").find(u"display:none")) > -1:
                    favorite = False
                else:
                    favorite = True

                ongoingRec = None

                if self._get_style_from_selectors(
                        soup,
                        "#pictoDefinition").find(u"display: none") > -1:
                    pictoHD = False
                else:
                    pictoHD = True
                if self._get_style_from_selectors(
                        soup,
                        "#pictoAudioDesc").find(u"display: none") > -1:
                    pictoAudioDesc = False
                else:
                    pictoAudioDesc = True
                if self._get_style_from_selectors(
                        soup,
                        "#pictoAspectRatio").find(u"display: none") > -1:
                    pictoRatio = False
                else:
                    pictoRatio = True
                if self._get_style_from_selectors(
                        soup,
                        "#pictoImpairedHearing")\
                        .find(u"display: none") > -1:
                    pictoEar = False
                else:
                    pictoEar = True
                if self._get_style_from_selectors(
                        soup,
                        "#pictoDolby").find(u"display: none") > -1:
                    pictoDolby = False
                else:
                    pictoDolby = True
                if self._get_style_from_selectors(
                        soup,
                        "#pictoDts").find(u"display: none") > -1:
                    pictoDts = False
                else:
                    pictoDts = True
                if self._get_style_from_selectors(
                        soup,
                        "#pictoContent").find(u"display: none") > -1:
                    pictoContentVersion = False
                else:
                    pictoContentVersion = True

                try:
                    pictoCsa = int(self._get_classes_from_selectors(
                        soup, " #pictoCsa")[1].lstrip("csa0"))
                except Exception:
                    pictoCsa = None

                pg = ProgramInfoItem(
                    lcn, channelName, program, start, end, length, genre,
                    favorite, ongoingRec, nextProgram, pictoCsa,
                    pictoHD, pictoRatio, pictoAudioDesc,
                    pictoContentVersion, pictoDts, pictoDolby, pictoEar)
                pg.display()
                return pg

            elif len(soup.select(".tonightContainer .tonightList ")) > 0:
                try:
                    channelList = soup.select(".tonightView .tonightList .tonightItem")
                    find = False
                    for channel in channelList:
                        if find:
                            break
                        programList = channel.select(".primeTime")
                        for programInfo in programList:
                            if find:
                                break
                            if 'selected' in programInfo.attrs['class']:
                                find = True
                                element = BeautifulSoup(unicode(programInfo))
                                lcn = -1
                                channelName = ""
                                pictoCsa = None
                                pictoHD = None
                                pictoRatio = None
                                pictoAudioDesc = None
                                pictoContentVersion = None
                                pictoDts = None
                                pictoDolby = None
                                pictoEar = None
                                date = None
                                program = ''
                                genre = ''
                                nextProgram = ""
                                start = None
                                end = None
                                length = None
                                if(self._get_style_from_selectors(element,
                                   ".noDataAvailable").find("display: none")) > -1:
                                    startTxt = self._get_text_from_selectors(element, ".date")
                                    hour = startTxt.split(" ")[0].split(":")[0]
                                    minutes = startTxt.split(" ")[0].split(":")[1]
                                    now = datetime.now()
                                    start = datetime(now.year, now.month,
                                                     now.day, int(hour),
                                                     int(minutes))

                                    if len(element.select(".information .footer .dataAvailable")) > 0:
                                        program = self._get_text_from_selectors(element, ".footer .dataAvailable .name")
                                        genre = self._get_text_from_selectors(element, ".footer .dataAvailable .genre")

                                if self._get_style_from_selectors(BeautifulSoup(str(channel)), ".epgHeart").find(u"display: none") > -1:
                                    favorite = False
                                else:
                                    favorite = True

                                if self._get_style_from_selectors(
                                        element,
                                        "#pictoDefinition").find(u"display: none") > -1:
                                    pictoHD = False
                                else:
                                    pictoHD = True

                                if self._get_style_from_selectors(
                                        element,
                                        "#pictoAudioDesc").find(u"display: none") > -1:
                                    pictoAudioDesc = False
                                else:
                                    pictoAudioDesc = True
                                if self._get_style_from_selectors(
                                        element,
                                        "#pictoAspectRatio").find(u"display: none") > -1:
                                    pictoRatio = False
                                else:
                                    pictoRatio = True
                                if self._get_style_from_selectors(
                                        element,
                                        "#pictoImpairedHearing")\
                                        .find(u"display: none") > -1:
                                    pictoEar = False
                                else:
                                    pictoEar = True
                                if self._get_style_from_selectors(
                                        element,
                                        "#pictoDolby").find(u"display: none") > -1:
                                    pictoDolby = False
                                else:
                                    pictoDolby = True
                                if self._get_style_from_selectors(
                                        element,
                                        "#pictoDts").find(u"display: none") > -1:
                                    pictoDts = False
                                else:
                                    pictoDts = True
                                if self._get_style_from_selectors(
                                        element,
                                        "#pictoContent").find(u"display: none") > -1:
                                    pictoContentVersion = False
                                else:
                                    pictoContentVersion = True

                                try:
                                    pictoCsa = int(self._get_classes_from_selectors(
                                        element, " #pictoCsa")[1].lstrip("csa0"))
                                except Exception:
                                    pictoCsa = None

                except Exception as e:
                    self.logger.debug(" >>> framework debug >>> %s", str(e))

                programInfo = ProgramInfoItem(lcn, channelName, program, start, end, length, genre, favorite, None, nextProgram, pictoCsa, pictoHD, pictoRatio, pictoAudioDesc, pictoContentVersion, pictoDts, pictoDolby, pictoEar)
                programInfo.display()
                return programInfo
            else:
                self.logger.debug(" >>> framework debug >>> no selector")
                return False

    def getInfoFromMosaicFocus(self):
        """
        Returns a ProgramInfoItem object if a mosaic is displayed on the
        screen

        :returns: details of a TV program
        :rtype: ProgramInfoItem
        """
        self.logger.info("  <<   Mosaic Get program info")
        soup = self._get_dom()
        if LocalConfig().stb_model == 'WHD81':
            alltxt_list = soup.select(".highlight .focusLabel")[0].get_text().split(u' ')
            num = int(alltxt_list[0])
            txt = ""
            inter = ""
            for index in range(2, len(alltxt_list)):
                txt = txt + inter + alltxt_list[index]
                inter = " "
            favorite = self._not_find_class_from_selectors(
                soup,
                ".highlight .favorite",
                "hidden")
            program_info = ProgramInfoItem(
                num, txt, "", None, None, None, "",
                favorite, None, "", None, None,
                None, None, None, None,
                None, None, None, None, None, None,
                None)
            program_info.display()
            return program_info
        else:
            try:

                if len(soup.select(".scene.mosaicView .mosaic .mosaicItem.row1.highlight")) > 0:
                    alltxt_list = soup.select(".scene.mosaicView .mosaic .mosaicItem.row1.highlight .focusLabel")[0].get_text().split(u' ')
                    favorite = self._not_find_class_from_selectors(
                        soup,
                        ".scene.mosaicView .mosaic .mosaicItem.row1.highlight",
                        ".favorite.hidden")
                    ongoingRec = self._find_class_from_selectors(
                        soup,
                        ".scene.mosaicView .mosaic .mosaicItem.row1.highlight",
                        "recording")

                elif len(soup.select(".scene.mosaicView .mosaic .mosaicItem.row2.highlight")) > 0:
                    alltxt_list = soup.select(".scene.mosaicView .mosaic .mosaicItem.row2.highlight .focusLabel")[0].get_text().split(u' ')
                    favorite = self._not_find_class_from_selectors(
                        soup,
                        ".scene.mosaicView .mosaic .mosaicItem.row1.highlight",
                        ".favorite.hidden")
                    ongoingRec = self._find_class_from_selectors(
                        soup,
                        ".scene.mosaicView .mosaic .mosaicItem.row1.highlight",
                        "recording")
                elif len(soup.select(".scene.mosaicView .mosaic .mosaicItem.row3.highlight")) > 0:
                    alltxt_list = soup.select(".scene.mosaicView .mosaic .mosaicItem.row3.highlight .focusLabel")[0].get_text().split(u' ')
                    favorite = self._not_find_class_from_selectors(
                        soup,
                        ".scene.mosaicView .mosaic .mosaicItem.row1.highlight",
                        ".favorite.hidden")
                    ongoingRec = self._find_class_from_selectors(
                        soup,
                        ".scene.mosaicView .mosaic .mosaicItem.row1.highlight",
                        "recording")
                elif len(soup.select(".scene.mosaicView .mosaic .mosaicItem.row4.highlight")) > 0:
                    alltxt_list = soup.select(".scene.mosaicView .mosaic .mosaicItem.row4.highlight .focusLabel")[0].get_text().split(u' ')
                    favorite = self._not_find_class_from_selectors(
                        soup,
                        ".scene.mosaicView .mosaic .mosaicItem.row1.highlight",
                        ".favorite.hidden")
                    ongoingRec = self._find_class_from_selectors(
                        soup,
                        ".scene.mosaicView .mosaic .mosaicItem.row1.highlight",
                        "recording")

                num = int(alltxt_list[0])
                txt = ""
                inter = ""
                for index in range(2, len(alltxt_list)):
                    txt = txt + inter + alltxt_list[index]
                    inter = " "

                currentProgram = self._get_text_from_selectors(
                    soup, ".scene.mosaicView .mosaicTooltip .program")

                genre = self._get_text_from_selectors(
                    soup, ".scene.mosaicView .mosaicTooltip .kind")

                pictoCsa = int(self._get_classes_from_selectors(
                    soup, ".scene.mosaicView .mosaicTooltip .pictoCsa")[1].lstrip("csa0"))

                pictoDefinition = self._not_find_class_from_selectors(
                    soup, ".scene.mosaicView .mosaicTooltip .pictoHD",
                    "hidden")

                pictoRatio = self._not_find_class_from_selectors(
                    soup, ".scene.mosaicView .mosaicTooltip .picto16_9",
                    "hidden")

                pictoAudioDesc = self._not_find_class_from_selectors(
                    soup, ".scene.mosaicView .mosaicTooltip .pictoAD",
                    "hidden")

                pictoContentVersion = self._not_find_class_from_selectors(
                    soup, ".scene.mosaicView .mosaicTooltip .pictoVM",
                    "hidden")

                pictoDts = self._not_find_class_from_selectors(
                    soup, ".scene.mosaicView .mosaicTooltip .pictoDTS",
                    "hidden")

                pictoDolby = self._not_find_class_from_selectors(
                    soup, ".scene.mosaicView .mosaicTooltip .pictoDolby",
                    "hidden")

                pictoEar = self._not_find_class_from_selectors(
                    soup, ".scene.mosaicView .mosaicTooltip .pictoEar",
                    "hidden")

                program_info = ProgramInfoItem(
                    num, txt, currentProgram, None, None, None, genre,
                    favorite, ongoingRec, "", pictoCsa, pictoDefinition,
                    pictoRatio, pictoAudioDesc, pictoContentVersion, pictoDts,
                    pictoDolby, pictoEar, None, None, None, None,
                    None)
                program_info.display()
                return program_info
            except Exception as e:
                self.logger.debug(" >>> framework debug >>> %s", str(e))
                return None

    def getStatus(self):
        """
        Returns a Status object

        :returns: details of STB status
        :rtype: Status
        """
        self.logger.info("  <<   Get Status")
        noRightPanel = None
        scene = None
        dialog = None
        mini_live = None
        soup = None

        power, state = self.rc.get_stb_status()
        if not power:
            stb = "DOWN"
        elif state == "MAIN_PROCESS":
            stb = "STANDBY"
        else:
            stb = "READY"

        if stb == "READY":
            if state is None:
                scene = "UNKNOWN"
            elif state == "LIVE":
                soup = self._get_dom()
                isToolbox = soup is not False and (len(soup.select(".ToolBoxLive")) > 0 or len(soup.select((".toolbox"))) > 0)
                isZappingBanner = not(isToolbox) and soup is not False and len(soup.select(".virtualZappingBanner")) > 0
                if isToolbox:
                    scene = "TOOLBOX"
                elif isZappingBanner:
                    scene = "ZAPPING_BANNER"
                else:
                    scene = "LIVE"
            elif state == "HOMEPAGE":
                scene = "DESK"
            else:
                scene = state
            if not soup:
                soup = self._get_dom()
            try:
                element = soup.select(".scene.noRightView")
                if str(element).find('aria-hidden="false"') > -1:
                    noRightPanel = True
                else:
                    noRightPanel = False
            except Exception:
                noRightPanel = False

            try:
                if len(soup.select(".dialog")) > 0:
                    dialog = True
                else:
                    dialog = False
            except Exception:
                dialog = False

            try:
                style = self._get_style_from_selectors(soup, '.PIP')
                if style is not None:
                    if style.find('none') > -1:
                        mini_live = False
                    else:
                        mini_live = True
                else:
                    mini_live = False
            except Exception:
                mini_live = False

        status = Status(stb, noRightPanel, scene, dialog, mini_live)
        status.display()
        return status

    def getInfoFromVodPage(self):
        """
        Returns a VodItem object if a VOD page is displayed on the
        screen

        :returns: details of a VOD
        :rtype: VodItem
        """
        soup = self._get_dom()

        if len(soup.select(".scene .dockCenter")) > 0:
            title = self._get_text_from_selectors(soup, ".breadcrumb .first")
            genre = self._get_text_from_selectors(soup, ".scene .dockCenter .genres")

            cover_el = soup.select(".presentationScreen .leftSideContainer .cover")
            if cover_el[0] is not None:
                cover = cover_el[0].attrs['src']
                if cover == "":
                    cover = None
            else:
                cover = None

            # Rating
            # press_el = soup.select(".rightSideContainer .ratingarea .press .opinionNumber")
            #
            # users_el = soup.select(".rightSideContainer .ratingarea .user .opinionNumber")
            #
            # vod_rating = VodRating(press_rating, users_rating)

            try:
                lengthTxt = self._get_text_from_selectors(soup, ".scene .dockCenter .duration")
                regex = re.compile(r'[0-9]{1,3}')
                match_result = regex.search(lengthTxt)
                if match_result:
                    lengthTxt = str(match_result.group(0))
                    length = timedelta(minutes=int(lengthTxt))
                else:
                    length = None
            except Exception:
                length = None

            try:
                dateTxt = self._get_text_from_selectors(soup, ".scene .dockCenter .labelDuree")
                day = dateTxt.split(" ")[2]
                hour = dateTxt.split(" ")[4]
                endDate = datetime(int(day.split("/")[2]),
                                   int(day.split("/")[1]),
                                   int(day.split("/")[0]),
                                   int(hour.split(":")[0]),
                                   int(hour.split(":")[1]))
            except Exception:
                endDate = None

            try:
                value = self._get_text_from_selectors(soup,
                                                      ".scene .price").strip(u"€. ").replace(',', '.')
                price = float(value)
            except Exception:
                price = None

            pictoRatio = None
            pictoAudioDesc = None
            pictoContentVersion = None
            pictoDts = None
            pictoDolby = None
            pictoEar = None

            if self._get_style_from_selectors(
                    soup,
                    "#pictoDefinition").find(u"display: none") > -1:
                pictoDefinition = False
            else:
                pictoDefinition = True
            if self._get_style_from_selectors(
                    soup,
                    "#pictoAspectRatio").find(u"display: none") > -1:
                pictoRatio = False
            else:
                pictoRatio = True
            if self._get_style_from_selectors(
                    soup,
                    "#pictoAudioDesc").find(u"display: none") > -1:
                pictoAudioDesc = False
            else:
                pictoAudioDesc = True
            if self._get_style_from_selectors(
                    soup,
                    "#pictoContent").find(u"display: none") > -1:
                pictoContentVersion = False
            else:
                pictoContentVersion = True
            if self._get_style_from_selectors(
                    soup,
                    "#pictoDts").find(u"display: none") > -1:
                pictoDts = False
            else:
                pictoDts = True
            if self._get_style_from_selectors(
                    soup,
                    "#pictoDolby").find(u"display: none") > -1:
                pictoDolby = False
            else:
                pictoDolby = True
            if self._get_style_from_selectors(
                    soup,
                    "#pictoImpairedHearing").find(u"display: none") > -1:
                pictoEar = False
            else:
                pictoEar = True

            pictoCsa = None
            try:
                pictoCsa = int(self._get_classes_from_selectors(
                    soup, " #pictoCsa")[1].lstrip("csa0"))
            except Exception:
                pictoCsa = None

            vod = VodItem(title, genre, length, endDate, price, pictoCsa,
                          pictoDefinition, pictoRatio, pictoAudioDesc,
                          pictoContentVersion, pictoDts, pictoDolby, pictoEar)
            vod.setCover(cover)
            # vod.setRating(vod_rating)
            vod.display()
            return vod
        else:
            return False

    def getInfoFromRecordPage(self):
        """
        Returns a RecordItem object if a record page is displayed on the
        screen

        :returns: details of a record
        """
        focusTitle = ''
        beginRecTime = None
        endRecTime = None
        channel = None

        time.sleep(3)
        soup = self._get_dom()
        time.sleep(3)

        try:
            if len(soup.select(".dockCenter .info .times")) > 0:
                focusTitle = self._get_text_from_selectors(
                    soup, ".dockCenter .info .title")
                date = self._get_text_from_selectors(
                    soup, ".dockCenter .info .date")

                channel = self._get_text_from_selectors(
                    soup, ".dockCenter .info .bouquetInfo .orange")

                details = self._get_text_from_selectors(
                    soup, ".dockCenter .info .times")
                begin = details.split(" ")[0]
                end = details.split(" ")[2]

                beginHour = begin.split(":")[0]
                beginMinute = begin.split(":")[1]

                endHour = end.split(":")[0]
                endMinute = end.split(":")[1]

                beginRecTime = timedelta(hours=int(beginHour),
                                         minutes=int(beginMinute))
                endRecTime = timedelta(hours=int(endHour),
                                       minutes=int(endMinute))
                lenght = endRecTime - beginRecTime

                focusDate = datetime(int(date.split("/")[2]),
                                     int(date.split("/")[1]),
                                     int(date.split("/")[0])).date()
                pg = RecordItem(focusTitle, focusDate, lenght, beginRecTime,
                                endRecTime, channel, None, None)
                pg.display()
                return pg
            else:
                self.logger.debug(" >>> framework debug >>> no selector")
                return False
        except Exception as e:
            self.logger.debug(" >>> framework debug >>> %s", str(e))
            return False

    def getInfoFromRecordFocus(self):
        """
        Returns a RecordItem object if a record page is displayed on the
        screen

        :returns: details of a record
        """
        focusDate = None
        focusLength = None
        focusTitle = None
        status = None
        onGoing = False
        beginRecTime = None
        endRecTime = None

        time.sleep(5)
        soup = self._get_dom()
        time.sleep(3)
        try:
            if len(soup.select(".recordMosaic .itemsContainer .item")) > 0:
                focusTitle = self._get_text_from_selectors(soup, ".recordMosaic .itemsContainer .item.zoomed .title")
                details = self._get_text_from_selectors(soup, ".item.zoomed .airingDatas .grey")

                text_airing = self._get_text_from_selectors(soup, ".recordMosaic .itemsContainer .item.zoomed .airingDatas")
                onGoing = re.search(re.compile(Wording().pvrOnGoing), text_airing) is not None

                dateTxt = details.split(" ")[0]
                focusDate = datetime(int(dateTxt.split("/")[2]),
                                     int(dateTxt.split("/")[1]),
                                     int(dateTxt.split("/")[0])).date()

                if len(details.split("-")) > 1:
                    regex = re.compile(r" ([0-9]*) h ([0-9]*) min")
                    match_result = regex.search(details.split("-")[1])
                    if match_result:
                        focusLength = timedelta(hours=int(str(match_result.group(1))),
                                                minutes=int(str(match_result.group(2))))
                    else:
                        regex = re.compile(r" ([0-9]*) min")
                        match_result = regex.search(details.split("-")[1])
                        if match_result:
                            focusLength = timedelta(minutes=int(str(match_result.group(1))))
                        else:
                            regex = re.compile(r" ([0-9]*) h")
                            match_result = regex.search(details.split("-")[1])
                            if match_result:
                                focusLength = timedelta(hours=int(str(match_result.group(1))))
                            else:
                                focusLength = None
                else:
                    focusLength = None

                if len(soup.select(".recordMosaic .itemsContainer .item.zoomed. status.failed")) > 0:
                    status = "KO"
                else:
                    status = "OK"

                pg = RecordItem(focusTitle, focusDate, focusLength, endRecTime,
                                beginRecTime, None, onGoing, status, None)
                pg.display()
                return pg

            elif len(soup.select(".schedulingMosaic .itemsContainer .item.zoomed")) > 0:
                focusTitle = self._get_text_from_selectors(soup, ".schedulingMosaic .itemsContainer .item.zoomed .title")
                details = self._get_text_from_selectors(soup, ".schedulingMosaic .itemsContainer .item.zoomed .airingDatas .grey")
                dateTxt = details.split(" ")[0]
                hourTxt = details.split(" ")[2]
                focusDate = datetime(int(dateTxt.split("/")[2]),
                                     int(dateTxt.split("/")[1]),
                                     int(dateTxt.split("/")[0]),
                                     int(hourTxt.split(":")[0]),
                                     int(hourTxt.split(":")[1]))
                if len(soup.select(".recordMosaic .itemsContainer .item.zoomed. status.failed")) > 0:
                    status = "KO"
                else:
                    status = "OK"
                pg = RecordItem(focusTitle, focusDate, focusLength, endRecTime,
                                beginRecTime, None, onGoing, status, None)
                pg.display()
                return pg

            else:
                self.logger.debug(" >>> framework debug >>> no selector")
                return None
        except Exception as e:
            self.logger.debug(" >>> framework debug >>> %s", str(e))
            return None

    def _loadPageList(self):
        '''
        Return the current selected block in focus :
            (
                highlight - int,
                items - str
                activeHighlight - int
                activeItems - str
            ) - tuple
        '''
        highlight = None
        items = []
        activeHighlight = None
        activeItems = []
        soup = self._get_dom()
        if soup:
            try:
                if len(soup.select(".PVRPlayer .toolboxMenu .listItem")) > 0:
                    elements = soup.select(".PVRPlayer .toolboxMenu .listItem")
                elif len(soup.select(".PVRPlayer .radiolist .listItem")) > 0:
                    elements = soup.select(".PVRPlayer .radiolist .listItem")
                elif len(soup.select(".radiolist .listItem")) > 0:
                    elements = soup.select(".radiolist .listItem")
                elif len(soup.select(".box .listItem")) > 0:
                    elements = soup.select(".box .listItem")
                elif len(soup.select(".menuList .list .container .listItem")) > 0:
                    elements = soup.select(".menuList .list .container .listItem")
                else:
                    return False

                indexActive = 0

                for index, element in enumerate(elements):
                    text = element.get_text().rstrip().replace("  ", " ")
                    if 'highlight' in element.attrs['class']:
                        highlight = index
                        activeHighlight = indexActive

                    if 'selected' in element.attrs['class'] or 'checked' in element.attrs['class']:
                        selected = True
                    else:
                        selected = False
                    if 'inactive' in element.attrs['class']:
                        active = False
                    else:
                        active = True
                        activeItems.append(ListItem(text, selected, active))
                        indexActive = indexActive + 1
                    items.append(ListItem(text, selected, active))
                return highlight, activeHighlight, activeItems, items

            except Exception as e:
                self.logger.debug(" >>> framework debug >>> " + str(e))
                return False
        else:
            return None

    def _moveToTile(self, currentTile, currentX, destinationTile):
        '''
        Move cursor to a destination Tile

        @return: boolean
        '''
        deltaTile = destinationTile - currentTile
        move = False
        key = None
        try:
            if deltaTile < 0:
                key = "KEY_UP"
                move = True
            elif deltaTile > 0:
                key = "KEY_DOWN"
                move = True

            if currentX > 0:
                    keysX = ["KEY_LEFT"] * int(currentX)
                    self.rc.send_keys(keysX)

            if move:
                keys = [key] * abs(deltaTile)
                self.rc.send_keys(keys)

            return move
        except Exception as e:
            self.logger.debug(" >>> framework debug >>> %s", str(e))
            return move

    def _getBlockFromDeskFocus(self):
        '''
        Return the current selected block in focus :
            (
                block - WebDriver,
                is header - boolean
                tile number - int
            ) - tuple
        '''
        soup = self._get_dom()

        if len(soup.select(".desk.scene")) > 0:
            try:
                isHeader = False
                block = None
                found = False

                if self._find_class_from_selectors(soup,
                                                   ".desk-header .desk-cell",
                                                   "selected"):
                    block = soup.select(".desk-header .desk-cell")[0]
                    isHeader = True
                    found = True
                tile = 0

                if not found:
                    tiles = soup.select(".desk-table .th_2")
                    for tile, tileContains in enumerate(tiles):

                        soup2 = BeautifulSoup(str(tileContains), 'lxml')
                        blocks = soup2.select(".desk-cell")

                        i = 0
                        while not found and i < len(blocks):

                            if u'selected' in blocks[i].attrs['class']:
                                block = blocks[i]
                                found = True
                            else:
                                i += 1
                        if found:
                            break
                return (block, isHeader, tile)
            except Exception as e:
                self.logger.debug(" >>> framework debug >>> ", str(e))
                return None
        else:
            return None

    def _getBlockPositionFromDeskFocus(self):
        '''
        Return a tuple that contains pieces of information from selected
        desk block :

        @return:
            (
                ( x position, y position, width, height ) - tuple,
                is header - boolean,
                tile number - int
            ) - tuple
        '''
        try:
            block, isHeader, index = self._getBlockFromDeskFocus()
            if isHeader:
                return ([0, 0, 2, 1], isHeader, -1)
            elif block is not None:

                x = block.attrs['class'][1].replace('x_', '')
                y = block.attrs['class'][2].replace('y_', '')
                w = block.attrs['class'][3].replace('w_', '')
                h = block.attrs['class'][4].replace('h_', '')

                return ([int(x), int(y), int(w), int(h)], isHeader, index)
            else:
                return None
        except Exception:
            return None

    def _getmonth(self, month):
        switcher = {Wording().january: 1,
                    Wording().february: 2,
                    Wording().march: 3,
                    Wording().april: 4,
                    Wording().may: 5,
                    Wording().june: 6,
                    Wording().july: 7,
                    Wording().august: 8,
                    Wording().september: 9,
                    Wording().october: 10,
                    Wording().november: 11,
                    Wording().december: 12}
        return switcher.get(month)


class DeskObjectData(AttrDict):

    def getHeader(self):
        return self.header

    def getTiles(self):
        return self.tiles

    def getTile(self, idTile):
        self.logger.debug("just a test guy: %s", idTile)
        if 0 <= idTile < len(self.tiles):
            return self.tiles[idTile]
        return None


def _getDeskDataFromStb():
    """
    Return the content of Desk Json file store in resources folder.
    The method will try to Download the Desk Json file from STB and move
    it to resources folder each time it was called.
    Then open the file and return the content.
    If nothing found return None
    :return: content of desk.json file
    :rtype: json_data
    """
    """
    Open connection with STB
    """
    logger = logging.getLogger(__name__)
    serial = SerialManager()
    parser = SafeConfigParser()
    parser.read('resources/' + LocalConfig().stb_resources)
    stb_desk_path = parser.get('general', 'stb_desk_path')

    """
    Retrieve current desk folder
    """
    result = serial.send_command("ls -t " + stb_desk_path)

    """
    workarounds with inconsistent result
    """
    # result = result.replace("[1;34m", "")
    # result = result.replace("[0m", "")

    result = result["response"]
    logger.debug("File found: " + str(result))
    """
    Send command to download Desk file
    """
    result2 = serial.send_command(
                command="curl -i -F filedata=@" + stb_desk_path + result + "/desk.json" + " https://" + LocalConfig().server_ip + "/upload.php",
                is_transfer=True
    )
    desk_local_file = 'resources/STB_{}/desk.json'.format(
        LocalConfig().stb_model_name
    )
    if result2["status"] == "OK":
        srcfile = '/var/www/html/upload/desk.json'
        shutil.copy(srcfile, desk_local_file)
        logger.info("  <<   Desk file successfully downloaded from STB")
    else:
        logger.warning("Desk file download from STB failed -- Using local file")

    with open(desk_local_file) as json_file:
        data = json_file.read()
        json_data = json.loads(data)
        return json_data


def getDeskDataObject():
    '''
    Return the json desk data into a Python object
    '''
    if LocalConfig().json_desk_data is None:
        LocalConfig().json_desk_data = DeskObjectData(_getDeskDataFromStb())

    return LocalConfig().json_desk_data


class ConfAR (object):
    MAX_ITEMS = 10
    MAX_ITEMS_TOOLBOX = 8
