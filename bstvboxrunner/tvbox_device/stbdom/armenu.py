# -*- coding: utf-8 -*-
"""
----------------------------------
démarrage...
PLEIN ECRAN
17:40
Recherche
vidéo à la demande, programme tv...
TV
accédez au direct via OK ou P+




CANAL+ CANALSAT
enregistreur TV
programme TV
TV à la demande

-----------------------


    recherche: -1
    tv: 0
    vod: 1
    sports: 2
    jeux: 3
    musique: 4
    applis: 5
    orange: 6
    boutique: 7



"""

from slugify import slugify

from ardom import ArDomPage,ScreenView,ListItem


import logging
log= logging.getLogger(__name__)


recherche= -1

config= {

    'topology':[
        'tv',
        'vod',
        'sports'
        'jeux',
        'musique',
        'applis'
        'orange',
        'boutique'
        ],

#     'labels': [
#         [u'TV',u'CANAL+ CANALSAT',u'enregistreur TV',u'programme TV',u'TV à la demande'],
#         [u'vidéo à la demande',u'mes favoris',u'mes vidéos'],
#         [],
#         [ u"jeux vidéo",u"multi-joueurs",u"nouveautés"],
#         [u'musique',u'radios',u'Deezer',u'vidéos'],
#         [u'applications',u'accédez au meilleur du web',u'les chaînes web'],
#         [u'Orange et moi', u'visite guidée', u"Le Cloud d'Orange", u"accessibilité", u"media center",
#          u"mes réglages"],
#         [u'boutique']
#         ]
}


class StbMenu(ScreenView):
    """

    """
    root_class = "desk scene"

    config= config

    @classmethod
    def find_root(cls,soup):
        """

        :return:
        """
        log.debug('search root:  <? class="%s ...">' % cls.root_class)
        f = lambda tag: tag.has_attr('class') and tag['class'][0] == "desk" and tag['class'][1] == "scene"
        live_scene = soup.body.find_all(f)
        root= cls._first(live_scene)
        log.debug('found  root:  %s' % root)
        return root


    # def find_relative_desk_coordinates(self, text):
    #     """
    #
    #
    #     :param text:
    #     :return:
    #     """
    #     x = -1
    #     y = -1
    #
    #     cell_class = "desk-cell"
    #     #f = lambda tag: tag.has_attr('class') and tag['class'][0] == cell_class and tag.text == text
    #     f = lambda tag: tag.text == text
    #
    #     found = self.root.find_all(f)
    #     if found:
    #         element = found[-1]
    #         element= element.parent
    #         parent= element.parent
    #
    #
    #         for class_ in element.attrs['class']:
    #             #print class_
    #             if class_.startswith('x_'):
    #                 x = class_[2:]
    #             elif class_.startswith('y_'):
    #                 y = class_[2:]
    #         # print "x,y=(%s,%s)" % (x, y)
    #         x = int(x)
    #         y = int(y)
    #     return x, y


    def find_topics(self):
        """
            find this kind of elements
            <div class=" th_2 tileHidden">

        :return:
        """
        tag_class= 'th_2'
        f = lambda tag: tag.has_attr('class') and tag_class in tag['class']
        topics = self.root.find_all(f)
        return topics

    def find_labels(self,topic):
        """
        {u'class': [u'desk-cell', u'x_0', u'y_0', u'w_1', u'h_2', u'selected']}

        :param topic: instance ( soup element)
        :return:
        """
        tag_class = 'desk-cell'
        f = lambda tag: tag.has_attr('class') and tag_class in tag['class'] and tag.text
        labels = topic.find_all(f)
        return labels

    def extract_desk_cell_info(self,label):
        """

        <div class="desk-cell x_0 y_0 w_1 h_2 selected"><div class="text top">TV</div>

        extract X_? and Y_? class

        :param label: instance(soup element)
        :return: text,x,y
        """
        x = -1
        y = -1
        w = -1
        h = -1

        tags=[]
        # find all desk-cell elements with text
        f = lambda tag: tag.has_attr('class') and 'text' in tag['class']
        texts = label.find_all(f)
        # retain only the first one
        text= texts[0].text

        for class_ in label.attrs['class']:
            #print class_
            if class_.startswith('x_'):
                x = class_[2:]
            elif class_.startswith('y_'):
                y = class_[2:]
            elif class_.startswith('w_'):
                w = class_[2:]
            elif class_.startswith('h_'):
                h = class_[2:]
        # print "x,y=(%s,%s)" % (x, y)
        x = int(x)
        y = int(y)
        w = int(w)
        h = int(h)
        return text,x, y , w, h



    # def _manual_feed(self):
    #     """
    #
    #     :return:
    #     """
    #     config = self.config
    #     for index,topic in enumerate(config['topology']):
    #         log.debug("========= %s - %s =========" % (str(index),topic))
    #         for text in config['labels'][index]:
    #             coordinates = self.find_relative_desk_coordinates(text)
    #             log.debug("%s %s" % (text, coordinates))
    #             label= slugify(text)
    #             self._labels[label]= "%s/%s/%s" % (str(index),coordinates[0],coordinates[1])

    def _autofeed(self):
        """

        :return:
        """
        topics = self.find_topics()
        for index, topic in enumerate(topics):
            labels = self.find_labels(topic)
            for label in labels:
                text, x, y, w, h = self.extract_desk_cell_info(label)
                slug = slugify(text)
                # adapt to reverse coordinates
                if h == 1:
                    y = 1 - y
                path = "%s/%s/%s" % (index, x, y)
                self._labels[slug] = path
                log.debug("[%s]: %s" % (slug, path))

    def feed(self):
        """


        :return:
        """

        self._labels= {}
        self._labels["recherche"]= "-1"

        self._autofeed()

        # try:
        #     self._autofeed()
        # except Exception, e:
        #     self._manual_feed()

        return self._labels


    def get_slug_path(self,text):
        """

        :param slug:
        :return:
        """
        slug= slugify(text)
        return self._labels[slug]




if __name__=="__main__":

    html_file = "../tests/samples/stb_menu.html"

    logging.basicConfig(level=logging.DEBUG)


    # import json
    # import io
    # with open('samples/stb_menu.json','rb') as fh:
    #     content= fh.read()
    #     data= json.loads(content)
    #     with io.open("samples/stb_menu.html", 'w', encoding='utf8') as f:
    #         f.write(data)




    def test_basics():
        """


        :return:
        """
        page= ArDomPage.from_file(html_file)

        print page.prettify()

        body=page.soup.body

        return


    def test_StbMenu():


        l = StbMenu.from_file(html_file)

        l .feed()

        slug_path = l.get_slug_path(u'mes videos')
        assert slug_path == "2/1/1"

        return

    #
    #
    #test_basics()
    test_StbMenu()



print("Done")