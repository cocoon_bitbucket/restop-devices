"""

    keyinfo html screen (polaris)


    header:
        <title>
            Orange TV - New UI
        </title>

    body

        # Active channel
        <div id="fpn" style="visibility: visible; ">
            <div id="fpntxt">
                1 - TF1
            </div>


        # channel list
        <div aria-hidden="false" class="polaris virtualZappingList scene">
            <div class="channelList">

                ## one channel (the selected one )
                <div class="infoChannel selected">

                    ### main title
                     <div class="titleLine">
                        <div class="channelTitle">
                            1 - TF1
                        </div>

                    ### full info
                    <div class="programInfoHorizontalList maximumInfo">

                        #### fist info program
                        <div class="infoProgram">

                            ##### lines
                            <div class="firstLine">

                                        <span class="programTitle" style="max-width: 522px; ">
                                            Ma parole contre la leur
                                        </span>
                                        <div class="csa pictos CSA2"></div>

                            #####  <div class="secondLine">

                        #### second info program
                        <div class="infoProgram">

"""

from datetime import datetime, timedelta
import re

from slugify import slugify

from ardom import ArDomPage,ScreenView,ListItem


import logging
log= logging.getLogger(__name__)



class Keyinfo(ScreenView):
    """
        the overlay we get with KEY_INFO

    """
    root_class = None

    _items= {

        'lcn': '',
        'channelName': "nameOfChannel",
        'favorite' : 'favorite',
        'ongoingRec': '',
        'program': 'program',
        'start': 'start',
        'end': 'end',
        'length': '',
        'genre': 'kind',
        'nextProgram': 'next',

        'pictoCsa': 'pictoCsa',
        'pictoDefinition': 'pictoHD',
        'pictoRatio': 'picto16_9',
        'pictoAudioDesc':'pictoAD',
        'pictoContentVersion':'pictoVM',
        'pictoDts': 'pictoDTS',
        'pictoDolby': 'pictoDolby',
        'pictoEar': 'pictoEar',
        'picto3D': '',
        'pictoVO': '',
        'TS_width': 'scrollerBar',
        'TS_start': 'scrollerBar',
        'TS_cursor': 'tsCursor',

    }

    @classmethod
    def find_root(cls,soup):
        """

        :return:
        """
        log.debug('search root:  body')
        return soup.body

        # log.debug('search root:  <? class="%s ...">' % cls.root_class)
        # f = lambda tag: tag.has_attr('class') and tag['class'][0] == cls.root_class
        # elements = soup.body.find_all(f)
        # # if several retains the last one
        # root= cls._last(elements)
        # log.debug('found  root:  %s' % root)
        # return root


    #
    # stbplay
    #

    def find_channel_list(self,root=None):
        """
            <div class="channelList">
        :return: tag
        """
        root= root or self.root
        found= None

        if root:
            f = lambda tag: tag.has_attr('class') and tag['class'][0] == 'channelList'
            elements = root.find_all(f)
            if elements:
                found = self._last(elements)
                log.debug('found  channel list:  %s' % found)
            else:
                log.error('not found  channel list:  %s' % found)
        return found

    def find_selected_channel(self,root=None):
        """

            <.> channel_list
                <div class="infoChannel selected">

        :return: tag
        """
        root = root or self.find_channel_list()
        found = None
        if root:
            f = lambda tag: tag.has_attr('class') and 'infoChannel' in tag['class'] and  'selected' in tag['class']
            elements = root.find_all(f)
            if elements:
                found = self._last(elements)
                log.debug('found  active channel :  %s' % found)
            else:
                log.debug('not found  active channel :  %s' % found)
        return found

    def find_selected_channel_title(self,root=None):
        """

            <.> selected channel
                <div class="channelTitle">
                    1 - TF1
                </div>

        :return:
        """
        root = root or self.find_selected_channel()
        found = None
        if root:
            f = lambda tag: tag.has_attr('class') and 'channelTitle' in tag['class']
            elements = root.find_all(f)
            if elements:
                found = self._last(elements)
                log.debug('found  selected channel title :  %s' % found)
            else:
                log.debug('not found  selected channel title :  %s' % found)
        return found


    def _split_text(self,text):
        """

        :param text:
        :return:
        """
        if '-' in text:
            sep='-'
        elif '.' in text:
            sep='.'
        else :
            sep= None
        text= text.strip()
        if sep:
            a,b= text.split(sep,1)
        else:
            a=text
            b=text
        return a,b




    #
    # stb blancche
    #

    def find_virtualZappingBanner(self,root=None):

        root= root or self.root
        found= None

        if root:
            f = lambda tag: tag.has_attr('class') and 'virtualZappingBanner' in  tag['class']
            elements = root.find_all(f)
            if elements:
                found = self._last(elements)
                log.debug('found  : virtualZappingBanner %s' % found)
            else:
                log.error('not found  virtualZappingBanner:  %s' % found)
        return found


    def find_nameOfChannel(self):
        """


            search <div class="nameOfChannel">1 - TF1</div>

        :return:
        """
        found= None
        banner= self.find_virtualZappingBanner()

        if banner:
            f = lambda tag: tag.has_attr('class') and 'nameOfChannel' in tag['class']
            elements = banner.find_all(f)
            if elements:
                found = self._last(elements)
                log.debug('found  nameOfChannel:  %s' % found)
            else:
                log.error('not found  nameOfChannel')
        return found







    def feed_channel_name(self):
        """
            feed
              lcn and channelName from  <div class="channelTitle">1 - TF1</div>

        :return:
        """

        # search for selected channel ( stb play )
        tag= self.find_selected_channel_title()

        if tag is None:
            # try another way ( stb blanche )
            tag= self.find_nameOfChannel()

        lcn= '0'
        name= '?'
        if tag:
            lcn , name = self._split_text(tag.text)
        self._data['lcn']= lcn.strip()
        self._data['channelName']=name.strip()




    def feed(self):
        """


        :return:
        """

        self.feed_channel_name()

        # for name in ['channelName', 'program', 'genre', 'nextProgram']:
        #     tag = self.get_child(name)
        #     self._data[name] = tag.text
        # for name in ['start','end']:
        #     tag= self.get_child(name)
        #     if not tag.text:
        #         # search for alternatives
        #         if name == 'start':
        #             tag= self.get_child(name,label='clock')
        #         elif name == 'end':
        #             tag= self.get_child(name,label='nextTime')
        #     self._data[name]= tag.text
        #
        #
        #
        # for name in ['pictoAudioDesc','pictoContentVersion','pictoDolby','pictoDts','pictoEar','favorite']:
        #     tag = self.get_child(name)
        #     #print tag
        #     if 'hidden' in tag['class']:
        #         self._data[name] = False
        #     else:
        #         self._data[name]= True
        #
        # for name in ['pictoDefinition','pictoRatio']:
        #     try:
        #         tag = self.get_child(name)
        #         # print tag
        #         if 'hidden' in tag['class']:
        #             self._data[name] = False
        #         else:
        #             self._data[name] = True
        #     except:
        #         self._data[name]=False
        #
        # for name in ['pictoCsa']:
        #     tag = self.get_child(name)
        #     #print tag
        #     self._data[name]= tag['class'][1]
        #
        # for name in ['TS_cursor']:
        #     tag = self.get_child(name)
        #     # class ="tsCursor" style="left: 66%;" >
        #     style= tag['style']
        #     if style:
        #         r= re.compile('(\d+)')
        #         match= r.search(style)
        #         if match:
        #             value= match.group(1)
        #             self._data[name] = int(value)
        #
        # tag= self.get_child('TS_start')
        # #<div class ="scrollerBar" style="left: 0%; width: 0%;" >
        # style = tag['style']
        # if style:
        #     r = re.compile('(\d+).*?(\d+)')
        #     match = r.search(style)
        #     if match:
        #         left = match.group(1)
        #         width= match.group(2)
        #         self._data['TS_start'] = int(left)
        #         self._data['TS_width'] = int(width)
        #
        # # split channel name
        # channel_parts= self._data['channelName'].split(u' ')
        # num= int(channel_parts[0])
        # text= channel_parts[-1]
        # self._data['lcn']= num
        # self._data['channelName']= text
        #
        # # convert start end to datetime
        # for tag in ['start','end']:
        #     text= self._data[tag]
        #     if ':' in text:
        #         hour,minutes= text.split(':')
        #     else:
        #         hour,minutes= u'0',u'0'
        #     #hour = element.text.split(":")[0]
        #     #minutes = element.text.split(":")[1]
        #     now = datetime.now()
        #     self._data[tag] = datetime(now.year, now.month, now.day, int(hour), int(minutes))
        #
        # if self._data['end'] < self._data['start']:
        #     self._data['end'] = self._data['end'] + timedelta(days=1)
        # self._data['length'] = self._data['end'] - self._data['start']

        return self._data
