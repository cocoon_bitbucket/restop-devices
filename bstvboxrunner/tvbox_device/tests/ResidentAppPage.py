# -*- coding: utf-8 -*-

from datetime import datetime, timedelta
import itertools
import logging
from random import randint
import re
import time
import subprocess
import os
import signal

from selenium import webdriver

from Config import ConfAR, Env, getDeskDataObject, Rpi, Wording
from Containers import RecordItem, Status, ProgramInfoItem, ListItem, VodItem, VodRating, VodRatingDetail, pvrItem, IconItem
from RemoteControl import RpiRemoteControl
from NewTvTesting.Containers import ChannelDetail


class WebKitPage(object):

    def alarm_handler(self, sig, stack):
        self.logger.warning(" Chromium Webdriver init failed ")
        ret = subprocess.check_output(["ps", "-e"], shell=True)
        regex = re.compile(r'([0-9]{1,6}).*[0-9]{2}:[0-9]{2}:[0-9]{2} chrom')
        match_result = regex.findall(ret)
        if match_result:
            self.logger.warning(" >> killing PIDs: " + str(match_result))
            self.logger.removeHandler(self.handler)
            self.handler.close()
            for pid in match_result:
                try:
                    ret = subprocess.check_output(["kill", "-9", pid])
                except:
                    pass
            self.driver = webdriver.Chrome()
            formatter = logging.Formatter(u'%(asctime)s :: %(levelname)s :: %(message)s')
            self.handler = logging.StreamHandler()
            self.handler.setFormatter(formatter)
            self.logger.addHandler(self.handler)

    def __init__(self,remote=None):
        """

        """
        self.logger = logging.getLogger('NewTvTesting.WebKitPage')

        if Env.BROWSER == "firefox":
            self.driver = webdriver.Firefox()
            self.driver.maximize_window()
        else:
            if os.name == 'posix':
                signal.signal(signal.SIGALRM, self.alarm_handler)
                signal.alarm(20)
                self.driver = webdriver.Chrome()
                time.sleep(4)
                self.driver.maximize_window()
                signal.alarm(0)
            else:
                self.driver = webdriver.Chrome()
                self.driver.maximize_window()

        self.remote= remote or RpiRemoteControl()

    def load_driver(self):
        """

        """
        self.driver.get(Rpi.DUMP)

    def close(self):

        self.driver.quit()

        # driver.quit checking
        if os.name == 'posix':
            time.sleep(2)
            ret = subprocess.check_output(["ps", "-e"], shell=True)
            regex = re.compile(r'([0-9]{1,6}).*[0-9]{2}:[0-9]{2}:[0-9]{2} chrom')
            match_result = regex.findall(ret)
            if match_result:
                self.logger.warning(" >> webdriver not closed")
                self.logger.warning(" >> killing PIDs: " + str(match_result))
                for pid in match_result:
                    ret = subprocess.check_output(["kill", "-9", pid])
            else:
                self.logger.info(" >> Webdriver successfully closed")

    def actionSelect(self,t,partial=False):

        self.logger.info("  >>   selecting text >" + t + "<")
        self._loadPageList()
        find = False
        index = 0


        if partial is True:
            while t not in self.activeItems[index].text and index < (len(self.activeItems)-1):
                index = index+1

            if t in self.activeItems[index].text:
                find = True
        else:
            while self.activeItems[index].text!=t and index < (len(self.activeItems)-1):
                index = index+1

            if self.activeItems[index].text==t:
                find = True

        if find:
            gap = index - self.activeHighlight

            if gap >= 0:
                keyGap = "KEY_DOWN"
            else:
                keyGap = "KEY_UP"

            self.remote.sendKeys([keyGap] * abs(gap))
            self.remote.sendKey("KEY_OK")

            return True

        else:
            if len(self.driver.find_elements_by_css_selector(".ToolBoxLive"))>0:
                maxItems = ConfAR.MAX_ITEMS_TOOLBOX
            else:
                maxItems = ConfAR.MAX_ITEMS

            if len(self.items) == maxItems:

                end = False
                endText=self.activeItems[-1].text

                while not end:

                    self.remote.sendKeys(["KEY_UP"] * int(self.activeHighlight + 1))

                    self._loadPageList()
                    find = False
                    index = 0

                    if partial is True:
                        while t not in self.activeItems[index].text and index < (len(self.activeItems)-1):
                            index=index+1
                            if self.activeItems[index].text == endText:
                                end = True

                        if t in self.activeItems[index].text:
                            find = True
                            end = True
                    else:
                        while self.activeItems[index].text!=t and index < (len(self.activeItems)-1):
                            index=index+1
                            if self.activeItems[index].text == endText:
                                end = True

                        if self.activeItems[index].text==t:
                            find = True
                            end = True

                if find:

                    gap = index - self.activeHighlight

                    if gap >= 0:
                        keyGap = "KEY_DOWN"
                    else:
                        keyGap = "KEY_UP"

                    self.remote.sendKeys([keyGap] * abs(gap))
                    self.remote.sendKey("KEY_OK")

                    return True

                else:
                    self._detect_timeout()
                    return False

            else:
                self._detect_timeout()
                return False

    def actionSelectInToolboxPolaris(self, category, choice1=None, choice2=None):
        self.logger.info("  >>   selecting category in ToolBox >" + category + "<")

        self.load_driver()
        try:
            elements = self.driver.find_elements_by_css_selector(".menu_polaris .item_menu_polaris")
            find = False
            findFocus = False
            if len(elements)>0:
                for index, element in enumerate(elements):
                    if element.get_attribute("class").find("selected")>-1:
                        indexFocus = index
                        findFocus = True
                    if element.text == category:
                        indexCategory = index
                        find = True
                    if find and findFocus:
                        break

                if find:
                    self.logger.debug(" >>> category OK >>> "+str(indexCategory))
                    if (indexFocus - indexCategory)>0:
                        for i in range(indexFocus - indexCategory):
                            self.remote.sendKey("KEY_LEFT")
                    elif (indexFocus - indexCategory)<0:
                        for i in range(indexCategory - indexFocus):
                            self.remote.sendKey("KEY_RIGHT")

                    self.driver.refresh()

                    panels = self.driver.find_elements_by_css_selector(" .panel_polaris")

                    panel=panels[indexCategory]

                    if choice1 is not None or choice2 is not None:

                        if choice1 is not None:
                            #détection liste ou radio buttons
                            elements = panel.find_elements_by_css_selector(".simplebutton")

                            if len(elements)>0:
                                find = False
                                findFocus = False
                                for index, element in enumerate(elements):

                                    if element.get_attribute("class").find("focused")>-1:
                                        indexFocus = index
                                        findFocus = True
                                    if element.text == choice1:
                                        indexChoice1 = index
                                        find = True
                                    if find and findFocus:
                                        break

                                if find:
                                    self.logger.debug(" >>> choice1 OK >>> "+str(indexChoice1))
                                    if (indexFocus - indexChoice1)<0:
                                        for i in range(indexChoice1 - indexFocus):
                                            self.remote.sendKey("KEY_DOWN")
                                    elif (indexFocus - indexChoice1)>0:
                                        for i in range(indexFocus - indexChoice1):
                                            self.remote.sendKey("KEY_UP")

                                    self.remote.sendKey("KEY_OK")
                                    time.sleep(20)
                                    return True
                                else:
                                    return False


                            else:
                                bloc = panel.find_elements_by_css_selector(".select_polaris")
                                elements = bloc[0].find_elements_by_css_selector(".radio")
                                if len(elements)>0:

                                    find = False
                                    findFocus = False
                                    for index, element in enumerate(elements):

                                        if element.get_attribute("class").find("selected")>-1:

                                            indexFocus = index
                                            findFocus = True
                                        if element.text.endswith(u"…"):
                                            text = element.text[:-3]
                                            if choice1.startswith(text):
                                                indexChoice1 = index
                                                find = True
                                        elif choice1 in element.text:
                                            indexChoice1 = index
                                            find = True
                                        if find and findFocus:
                                            break

                                    if find:
                                        self.logger.debug(" >>> choice1 OK >>> "+str(indexChoice1))
                                        if (indexFocus - indexChoice1)<0:
                                            for i in range(indexChoice1 - indexFocus):
                                                self.remote.sendKey("KEY_DOWN")
                                        elif (indexFocus - indexChoice1)>0:
                                            for i in range(indexFocus - indexChoice1):
                                                self.remote.sendKey("KEY_UP")

                                        self.remote.sendKey("KEY_OK")


                                    else:
                                        self._detect_timeout()
                                        return False
                                else:
                                    self._detect_timeout()
                                    return False

                        if choice2!= None:

                            self.driver.refresh()
                            panels = self.driver.find_elements_by_css_selector(" .panel_polaris")

                            panel=panels[indexCategory]


                            bloc = panel.find_elements_by_css_selector(".select_polaris")
                            elements = bloc[0].find_elements_by_css_selector(".radio")
                            ignoreOption = len(elements)
                            elements = panel.find_elements_by_css_selector(".radio")


                            if len(elements)>0:
                                find = False
                                findFocus = False
                                for index, element in enumerate(elements):

                                    if element.get_attribute("class").find("selected")>-1:
                                        indexFocus = index
                                        findFocus = True
                                    if element.text == choice2 and index>=ignoreOption:
                                        indexChoice = index
                                        find = True
                                    if find and findFocus:
                                        break

                                if find:
                                    self.logger.debug(" >>> choice2 OK >>> "+str(indexChoice))
                                    if (indexFocus - indexChoice)<0:
                                        for i in range(indexChoice - indexFocus):
                                            self.remote.sendKey("KEY_DOWN")
                                    elif (indexFocus - indexChoice)>0:
                                        for i in range(indexFocus - indexChoice):
                                            self.remote.sendKey("KEY_UP")

                                    self.remote.sendKey("KEY_OK")
                                    time.sleep(20)
                                    return True

                                else:
                                    self._detect_timeout()
                                    return False
                            else:
                                self._detect_timeout()
                                return False

                        time.sleep(20)
                        return True

                    else:
                        return True

                else:
                    self._detect_timeout()
                    return False

            else:
                self._detect_timeout()
                return False

        except Exception, e:
            self._detect_timeout()
            self.logger.debug(" >>> framework debug >>> "+str(e))
            return False

    def actionSelectInLiveBanner(self, lcn, up = True, maxValue = 10):
        self.logger.info("  >>   select channel " + str(lcn))

        self._loadLiveBanner()

        m = 0
        if (up):
            key="KEY_UP"
        else:
            key="KEY_DOWN"
        while (self.programInfo.lcn != lcn and m < maxValue):
            self.remote.sendKey(key)

            time.sleep(0.2)
            self._loadLiveBanner()
            m = m+1
        if (self.programInfo.lcn == lcn):
            self.remote.sendKey("KEY_OK")

            time.sleep(3)
            return True
        else:
            self.remote.sendKey("KEY_BACK")
            self._detect_timeout()
            return False

    def actionSelectInLiveBannerPolaris(self, lcn, up = True, maxValue = 10):
        self.logger.info("  >>   select channel " + str(lcn))

        self._loadLiveBannerPolaris()

        m = 0
        if (up):
            key="KEY_UP"
        else:
            key="KEY_DOWN"
        while (self.programInfo.lcn != lcn and m < maxValue):
            self.remote.sendKey(key)

            time.sleep(0.2)
            self._loadLiveBannerPolaris()
            m = m+1
        if (self.programInfo.lcn == lcn):
            self.remote.sendKey("KEY_OK")

            time.sleep(3)
            return True
        else:
            self.remote.sendKey("KEY_BACK")
            self._detect_timeout()
            return False

    def actionSelectInFipPolaris(self,text,level=1):
        self.logger.info("  >>   selecting >" + text + "<")
        self.load_driver()
        try:
            #EPG or PVR FIP
            if len(self.driver.find_elements_by_css_selector(".fiptop .fiptopright"))>0:
                elements = self.driver.find_element_by_css_selector(".fiptop .fiptopright")
                if len(elements.find_elements_by_css_selector(".fipActions"))>0:
                    level1 = elements.find_elements_by_css_selector(".fipActions .fipButton")
                else:
                    level1=elements.find_elements_by_css_selector(".fipButtons .fipButton")

                level2=elements.find_elements_by_css_selector(".fipDescription .fipButton")
            # VOD VPS
            else:
                elements = self.driver.find_element_by_css_selector(".svftop .svftopright")
                level1 = elements.find_elements_by_css_selector(".pol2Actions .pol2Button")
                level2 = elements.find_elements_by_css_selector(".pol2Description .pol2Button")
                trailer = self.driver.find_element_by_css_selector(".svftop .svftopleft .pol2Button .playBtn")
            find = False
            findFocus = False


            if text == Wording.W["vodTrailer"]:
                if trailer.get_attribute("class").find("invisible")<0 or trailer.get_attribute('textContent')== Wording.W["vodTrailer"]:
                    find=True
                    indexText=-1
                    textlevel=1

            for i,e in enumerate(level1):
                if e.get_attribute("class").find("highlight")>-1:
                    findFocus=True
                    focuslevel=1
                    indexFocus=i
                if level == 1:
                    if text == Wording.W["vodAddToFavorites"] or text == Wording.W["vodRemoveFromFavorites"] or text == Wording.W["vodRate"]:
                        t = e.get_attribute('textContent')
                    else:
                        t= e.text.encode("utf-8")
                    if text in t:
                        find=True
                        indexText=i
                        textlevel=1

            for i,e in enumerate(level2):
                if e.get_attribute("class").find("highlight")>-1:
                    findFocus=True
                    focuslevel=2
                    indexFocus=i
                if level == 2:
                    if text in e.text.encode("utf-8"):
                        find=True
                        indexText=i
                        textlevel=2

            if findFocus and find:
                while focuslevel != textlevel:
                    if focuslevel>textlevel:
                        self.remote.sendKey("KEY_UP")
                        focuslevel-=1
                        indexFocus=0
                    if focuslevel<textlevel:
                        self.remote.sendKey("KEY_DOWN")
                        focuslevel+=1
                        indexFocus=0

                while indexFocus != indexText:
                    if indexFocus>indexText:
                        self.remote.sendKey("KEY_LEFT")
                        indexFocus-=1

                    if indexFocus<indexText:
                        self.remote.sendKey("KEY_RIGHT")
                        indexFocus+=1
                self.remote.sendKey("KEY_OK")
                return True
            else:
                self._detect_timeout()
                return False
        except:
            self._detect_timeout()
            return False


    def actionDeskMoveTo(self, text):
        '''
        Move cursor to a destination block
        which contain text passed in parameter
        @return: boolean
        '''
        try:
            if Env.STB == "NEWBOX":
                currentPosition, isHeader, currentTile = ((0, 0, 1, 2), False, 0)
            else:
                currentPosition, isHeader, currentTile = self._getBlockPositionFromDeskFocus()
            currentX = currentPosition[0]

            self.logger.info("  >>   selecting text >" + text + "<")
            desk = getDeskDataObject()
            try:
                header = desk.getHeader()
                if header.button_name == text and not isHeader:
                    self._moveToTile(currentTile, currentX, 0)
                    self.remote.sendKey("KEY_UP")
                    find = True
                elif header.button_name == text and isHeader:
                    find = True
                else:
                    tiles = desk.getTiles()
                    for i, tile in enumerate(tiles):
                        for block in tile.blocks:
                            if block.label == text:
                                self._moveToTile(currentTile, currentX, i)
                                self.remote.sendKeys(["KEY_RIGHT"] * int(block.x))
                                if int(block.y) == 0 and int(block.h) == 1:
                                    self.remote.sendKey("KEY_DOWN")
                                find = True
                                break

                return find

            except:
                return False

        except:
            return False



    def actionDeskSelect(self,text):
        '''
        Select a block in Desk scene
        '''
        select = False

        if self.actionDeskMoveTo(text):
            self.remote.sendKey("KEY_OK")
            select = True

        return select



    def actionInstantRecord(self, length = 5):
        self.logger.info("  >>   instant recording")

        self.load_driver()

        try:
            #element = self.driver.find_element_by_css_selector(".dialog.irDialog")
            currentDate = datetime.now()

            endDate = currentDate + timedelta(minutes = length)
            endHourTxt = str(endDate.hour)
            endMinTxt = str(endDate.minute)
            self.remote.sendKey("KEY_LEFT")

            time.sleep(5)

            if len(endHourTxt) == 2:
                self.remote.sendKeys(["KEY_"+endHourTxt[0],"KEY_"+endHourTxt[1]])
            else:
                self.remote.sendKeys(["KEY_0","KEY_"+endHourTxt[0]])


            if len(endMinTxt) == 2:
                self.remote.sendKeys(["KEY_"+endMinTxt[0],"KEY_"+endMinTxt[1]])
            else:
                self.remote.sendKeys(["KEY_0","KEY_"+endHourTxt[0]])

            time.sleep(5)

            self.remote.sendKey("KEY_OK")

            return True

        except Exception, e:
            self._detect_timeout()
            self.logger.debug(" >>> framework debug >>> "+str(e))
            return False



    def actionScheduleRecord(self, package, lcn, date, length, recurrence = 0, idWord = None):
        self.logger.info("  >>   scheduled recording")

        self.load_driver()

        try:
            element = self.driver.find_element_by_css_selector(".dialog.pvrScheduling")


            #contournement comportement MIB4 (focus mal placé)
            inputs = element.find_elements_by_css_selector('.input')
            for i, el in enumerate(inputs):
                if el.get_attribute("class").find("focused")>-1:
                    break
            keys = ["KEY_UP"] * i
            if len(keys)>0:
                self.remote.sendKeys(keys)
                #end contournement comportement MIB4

            if idWord == None:
                idWord = str(randint(1000,9999)).replace("0","1")
            self.remote.sendWord(idWord)

            self.remote.sendKey("KEY_DOWN")
            self.remote.sendKey("KEY_OK")
            self.remote.sendKeys(["KEY_DOWN"] * int(package))
            self.remote.sendKey("KEY_OK")

            self.remote.sendKey("KEY_DOWN")
            self.remote.sendKey("KEY_OK")
            if Env.STB == 'STBU':
                #self.remote.sendKeys(["KEY_DOWN"] * int(lcn-1))
                #self.remote.sendKey("KEY_OK")
                i = 0
                focussedEl = self.getLabelFromListFocus_manualpvr()
                # Pour bien cibler l'élément en highlight de la popup et non de la liste
                while (not str(lcn) in focussedEl.text) and i < lcn + 1:
                    self.remote.sendKey("KEY_DOWN")
                    focussedEl = self.getLabelFromListFocus_manualpvr()
                    i = i + 1
                if str(lcn) in focussedEl.text:
                    self.remote.sendKey("KEY_OK")
                else:
                    self.logger.info("lcn " + str(lcn) + " not found")
            else:
                self.remote.zap(lcn)
                self.remote.sendKey("KEY_OK")

            time.sleep(5)
            self.remote.sendKey("KEY_DOWN")

            if len(str(date.day)) == 2:
                self.remote.sendKeys(["KEY_"+str(date.day)[0],"KEY_"+str(date.day)[1]])
            else:
                self.remote.sendKeys(["KEY_0","KEY_"+str(date.day)[0]])

            if len(str(date.month)) == 2:
                self.remote.sendKeys(["KEY_"+str(date.month)[0],"KEY_"+str(date.month)[1]])
            else:
                self.remote.sendKeys(["KEY_0","KEY_"+str(date.month)[0]])

            self.remote.sendKeys(["KEY_"+str(date.year)[2],"KEY_"+str(date.year)[3]])

            #contournement comportement MIB4 (focus mal placé)
            #self.driver.refresh()
            #elements = self.driver.find_elements_by_css_selector(".dialog.pvrScheduling .input.startTB")
            #if elements[0].get_attribute("class").find("focused")<0:
            #    self.remote.sendKey("KEY_LEFT")
            #end contournement comportement MIB4

            if len(str(date.hour)) == 2:
                self.remote.sendKeys(["KEY_"+str(date.hour)[0],"KEY_"+str(date.hour)[1]])
            else:
                self.remote.sendKeys(["KEY_0","KEY_"+str(date.hour)[0]])

            if len(str(date.minute)) == 2:
                self.remote.sendKeys(["KEY_"+str(date.minute)[0],"KEY_"+str(date.minute)[1]])
            else:
                self.remote.sendKeys(["KEY_0","KEY_"+str(date.minute)[0]])

            endDate=date + timedelta(minutes = length)

            if len(str(endDate.hour)) == 2:
                self.remote.sendKeys(["KEY_"+str(endDate.hour)[0],"KEY_"+str(endDate.hour)[1]])
            else:
                self.remote.sendKeys(["KEY_0","KEY_"+str(endDate.hour)[0]])

            if len(str(endDate.minute)) == 2:
                self.remote.sendKeys(["KEY_"+str(endDate.minute)[0],"KEY_"+str(endDate.minute)[1]])
            else:
                self.remote.sendKeys(["KEY_0","KEY_"+str(endDate.minute)[0]])

            self.remote.sendKey("KEY_OK")
            self.remote.sendKeys(["KEY_DOWN"] * int(recurrence))
            self.remote.sendKey("KEY_OK")

            self.remote.sendKey("KEY_DOWN")
            self.remote.sendKey("KEY_OK")

            '''' Analyse pop-up'''
            time.sleep(8)
            self.driver.refresh()
            try:

                if self.findInDialogBox(Wording.W['pvrScheduleOk']) is True:
                    self.remote.sendKey("KEY_OK")
                    return idWord
                else:
                    self.remote.sendKey("KEY_OK")
                    self._detect_timeout()
                    return False

            except Exception, e:
                self._detect_timeout()
                self.logger.debug(" >>> framework debug >>> "+str(e))
                self.remote.sendKeys(["KEY_BACK", "KEY_BACK","KEY_CHANNELUP"])
                return False

        except Exception, e:
            self._detect_timeout()
            self.logger.debug(" >>> framework debug >>> "+str(e))
            self.remote.sendKeys(["KEY_BACK", "KEY_BACK","KEY_CHANNELUP"])
            return False

    def actionSelectInVodMenuPolaris(self, category, name):

        '''
        possible values for category :
            TOP_BANNER
            None
            *name_of_a_category*
        possible values for name :
            *name_of_a_carousel_item*
        '''

        self.logger.info("  >>   selecting "+ category +" "+ name)
        self.remote.setCss(u'nuxp-no_css')
        self.driver.refresh()

        try:

            if category == u'TOP_BANNER':
                el=self.driver.find_element_by_css_selector(".VodHeaderSearchElt")
                if el.text.find(name) !=-1:
                    self.remote.sendKeys(["KEY_UP","KEY_OK"])
                    self.remote.setCss(u'nuxp-polaris')
                    self.driver.refresh()
                    return True
                elements = self.driver.find_elements_by_css_selector(".carouselItem .edito")
                find = False
                for index, element in enumerate(elements):
                    if element.text.find(name) != -1:
                        find = True
                        break
                if find:
                    for i in range(index):
                        self.remote.sendKey('KEY_RIGHT')
                    self.remote.sendKey('KEY_OK')
                    self.remote.setCss(u'nuxp-polaris')
                    self.driver.refresh()
                    return True
                else:
                    self.remote.setCss(u'nuxp-polaris')
                    self.driver.refresh()
                    return False

            elif category == 'None':
                #self.remote.setCss(u'nuxp-polaris')
                self.driver.refresh()

                element = self.driver.find_element_by_css_selector(".highlight")
                if element.text.find(name) != -1:
                    self.remote.sendKey('KEY_OK')
                    self.remote.setCss(u'nuxp-polaris')
                    self.driver.refresh()
                    return True
                else:
                    startLabel = element.text
                    self.remote.sendKey('KEY_RIGHT')
                    self.driver.refresh()
                    nb_items = 0
                    while self.driver.find_element_by_css_selector(".highlight").text != startLabel and nb_items < 100:
                        if self.driver.find_element_by_css_selector(".highlight").text.find(name) != -1:
                            self.remote.sendKey('KEY_OK')
                            self.remote.setCss(u'nuxp-polaris')
                            self.driver.refresh()
                            return True
                        else:
                            self.remote.sendKey('KEY_RIGHT')
                            nb_items = nb_items + 1
                            self.driver.refresh()
                    self._detect_timeout()
                    self.remote.setCss(u'nuxp-polaris')
                    self.driver.refresh()
                    return False
            else:
                elements = self.driver.find_elements_by_css_selector(".VodHorizontalWidget .carouselTitle .txt_title2")
                find = False
                for index, element in enumerate(elements):
                    if element.text.find(category) != -1:
                        find = True
                        break
                if find:
                    if len(self.driver.find_elements_by_css_selector(".headerContainer .herozone"))>0:
                        self.remote.sendKey('KEY_DOWN')
                    for i in range(index):
                        self.remote.sendKey('KEY_DOWN')
                else:
                    self.remote.setCss(u'nuxp-polaris')
                    self.driver.refresh()
                    return False

                self.driver.refresh()
                time.sleep(2)
                element = self.driver.find_element_by_css_selector(".highlight")
                if element.text.find(name) != -1:
                    self.remote.sendKey('KEY_OK')
                    self.remote.setCss(u'nuxp-polaris')
                    self.driver.refresh()
                    return True
                else:
                    startLabel = element.text
                    self.remote.sendKey('KEY_RIGHT')
                    self.driver.refresh()
                    nb_items = 0
                    while self.driver.find_element_by_css_selector(".highlight").text != startLabel and nb_items < 100:
                        if self.driver.find_element_by_css_selector(".highlight").text.find(name) != -1:
                            self.remote.sendKey('KEY_OK')
                            self.remote.setCss(u'nuxp-polaris')
                            self.driver.refresh()
                            return True
                        else:
                            self.remote.sendKey('KEY_RIGHT')
                            nb_items = nb_items + 1
                            self.driver.refresh()
                    self._detect_timeout()
                    self.remote.setCss(u'nuxp-polaris')
                    self.driver.refresh()
                    return False

        except Exception, e:
            self._detect_timeout()
            self.remote.setCss(u'nuxp-polaris')
            self.driver.refresh()
            self.logger.debug(" >>> framework debug >>> "+str(e))
            return False

    def getLabelFromDeskFocus(self):
        '''
        Return the label from focus element:

        @return: string
        '''
        try:
            informationBlock = self._getBlockFromDeskFocus()
            block = informationBlock[0]
            isHeader = informationBlock[1]
            labelFocusedElement = None
            if block is not None:
                if isHeader:
                    labelFocusedElement = unicode(block.find_element_by_css_selector(".text.bottom").text)
                elif len(block.find_elements_by_css_selector(".text.top")) > 0:
                    labelFocusedElement = unicode(block.find_element_by_css_selector(".text.top").text)
                else:
                    labelFocusedElement = unicode(block.text)

                self.logger.info("  >>   focused Element >" + labelFocusedElement + "<")

            return labelFocusedElement
        except:
            return None

    def getList(self):
        if self._loadPageList():
            return self.items
        else:
            return None

    def getLabelFromListFocus(self):
        if self._loadPageList():
            return self.items[self.highlight]
        else:
            return None

    def getLabelFromListFocus_manualpvr(self):
        if self._loadPageList_manualpvr():
            return self.items[self.highlight]
        else:
            return None

    def getInfoFromPvrBanner(self):
        if self._loadPvrBanner():
            return self.programInfo
        else:
            return None

    def getInfoFromLiveBanner(self):
        if self._loadLiveBanner():
            return self.programInfo
        else:
            return None

    def getInfoFromLiveBannerPolaris(self):
        if self._loadLiveBannerPolaris():
            return self.programInfo
        else:
            return None

    def getInfoFromVodBannerPolaris(self):
        if self._loadVodBannerPolaris():
            return self.vod
        else:
            return None

    def getInfoFromVodBanner(self):
        if self._loadVodBanner():
            return self.vod
        else:
            return None

    def getInfoFromVodBaner(self):
        self.load_driver()
        try:

            pass

        except:
            self._detect_timeout()
            return None

    def getInfoFromVodTrailerBanner(self):
        if self._loadVodTrailerBanner():
            return self.vodTrailerBanner
        else:
            return None

    def getInfoFromEpgFocus(self):
        if self._loadEpg():
            return self.programInfo
        else:
            return None

    def getInfoFromMosaicFocus(self):
        if self._loadMosaic():
            return self.programInfo
        else:
            return None

    def getInfoFromRecordFocus(self):
        if self._loadRecord():
            return RecordItem(self.focusTitle, self.focusDate, self.focusLength, None, None, None, self.onGoing, self.status)
        else:
            return None

    def getInfoFromRecordPage(self):
        if self._loadRecordFromRecPage():
            return RecordItem(self.focusTitle, self.focusDate, None, self.beginRecTime, self.endRecTime, None, None, None)
        else:
            return None

    def getRecordInfoFromFip(self):
        if self._loadFipInfoPolaris():
            return self.recordItem
        else:
            return None

    def getEpgInfoFromFip(self):
        if self._loadFipInfoPolaris(True):
            return self.programInfo
        else:
            return None

    def getInfoFromVodPagePolaris(self):
        if self._loadVodPagePolaris():
            return self.vod
        else:
            return None

    def getInfoFromVodPage(self):
        if self._loadVodPage():
            return self.vod
        else:
            return None

    def getInfoFromVodPageMIB4(self):
        if self._loadVodPageMIB4():
            return self.vod
        else:
            return None

    def getTitleFromVodFocus(self):
        self.load_driver()
        try:
            element = self.driver.find_element_by_css_selector(".mosaicCover .tooltipTitle")
            return element.text
        except Exception, e:
            self._detect_timeout()
            self.logger.debug(" >>> framework debug >>> "+str(e))
            return False

    def getClock(self):
        '''
            Get the clock in header
        '''
        self.load_driver()
        try:
            element = self.driver.find_element_by_css_selector(".clock")
            return element.text
        except Exception, e:
            self._detect_timeout()
            self.logger.debug(" >>> framework debug >>> "+str(e))
            return False

    def getInfoFromToolboxPolaris(self, category, parameter=1,focus=False):
        self.logger.info("  >>   selecting category in ToolBox >" + category + "<")

        self.load_driver()
        try:
            elements = self.driver.find_elements_by_css_selector(".menu_polaris .item_menu_polaris")
            find = False
            findFocus = False
            if len(elements)>0:
                for index, element in enumerate(elements):
                    if element.get_attribute("class").find("selected")>-1:
                        indexFocus = index
                        findFocus = True
                    if element.text == category:
                        indexCategory = index
                        find = True
                    if find and findFocus:
                        break

                if find:
                    self.logger.debug(" >>> category OK >>> "+str(indexCategory))
                    if (indexFocus - indexCategory)>0:
                        for i in range(indexFocus - indexCategory):
                            self.remote.sendKey("KEY_LEFT")
                    elif (indexFocus - indexCategory)<0:
                        for i in range(indexCategory - indexFocus):
                            self.remote.sendKey("KEY_RIGHT")


                    self.driver.refresh()
                    panels = self.driver.find_elements_by_css_selector(" .panel_polaris")
                    panel=panels[indexCategory]


                    if len(panel.find_elements_by_css_selector(".simplebutton"))>0 and parameter >0:
                        elements = panel.find_elements_by_css_selector(".simplebutton")
                        for element in elements:
                            if element.get_attribute("class").find("focused")>-1:
                                return element.text


                        return panel.text


                    elif len(panel.find_elements_by_css_selector(".radio"))>0 and parameter >0:
                        if focus:
                            return panel.find_element_by_css_selector(".radio.selected").text
                        else:
                            elements = panel.find_elements_by_css_selector(".radio")
                            p = 0
                            for element in elements:
                                if len(element.find_elements_by_css_selector(".checked"))>0:
                                    p = p+1
                                    if parameter == p:
                                        return element.text

                            return panel.text



                    else:
                        return panel.text



                else:
                    self._detect_timeout()
                    return False
            else:
                self._detect_timeout()
                return False
        except Exception, e:
            self._detect_timeout()
            self.logger.debug(" >>> framework debug >>> "+str(e))
            return False



    def getStatus(self):

        html = self.remote.getStbStatus()

        noRightPanel = None
        scene = None
        dialog = None
        frontPanel = None
        miniLive = None
        mosaicAdBanner = None
        redCross = None

        if html.find("READY") > -1 or html.find("Up") > -1:
            html="READY"
            self.load_driver()

            if Env.STB == "NEWBOX":
                try:
                    element = self.driver.find_element_by_id("fpntxt")
                    frontPanel = element.text

                    if element.text == u"TV d'Orange":
                        scene = "DESK"
                    elif len(self.driver.find_elements_by_css_selector(".ToolBoxLive"))>0:
                        scene = "TOOLBOX"
                    elif len(self.driver.find_elements_by_css_selector(".virtualZappingBanner"))>0:
                        scene = "ZAPPING_BANNER"
                    else:
                        try:
                            nb = element.text.split(" ")[0]
                            if int(nb)>=0:
                                scene = "LIVE"

                        except:
                            scene = "UNKNOWN"

                except Exception, e:
                    self._detect_timeout()
                    self.logger.debug(" >>> framework debug >>> "+str(e))
                    scene = "UNKNOWN"

            else:
                try:
                    if Env.STB=="NEWBOXP":
                        element = self.driver.find_element_by_id("fpntxt")
                        frontPanel = element.text
                    if len(self.driver.find_elements_by_css_selector(".ToolBoxLive"))>0 or len(self.driver.find_elements_by_css_selector(".toolbox"))>0:
                        scene = "TOOLBOX"
                    elif len(self.driver.find_elements_by_css_selector(".virtualZappingBanner"))>0:
                        scene = "ZAPPING_BANNER"
                    elif len(self.driver.find_elements_by_css_selector(".live.scene"))>0:
                        scene = "LIVE"
                    elif len(self.driver.find_elements_by_css_selector(".whiteBg"))>0 or len(self.driver.find_elements_by_css_selector(".bg_whiteOff"))>0 or len(self.driver.find_elements_by_css_selector(".r4.bg_whiteOff"))>0:
                        scene = "PORTAL"
                    elif len(self.driver.find_elements_by_css_selector(".desk.scene"))>0:
                        scene = "DESK"
                    elif len(self.driver.find_elements_by_css_selector(".scene.mosaicView"))>0:
                        scene = "MOSAIC"
                    else:
                        scene = "UNKNOWN"

                except:
                    self._detect_timeout()
                    scene = "UNKNOWN"

            try:
                element = self.driver.find_element_by_css_selector(".scene.noRightView")
                if element.get_attribute("aria-hidden").find("false")>-1:
                    noRightPanel = True
            except:
                noRightPanel = False

            try:
                element = self.driver.find_element_by_css_selector(".dialog")
                dialog = True
            except:
                dialog = False

            try:
                # if len(self.driver.find_elements_by_css_selector(".virtualZappingBanner"))>0 or len(self.driver.find_elements_by_css_selector(".live.scene"))>0 or len(self.driver.find_elements_by_css_selector(".desk.scene"))>0:
                #     miniLive= False
                if len(self.driver.find_elements_by_class_name("PIP"))>0:
                    if self.driver.find_element_by_class_name("PIP").get_attribute("style").find("none")>-1:
                        miniLive = False
                    else:
                        miniLive = True
                else:
                    miniLive = False
            except:
                miniLive= False

            try:
                if len(self.driver.find_elements_by_css_selector(".trick.hidden"))>0:
                    redCross= False
                else:
                    redCross = True
            except:
                redCross= False

            try:
                element = self.driver.find_element_by_css_selector(".mosaicAdBanner")
                mosaicAdBanner = True
            except:
                mosaicAdBanner = False

        return Status(str(html), noRightPanel, scene, dialog, str(frontPanel), miniLive, redCross, mosaicAdBanner)

    def getManualRecordingSchedulerStatus(self):
        title = True
        package = True
        lcn = True
        date = True
        start = True
        end = True
        periodicity = True

        self.load_driver()
        try:
            element = self.driver.find_elements_by_css_selector(".pvrScheduling .input")
            if element[0].get_attribute("class").find("disabled")>-1:
                title = False
            if element[1].get_attribute("class").find("disabled")>-1:
                package = False
            if element[2].get_attribute("class").find("disabled")>-1:
                lcn = False
            if element[3].get_attribute("class").find("disabled")>-1:
                date = False
            if element[4].get_attribute("class").find("disabled")>-1:
                start = False
            if element[5].get_attribute("class").find("disabled")>-1:
                end = False
            if element[6].get_attribute("class").find("disabled")>-1:
                periodicity = False
            return dict([('title',title),('package',package),('lcn',lcn),('date',date),('start',start),('end',end),('periodicity',periodicity)])

        except:
            self._detect_timeout()
            return False

    def getAmountFromPrepaidAccount(self):
        self.load_driver()
        try:
            element = self.driver.find_element_by_css_selector(".myaccount .content .value")
            search_result = re.search(r'\d*', element.text)
            value = int(search_result.group())
            #value = int(element.text.strip(u"€. "))
            return value
        except Exception, e:
            self._detect_timeout()
            self.logger.debug(" >>> framework debug >>> " + str(e))
            return False

    def getTextFromDialogBox(self):
        self.load_driver()
        find = False
        try:
            if len(self.driver.find_elements_by_css_selector(".dialog"))>0:
                element = self.driver.find_element_by_css_selector(".box")
                find = element.find_element_by_css_selector(".title").text
                find =find+'\n'+ element.find_element_by_css_selector(".content").text
            return find
        except:
            self._detect_timeout()
            return find

    def findInDialogBox(self, txt, regex = False):
        self.load_driver()
        find = False
        try:
            if len(self.driver.find_elements_by_css_selector(".dialog"))>0:
                element = self.driver.find_element_by_css_selector(".box")
                if regex:
                    find = re.search(re.compile(txt), element.text) is not None
                else:
                    find = element.text.find(txt) > -1

            return find
        except:
            self._detect_timeout()
            return find

    def getRemainingAttemptInDialogBox(self):
        self.load_driver()

        remaining_attempts = None
        try:
            str_attempt = self.driver.find_element_by_css_selector(".remainingAttempts")
            if str_attempt is not None:
                list_r_attempts = re.findall('\d+', str_attempt.text)
                if len(list_r_attempts) > 0:
                    remaining_attempts = int(list_r_attempts[0])

        except:
            remaining_attempts = None

        return remaining_attempts

    def findInLongList(self, t, regex=False, last_item=None):
        find = False
        looped = False

        if self._loadPageList():
            last_index = len(self.items) - 1
            for item in self.items:
                if regex:
                    find = re.search(re.compile(t), item.text) is not None
                    if find:
                        break
                if item.text == t:
                    find = True
                    break
                elif last_item is not None and last_item.text == item.text:
                    looped = True

            if len(self.driver.find_elements_by_css_selector(".ToolBoxLive")) > 0:
                max_item = ConfAR.MAX_ITEMS_TOOLBOX
            else:
                max_item = ConfAR.MAX_ITEMS

            if find is False and looped is False and len(self.items) >= max_item:
                if last_item is None:
                    ''' Check for the first time by bottom of the list '''
                    last_item = self.items[last_index]

                    ''' Hotfix for (very) small screen '''
                    # last item is screwed... But exists !!!
                    minus = 1
                    while last_item.text == "":
                        last_item = self.items[last_index - minus]
                        minus = minus + 1
                    ''' Go to bottom of the list '''
                    self.remote.sendKeys(["KEY_UP"] * (self.activeHighlight + 1))
                    ''' Check for the current list '''
                    if self._loadPageList():
                        for item in self.items:
                            if regex:
                                find = re.search(re.compile(t), item.text) is not None
                                if find:
                                    break
                            if item.text == t:
                                find = True
                                break
                            elif (last_item is not None and
                                  last_item.text == item.text):
                                looped = True

                if find is False and looped is False:
                    self.remote.sendKeys(["KEY_UP"] * last_index)
                    ''' Call again this function '''
                    find = self.findInLongList(t, regex, last_item)

        else:
            self._detect_timeout()

        ''' Return if element is find in the list (True) or not (False) '''
        return find

    def findInList(self, t, regex=False):

        find = False

        if self._loadPageList():
            for item in self.items:
                if item.text == t:
                    return True

            if len(self.driver.find_elements_by_css_selector(".ToolBoxLive"))>0:
                maxItem = ConfAR.MAX_ITEMS_TOOLBOX
            else:
                maxItem = ConfAR.MAX_ITEMS

            if len(self.items) == maxItem:
                activeItems = []
                index = 0
                activeHighlight = 0

                for firstIndex, el in enumerate(self.items):
                    if el.active:
                        activeItems.append(el)
                        if firstIndex == self.highlight:
                            activeHighlight = index
                        index = index +1

                endText = activeItems[-1].text

                for _ in itertools.repeat(None, activeHighlight + 1):
                    self.driver.get(Rpi.KEY_UP)
                    time.sleep(0.2)

                end = False
                while end == False and find == False:
                    self._loadPageList()

                    for item in self.items:
                        if regex:
                            find = re.search(re.compile(t), item.text) is not None
                        else:
                            find = item.text == t
                        if item.text == endText:
                            end = True
                        if find:
                            break
                    if find == False:
                        self.driver.get(Rpi.KEY_UP)
                        time.sleep(0.2)

                return find
            else:
                self._detect_timeout()
                return find

        else:
            self._detect_timeout()
            return find


    def findInPage(self, txt, regex = False):

        self.load_driver()

        find = False

        try:
            if len(self.driver.find_elements_by_tag_name('body')) > 0:
                element = self.driver.find_element_by_tag_name('body')
                if regex:
                    find = re.search(re.compile(txt), element.text) is not None
                else:
                    find = element.text.find(txt) > -1

            return find
        except:
            self._detect_timeout()
            return find



    def findProgressBar(self):

        self.load_driver()

        find = False

        try:
            if len(self.driver.find_elements_by_css_selector(".progressScan"))>0:
                boxEl = self.driver.find_element_by_css_selector(".progressScan")
                if boxEl.find_element_by_css_selector(".progressBar") and boxEl.find_element_by_css_selector(".percentText"):
                    find = True

            return find
        except:
            return find


    def getBreadcrumb(self):
        self.load_driver()
        time.sleep(1)

        breadcrumb = None
        if len(self.driver.find_elements_by_css_selector("#breadCrumb")) > 0:
            breadcrumb = {}
            breadcrumb_html = self.driver.find_element_by_css_selector("#breadCrumb")
            lis = breadcrumb_html.find_elements_by_tag_name('li')

            for li in lis:
                classes = li.get_attribute('class').split()
                if 'last' in classes:
                    breadcrumb['last'] = "{0}".format(li.text)
                elif 'first' in classes:
                    breadcrumb['first'] = "{0}".format(li.text)

        return breadcrumb

    def findPvrAdvancedConflictPicto(self):

        self.load_driver()

        picto = False

        try:
            if len(self.driver.find_elements_by_css_selector(".box .content .conflict"))>0:
                element = self.driver.find_element_by_css_selector(".box .content .conflict")
                if element.get_attribute("class").find("hidden") == -1:
                    picto = True
            return picto
        except:
            return picto



    def resolvePvrAdvancedConflict(self):
        self.logger.info("  >>   resolving advanced recording conflict")

        self.load_driver()

        try:
            if self.findPvrAdvancedConflictPicto():

                if len(self.driver.find_elements_by_css_selector(".firstEndTB .textContainer .timeBox"))>0:
                    # Ligne Inutile
                    time1 = self.driver.find_element_by_css_selector(".firstEndTB .textContainer .timeBox").text
                    date1End = timedelta(hours = int(time1.split(":")[0]), minutes = int(time1.split(":")[1]))

                if len(self.driver.find_elements_by_css_selector(".secondStartTB .textContainer .timeBox"))>0:
                    # Ligne Inutile
                    time2 = self.driver.find_element_by_css_selector(".secondStartTB .textContainer .timeBox").text
                    date2Begin = timedelta(hours = int(time2.split(":")[0]), minutes = int(time2.split(":")[1]))

                if date1End > date2Begin:
                    # Ligne Inutile
                    newTime1End = date2Begin - timedelta(minutes = 1)
                    now=datetime.now()
                    newDate1End = datetime(now.year, now.month, now.day, int(str(newTime1End).split(":")[0]), int(str(newTime1End).split(":")[1]))

                if len(str(newDate1End.hour)) == 2:
                    self.remote.sendKeys(["KEY_"+str(newDate1End.hour)[0],"KEY_"+str(newDate1End.hour)[1]])
                else:
                    self.remote.sendKeys(["KEY_0","KEY_"+str(newDate1End.hour)[0]])

                if len(str(newDate1End.minute)) == 2:
                    self.remote.sendKeys(["KEY_"+str(newDate1End.minute)[0],"KEY_"+str(newDate1End.minute)[1]])
                else:
                    self.remote.sendKeys(["KEY_0","KEY_"+str(newDate1End.minute)[0]])


                if self.findPvrAdvancedConflictPicto() == False:
                    self.remote.sendKeys(["KEY_DOWN", "KEY_OK"])

                return True

        except Exception, e:
            self._detect_timeout()
            self.logger.debug(" >>> framework debug >>> "+str(e))
            self.remote.sendKeys(["KEY_BACK", "KEY_BACK", "KEY_CHANNELUP"])
            return False


    def getIcons(self):

        self.load_driver()
        try:
            element = self.driver.find_element_by_css_selector("#breadCrumb .icons")
            fiberIcon = False
            optinIcon = False
            parentalCtrlIcon = False
            accessibilityIcon = False

            if element.find_element_by_class_name("fiberIcon").get_attribute("style").find("none")>-1:
                fiberIcon = False
            else:
                fiberIcon = True
            if element.find_element_by_class_name("optinIcon").get_attribute("style").find("none")>-1:
                optinIcon = False
            else:
                optinIcon = True
            if element.find_element_by_class_name("parentalCtrlIcon").get_attribute("style").find("none")>-1:
                parentalCtrlIcon = False
            else:
                parentalCtrlIcon = True
            if element.find_element_by_class_name("accessibilityIcon").get_attribute("style").find("none")>-1:
                accessibilityIcon = False
            else:
                accessibilityIcon = True


            self.icons = IconItem(fiberIcon,optinIcon,parentalCtrlIcon,accessibilityIcon)
            return self.icons
        except:
            self._detect_timeout()
            return None

    def _getBlockFromDeskFocus(self):
        '''
        Return the current selected block in focus :
            (
                block - WebDriver,
                is header - boolean
                tile number - int
            ) - tuple
        '''

        self.load_driver()

        if len(self.driver.find_elements_by_css_selector(".desk.scene"))>0:
            try:
                isHeader = False
                block = None
                found=False
                element = self.driver.find_element_by_css_selector(".desk-table")
                header = element.find_element_by_css_selector(".desk-header .desk-cell")
                if header.get_attribute("class").find("selected") > -1:
                    block = header
                    isHeader = True
                    found = True
                tile = 0

                if not found:
                    tiles = element.find_elements_by_css_selector(".th_2")
                    for tile, tileContains in enumerate(tiles):
                        blocks = tileContains.find_elements_by_css_selector(".desk-cell")
                        i = 0
                        while not found and i < len(blocks):
                            if blocks[i].get_attribute("class").find("selected") > -1:
                                block = blocks[i]
                                found = True
                            else:
                                i += 1

                        if found:
                            break

                return (block, isHeader, tile)

            except:
                self._detect_timeout()
                return None
        else:
            self._detect_timeout()
            return None



    def _getBlockPositionFromDeskFocus(self):
        '''
        Return a tuple that contains pieces of information from selected desk block :

        @return:
            (
                ( x position, y position, width, height ) - tuple,
                is header - boolean,
                tile number - int
            ) - tuple
        '''
        try:
            block, isHeader, index = self._getBlockFromDeskFocus()
            if isHeader:
                return ([0,0,2,1], isHeader,-1)
            elif block is not None:
                strClass = unicode(block.get_attribute("class"))
                p = re.compile(ur'.*x_([0-9]*).*y_([0-9]*).*w_([0-9]*).*h_([0-9]*).*', re.UNICODE)
                result = re.search(p, strClass)
                if result:
                    return (result.groups(), isHeader, index)
                else:
                    return None
            else:
                return None
        except:
            return None

    def _loadVodPage(self):

        self.load_driver()
        time.sleep(1)

        try:
            if len(self.driver.find_elements_by_css_selector(".scene .dockCenter"))>0:
                element = self.driver.find_element_by_css_selector(".breadcrumb .first")
                title = element.text

                element = self.driver.find_element_by_css_selector(".scene .dockCenter .genres")
                genre = element.text

                cover_el = self.driver.find_element_by_css_selector(".presentationScreen .leftSideContainer .cover")
                if cover_el is not None:
                    cover = cover_el.get_attribute('src')
                    if cover == "":
                        cover = None
                else:
                    cover = None

                # Rating
                press_el = self.driver.find_element_by_css_selector(".rightSideContainer .ratingarea .press")
                press_rating = self._buildRatingDetail(press_el)
                user_el = self.driver.find_element_by_css_selector(".rightSideContainer .ratingarea .user")
                user_rating = self._buildRatingDetail(user_el)

                vod_rating = VodRating(press_rating, user_rating)

                try:
                    element = self.driver.find_element_by_css_selector(".scene .dockCenter .duration")
                    lengthTxt = element.text
                    if genre == "":
                        lengthMinTxt = lengthTxt.split(" ")[0]
                    else:
                        lengthMinTxt = lengthTxt.split(" ")[2]
                    length = timedelta(minutes=int(lengthMinTxt))
                except:
                    length = None

                try:
                    element = self.driver.find_element_by_css_selector(".scene .dockCenter .labelDuree")
                    dateTxt = element.text
                    day = dateTxt.split(" ")[2]
                    hour = dateTxt.split(" ")[4]
                    endDate = datetime(int(day.split("/")[2]), int(day.split("/")[1]), int(day.split("/")[0]), int(hour.split(":")[0]), int(hour.split(":")[1]))
                except:
                    endDate = None

                try:
                    element = self.driver.find_element_by_css_selector(".scene .price")
                    value = element.text.strip(u"€. ")
                    price = float(value)
                except Exception as e:
                    price = None

                try:
                    element = self.driver.find_element_by_css_selector(".pictoBox")
                    pictoDefinition = None
                    pictoRatio = None
                    pictoAudioDesc = None
                    pictoContentVersion = None
                    pictoDts = None
                    pictoDolby = None
                    pictoEar = None

                    if element.find_element_by_id("pictoDefinition").get_attribute("style").find("none")>-1:
                        pictoDefinition = False
                    else:
                        pictoDefinition = True
                    if element.find_element_by_id("pictoAspectRatio").get_attribute("style").find("none")>-1:
                        pictoRatio = False
                    else:
                        pictoRatio = True
                    if element.find_element_by_id("pictoAudioDesc").get_attribute("style").find("none")>-1:
                        pictoAudioDesc = False
                    else:
                        pictoAudioDesc = True
                    if element.find_element_by_id("pictoContent").get_attribute("style").find("none")>-1:
                        pictoContentVersion = False
                    else:
                        pictoContentVersion = True
                    if element.find_element_by_id("pictoDts").get_attribute("style").find("none")>-1:
                        pictoDts = False
                    else:
                        pictoDts = True
                    if element.find_element_by_id("pictoDolby").get_attribute("style").find("none")>-1:
                        pictoDolby = False
                    else:
                        pictoDolby = True
                    if element.find_element_by_id("pictoImpairedHearing").get_attribute("style").find("none")>-1:
                        pictoEar = False
                    else:
                        pictoEar = True
                    pictoCsa = None
                    try:
                        pictoCsa = int(str(self.driver.find_element_by_id("pictoCsa").get_attribute("class")).split(' ')[1].lstrip("csa0"))
                    except Exception, e:
                        self.logger.debug(" >>> framework debug >>> "+str(e))
                        pass
                except:
                    pass
                self.vod = VodItem(title, genre, length, endDate, price, pictoCsa, pictoDefinition, pictoRatio, pictoAudioDesc, pictoContentVersion, pictoDts, pictoDolby, pictoEar)
                self.vod.setCover(cover)
                self.vod.setRating(vod_rating)
                return True
            else:
                self._detect_timeout()
                return False

        except Exception, e:
            self._detect_timeout()
            self.logger.debug(" >>> framework debug >>> "+str(e))
            return False

    def _buildRatingDetail(self, element):
        if element is not None:
            rate_str = element.find_element_by_css_selector(".opinionNumber")
            rate_reg = re.compile("^\((.*?)\)$")
            rate_list = re.findall(rate_reg, rate_str.text)
            if len(rate_list) > 0:
                rate = rate_list[0]
            else:
                rate = None
            display_reg = re.compile(".*display: none;.*")
            display = display_reg.match(element.get_attribute('style')) is None
            rating = VodRatingDetail(rate, display)
            print rating
        else:
            rating = None

        return rating

    def _loadVodPageMIB4(self):

        self.load_driver()
        time.sleep(1)

        try:
            if len(self.driver.find_elements_by_css_selector(".scene .dockCenter"))>0:
                element = self.driver.find_element_by_css_selector(".breadcrumb .first")
                title = element.text

                element = self.driver.find_element_by_css_selector(".scene .dockCenter .genres")
                genre = element.text

                cover_el = self.driver.find_element_by_css_selector(".presentationScreen .leftSideContainer .cover")
                if cover_el is not None:
                    cover = cover_el.get_attribute('src')
                    if cover == "":
                        cover = None
                else:
                    cover = None

                try:
                    element = self.driver.find_element_by_css_selector(".scene .dockCenter .duration")
                    lengthTxt = element.text
                    if genre == "":
                        lengthMinTxt = lengthTxt.split(" ")[0]
                    else:
                        lengthMinTxt = lengthTxt.split(" ")[2]
                    length = timedelta(minutes=int(lengthMinTxt))
                except:
                    length = None

                try:
                    element = self.driver.find_element_by_css_selector(".scene .dockCenter .labelDuree")
                    dateTxt = element.text
                    day = dateTxt.split(" ")[2]
                    hour = dateTxt.split(" ")[4]
                    endDate = datetime(int(day.split("/")[2]), int(day.split("/")[1]), int(day.split("/")[0]), int(hour.split(":")[0]), int(hour.split(":")[1]))
                except:
                    endDate = None

                try:
                    element = self.driver.find_element_by_css_selector(".scene .price")
                    value = element.text.strip(u"€. ")
                    price = float(value)
                except Exception as e:
                    price = None

                try:
                    element = self.driver.find_element_by_css_selector(".pictoBox")
                    pictoDefinition = None
                    pictoRatio = None
                    pictoAudioDesc = None
                    pictoContentVersion = None
                    pictoDts = None
                    pictoDolby = None
                    pictoEar = None

                    if element.find_element_by_id("pictoDefinition").get_attribute("style").find("none")>-1:
                        pictoDefinition = False
                    else:
                        pictoDefinition = True
                    if element.find_element_by_id("pictoAspectRatio").get_attribute("style").find("none")>-1:
                        pictoRatio = False
                    else:
                        pictoRatio = True
                    if element.find_element_by_id("pictoAudioDesc").get_attribute("style").find("none")>-1:
                        pictoAudioDesc = False
                    else:
                        pictoAudioDesc = True
                    if element.find_element_by_id("pictoContent").get_attribute("style").find("none")>-1:
                        pictoContentVersion = False
                    else:
                        pictoContentVersion = True
                    if element.find_element_by_id("pictoDts").get_attribute("style").find("none")>-1:
                        pictoDts = False
                    else:
                        pictoDts = True
                    if element.find_element_by_id("pictoDolby").get_attribute("style").find("none")>-1:
                        pictoDolby = False
                    else:
                        pictoDolby = True
                    if element.find_element_by_id("pictoImpairedHearing").get_attribute("style").find("none")>-1:
                        pictoEar = False
                    else:
                        pictoEar = True
                    pictoCsa = None
                    try:
                        pictoCsa = int(str(self.driver.find_element_by_id("pictoCsa").get_attribute("class")).split(' ')[1].lstrip("csa0"))
                    except Exception, e:
                        self.logger.debug(" >>> framework debug >>> "+str(e))
                        pass
                except:
                    pass
                self.vod = VodItem(title, genre, length, endDate, price, pictoCsa, pictoDefinition, pictoRatio, pictoAudioDesc, pictoContentVersion, pictoDts, pictoDolby, pictoEar)
                self.vod.setCover(cover)
                return True
            else:
                self._detect_timeout()
                return False

        except Exception, e:
            self._detect_timeout()
            self.logger.debug(" >>> framework debug >>> "+str(e))
            return False


    def _loadVodPagePolaris(self):

        self.load_driver()
        time.sleep(1)

        try:
            if len(self.driver.find_elements_by_css_selector(".svftop .svftopright"))>0:
                element = self.driver.find_element_by_css_selector(".svftop .svftopright")

                head = element.find_element_by_css_selector(".pol2Header")
                title = head.text
                pictoCsa = None
                try:
                    pictoCsa = int(str(element.find_element_by_id("pictoCsa").get_attribute("class")).split(' ')[1].lstrip("csa0"))
                except Exception, e:
                    self.logger.debug(" >>> framework debug >>> "+str(e))
                    pass


                try:
                    message = element.find_element_by_css_selector(".pol2Messages .watchMessages .message")
                    dateTxt = message.text
                    day = dateTxt.split(" ")[4]
                    hour = dateTxt.split(" ")[6]
                    endDate = datetime(int(day.split("/")[2]), int(day.split("/")[1]), int(day.split("/")[0]), int(hour.split(":")[0]), int(hour.split(":")[1]))
                except:
                    endDate = None


                try:
                    description = element.find_element_by_css_selector(".pol2Description")
                    genreLength = description.find_element_by_css_selector(".genreDateCountryDuration").text.replace(" ","")
                    if genreLength.split("|")[0].isdigit():
                        genre = ""
                    else:
                        genre = genreLength.split("|")[0]
                    if genreLength.split("|")[-1] =="":
                        length = timedelta(minutes = int(genreLength.split("|")[-2].replace("mn","")))
                    else:
                        length = timedelta(minutes = int(genreLength.split("|")[-1].replace("mn","")))
                except:
                    length = None

                try:
                    element = description.find_element_by_css_selector(".pol2PictosBox .pictoBox")
                    pictoDefinition = None
                    # pictoRatio = None
                    pictoAudioDesc = None
                    pictoContentVersion = None
                    pictoDts = None
                    pictoDolby = None
                    pictoEar = None

                    if element.find_element_by_id("pictoDefinition").get_attribute("style").find("none")>-1:
                        pictoDefinition = False
                    else:
                        pictoDefinition = True
                    if element.find_element_by_id("pictoAudioDesc").get_attribute("style").find("none")>-1:
                        pictoAudioDesc = False
                    else:
                        pictoAudioDesc = True
                    if element.find_element_by_id("pictoContent").get_attribute("style").find("none")>-1:
                        pictoContentVersion = False
                    else:
                        pictoContentVersion = True
                    if element.find_element_by_id("pictoDts").get_attribute("style").find("none")>-1:
                        pictoDts = False
                    else:
                        pictoDts = True
                    if element.find_element_by_id("pictoDolby").get_attribute("style").find("none")>-1:
                        pictoDolby = False
                    else:
                        pictoDolby = True
                    if element.find_element_by_id("pictoImpairedHearing").get_attribute("style").find("none")>-1:
                        pictoEar = False
                    else:
                        pictoEar = True
                except:
                    pass
                self.vod = VodItem(title, genre, length, endDate, None, pictoCsa, pictoDefinition, False, pictoAudioDesc, pictoContentVersion, pictoDts, pictoDolby, pictoEar)
                return True
            else:
                self._detect_timeout()
                return False

        except Exception, e:
            self._detect_timeout()
            self.logger.debug(" >>> framework debug >>> "+str(e))
            return False

    def _loadPageList(self):
        self.highlight = None
        self.items=[]
        self.activeHighlight = None
        self.activeItems = []

        self.load_driver()
        time.sleep(1)
        try:
            if len(self.driver.find_elements_by_css_selector(".PVRPlayer"))>0:
                if self.driver.find_element_by_css_selector(".PVRPlayer .radiolist").get_attribute("style").find("none") > -1:
                    elements=self.driver.find_elements_by_css_selector(".PVRPlayer .list .listItem")
                else:
                    elements=self.driver.find_elements_by_css_selector(".PVRPlayer .radiolist .listItem")
            elif len(self.driver.find_elements_by_css_selector(".PVRPlayer .radiolist .listItem"))>0:
                elements=self.driver.find_elements_by_css_selector(".PVRPlayer .radiolist .listItem")
            elif len(self.driver.find_elements_by_css_selector(".radiolist .listItem"))>0:
                elements=self.driver.find_elements_by_css_selector(".radiolist .listItem")
            elif len(self.driver.find_elements_by_css_selector(".box"))>0:
                elements=self.driver.find_elements_by_css_selector(".box .listItem")
            elif len(self.driver.find_elements_by_css_selector(".menuList .list .container .listItem"))>0:
                elements=self.driver.find_elements_by_css_selector(".menuList .list .container .listItem")
            else:
                self._detect_timeout()
                return False

            indexActive = 0


            for index, element in enumerate(elements):
                if element.get_attribute("class").find("highlight")>-1:
                    self.highlight=index
                    self.activeHighlight=indexActive
                if element.get_attribute("class").find("selected")>-1 or element.get_attribute("class").find("checked")>-1:
                    selected=True
                else:
                    selected=False
                if element.get_attribute("class").find("inactive")>-1:
                    active=False
                else:
                    active=True
                    self.activeItems.append(ListItem(element.text, selected, active))
                    indexActive=indexActive + 1

                self.items.append(ListItem(element.text, selected, active))
            return True

        except Exception, e:
            self._detect_timeout()
            self.logger.debug(" >>> framework debug >>> "+str(e))
            return False

    def _loadPageList_manualpvr(self):
        self.highlight = None
        self.items=[]
        self.activeHighlight = None
        self.activeItems = []

        self.load_driver()
        time.sleep(1)
        try:
            if len(self.driver.find_elements_by_css_selector(".channelsCB"))>0:
                elements=self.driver.find_elements_by_css_selector(".channelsCB .listItem")
            else:
                self._detect_timeout()
                return False

            indexActive = 0

            for index, element in enumerate(elements):
                if element.get_attribute("class").find("highlight")>-1:
                    self.highlight=index
                    self.activeHighlight=indexActive
                if element.get_attribute("class").find("selected")>-1 or element.get_attribute("class").find("checked")>-1:
                    selected=True
                else:
                    selected=False
                if element.get_attribute("class").find("inactive")>-1:
                    active=False
                else:
                    active=True
                    self.activeItems.append(ListItem(element.text, selected, active))
                    indexActive=indexActive + 1

                self.items.append(ListItem(element.text, selected, active))
            return True

        except Exception, e:
            self._detect_timeout()
            self.logger.debug(" >>> framework debug >>> "+str(e))
            return False

    def _loadLiveBanner(self):

        self.load_driver()
        time.sleep(1)

        try:
            elements = self.driver.find_elements_by_css_selector(".programInformation")
            # if several programInformation blocks are detected only the last one must be taken into account
            i = len(elements)-1

            if i >= 0:
                element = elements[i].find_elements_by_css_selector(".nameOfChannel")
                alltxt_list=element[0].text.split(u' ')
                num = int(alltxt_list[0])
                txt=""
                inter=""
                for index in range(2,len(alltxt_list)):
                    txt=txt+inter+alltxt_list[index]
                    inter=" "

                if len(elements[i].find_elements_by_css_selector(".favorite.hidden"))>0:
                    favorite = False
                else:
                    favorite = True

                element=elements[i].find_element_by_css_selector(".program")
                currentProgram = element.text
                element=elements[i].find_element_by_css_selector(".kind")
                genre = element.text
                if len(genre) == 0:
                    genre = ''

                element=elements[i].find_element_by_css_selector(".next")
                nextProgram = element.text
                if len(nextProgram) == 0:
                    nextProgram = ''

                element=elements[i].find_element_by_css_selector(".start")
                start = None
                end = None
                length = None
                if len(element.text) == 5:
                    hour = element.text.split(":")[0]
                    minutes = element.text.split(":")[1]
                    now=datetime.now()
                    start = datetime(now.year, now.month, now.day, int(hour), int(minutes))

                    element=elements[i].find_element_by_css_selector(" .end")
                    if len(element.text) == 5:
                        hour = element.text.split(":")[0]
                        minutes = element.text.split(":")[1]
                        now=datetime.now()
                        end = datetime(now.year, now.month, now.day, int(hour), int(minutes))
                        if end < start:
                            end = end + timedelta(days=1)
                        length = end - start

                element=elements[i].find_element_by_css_selector(".moviePictos")
                pictoDefinition = None
                pictoRatio = None
                pictoAudioDesc = None
                pictoContentVersion = None
                pictoDts = None
                pictoDolby = None
                pictoEar = None

                try:
                    if element.find_element_by_css_selector(".pictoHD").get_attribute("class").find("hidden")>-1:
                        pictoDefinition = False
                    else:
                        pictoDefinition = True
                except Exception, e:
                    self.logger.debug(" >>> framework debug >>> "+str(e))
                    pass
                try:
                    if element.find_element_by_css_selector(".picto16_9").get_attribute("class").find("hidden")>-1:
                        pictoRatio = False
                    else:
                        pictoRatio = True
                except Exception, e:
                    self.logger.debug(" >>> framework debug >>> "+str(e))
                    pass
                try:
                    if element.find_element_by_css_selector(".pictoAD").get_attribute("class").find("hidden")>-1:
                        pictoAudioDesc = False
                    else:
                        pictoAudioDesc = True
                except Exception, e:
                    self.logger.debug(" >>> framework debug >>> "+str(e))
                    pass

                try:
                    if element.find_element_by_css_selector(".pictoVM").get_attribute("class").find("hidden")>-1:
                        pictoContentVersion = False
                    else:
                        pictoContentVersion = True
                except Exception, e:
                    self.logger.debug(" >>> framework debug >>> "+str(e))
                    pass

                try:
                    if element.find_element_by_css_selector(".pictoDTS").get_attribute("class").find("hidden")>-1:
                        pictoDts = False
                    else:
                        pictoDts = True
                except Exception, e:
                    self.logger.debug(" >>> framework debug >>> "+str(e))
                    pass

                try:
                    if element.find_element_by_css_selector(".pictoDolby").get_attribute("class").find("hidden")>-1:
                        pictoDolby = False
                    else:
                        pictoDolby = True
                except Exception, e:
                    self.logger.debug(" >>> framework debug >>> "+str(e))
                    pass

                try:
                    if element.find_element_by_css_selector(".pictoEar").get_attribute("class").find("hidden")>-1:
                        pictoEar = False
                    else:
                        pictoEar = True
                except Exception, e:
                    self.logger.debug(" >>> framework debug >>> "+str(e))
                    pass

                pictoCsa = None
                try:
                    pictoCsa = int(str(elements[i].find_element_by_css_selector(".pictoCsa").get_attribute("class")).split(' ')[1].lstrip("csa0"))
                except Exception, e:
                    self.logger.debug(" >>> framework debug >>> "+str(e))
                    pass

                TS_start = None
                TS_width = None
                TS_cursor = None
                try:
                    TS_cursor = elements[i].find_element_by_css_selector(".tsCursor").get_attribute("style")
                    TS_cursor = TS_cursor.replace('left: ','')
                    TS_cursor = int(TS_cursor.replace('%;',''))
                    TS_element = elements[i].find_element_by_css_selector(".scrollerBar").get_attribute("style").split(';')
                    TS_start = TS_element[0].replace('left: ','')
                    TS_start = int(TS_start.replace('%',''))
                    TS_width = TS_element[1].replace('width: ','')
                    TS_width = int(TS_width.replace('%',''))
                except:
                    pass

                self.programInfo = ProgramInfoItem(num, txt, currentProgram, start, end, length, genre, favorite, None, nextProgram, pictoCsa, pictoDefinition, pictoRatio, pictoAudioDesc, pictoContentVersion, pictoDts, pictoDolby, pictoEar, None, None, TS_start, TS_width, TS_cursor)
                return True
            else:
                self._detect_timeout()
                return False

        except Exception, e:
            self._detect_timeout()
            self.logger.debug(" >>> framework debug >>> "+str(e))
            return False


    def _loadPvrBanner(self):

        self.load_driver()
        time.sleep(1)

        try:
	    if self.driver.find_element_by_css_selector(".pvrbanner").get_attribute("class").find("hidden")>-1:
		elements = []
	    else:
		elements = self.driver.find_elements_by_css_selector(".recordContent")
            # if several recordContent blocks are detected only the last one must be taken into account
            i = len(elements)-1

            if i >= 0:
                element=elements[i].find_element_by_css_selector(".recordInformation")
		recordInformation = element.text
                if len(recordInformation) == 0:
                    recordInformation = ''
                element=elements[i].find_element_by_css_selector(".recordTitle")
                recordTitle = element.text
                if len(recordTitle) == 0:
                    recordTitle = ''
                element=elements[i].find_element_by_css_selector(".genre")
                recordGenre = element.text
                if len(recordGenre) == 0:
                    recordGenre = ''
                element=elements[i].find_element_by_css_selector(".time")
                recordTime = element.text
                if len(recordTime) == 0:
                    recordTime = ''
                element=elements[i].find_element_by_css_selector(".duration")
                recordDuration = element.text
                if len(recordDuration) == 0:
                    recordDuration = ''
                element=elements[i].find_element_by_css_selector(".progressBar .scrollerBar")
                recordProgress = int(element.get_attribute('style').split(' ')[1].replace(';','').replace('%',''))
                element=elements[i].find_element_by_css_selector(".moviePictos")
                pictoDefinition = None
                pictoRatio = None
                pictoAudioDesc = None
                pictoContentVersion = None
                pictoDts = None
                pictoDolby = None
                pictoEar = None

                if element.find_element_by_css_selector(".pictoHD").get_attribute("class").find("hidden")>-1:
                    pictoDefinition = False
                else:
                    pictoDefinition = True
                try:
                    if element.find_element_by_css_selector(".picto16_9").get_attribute("class").find("hidden")>-1:
                        pictoRatio = False
                except:
                    pass

                else:
                    pictoRatio = True
                if element.find_element_by_css_selector(".pictoAD").get_attribute("class").find("hidden")>-1:
                    pictoAudioDesc = False
                else:
                    pictoAudioDesc = True
                if element.find_element_by_css_selector(".pictoVM").get_attribute("class").find("hidden")>-1:
                    pictoContentVersion = False
                else:
                    pictoContentVersion = True
                if element.find_element_by_css_selector(".pictoDTS").get_attribute("class").find("hidden")>-1:
                    pictoDts = False
                else:
                    pictoDts = True
                if element.find_element_by_css_selector(".pictoDolby").get_attribute("class").find("hidden")>-1:
                    pictoDolby = False
                else:
                    pictoDolby = True
                if element.find_element_by_css_selector(".pictoEar").get_attribute("class").find("hidden")>-1:
                    pictoEar = False
                else:
                    pictoEar = True

                pictoCsa = None
                try:
                    pictoCsa = int(str(elements[i].find_element_by_css_selector(".pictoCsa").get_attribute("class")).split(' ')[1].lstrip("csa0"))
                except Exception, e:
                    self.logger.debug(" >>> framework debug >>> "+str(e))
                    pass

                self.programInfo = pvrItem(recordInformation, recordTitle, recordGenre, recordTime, recordDuration, recordProgress, pictoCsa, pictoDefinition, pictoRatio, pictoAudioDesc, pictoContentVersion, pictoDts, pictoDolby, pictoEar)
                return True
            else:
                self._detect_timeout()
                return False

        except Exception, e:
            self._detect_timeout()
            self.logger.debug(" >>> framework debug >>> "+str(e))
            return False

    def _loadLiveBannerPolaris(self):

        self.load_driver()
        time.sleep(1)
        try:
            # ATTENTION nécessaire pour tests sprint 28 (absence de vzl)
            if len(self.driver.find_elements_by_css_selector(".infoBanner"))>=1:
                elements = self.driver.find_element_by_css_selector(".infoBanner")
            else:
                elements = self.driver.find_element_by_css_selector(".infoChannel.selected")

            # channel name, lcn, favorite
            element = elements.find_element_by_css_selector(".titleLine")
            # ATTENTION Gestion de la récupération du nom de chaine avant/après harmonisation avec la mosaaïque (nécessaire pendant valid sprint 28)
            if '-' in element.text:
                alltxt_list=element.text.split(u'-')
            else:
                alltxt_list=element.text.split(u'.')

            num = int(alltxt_list[0])
            txt=alltxt_list[1]
            if len(element.find_elements_by_css_selector(".favIcon"))>0:
                favorite = True
            else:
                favorite = False
            #current program info
            if len(elements.find_elements_by_css_selector(".infoProgram .programTitle"))>0:
                currentProgram = elements.find_element_by_css_selector(".infoProgram .programTitle").text
            elif len(elements.find_elements_by_css_selector(".infoProgram .slogan"))>0:
                currentProgram = elements.find_element_by_css_selector(".infoProgram .slogan").text
            else:
                currentProgram = ''

            if len(elements.find_elements_by_css_selector(".infoProgram .kind"))>0:
                genre = elements.find_element_by_css_selector(".infoProgram .kind").text
            else:
                genre = ''

            nextProgram = '' # A PRIORI DONNEES PAS DISPONIBLES

            element=elements.find_element_by_css_selector(".infoProgram .progressBarWidget .timeLeft")
            start = None
            length = None
            end = None
            recordOnGoing =len(elements.find_elements_by_css_selector(".infoProgram .progressBarWidget .middleContainer .cursorLeft .tooltip.record"))>0
            if len(element.text) == 5:
                hour = element.text.split(":")[0]
                minutes = element.text.split(":")[1]
                now=datetime.now()
                start = datetime(now.year, now.month, now.day, int(hour), int(minutes))

                element=elements.find_element_by_css_selector(".infoProgram .progressBarWidget .timeRight")
                if len(element.text) == 5:
                    hour = element.text.split(":")[0]
                    minutes = element.text.split(":")[1]
                    now=datetime.now()
                    end = datetime(now.year, now.month, now.day, int(hour), int(minutes))
                    if end < start:
                        end = end + timedelta(days = 1)
                    length = end - start

            element=elements.find_element_by_css_selector(".infoProgram .logoContainer")
            pictoDefinition = False
            pictoRatio = False
            pictoAudioDesc = False
            pictoContentVersion = False
            pictoDts = False
            pictoDolby = False
            pictoEar = False
            picto3D = False
            pictoVO = False
            pictoCsa = False

            try:

                if len(element.find_elements_by_css_selector(".secondaryLogo.hd"))>0:
                    pictoDefinition = True

                if len(element.find_elements_by_css_selector(".secondaryLogo.format16_9"))>0:
                    pictoRatio = True

                if len(element.find_elements_by_css_selector(".secondaryLogo.audioDesc"))>0:
                    pictoAudioDesc = True

                if len(element.find_elements_by_css_selector(".secondaryLogo.vm"))>0:
                    pictoContentVersion = True

                if len(element.find_elements_by_css_selector(".secondaryLogo.dolby"))>0:
                    pictoDolby = True

                if len(element.find_elements_by_css_selector(".secondaryLogo.impairedHearing"))>0:
                    pictoEar = True

                if len(element.find_elements_by_css_selector(".secondaryLogo.threed"))>0:
                    picto3D = True

                if len(element.find_elements_by_css_selector(".secondaryLogo.vovf"))>0:
                    pictoVO = True

                try:
                    pictoCsa = self._getCSANumber(int(str(elements.find_element_by_css_selector(".infoProgram .csa").get_attribute("class")).split(' ')[2].lstrip("CSA")))
                except Exception:
                    pass
            except Exception:
                pass

            self.programInfo = ProgramInfoItem(num, txt, currentProgram, start, end, length, genre, favorite, recordOnGoing, nextProgram, pictoCsa, pictoDefinition, pictoRatio, pictoAudioDesc, pictoContentVersion, pictoDts, pictoDolby, pictoEar, picto3D, pictoVO)
            return True

        except Exception, e:
            self._detect_timeout()
            print e
            return False

    def _loadVodBannerPolaris(self):

        self.load_driver()
        time.sleep(1)
        try:
            pictoDefinition = False
            pictoRatio = False
            pictoAudioDesc = False
            pictoContentVersion = False
            pictoDts = False
            pictoDolby = False
            pictoEar = False
            pictoCsa = False

            elements = self.driver.find_element_by_css_selector(".infoBanner .infoProgram")
            title = elements.find_element_by_css_selector(".firstLine .programTitle").text

            try:
                pictoCsa = self._getCSANumber(int(str(elements.find_element_by_css_selector(".firstLine .csa").get_attribute("class")).split(' ')[2].lstrip("CSA")))
            except Exception:
                pass

            element=elements.find_element_by_css_selector(".secondLine")
            kind = element.find_element_by_css_selector(".kind").text

            try:
                element=element.find_element_by_css_selector(".logoContainer")

                if len(element.find_elements_by_css_selector(".secondaryLogo.hd"))>0:
                    pictoDefinition = True

                if len(element.find_elements_by_css_selector(".secondaryLogo.format16_9"))>0:
                    pictoRatio = True

                if len(element.find_elements_by_css_selector(".secondaryLogo.audioDesc"))>0:
                    pictoAudioDesc = True

                if len(element.find_elements_by_css_selector(".secondaryLogo.vm"))>0:
                    pictoContentVersion = True

                if len(element.find_elements_by_css_selector(".secondaryLogo.dolby"))>0:
                    pictoDolby = True

                if len(element.find_elements_by_css_selector(".secondaryLogo.impairedHearing"))>0:
                    pictoEar = True

            except Exception:
                pass

            element = elements.find_element_by_css_selector(".progressBarWidget .timeRight").text
            length = timedelta(hours = int(element.split(":")[0]), minutes = int(element.split(":")[1]))


            self.vod = VodItem(title, kind, length, None, None, pictoCsa, pictoDefinition, pictoRatio, pictoAudioDesc, pictoContentVersion, pictoDts, pictoDolby, pictoEar)
            return True

        except Exception, e:
            self._detect_timeout()
            print e
            return False

    def _loadVodTrailerBanner(self):
        self.load_driver()
        time.sleep(1)
        try:
            elements = self.driver.find_element_by_css_selector(".playerbanner")
            if elements.get_attribute("class").find("hidden")>-1:
                return False
            self.vodTrailerBanner = self.driver.find_element_by_css_selector(".playerbanner .title").text
            return True
        except Exception, e:
            self._detect_timeout()
            print e
            return False


    def _loadVodBanner(self):

        self.load_driver()
        time.sleep(1)
        try:
            pictoDefinition = False
            pictoRatio = False
            pictoAudioDesc = False
            pictoContentVersion = False
            pictoDts = False
            pictoDolby = False
            pictoEar = False
            pictoCsa = False

            elements = self.driver.find_element_by_css_selector(".infoBanner .infoProgram")
            title = elements.find_element_by_css_selector(".firstLine .programTitle").text

            try:
                pictoCsa = self._getCSANumber(int(str(elements.find_element_by_css_selector(".firstLine .csa").get_attribute("class")).split(' ')[2].lstrip("CSA")))
            except Exception:
                pass

            element = self.driver.find_element_by_css_selector(".scene .dockCenter .genres")
            genre = element.text
            #element=elements.find_element_by_css_selector(".secondLine")
            #kind = element.find_element_by_css_selector(".kind").text

            try:
                element=element.find_element_by_css_selector(".logoContainer")

                if len(element.find_elements_by_css_selector(".secondaryLogo.hd"))>0:
                    pictoDefinition = True

                if len(element.find_elements_by_css_selector(".secondaryLogo.format16_9"))>0:
                    pictoRatio = True

                if len(element.find_elements_by_css_selector(".secondaryLogo.audioDesc"))>0:
                    pictoAudioDesc = True

                if len(element.find_elements_by_css_selector(".secondaryLogo.vm"))>0:
                    pictoContentVersion = True

                if len(element.find_elements_by_css_selector(".secondaryLogo.dolby"))>0:
                    pictoDolby = True

                if len(element.find_elements_by_css_selector(".secondaryLogo.impairedHearing"))>0:
                    pictoEar = True

            except Exception:
                pass

            element = elements.find_element_by_css_selector(".progressBarWidget .timeRight").text
            length = timedelta(hours = int(element.split(":")[0]), minutes = int(element.split(":")[1]))


            self.vod = VodItem(title, genre, length, None, None, pictoCsa, pictoDefinition, pictoRatio, pictoAudioDesc, pictoContentVersion, pictoDts, pictoDolby, pictoEar)
            return True

        except Exception, e:
            self._detect_timeout()
            print e
            return False


    def _loadEpg(self):

        lcn = None
        channelName = None
        favorite = None
        ongoingRec = None
        program = ''
        start = None
        end = None
        length = None
        genre = ''
        nextProgram = ''

        self.load_driver()
        time.sleep(1)

        try:
            if len(self.driver.find_elements_by_css_selector(".epg .grid .content"))>0:

                element = self.driver.find_element_by_css_selector(".epg .grid .content .row.selected .channelText")
                if(self.driver.find_element_by_css_selector(".epg .grid .content .row.selected .epgHeart").get_attribute("style").find("display: none"))>-1:
                    favorite = False
                else:
                    favorite = True

                ''' Améliorer absolument car ne marche que si on ne bouge pas de la case '''
                try:
                    elements = self.driver.find_elements_by_css_selector(".epg .grid .content .cellContainers .cellContainer.selected")
                    for el in elements:
                        if el.find_element_by_css_selector(".cell.selected .recording"):
                            ongoingRec = True
                        else:
                            ongoingRec = False
                except:
                    pass

                alltxt_list=element.text.split(u' ')
                lcn = int(alltxt_list[0].replace(".",""))

                channelName = ""
                inter = ""
                for index in range(1,len(alltxt_list)):
                    channelName = channelName + inter + alltxt_list[index]
                    inter = " "

                try:
                    elements = self.driver.find_elements_by_css_selector(".cellContainer.selected .cell")
                    program = self.driver.find_element_by_css_selector(".epg .grid .description .title").text
                    genre = self.driver.find_element_by_css_selector(" .epg .grid .description .genre").text

                    exitLoop = False
                    if len(self.driver.find_elements_by_css_selector(".row.selected .noprogram.cell.selected")) < 1:
                        for e in elements:
                            if exitLoop:
                                nextProgram = e.text
                                break
                            if e.get_attribute("class").find("selected") >-1:
                                exitLoop = True

                    element=self.driver.find_element_by_css_selector(".epg .grid .description .date")
                    date=self.driver.find_element_by_css_selector(".epg .grid .timeline .time .middleHour").text
                    if date == "aujourd'hui":
                        now = datetime.now()
                        day = now.day
                        month = now.month
                    elif date=="demain":
                        now = datetime.now() + timedelta(days=1)
                        day = now.day
                        month = now.month
                    elif date=="hier":
                        now = datetime.now() - timedelta(days=1)
                        day = now.day
                        month = now.month
                    else:
                        now = datetime.now()
                        day = date.split(" ")[1]
                        month = self._getmonth(date.split(" ")[2])

                    if len(element.text) > 10:
                        startTxt = element.text.split(" ")[0]
                        hour = startTxt.split(":")[0]
                        minutes = startTxt.split(":")[1]
                        start = datetime(now.year, month, int(day), int(hour), int(minutes))

                        endTxt = element.text.split(" ")[2]
                        hour = endTxt.split(":")[0]
                        minutes = endTxt.split(":")[1]
                        now=datetime.now()
                        end = datetime(now.year, month, int(day), int(hour), int(minutes))
                        if end < start:
                            end = end + timedelta(days = 1)
                        length = end - start

                    element=self.driver.find_element_by_css_selector(".epg .grid .description .pictoBox")
                    pictoDefinition = None
                    pictoRatio = None
                    pictoAudioDesc = None
                    pictoContentVersion = None
                    pictoDts = None
                    pictoDolby = None
                    pictoEar = None

                    if element.find_element_by_id("pictoDefinition").get_attribute("style").find("display: none")>-1:
                        pictoDefinition = False
                    else:
                        pictoDefinition = True
                    if element.find_element_by_id("pictoAspectRatio").get_attribute("style").find("display: none")>-1:
                        pictoRatio = False
                    else:
                        pictoRatio = True
                    if element.find_element_by_id("pictoAudioDesc").get_attribute("style").find("display: none")>-1:
                        pictoAudioDesc = False
                    else:
                        pictoAudioDesc = True
                    if element.find_element_by_id("pictoContent").get_attribute("style").find("display: none")>-1:
                        pictoContentVersion = False
                    else:
                        pictoContentVersion = True
                    if element.find_element_by_id("pictoDts").get_attribute("style").find("display: none")>-1:
                        pictoDts = False
                    else:
                        pictoDts = True
                    if element.find_element_by_id("pictoDolby").get_attribute("style").find("display: none")>-1:
                        pictoDolby = False
                    else:
                        pictoDolby = True
                    if element.find_element_by_id("pictoImpairedHearing").get_attribute("style").find("display: none")>-1:
                        pictoEar = False
                    else:
                        pictoEar = True

                    pictoCsa = None
                    pictoCsa = int(str(element.find_element_by_id("pictoCsa").get_attribute("class")).split(' ')[1].lstrip("csa0"))

                except:
                    pass

                self.programInfo = ProgramInfoItem(lcn, channelName, program, start, end, length, genre, favorite, ongoingRec, nextProgram, pictoCsa, pictoDefinition, pictoRatio, pictoAudioDesc, pictoContentVersion, pictoDts, pictoDolby, pictoEar)

                return True
            elif len(self.driver.find_elements_by_css_selector(".tonightView  .tonightList "))>0:
                try:
                    channelList = self.driver.find_elements_by_css_selector(".tonightView  .tonightList .tonightItem")
                    for channel in channelList:
                        programList = channel.find_elements_by_css_selector(".primeTime")
                        for programInfo in programList:
                            if programInfo.get_attribute("class").find("selected")>-1:

                                lcn = -1
                                channelName=""
                                if(channel.find_element_by_css_selector(".epgHeart").get_attribute("style").find("display: none"))>-1:
                                    favorite = False
                                else:
                                    favorite=True

                                pictoCsa = None
                                pictoDefinition = None
                                pictoRatio = None
                                pictoAudioDesc = None
                                pictoContentVersion = None
                                pictoDts = None
                                pictoDolby = None
                                pictoEar = None

                                date = None
                                program = ''
                                genre =''

                                if(channel.find_element_by_css_selector(".noDataAvailable").get_attribute("style").find("display: none"))>-1:
                                    date = programInfo.find_element_by_css_selector(".date")
                                    startTxt = date.text
                                    hour = startTxt.split(":")[0]
                                    minutes = startTxt.split(":")[1]
                                    now=datetime.now()
                                    start = datetime(now.year, now.month, now.day, int(hour), int(minutes))

                                    if len(programInfo.find_elements_by_css_selector(".information .footer .dataAvailable"))>0:
                                        program = programInfo.find_element_by_css_selector(".name").text
                                        genre = programInfo.find_element_by_css_selector(".genre").text
                                        element=programInfo.find_element_by_css_selector(" .pictoBox")


                                        if element.find_element_by_id("pictoDefinition").get_attribute("style").find("display: none")>-1:
                                            pictoDefinition = False
                                        else:
                                            pictoDefinition = True
                                        if element.find_element_by_id("pictoAspectRatio").get_attribute("style").find("display: none")>-1:
                                            pictoRatio = False
                                        else:
                                            pictoRatio = True
                                        if element.find_element_by_id("pictoAudioDesc").get_attribute("style").find("display: none")>-1:
                                            pictoAudioDesc = False
                                        else:
                                            pictoAudioDesc = True
                                        if element.find_element_by_id("pictoContent").get_attribute("style").find("display: none")>-1:
                                            pictoContentVersion = False
                                        else:
                                            pictoContentVersion = True
                                        if element.find_element_by_id("pictoDts").get_attribute("style").find("display: none")>-1:
                                            pictoDts = False
                                        else:
                                            pictoDts = True
                                        if element.find_element_by_id("pictoDolby").get_attribute("style").find("display: none")>-1:
                                            pictoDolby = False
                                        else:
                                            pictoDolby = True
                                        if element.find_element_by_id("pictoImpairedHearing").get_attribute("style").find("display: none")>-1:
                                            pictoEar = False
                                        else:
                                            pictoEar = True

                                        pictoCsa = None
                                        pictoCsa = int(str(element.find_element_by_id("pictoCsa").get_attribute("class")).split(' ')[1].lstrip("csa0"))

                                else:
                                    program = programInfo.find_element_by_css_selector(".noDataAvailable").text

                except Exception, e:
                    self.logger.debug(" >>> framework debug >>> "+str(e))
                    pass
                self.programInfo = ProgramInfoItem(lcn, channelName, program, start, end, length, genre, favorite, None, nextProgram, pictoCsa, pictoDefinition, pictoRatio, pictoAudioDesc, pictoContentVersion, pictoDts, pictoDolby, pictoEar)

                return True
            else:
                self._detect_timeout()
                return False
        except Exception, e:
            self._detect_timeout()
            self.logger.debug(" >>> framework debug >>> "+str(e))
            return False



    def _loadMosaic(self):
        self.grid=[]
        channelFocus= None
        ongoingRec = False
        favorite = None
        self.load_driver()
        time.sleep(1)
        pictoDefinition = False
        pictoRatio = False
        pictoContentVersion = False
        pictoDts = False
        pictoEar = False



        try:
            if len(self.driver.find_elements_by_css_selector(".scene.mosaicView"))>0:
                    self.grid.append([])
                    elements=self.driver.find_elements_by_css_selector(".scene.mosaicView .mosaic .mosaicItem.row1")
                    for index, element in enumerate(elements):
                        self.grid[0].append(element.find_element_by_css_selector(".focusLabel").text)
                        if element.get_attribute("class").find("highlight")>-1:
                            channelFocus=self.grid[0][index]
                            if len(element.find_elements_by_css_selector(".recording"))>0:
                                ongoingRec = True
                            if len(element.find_elements_by_css_selector(".favorite.hidden"))>0:
                                    favorite = False
                            else:
                                    favorite = True
                            """"if len(element.find_elements_by_css_selector(".favorite"))>0:
                                favorite = True
                            else:
                                favorite = False"""

                    self.grid.append([])
                    elements=self.driver.find_elements_by_css_selector(".scene.mosaicView .mosaic .mosaicItem.row2")
                    for index, element in enumerate(elements):
                        self.grid[1].append(element.find_element_by_css_selector(".focusLabel").text)
                        if element.get_attribute("class").find("highlight")>-1:
                            channelFocus=self.grid[1][index]
                            if len(element.find_elements_by_css_selector(".recording"))>0:
                                ongoingRec = True
                            if len(element.find_elements_by_css_selector(".favorite.hidden"))>0:
                                    favorite = False
                            else:
                                    favorite = True

                    self.grid.append([])
                    elements=self.driver.find_elements_by_css_selector(".scene.mosaicView .mosaic .mosaicItem.row3")
                    for index, element in enumerate(elements):
                        self.grid[2].append(element.find_element_by_css_selector(".focusLabel").text)
                        if element.get_attribute("class").find("highlight")>-1:
                            channelFocus=self.grid[2][index]
                            if len(element.find_elements_by_css_selector(".recording"))>0:
                                ongoingRec = True
                            if len(element.find_elements_by_css_selector(".favorite.hidden"))>0:
                                    favorite = False
                            else:
                                    favorite = True

                    self.grid.append([])
                    elements=self.driver.find_elements_by_css_selector(".scene.mosaicView .mosaic .mosaicItem.row4")
                    for index, element in enumerate(elements):
                        self.grid[3].append(element.find_element_by_css_selector(".focusLabel").text)
                        if element.get_attribute("class").find("highlight")>-1:
                            channelFocus=self.grid[3][index]
                            if len(element.find_elements_by_css_selector(".recording"))>0:
                                ongoingRec = True
                            if len(element.find_elements_by_css_selector(".favorite.hidden"))>0:
                                    favorite = False
                            else:
                                    favorite = True

                    element=self.driver.find_element_by_css_selector(".scene.mosaicView .mosaicTooltip")
                    programFocus=element.find_element_by_css_selector(".program").text
                    programGenreFocus=element.find_element_by_css_selector(".kind").text


                    if len(element.find_elements_by_css_selector(".pictoHD"))>0:
                        if not element.find_element_by_css_selector(".pictoHD").get_attribute("class").find("hidden")>-1:
                            pictoDefinition = True

                    if len(element.find_elements_by_css_selector(".picto16_9"))>0:
                        if element.find_element_by_css_selector(".picto16_9").get_attribute("class").find("hidden")>-1:
                            pictoRatio = True

                    if len(element.find_elements_by_css_selector(".pictoVM"))>0:
                        if element.find_element_by_css_selector(".pictoVM").get_attribute("class").find("hidden")>-1:
                            pictoContentVersion = True

                    if len(element.find_elements_by_css_selector(".pictoDTS"))>0:
                        if element.find_element_by_css_selector(".pictoDTS").get_attribute("class").find("hidden")>-1:
                            pictoDts = True

                    if len(element.find_elements_by_css_selector(".pictoDolby"))>0:
                        if element.find_element_by_css_selector(".pictoDolby").get_attribute("class").find("hidden")>-1:
                            pictoDolby = False
                        else:
                            pictoDolby = True

                    if len(element.find_elements_by_css_selector(".pictoEar"))>0:
                        if element.find_element_by_css_selector(".pictoEar").get_attribute("class").find("hidden")>-1:
                            pictoEar = True

                    pictoCsa = int(str(element.find_element_by_css_selector(".pictoCsa").get_attribute("class")).split(' ')[1].lstrip("csa0"))



                    alltxt_list=channelFocus.split(u' ')
                    channelNum=int(alltxt_list[0])

                    channelTxt=""
                    inter=""
                    for index in range(2,len(alltxt_list)):
                        channelTxt=channelTxt+inter+alltxt_list[index]
                        inter=" "

                    self.programInfo=ProgramInfoItem(channelNum, channelTxt, programFocus, None, None, None, programGenreFocus, favorite, ongoingRec, '', pictoCsa, pictoDefinition, pictoRatio, None, pictoContentVersion, pictoDts, pictoDolby, pictoEar)

                    return True
            else:
                self._detect_timeout()
                return False
        except Exception, e:
            self._detect_timeout()
            self.logger.debug(" >>> framework debug >>> "+str(e))
            return False



    def _loadRecord(self):

        self.focusDate=None
        self.focusLength=None
        self.focusTitle=None
        self.status=None
        self.onGoing=False

        time.sleep(5)
        self.load_driver()
        time.sleep(3)

        try:
            if len(self.driver.find_elements_by_css_selector(".recordMosaic .itemsContainer .item"))>0:
                element = self.driver.find_element_by_css_selector(".recordMosaic .itemsContainer .item.zoomed")
                self.focusTitle = element.find_element_by_css_selector(".title").text
                details = element.find_element_by_css_selector(".grey").text

                enr = element.find_element_by_css_selector(".airingDatas").text
                self.onGoing = re.search(re.compile(Wording.W['pvrOnGoing']), enr) is not None

                dateTxt = "0/0/0"

                if len(details.split(" ")) == 6:
                    dateTxt = details.split(" ")[0]
                    self.focusLength = timedelta(hours = int(details.split(" ")[2]), minutes = int(details.split(" ")[4]))
                elif len(details.split(" ")) == 4:
                    dateTxt = details.split(" ")[0]
                    self.focusLength = timedelta(minutes = int(details.split(" ")[2]))
                elif len(details.split(" ")) <= 2:
                    dateTxt = details.split(" ")[0]

                self.focusDate = datetime(int(dateTxt.split("/")[2]),int(dateTxt.split("/")[1]),int(dateTxt.split("/")[0])).date()

                try:
                    element.find_element_by_css_selector(".status.failed")
                    self.status = "KO"
                except:
                    self.status ="OK"
                return True

            elif len(self.driver.find_elements_by_css_selector(".schedulingMosaic .itemsContainer .item"))>0:
                element = self.driver.find_element_by_css_selector(".schedulingMosaic .itemsContainer .item.zoomed")
                self.focusTitle = element.find_element_by_css_selector(".title").text
                details = element.find_element_by_css_selector(".grey").text
                dateTxt = details.split(" ")[0]
                hourTxt = details.split(" ")[2]
                self.focusDate = datetime(int(dateTxt.split("/")[2]),int(dateTxt.split("/")[1]),int(dateTxt.split("/")[0]),int(hourTxt.split(":")[0]),int(hourTxt.split(":")[1]))
                try:
                    element.find_element_by_css_selector(".status.failed")
                    self.status = "KO"
                except:
                    self.status ="OK"
                return True
            else:
                self._detect_timeout()
                return False

        except Exception, e:
            self._detect_timeout()
            self.logger.debug(" >>> framework debug >>> "+str(e))
            return False



    def _loadRecordFromRecPage(self):

        self.focusTitle=''
        self.beginRecTime=None
        self.endRecTime=None
        #self.onGoing=None

        time.sleep(3)
        self.load_driver()
        time.sleep(3)

        try:
            if len(self.driver.find_elements_by_css_selector(".dockCenter .info .times")) > 0:
                element = self.driver.find_element_by_css_selector(".dockCenter .info")
                self.focusTitle = element.find_element_by_css_selector(".title").text
                date = element.find_element_by_css_selector(".date").text
                details = element.find_element_by_css_selector(".times").text

                begin = details.split(" ")[0]
                end = details.split(" ")[2]

                beginHour = begin.split(":")[0]
                beginMinute = begin.split(":")[1]

                endHour = end.split(":")[0]
                endMinute = end.split(":")[1]

                self.beginRecTime = timedelta(hours = int(beginHour), minutes = int(beginMinute))
                self.endRecTime = timedelta(hours = int(endHour), minutes = int(endMinute))

                self.focusDate = datetime(int(date.split("/")[2]),int(date.split("/")[1]),int(date.split("/")[0])).date()

                return True
            else:
                self._detect_timeout()
                return False

        except Exception, e:
            self._detect_timeout()
            self.logger.debug(" >>> framework debug >>> "+str(e))
            return False


    def _loadFipInfoPolaris(self,epg=False):
        title = None
        date = None
        length = None
        begin = None
        end = None
        lcn = None
        onGoing = False
        channelDetail = None

        self.load_driver()
        try:
            channel =self.driver.find_element_by_css_selector(".fiptopleft .fipChannelNumberAndName").text
            lcn = int(channel.split('-')[0])
            nameSource = channel.split('-')[1]
            name = ""
            for i,e in enumerate(nameSource.split(" ")):
                if e!=u"TNT" and e!=u"IP"and e!=u"HD"and e!=u"SD":
                    if name=='':
                        name+=str(e)
                    else:
                        name+=' '+str(e)
            if "TNT" in nameSource:
                source = "TNT"
            else:
                source="IP"

            elements = self.driver.find_element_by_css_selector(".fiptopright")
            element = elements.find_element_by_css_selector(".fipDescription .fipPictosBox")
            definition = None
            if element.find_element_by_id("pictoDefinition").get_attribute("style").find("none")>-1:
                definition = "SD"
            else:
                definition = "HD"
            try:
                pictoDefinition = None
                pictoRatio = None
                pictoAudioDesc = None
                pictoContentVersion = None
                pictoDts = None
                pictoDolby = None
                pictoEar = None

                if element.find_element_by_id("pictoDefinition").get_attribute("style").find("none")>-1:
                    pictoDefinition = False
                else:
                    pictoDefinition = True
                if element.find_element_by_id("pictoAudioDesc").get_attribute("style").find("none")>-1:
                    pictoAudioDesc = False
                else:
                    pictoAudioDesc = True
                if element.find_element_by_id("pictoContent").get_attribute("style").find("none")>-1:
                    pictoContentVersion = False
                else:
                    pictoContentVersion = True
                if element.find_element_by_id("pictoDts").get_attribute("style").find("none")>-1:
                    pictoDts = False
                else:
                    pictoDts = True
                if element.find_element_by_id("pictoDolby").get_attribute("style").find("none")>-1:
                    pictoDolby = False
                else:
                    pictoDolby = True
                if element.find_element_by_id("pictoImpairedHearing").get_attribute("style").find("none")>-1:
                    pictoEar = False
                else:
                    pictoEar = True
                pictoCsa = int(str(element.find_element_by_css_selector(".pictoCsa").get_attribute("class")).split(' ')[1].lstrip("csa0"))
            except:
                pass

            channelDetail = ChannelDetail(name, definition, source)
            title = elements.find_element_by_css_selector(".fipTitles .title").text.encode('utf-8')
            if elements.find_element_by_css_selector(".fipTitles .recordPicto").get_attribute("style").find("display: none"):
                onGoing = True
            if len(elements.find_elements_by_css_selector(".fipActions .programDates"))>0:
                element = elements.find_element_by_css_selector(".fipActions .programDates").text.encode('utf-8')
                el = element.splitlines()
                l2 = el[1].split(' ')
            else:
                element = elements.find_element_by_css_selector(".fipButtons .programDates").text.encode('utf-8')
                l2 = element.split(' ')
            now =datetime.now().year
            date = datetime(now, int(l2[1].split('/')[1]),int(l2[1].split('/')[0]))
            begin = l2[3]
            end = l2[5]

            beginHour = begin.split("h")[0]
            beginMinute = begin.split("h")[1]

            endHour = end.split("h")[0]
            endMinute = end.split("h")[1]

            begin = timedelta(hours = int(beginHour), minutes = int(beginMinute))
            end = timedelta(hours = int(endHour), minutes = int(endMinute))
            length = end - begin
            startTime = datetime(date.year, date.month, date.day, int(beginHour), int(beginMinute))
            endTime = datetime(date.year, date.month, date.day, int(endHour), int(endMinute))



            elements = self.driver.find_element_by_css_selector(".fiptopleft ")
            if len(elements.find_elements_by_css_selector(".status.hidden"))>0:
                status= "OK"
            else:
                status = "KO"

            if epg:
                self.programInfo = ProgramInfoItem(lcn, nameSource, title, startTime, endTime, length, "", None, onGoing, "", pictoCsa, pictoDefinition, pictoRatio, pictoAudioDesc, pictoContentVersion, pictoDts, pictoDolby, pictoEar)
            else:
                self.recordItem = RecordItem(title, date.date(), length, begin, end, lcn, onGoing, status, channelDetail)
            return True
        except:
            self._detect_timeout()
            return False

    def _moveToTile(self, currentTile, currentX, destinationTile):
        '''
        Move cursor to a destination Tile

        @return: boolean
        '''
        deltaTile = destinationTile - currentTile
        move = False
        key = None
        try:
            if deltaTile < 0:
                key = "KEY_UP"
                move = True
            elif deltaTile > 0:
                key = "KEY_DOWN"
                move = True

            if currentX > 0:
                    keysX = ["KEY_LEFT"] * int(currentX)
                    self.remote.sendKeys(keysX)

            if move:
                keys = [key] * abs(deltaTile)
                self.remote.sendKeys(keys)

            return move
        except Exception, e:
            self.logger.debug(" >>> framework debug >>> "+str(e))
            return move



    def _getmonth(self,month):
        switcher = {
            "janvier": 1,
            "février":2,
            "mars": 3,
            "avril": 4,
            "mai": 5,
            "juin": 6,
            "juillet": 7,
            "août": 8,
            "septembre": 9,
            "octobre": 10,
            "novembre": 11,
            "décembre": 12
        }
        return switcher.get(month)

    def _getCSANumber(self,age):
        switcher ={
            10: 2,
            12: 3,
            16: 4,
            18: 5
        }
        return switcher.get(age,1)

    def _detect_timeout(self):
        if len(self.driver.find_elements_by_tag_name('body')) > 0:
            element = self.driver.find_element_by_tag_name('body')
            if element.text.find(u"Timeout getting result")>-1:
                self.logger.warning(" >>> framework error >>> DUMP TIMEOUT ")
