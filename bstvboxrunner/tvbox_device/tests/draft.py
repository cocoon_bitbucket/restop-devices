# -*- coding: utf-8 -*-

import time
from slugify import slugify
from pprint import pprint

from tvboxrunner.tvbox_device.stbdom.ardom import ArDomPage, ScreenView
from tvboxrunner.tvbox_device.stbdom.arviews import LiveBanner,ListItem,ToolBoxLive



import logging
log=logging.getLogger(__name__)



class Page(object):
    """

    """

    def _loadLiveBannerPolaris(self):

        self.load_driver()
        time.sleep(1)
        try:
            # ATTENTION nécessaire pour tests sprint 28 (absence de vzl)
            if len(self.driver.find_elements_by_css_selector(".infoBanner"))>=1:
                elements = self.driver.find_element_by_css_selector(".infoBanner")
            else:
                elements = self.driver.find_element_by_css_selector(".infoChannel.selected")

            # channel name, lcn, favorite
            element = elements.find_element_by_css_selector(".titleLine")
            # ATTENTION Gestion de la récupération du nom de chaine avant/après harmonisation avec la mosaaïque (nécessaire pendant valid sprint 28)
            if '-' in element.text:
                alltxt_list=element.text.split(u'-')
            else:
                alltxt_list=element.text.split(u'.')

            num = int(alltxt_list[0])
            txt=alltxt_list[1]
            if len(element.find_elements_by_css_selector(".favIcon"))>0:
                favorite = True
            else:
                favorite = False
            #current program info
            if len(elements.find_elements_by_css_selector(".infoProgram .programTitle"))>0:
                currentProgram = elements.find_element_by_css_selector(".infoProgram .programTitle").text
            elif len(elements.find_elements_by_css_selector(".infoProgram .slogan"))>0:
                currentProgram = elements.find_element_by_css_selector(".infoProgram .slogan").text
            else:
                currentProgram = ''

            if len(elements.find_elements_by_css_selector(".infoProgram .kind"))>0:
                genre = elements.find_element_by_css_selector(".infoProgram .kind").text
            else:
                genre = ''

            nextProgram = '' # A PRIORI DONNEES PAS DISPONIBLES

            element=elements.find_element_by_css_selector(".infoProgram .progressBarWidget .timeLeft")
            start = None
            length = None
            end = None
            recordOnGoing =len(elements.find_elements_by_css_selector(".infoProgram .progressBarWidget .middleContainer .cursorLeft .tooltip.record"))>0
            if len(element.text) == 5:
                hour = element.text.split(":")[0]
                minutes = element.text.split(":")[1]
                now=datetime.now()
                start = datetime(now.year, now.month, now.day, int(hour), int(minutes))

                element=elements.find_element_by_css_selector(".infoProgram .progressBarWidget .timeRight")
                if len(element.text) == 5:
                    hour = element.text.split(":")[0]
                    minutes = element.text.split(":")[1]
                    now=datetime.now()
                    end = datetime(now.year, now.month, now.day, int(hour), int(minutes))
                    if end < start:
                        end = end + timedelta(days = 1)
                    length = end - start

            element=elements.find_element_by_css_selector(".infoProgram .logoContainer")
            pictoDefinition = False
            pictoRatio = False
            pictoAudioDesc = False
            pictoContentVersion = False
            pictoDts = False
            pictoDolby = False
            pictoEar = False
            picto3D = False
            pictoVO = False
            pictoCsa = False

            try:

                if len(element.find_elements_by_css_selector(".secondaryLogo.hd"))>0:
                    pictoDefinition = True

                if len(element.find_elements_by_css_selector(".secondaryLogo.format16_9"))>0:
                    pictoRatio = True

                if len(element.find_elements_by_css_selector(".secondaryLogo.audioDesc"))>0:
                    pictoAudioDesc = True

                if len(element.find_elements_by_css_selector(".secondaryLogo.vm"))>0:
                    pictoContentVersion = True

                if len(element.find_elements_by_css_selector(".secondaryLogo.dolby"))>0:
                    pictoDolby = True

                if len(element.find_elements_by_css_selector(".secondaryLogo.impairedHearing"))>0:
                    pictoEar = True

                if len(element.find_elements_by_css_selector(".secondaryLogo.threed"))>0:
                    picto3D = True

                if len(element.find_elements_by_css_selector(".secondaryLogo.vovf"))>0:
                    pictoVO = True

                try:
                    pictoCsa = self._getCSANumber(int(str(elements.find_element_by_css_selector(".infoProgram .csa").get_attribute("class")).split(' ')[2].lstrip("CSA")))
                except Exception:
                    pass
            except Exception:
                pass

            self.programInfo = ProgramInfoItem(num, txt, currentProgram, start, end, length, genre, favorite, recordOnGoing, nextProgram, pictoCsa, pictoDefinition, pictoRatio, pictoAudioDesc, pictoContentVersion, pictoDts, pictoDolby, pictoEar, picto3D, pictoVO)
            return True

        except Exception, e:
            self._detect_timeout()
            print e
            return False




if __name__=="__main__":

    html_file = "samples/ardom_sample_1.html"

    logging.basicConfig(level=logging.DEBUG)


    def test_basics():
        """


        :return:
        """
        page= ArDomPage.from_file(html_file)

        print page.prettify()

        body=page.soup.body

        l= LiveBanner(page.soup)


        print l.get_child('start').text
        print l.get_child('end').text
        print l.get_child('genre').text


        l.feed()


        print l._data

        t= ToolBoxLive(page.soup)

        items= t.list_items()

        # active = [ tag for tag in items if not 'separator' in tag['class']]
        #
        # for e in active:
        #     for s in e.strings:
        #         print s

        t.feed()
        print t.get_active_index()

        delta= t.get_delta_index_for('> Multicam')
        assert delta == 0

        delta= t.get_delta_index_for('no such item')
        assert delta == None

        delta= t.get_delta_index_for(u'à la demande')
        assert delta == 1

        delta= t.get_delta_index_for(u'résumé')
        assert delta == 2

        delta= t.get_delta_index_for(u'chaînes  (non)')
        assert delta == 3

        delta= t.get_delta_index_for(u'langue (français)')
        assert delta == 4

        delta= t.get_delta_index_for(u'langue')
        assert delta == 4

        delta= t.get_delta_index_for(u'français')
        assert delta == 4


        labels = [
            '> Multicam',
            u'à la demande',
            u'résumé',
            u'chaînes  (non)',
            u'langue (français)'
            ]

        for l in labels:
            print slugify(l)


        items= t.getList()

        r= t.getLabelFromListFocus()

        r= t.findInList('résumé')
        assert r is True

        r= t.findInList('dummy')
        assert r is False

        return

    def test_live_banner():

        l= LiveBanner.from_file(html_file)

        # channel= l.get_child("channelName")
        # alltxt_list= channel.text.split(u' ')
        # num = int(alltxt_list[0])
        # txt = ""
        # inter = ""
        # for index in range(2, len(alltxt_list)):
        #     txt = txt + inter + alltxt_list[index]
        #     inter = " "

        l.feed()

        pprint(l._data)




        return


    #
    #
    #test_basics()
    test_live_banner()



    print "Done"