
import codecs
from bs4 import BeautifulSoup

from bstvboxrunner.tvbox_device.stbdom.ardom import ArDomPage
from bstvboxrunner.tvbox_device.stbdom.keyinfo import Keyinfo

import logging
logging.basicConfig(level=logging.DEBUG)



#filename= '../samples/stb-play/ardom-stbplay.html'
filename= '../samples/stb-blanche/ardom-stbblanche.html'


page= ArDomPage.from_file(filename)
pretty= page.prettify()

dom= page.soup

#dom = BeautifulSoup(codecs.open(filename, "r", "utf8"),'html.parser')


page = Keyinfo(dom)
data = page.feed()
data= page.export(data)


print "Done"