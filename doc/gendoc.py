#!/usr/bin/env python
import json
import re
import pprint
from restop_apidoc import DocGenerator

from bstvboxrunner.resources import DeviceResource as TvboxResource
from droydrunner.resources import DeviceResource as DroydResource
from tvstickrunner.resources import DeviceResource as TvstickResource
from tvstickappdriver.resources import DeviceResource as TvstickAppResource
from liveboxrunner.resources import DeviceResource as LiveboxRessource
from applications.samples.resources import DeviceResource as SamplesResource


doc_data = (
    # html_name , Resouurce , json_data
    [   "TvboxRunner",          TvboxResource ,         'tvbox_data'],
    [   "DroydRunner",          DroydResource,       ''  ],
    [   "TvstickRunner",        TvstickResource,     ''  ],
    [   "Tvstickappdriver",     TvstickAppResource,  ''  ],
    [   "LiveboxRessource",     LiveboxRessource,   'livebox_data'  ],
    [   "SamplesRessource",     SamplesResource,   'samples_data'],
)

# doc_data = (
#     # html_name , Resouurce , json_data
#     [   "TvboxRunner",          TvboxResource ,         'tvbox_data'],
#
# )



for html_filename,resource,json_filename in doc_data:

    d = DocGenerator(html_filename, resource)

    # gen robot framework doc
    d.setup()

    # gen json data

    if json_filename:
        json_data = json.dumps(d.data)
        json_data= re.sub('op_col_', '',json_data)
        json_data= re.sub('op_', '',json_data)

        with open('%s.json' % json_filename,'wb') as fh:
            fh.write(json_data)


#DocGenerator("TvboxRunner",TvboxResource).setup()
#DocGenerator("DroydRunner",DroydResource).setup()
#DocGenerator("TvstickRunner",TvstickResource).setup()
#DocGenerator("Tvstickappdriver",TvstickAppResource).setup()
#DocGenerator("LiveboxRessource",LiveboxRessource).setup()
#DocGenerator("SamplesRessource",SamplesResource).setup()

# d= DocGenerator("TvboxRunner",TvboxResource)
#
# d.setup()
# json_data= json.dumps(d.data)
#
# data= json.loads(json_data)
#
# print json_data
#pprint.pprint(json,indent=2)