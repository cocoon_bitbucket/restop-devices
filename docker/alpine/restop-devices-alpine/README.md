build the restop-devices image
===============================

restop-core + restop-devices


build the image
---------------

    docker build -t cocoon/restop-devices .
    docker push cocoo/restop-devices

try it
------

    docker run -ti  restop-alpine ash
    
    # /env/bin/python app.py


build the restop-devices:arm  image
====================================

get cocoon/restop image
-----------------------
    docker pull cocoon/restop:rpi

create arm Dockerfile
---------------------
    cp Dockerfile arm-Dockerfile
    
edit arm-Dockerfile
-------------------
    #FROM cocoon/restop
    FROM cocoon/restop:rpi


build it
--------

    docker build -f arm-Dockerfile -t cocoon/restop-devices:arm .
    docker push cocoon/restop-devices:arm
    


    
    



