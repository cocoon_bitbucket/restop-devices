
restop-run
==========


start the services
------------------
    docker-compose up &

stop the services
-----------------

    docker-compose pause
    
restart services
----------------
    docker-compose unpause




stop services and remove local images
-------------------------------------
    docker-compose down --rmi local



