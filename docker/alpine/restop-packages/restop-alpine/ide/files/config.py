# Statement for enabling the development environment
DEBUG = True

# Define the application directory
import os
BASE_DIR = os.path.abspath(os.path.dirname(__file__))


BLOCKY_APP_HOST= "0.0.0.0"
BLOCKLY_APP_PORT= 5016
BLOCKLY_APP_DEBUG= True


# distant restop platform
#RESTOP_MASTER_URL= 'http://localhost:5000/restop/api/v1'
RESTOP_MASTER_URL= 'http://localhost:/restop/api/v1'
RESTOP_PLATFORM_NAME= 'default'

# Define the database - we are working with
REDIS_HOST='localhost'
REDIS_PORT= 6379
REDIS_DB= 0



# Application threads. A common general assumption is
# using 2 per available processor cores - to handle
# incoming requests using one and performing background
# operations using the other.
THREADS_PER_PAGE = 2

# Enable protection agains *Cross-site Request Forgery (CSRF)*
CSRF_ENABLED     = True

# Use a secure, unique and absolutely secret key for
# signing the data.
CSRF_SESSION_KEY = "secret"

# Secret key for signing cookies
SECRET_KEY = "secret"