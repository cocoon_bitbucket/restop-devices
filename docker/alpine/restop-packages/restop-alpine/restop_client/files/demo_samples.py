# -*- coding: utf-8 -*-

__author__ = 'cocoon'

import time

import os
from restop_client import Pilot



phonehub_url= "http://localhost:5000/restop/api/v1"


#phonehub_url= "http://192.168.99.100:5000/restop/api/v1"
#phonehub_url= "http://10.179.1.246/restop/api/v1"



def test_fetch_restop_interface():
    """



    :return:
    """
    # to test localy


    p= Pilot()
    data= p.fetch_restop_interface(url=phonehub_url)
    return data


def test_samples_basic():

    p =Pilot()
    p.setup_pilot("demo", "demo_qualif", phonehub_url)


    try:

        res= p.open_Session('sample_1','sample_2')

        try:
            res= p.raise_error('sample_1')
        except:
            pass

        try:
            res= p.application_error('sample_1')
        except:
            pass

        try:
            res = p.not_implemented('sample_1')
        except:
            pass

        res= p.dummy('sample_1')
        res= p.dummy('sample_2')

        # read alarm on samples
        time.sleep(2)

        res = p.read('sample_1')
        #assert res.startswith('Signal')
        res = p.read('sample_2')
        #assert res.startswith('Signal')


        res= p.ping('sample_1')
        time.sleep(2)

        res= p.read('sample_1')
        assert res=='pong\n'

    except RuntimeError,e:
        print "interrupted: %s" % e.message

    except Exception,e:
        print e.message
    finally:
        r= p.close_session()

    return


if __name__=="__main__":


    test_fetch_restop_interface()

    test_samples_basic()


    print "Done."
