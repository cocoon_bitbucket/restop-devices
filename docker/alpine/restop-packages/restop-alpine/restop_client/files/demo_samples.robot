*** Settings ***
Documentation     standard tests
...

# libraries

#Library  restop_client.rf_baseplugin.Pilot
Library    restop_client

#Suite Setup  init suite
Suite Setup    fetch restop interface  ${platform_url}
Test Setup     setup pilot     ${platform_name}    ${platform_version}  ${platform_url}
Test Teardown  close session


*** Variables ***
${platform_name}=   demo
${platform_version}=    demo_qualif

#${platform_url}=  http://localhost:5001/restop/api/v1
${platform_url}=  http://localhost/restop/api/v1

#${pilot_mode}=  dry
${pilot_mode}=  normal



*** Keywords ***



############### units and macros


Unit demo samples
	[Arguments] 	${a1}  ${a2}
	[Documentation]  demo samples

	Open session	${a1}  ${a2}

    dummy  ${a1}
    dummy  ${a1}

    ping  ${a1}

    builtin.sleep  2

    ${received}=  read   ${a1}
    Log  ${received}
    #    assert res=='pong\n'







*** Test Cases ***

demo samples
  Unit demo samples  sample_1  sample_2

