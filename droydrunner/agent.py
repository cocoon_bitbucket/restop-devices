# -*- coding: utf-8 -*-

from android_device.mobiles import ADB,Mobile,adb_devices

class Agent(Mobile):
    """



    """
    def __init__(self,agent_model,**kwargs):
        """

        :param agent_model:
        :param kwargs:
        """
        self.model= agent_model
        self.agent_id= self.model.get_hash_id()
        self.kwargs= kwargs

        serial= self.model.parameters['username']
        self._set_serial(serial)


    @classmethod
    def adb_devices(cls):
        """

        :return:
        """
        return adb_devices()


    def start(self):
        """
            start the agent:
                check adb_devices not empty
                send WAKEUP to mobile

        :return:
        """
        try:
            devices= adb_devices()
        except RuntimeError as  e:
            self.model.logs.append("adb_devices: %s" % e.message)
            raise

        if len(devices) == 0:
            message= "no android devices connected"
            self.model.logs.append(message)
            raise RuntimeError(message)

        # retrieve agent serial
        #serial= self.model.parameters['username']
        #adb= ADB(serial)
        #rc= adb.cmd('shell input keyevent KEYCODE_WAKEUP')
        rc = self.adb.cmd('shell input keyevent KEYCODE_WAKEUP')
        #return self.model.make_response(True)
        return True

    def stop(self):
        """
            stop the fake terminal

        :return:
        """
        #return self.model.make_response(True)
        return True


    def wait_update(self):
        """
            wait for the update of the view

            Example:
            | Wait Update | # wait until the update of the view |
        """
        return self.device.wait.update()

    def click_by_resourceId(self, resourceId):
        """
            click on object with the specified ID

            Return true if object is found

            Example:
            | Click By ResourceId | com.orange.lacletv:id/polaris_bloc_inner_tv_live | # click on the object with the id com.orange.lacletv:id/polaris_bloc_inner_tv_live |
        """
        # find the specified Id, click on the object*
        # d(resourceId="com.orange.lacletv:id/polaris_bloc_inner_tv_live_now").click()
        rc = False
        tabs = self.device(resourceId=resourceId)
        for tab in tabs:
            # find child object
            obj = tab
            if obj:
                result = obj.click()
                return True
        return rc

    def click_by_desc(self, classNameMenu, description):
        """
            click on the object with the specified className which have the specified description

            Return true if object is found

            Example:
            | Click By Desc | android.widget.TextView  |  jeu | # click on the object with the description jeu in the android.widget.TextView  |
        """
        # d(className="android.app.ActionBar$Tab").child(text="Bluetooth")
        rc = False
        tab = self.device(description=description, className=classNameMenu)
        if tab:
            rc = tab.click()
            return True
        return rc

    def click_by_desc_contains(self, classNameMenu, description):
        """
            click on the object with the specified className with the description which contains the specified text

            Return true if object is found

            Example:
            | Click By Desc Contain | android.widget.TextView  |  jeu | # click on the object with the description jeu in the android.widget.TextView  |
        """
        rc = False
        tab = self.device(descriptionContains=description, className=classNameMenu)  #
        if tab:
            print("tab: %s " % str(tab.selector))
            rc = tab.click()
            return True
        return rc

    def click_first_by_resourceId(self, ressourceId, className):
        """
            click on the first object of the the specified className in the resourceId

            Return true if object is found

            Example:
            | Click First By ResourceId | com.orange.lacletv:id/catchup_tv_gridview_video  | android.widget.ImageView  | # click on the first android.widget.ImageView of com.orange.lacletv:id/catchup_tv_gridview_video   |
        """
        rc = False
        tabs = self.device(resourceId=ressourceId)
        for tab in tabs:
            # find child object
            obj = tab.child(instance=0, className=className)
            if obj:
                rc = obj.click()
                return True
        return rc

    def click_by_child_by_desc(self, ressourceId, channelName, className):
        """
            click on the child with the specified description in the specified className in the resourceId
            The search is done with scroll

            Return true if object is found

            Example:
            | Click By Child Desc | com.orange.lacletv:id/live_tv_channel_listview  | Démarrer la lecture de : M6  | android.widget.FrameLayout  | # click on the frame in the FrameLayout of the com.orange.lacletv:id/live_tv_channel_listview with the description Démarrer la lecture de : M6  |
        """
        rc = False
        tab = self.device(resourceId=ressourceId)
        obj = tab.child_by_description(channelName, allow_scroll_search=True, className=className)
        if obj:
            rc = obj.click()
            return True
        return rc