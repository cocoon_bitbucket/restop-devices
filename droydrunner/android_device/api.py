
import inspect


from mobiles import Mobile



def get_keywords():
    """
        extract keywords from methods name

    :return:
    """
    keywords= set()

    # add keywords for mobiles
    for name, func in inspect.getmembers(Mobile, predicate=inspect.ismethod):
        if not name.startswith('_'):
            keywords.add(name)
    keywords.add('adb_devices')


    return keywords




if __name__=="__main__":

    ks= get_keywords()

    print(ks)
