__author__ = 'cocoon'


import os
import time
import subprocess
import uuid

from uiautomatorlibrary.Mobile import Mobile as BaseMobile
from uiautomatorlibrary.Mobile import ADB as BaseADB



#cmd_adb_devices = '/usr/local/bin/adb'
cmd_adb_devices = '/usr/local/dummy'

def adb_devices(cmd= None):
    """
        list connected android devices

        return a list of array
            0: device_id
            1: status

        eg: [
                ['e7f54be6', 'device'],
                ['388897e5', 'device']
            ]


    """
    cmd= ADB.find_adb()
    if not cmd:
        raise RuntimeError('cannot find adb executable')

    device_list=[]
    result = subprocess.Popen( "%s devices" % cmd, shell=True, stdout=subprocess.PIPE, stderr=subprocess.PIPE).communicate()
    lst = result[0].split("\n")
    # skip first line
    lst.pop(0)
    for line in lst:
        if line:
            device_id,status = line.split('\t')
            device_list.append([device_id,status])
    return device_list



class Mobile(BaseMobile):
    """
        interface to a single mobile

    """
    def wait_update(self):
        """

        """
        return self.device.wait.update()

    def wait_idle(self):
        """

        """
        return self.device.wait.idle()

    def dump(self,filepath=None):
        """
            get the dumped window hierarchy content(unicode)
        """
        content= self.device.dump()
        print "dump:\n"
        print content
        print "---\n"
        return unicode(content)

    def flash(self,scale=1.0,quality=100):
        """
            return base64 screenshot

        :return:
        """
        self.device.screenshot("image.png",scale=scale,quality=quality)
        with open("image.png","rb") as fh:
            data = fh.read()
            encoded= data.encode("base64")
            os.unlink("image.png")
            print("flash:\n")
            print encoded
            print("----\n")
            return encoded


    def get_object_data(self, **selectors):
        """

            DEPRECATED use get_object instead

        Get data dictionary of the UI object with selectors *selectors*

        See `introduction` for details about identified UI object.

        Example:
        | ${main_layer} | Get object_data | className=android.widget.FrameLayout | index=0 | # Get main layer which class name is FrameLayout |
        """
        obj= self.device( **selectors)
        return obj.selector


    def scroll_to_vertical(self, scroll_class,**selectors):
        """
        Scroll(vertically) on the object found with classname=scroll_class: obj to specific UI object which has *selectors* attributes appears.

        Return

        Example:

        | ${obj_data}    | Get Object Data      | className==android.widget.ListView |              | # Get the list object     |
        | ${found} | Scroll to Vertical | scroll_class=${obj_data['className']} | text=WebView | # Scroll to text:WebView. |
        """
        # LT: new keyword
        scroll_obj= self.device(className=scroll_class)
        is_here= scroll_obj.scroll.vert.to(**selectors)
        return  is_here

    def scroll_to_horizontal(self, scroll_class,**selectors):
        """
        Scroll(horizontally) on the object found with classname=scroll_class: obj to specific UI object which has *selectors* attributes appears.

        Return data of the target obj

        Example:

        | ${obj_data}    | Get Object Data      | className==android.widget.ListView |              | # Get the list object     |
        | ${target_data} | Scroll to Horizontal | scroll_class=${obj_data['className']} | text=WebView | # Scroll to text:WebView. |
        """
        # LT: new keyword
        scroll_obj= self.device(className=scroll_class)
        is_here= scroll_obj.scroll.horiz.to(**selectors)
        return  is_here


    # d(className="android.app.ActionBar$Tab").child(text="Bluetooth")
    def click_tab_by_text(self,className, text):
        """
            select all objects with className  ( eg android.app.ActionBar$Tab )
            for each find a child with text= text
            if found : click on it

        """
        # d(className="android.app.ActionBar$Tab").child(text="Bluetooth")
        result=False
        tabs= self.device(className=className)
        for tab in tabs:
            # find child object
            obj= tab.child(text=text)
            if obj:
                result= obj.click()
                return True
        return result

    def click_linear_by_text(self,className, text):
        """
            select all objects with className  ( eg android.app.ActionBar$Tab )
            for each find a child with text= text
            if found : click on it

        """
        # android.widget.LinearLayout
        result=False
        tabs= self.device(className=className,clickable=True,scrollable=False)
        for tab in tabs:
            # find child object
            print "tab: %s" % str(tab.selector)
            obj= tab.child(text=text,className="android.widget.TextView")
            if obj:
                print "obj: %s " % str(obj.selector)
                result= obj.click()
                return True
        return result


    def snapshot(self,name=None,scale=1.0,quality=100):
        """
            take a screenshot  and save to  snapshot_<name>.png
            take a dump and save to snapshot_<name>.uix

            if no name specified take a random uuid

        """
        if not name:
            name=uuid.uuid4()
        png_name= "snapshot_%s.png" % name
        uix_name= "snapshot_%s.uix" % name
        # take the screenshot
        self.device.screenshot(png_name,scale,quality)
        # dump to uix
        self.device.dump(uix_name)
        #logger.info('\n<a href="%s">%s</a><br><img src="%s">' % (screenshot_path, st, screenshot_path), html=True)
        return True

    #
    # telephony functions
    #

    # def op_call(self,item,**kwargs):
    #     """
    #         cocoon:~ cocoon$ adb -s 95d03672 shell am start -a android.intent.action.CALL -d tel:555-5555
    #         Starting: Intent { act=android.intent.action.CALL dat=tel:xxx-xxxx }
    #
    #     """
    #     data= self.request.json
    #     destination= data['destination']
    #
    #     return self.call(item,destination=destination)


    def _telephony_call(self,device_serial,destination):
        """
            cocoon:~ cocoon$ adb -s 95d03672 shell am start -a android.intent.action.CALL -d tel:555-5555
            Starting: Intent { act=android.intent.action.CALL dat=tel:xxx-xxxx }

        """
        m=ADB(device_serial)
        rc= m.cmd('shell am start -a android.intent.action.CALL -d tel:%s' % destination)
        return rc



    # def op_wait_incoming_call(self,item,**kwargs):
    #     """
    #
    #         check state of incoming call: dumpsys telephony.registry | grep "mCallState"
    #
    #
    #         based on the call state I can accept the call by
    #
    #         adb -s <serial no> shell input keyevent 5
    #
    #         and reject the call by
    #         adb -s <serial no> shell input keyevent 6
    #
    #
    #         Now if I want to make a conference call, I have to hold the call then dial again and on receive
    #         I will have to merge the call. I have used KEYCODE_BREAK as I found it as the break and pause button
    #         from the page android keyevent
    #
    #         I also need the merge button key map.
    #
    #
    #     """
    #     #t0= time.time()
    #     n=10
    #     incoming= ''
    #     logs=[]
    #     while n >= 0:
    #         incoming = self.has_incoming_call(item)
    #         if incoming:
    #             logs.append('incoming call from: %s' % incoming)
    #             return self.response(incoming,logs=logs)
    #         time.sleep(1)
    #         n=n-1
    #     message= 'No Incoming Call'
    #     logs.append(message)
    #     return self.response(message,logs=logs,code=520)




    # def op_answer_call(self,item,code='200',**kwargs):
    #     """
    #
    #     """
    #     data= self.request.json
    #     code= str(data['code'])
    #
    #
    #     logs=[]
    #     agent_data= self.backend.item_get(self.collection,item)
    #     serial= agent_data['parameters']['device_id']
    #
    #     adb=ADB(serial)
    #
    #     message=''
    #     if code == '200' :
    #         # accept call
    #         keycode= 5
    #         message= 'accept call'
    #         logs.append(message)
    #     else:
    #         #reject call
    #         message= 'reject call'
    #         logs.append(message)
    #         keycode= 6
    #
    #     r= adb.cmd('shell input keyevent %s' % str(keycode))
    #
    #
    #     return self.response(r,logs=logs)
    #
    #     #rc= { 'code': 200, 'message': r, 'logs':[]}
    #     #return rc


    def _telephony_hangup(self,device_serial):
        """
        adb shell input keyevent KEYCODE_ENDCALL

        """
        adb=ADB(device_serial)
        rc= adb.cmd('shell input keyevent KEYCODE_ENDCALL' )
        return rc

        #rc= { 'code': 200, 'message': r, 'logs':[]}
        #return rc




    # def op_check_call(self,item,**kwargs):
    #     """
    #
    #     :param item:
    #     :param kwargs:
    #     :return:
    #     """
    #     data= self.request.json
    #     state_wanted= str(data['state'])
    #
    #     states= ['CONFIRMED','DISCONNECTED']
    #     if not state_wanted in states:
    #         raise ValueError('incorrect state: %s' % state_wanted)
    #
    #     n=10
    #     logs=[]
    #     while n >= 0:
    #         registry= self.telephony_registry(item)
    #         state= registry['fields']['mCallState']
    #         if state== '0':
    #             # no pending call
    #             if state_wanted == 'DISCONNECTED':
    #                 return self.response('no pending call')
    #         else :
    #             # a call is ongoing
    #             if state_wanted == 'CONFIRMED':
    #                 return self.response('ongoing call')
    #         time.sleep(1)
    #         n=n-1
    #     # not reached
    #     message= 'failed to match state: %s' % state_wanted
    #     return self.response(message,logs=logs,code=520)
    #


    #
    #  internal methods
    #

    def telephony_registry(self,device_serial):
        """


            check state of incoming call: dumpsys telephony.registry

        """
        labels=['mCallState','mCallIncomingNumber','mCallForwarding']

        #agent_data= self.backend.item_get(self.collection,item)
        #serial= agent_data['parameters']['device_id']

        adb=ADB(device_serial)

        r= adb.cmd('shell dumpsys telephony.registry')

        if r:
            result= { 'text': r[0], 'fields': {}}

            lines= r[0].split('\n')
            for index,line in enumerate(lines):
                if '=' in line:
                    try:
                        key,value= line.split('=',1)
                        result['fields'][key.strip()]=value.strip()
                    except Exception,e:
                        error_key= 'scan_error_%d' % index+1
                        error_value= e.message
                        result['fields'][error_key]= error_value
        else:
            result= { 'text': 'dumpsys telephony.registry has failed', 'fields': {}}
        return result


    def has_incoming_call(self,device_serial):
        """


            check state of incoming call: dumpsys telephony.registry | grep "mCallState"

        """
        registry= self.telephony_registry(device_serial)
        if registry['fields'].get('mCallState','0') != '0':
            return registry['fields'].get('mCallIncomingNumber','')
        else:
            return None



    def shell_input_keyevent(self,device_serial,keycode):
        """

        :param device_serial:
        :return:
        """
        adb = ADB(device_serial)
        rc = adb.cmd('shell input keyevent %s' % str(keycode))
        return rc



class ADB(BaseADB):
    """

    """

    @classmethod
    def devices(cls):
        """

        :return: a list of connected devices  (device_id,status)
        """
        return adb_devices()