

DEVICE_NAME= "droydrunner_agents"

from walrus import *
from wbackend.model import Model
from wbackend.graph import Graph

from restop_platform.models import *


class DroydrunnerAgents(AgentModel):
    """

        parameters: {u
            'profile': u'android',
            u'username': u'0915f9ece8942b04', u'domain': u'android',
            u'collection': u'droydrunner_agents', u'password': u'0000',
            u'id': u'+33784109762'}

    """
    collection= DEVICE_NAME

