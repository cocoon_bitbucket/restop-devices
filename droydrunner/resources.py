__author__ = 'cocoon'



import time
from copy import copy

from restop.application import ApplicationError,ApplicationResponse
from models import DEVICE_NAME
from models import DroydrunnerAgents

from agent import Agent, adb_devices
from restop.plugins.resources import RootResource, Resource

from restop_platform.resources import AgentResource


class Resource_Root(RootResource):
    """

    """
    collection= 'root'


class DeviceResource(AgentResource):
    """
        DROYDRUNNER  : an Android Device

    """
    collection= DEVICE_NAME
    protected= ''

    _AgentClass = Agent


    def configure_session(self,session_id,members,configuration):
        """

        :param session_id:
        :param members:
        :param configuration:
        :return:
        """
        return configuration

    def op_col_open_session(self, item, **kwargs):
        """
            open a local session

            POST /droydrunner_agents/-/open_session

        :return:
        """
        #super(DeviceResource,self).op_col_open_session(item,**kwargs)
        logs=[]
        agents= []
        data= self.request.json
        session_id= data['session_id']
        members= data['members']
        configuration=data['configuration']
        session_configuration= self.configure_session(session_id,members,configuration)
        # create agents
        for member_alias in members:
            agent=self.create_agent(member_alias,session_configuration[member_alias],session_id=session_id)
            agents.append(agent.get_id())

        # check that all devices are connected
        try:
            devices= adb_devices()
        except RuntimeError as e:
            message= "adb devices failed: %s" % e.message
            logs.append( message)
            raise ApplicationError('droydrunner open session failed',logs=logs)

        # check connected devices
        device_connected={}
        if not devices:
            # no device connected
            message= "No devices connected"
            logs.append(message)
            if len(members):
                #
                raise ApplicationError('droydrunner open session failed',500, logs=logs)
            else:
                # if no members do no raise application error
                pass
        else:
            device_connected={}
            for entry in devices:
                # serial => status
                serial= entry[0]
                status= entry[1]
                device_connected[serial]= {'status': status}
                logs.append("android device: %s is connected with status: %s" % (serial,status))

        # log connected devices
        #logs.append("connected devices: %s" % str(devices))
        # check all members are connected
        # collect all members serial num
        # serials= []
        # for device in DroydrunnerAgents.all():
        #     serial= device.parameters['username']
        #     serials.append(serial)

        # check each member is connected
        not_connected_members=[]
        for member_alias in members:
            #print member_alias
            member_data= session_configuration[member_alias]
            serial= member_data['username']
            if serial in device_connected.keys():
                logs.append("member %s connected with serial %s" % (member_alias,serial))
            else:
                not_connected_members.append(member_alias)
                logs.append("member %s is NOT connected (serial:%s)" % (member_alias,serial))
            if len(not_connected_members):
                raise ApplicationError("droydrunner open session has failed",logs=logs)

        # prepare  response
        data= { "agents": agents, "session_configuration":session_configuration}
        return { 'result':200, 'message':data,'logs':logs}


    def op_col_close_session(self, item, **kwargs):
        """
            close a local session

            POST /hub/droydrunner_agents/-/close_session
                body:
                    session_id
                    members

        :return:
        """
        # assert item == '-'
        data = self.request.json

        session_id = data['session_id']
        members = data['members']

        message = "close session: %s , with members: %s" % (session_id, str(members))

        return {'result': 200, 'message': data, 'logs': [message]}



    def op_col_adb_devices(self,item='-'):
        """

            list all connected devices

        :param item:
        :return:
        """
        # item should be '-'
        devices= self._AgentClass.adb_devices()
        response= ApplicationResponse(message=devices,result=200,logs=[])
        return response


    def op_dummy(self,item,**kwargs):
        """
         POST /restop/api/v1/hub/samples/1/dummy

        :param item:
        :param kwargs:
        :return:
        """
        return dict( result=200 , message="OK" , logs=['operation dummy called on item %s' % item])





    #
    # telephony_functions
    #
    def op_call(self,item,**kwargs):
        """
            cocoon:~ cocoon$ adb -s 95d03672 shell am start -a android.intent.action.CALL -d tel:555-5555
            Starting: Intent { act=android.intent.action.CALL dat=tel:xxx-xxxx }

        """
        data = self.request.json
        destination = data['destination']

        agent_model = self.model.load(item)
        agent_model.logs.append('agent [%s/%s] received telephony call command to %s' % (
            self.model.collection, item,destination))

        agent = Agent(agent_model)
        serial= agent.model.parameters['username']
        rc = agent._telephony_call(serial,destination=destination)

        return agent.model.make_response(rc)

    def op_wait_incoming_call(self,item,**kwargs):
        """

            check state of incoming call: dumpsys telephony.registry | grep "mCallState"


            based on the call state I can accept the call by

            adb -s <serial no> shell input keyevent 5

            and reject the call by
            adb -s <serial no> shell input keyevent 6


            Now if I want to make a conference call, I have to hold the call then dial again and on receive
            I will have to merge the call. I have used KEYCODE_BREAK as I found it as the break and pause button
            from the page android keyevent

            I also need the merge button key map.


        """
        agent_model = self.model.load(item)
        agent_model.logs.append('agent [%s/%s] received telephony wait_incoming_call command' % (
            self.model.collection, item))
        serial = agent_model.parameters['username']
        agent = Agent(agent_model)

        n=10
        incoming= ''
        logs=[]
        while n >= 0:
            incoming = agent.has_incoming_call(serial)
            if incoming:
                agent.model.logs.append('incoming call from: %s' % incoming)
                return agent.model.make_response(incoming)
            time.sleep(1)
            n=n-1
        message= 'No Incoming Call'
        agent.model.logs.append(message)
        return agent.model.make_response(message,result=520)


    def op_hangup(self, item):
        """
        adb shell input keyevent KEYCODE_ENDCALL

        """
        agent_model = self.model.load(item)
        agent_model.logs.append('agent [%s/%s] received telephony hangup command' % (
            self.model.collection, item))
        serial = agent_model.parameters['username']
        agent = Agent(agent_model)

        rc= agent._telephony_hangup(serial)

        return agent_model.make_response(rc)


    def op_check_call(self,item,**kwargs):
        """

        :param item:
        :param kwargs:
        :return:
        """
        data= self.request.json
        state_wanted= str(data['state'])
        states= ['CONFIRMED','DISCONNECTED']
        if not state_wanted in states:
            raise ValueError('incorrect state: %s' % state_wanted)

        agent_model = self.model.load(item)
        agent_model.logs.append('agent [%s/%s] received telephony check_call command' % (
            self.model.collection, item))
        serial = agent_model.parameters['username']
        agent = Agent(agent_model)

        n=10
        logs=[]
        while n >= 0:
            registry= agent.telephony_registry(serial)
            state= registry['fields']['mCallState']
            if state== '0':
                # no pending call
                if state_wanted == 'DISCONNECTED':
                    return agent_model.make_response('no pending call')
            else :
                # a call is ongoing
                if state_wanted == 'CONFIRMED':
                    return agent_model.make_response('ongoing call')
            time.sleep(1)
            n=n-1
        # not reached
        message= 'failed to match state: %s' % state_wanted
        return agent_model.make_response(message,result=520)


    def op_answer_call(self,item,code='200',**kwargs):
        """

        """
        data= self.request.json or {}
        code= str(data.get('code',200))

        agent_model = self.model.load(item)
        agent_model.logs.append('agent [%s/%s] received telephony answer_call command' % (
            self.model.collection, item))
        serial = agent_model.parameters['username']
        agent = Agent(agent_model)

        message=''
        if code == '200' :
            # accept call
            keycode= 5
            message= 'accept call'
            agent_model.logs.append(message)
        else:
            #reject call
            message= 'reject call'
            agent_model.logs.append(message)
            keycode= 6

        rc= agent.shell_input_keyevent(serial,str(keycode))
        #r= adb.cmd('shell input keyevent %s' % str(keycode))

        return agent_model.make_response(rc)


    #
    # high level function
    #
    def op_call_user(self, item, **kwargs):
        """

        /$agent_url/?/call_user  userX , format='universal

        :param userA:
        :param userX:
        :param format:
        :return:
        """
        input = self.request.json

        userX = input.get('userX',None)
        format = input.get('format', 'universal')

        agent_model = self.model.load(item)
        agent_model.logs.append('agent [%s/%s] received telephony call_user command' % (
            self.model.collection, item))

        # find target user
        try:
            destination_agent = DroydrunnerAgents.get(
                (DroydrunnerAgents.name==userX ) &
                (DroydrunnerAgents.session==agent_model.session))
        except Exception as e:
            return { 'result': 404 , 'message':None, 'logs':['cannot find destination agent %s' % str(userX)]}

        #device_data = self.backend.device_get_by_alias(userX)
        #tel_number = device_data['id']

        tel_number= destination_agent.parameters['id']

        # tel_number= self.SessionClass.get_sip_address(user=userX,format=format)
        if tel_number.startswith('sip:'):
            tel_number = tel_number[4:]
        if '@' in tel_number:
            tel_number = tel_number.split('@', 1)[0]


        serial = agent_model.parameters['username']
        agent = Agent(agent_model)

        res = agent._telephony_call(serial, destination=tel_number)
        return agent_model.make_response(res)

