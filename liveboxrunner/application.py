__author__ = 'cocoon'
"""

    /hub/samples/-/open_session    { session=1,  members= [] , configuration= {} }
                                  return urls of agents
    /hub/samples/-/close_session

    /hub/samples/1/start
    /hub/samples/1/stop

"""

from flask import request

from restop.application import get_application
from wbackend.model import Model


from restop.collection import GenericCollectionWithOperationApi

from models import DEVICE_NAME

# import resources
import resources




#
# create flask app
#

# get a flask application
app = get_application(__name__,with_error_handler=True)

app.config.from_pyfile('config.cfg')

redis_url= app.config.get('RESTOP_REDIS_URL',"")

# backend= Backend.new(redis_url,
#                      workspace_base=app.config['WORKSPACE_ROOT'],
#                      spec_base= app.config["SPEC_ROOT"],
#                      config=app.config)


#
app.config['backend'] = Model

#
# create collection blueprint
#
blueprint_name='restop_sample'
#url_prefix= '/restop/api/v1'

#url_prefix= app.config['RESTOP_URL_PREFIX'] + '/hub'
url_prefix= app.config['RESTOP_URL_PREFIX']

collections= [ "root", DEVICE_NAME]


app.config['collections']= {}
app.config['collections'][blueprint_name]= collections


my_blueprint= GenericCollectionWithOperationApi.create_blueprint(blueprint_name,__name__,url_prefix=url_prefix,
                collections= collections)

# register restop blueprint
app.register_blueprint(my_blueprint,url_prefix='')


#
#  urls
#

@app.route('/')
def index():
    """

    :return:
    """
    root_url=  "%s%s" % (request.base_url[:-1],app.config['RESTOP_URL_PREFIX']+'/hub')
    return 'hello from restop server: try <a href="%s">%s</a>' % (root_url,root_url)



def start_server(host=None,port=None,debug=True,threaded=True,**kwargs):
    """


    :param kwargs:
    :return:
    """

    host= host or app.config['RESTOP_HOST']
    port= port or int(app.config['RESTOP_PORT'])

    app.run(host=host, port=port, debug=debug, threaded=threaded)



if __name__=="__main__":

    from yaml import load
    import time
    from restop_platform.models import *

    import logging
    logging.basicConfig(level=logging.DEBUG)


    # ...
    db = Database(host='localhost', port=6379, db=1)
    Model.bind(database=db,namespace=None)

    try:
        p= Platform.get(Platform.name=='default')
    except Exception as e:
        # does not exists create it
        db.flushdb()
        stream = file('../../tests/platform.yml')
        data = load(stream)
        p = Platform.create(name='default', data=data)
        p.setup()


    p = Platform.get(Platform.name == 'default')


    start_server(host='0.0.0.0', port=5001, debug=True, threaded=True)
