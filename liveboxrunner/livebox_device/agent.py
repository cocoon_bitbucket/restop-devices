"""
    native agent

    use NewTvTesting with a

    * built-in Remote Controller
        ( drive stb http remote controller instead of IR )
    * or a local IR controller
        ( yet not available )



"""
import os
import time
import datetime

from restop.application import ApplicationResponse,ApplicationError

from restop_components.some_agents.pipe_agents import TelnetAgent


#from restop_components.powerswitch_device import PowerswitchPlug
from restop_components.powerswitch_device.powerswitch_client import PowerswitchClient

from socketconnector.boxconnector import LiveboxConnector

base = os.getcwd()


#class Agent(BaseAgent):
class Agent(TelnetAgent):
    """

        livebox  Agent
        -----------

        an agent to drive orange livebox

        * send commands and read logs via a telnet session


    """
    collection= 'livebox_agents'


    _default_parameters= dict (
        external_url= 'localhost',
        grafana_url= "localhost:3000/dashboard/db/restop" ,
        graphite_address= "localhost:8000",
        default_metrics= ['df', 'cpu','meminfo','loadavg'],
        telnet_user='root',
        telnet_password='sah',
        telnet_port= '23',
        peer_ip="localhost",
        with_curl=False,
        scheduler_feedback_url='http//localhost/feedback/stats',
    )


    @classmethod
    def _autodoc(cls,info):
        """
            generate autodocumentation

        : param info: instance of ScanMethods  (from restop.tools import ScanMethods)
        :param kwargs:
        :return:
        """
        # add own methods ( scheduler , telnet operation )
        info.add_methods(cls)
        # add methods for livebox connector
        info.add_methods(LiveboxConnector)
        return info


    # others
    def _setup(self):
        """
            create
                a remote: remote controller fot sending ir keys
                a serial: serial interface with stb
        :return:
        """
        super(Agent,self)._setup()

        self.powerswitch_port= None

        # add powerswitch driver
        if 'powerswitch_device' in self.model.parameters:
            if 'powerswitch_port' in self.model.parameters:
                self.powerswitch_port= self.model.parameters["powerswitch_port"]
                powerswitch_model= self.model.parameters['powerswitch_device']
                redis_pool= self.model.database.connection_pool
                self._powerswitch= PowerswitchClient(redis_pool,powerswitch_model)
                # self._powerswitch = PowerswitchPlug.get(
                #     self.model.parameters['powerswitch_device'],
                #     channel=self.model.parameters['powerswitch_port']
                # )
            else:
                raise ApplicationError('no powerswitch_port declared in platform for device: %s' % self.alias)
        else:
            self._powerswitch = None

        # # add powerswitch driver
        # if 'powerswitch_device' in self.model.parameters:
        #     if 'powerswitch_port' in self.model.parameters:
        #         self._powerswitch= PowerswitchPlug.get(
        #             self.model.parameters['powerswitch_device'],
        #             channel= self.model.parameters['powerswitch_port']
        #         )
        #     else:
        #         raise ApplicationError('no powerswitch_port declared in platform for device: %s' % self.alias)
        # else:
        #     self._powerswitch=None

        # add socket connector
        self.livebox_connector= LiveboxConnector(
            logger=self.log,
            host= self.model.parameters.get('socket_connector_ip','localhost'),
            port= self.model.parameters.get('socket_connector_port',23),
            livebox_ip= self.model.parameters.get('peer_ip','192.168.1.1'),
            user= self.model.parameters.get('http_user',"admin"),
            password= self.model.parameters.get('http_password',"admin1234"),
        )
        return True

    def start(self):
        """
            LiveboxAgent start

            start the loop proxy between redis queues and tv serial port

        :return:
        """
        rc= super(Agent,self).start()
        return rc


    def stop(self):
        """

        :return:
        """
        rc= super(Agent,self).stop()



    def telnet_watch(self,timeout=5):
        """
            watch serial log for a duration
        :param timeout:
        :return:
        """
        return self.watch(timeout=timeout)

    def telnet_expect(self,pattern,timeout=5,cancel_on=None, regex='no', **kwargs ):
        """
            read serial to find the pattern

        :param pattern:
        :param timeout:
        :return:
        """
        return self.expect(pattern,timeout=5,cancel_on=cancel_on, regex=regex, **kwargs )


    def telnet_sync(self,timeout=10):
        """
            empty the serial log
        :param timeout:
        :return:
        """
        return self.sync(timeout=timeout)

    def telnet_send(self,cmd):
        """

            send a command to stb via serial port
        :param cmd:
        :return:
        """
        return self.send(cmd)


    #
    #  powerswitch interface
    #

    def power_on(self):
        """

        :return:
        """
        if self._powerswitch:
            return self._powerswitch.switch_on(self.powerswitch_port)
        else:
            raise ApplicationError('no powerswitch declared in platform')

    def power_off(self):
        """

        :return:
        """
        if self._powerswitch:
            return self._powerswitch.switch_off(self.powerswitch_port)
        else:
            raise ApplicationError('no powerswitch declared in platform')

    def power_status(self):
            """

            :return: list of outlet status 0=ON , 1:OFF
            """
            if self._powerswitch:
                response= self._powerswitch.status()
                return response["message"]
            else:
                raise ApplicationError('no powerswitch declared in platform')

    #
    #  statscheduler interface
    #
    def init_metrics(self,metrics):
        """

        :param metrics:
        :return:
        """
        rc= self._scheduler.init_metrics(metrics)
        return rc



    # def PrintInfo(self):
    #     """
    #
    #     :return:
    #     """
    #     rc= self.livebox_connector.PrintInfo()
    #     return rc


    def __getattr__(self, item):
        """
            no attribute
        :param item:
        :return:
        """
        try:
            # try to find a method of livebox connector
            op= getattr(self.livebox_connector, item)
            # start liveboxconnector
            self.livebox_connector._connect()

        except AttributeError:
            raise ApplicationError(message='inexistant method',result=405)

        return  op