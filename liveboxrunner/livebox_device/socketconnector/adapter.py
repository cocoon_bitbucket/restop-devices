from restop_adapters.telnet_adapter import TelnetBaseAdapter
from boxconnector import LiveboxConnector

class SocketConnectorBaseAdapter(TelnetBaseAdapter):
    """


        base adapter for livebox SocketConnector


    """

    def __init__(self, agent_id, host="localhost", user=None, password=None, port=23, log=None, redis_db=None,
                 livebox_ip='192.168.1.1', **kwargs):
        """

        :param agent_id:
        :param host:
        :param user:
        :param password:
        :param port:
        :param log:
        :param redis_db:
        :param kwargs:
        """
        super(SocketConnectorBaseAdapter,self).__init__(self,agent_id,
                                                        host=host,user=user,password=password,port=port,
                                                        log=log,redis_db=redis_db)
        # boxconnector has no user or password
        self.user=None
        self.password=None

        self._livebox_user= user
        self._livebox_password= password

        self._livebox_ip= livebox_ip

        self._api= LiveboxConnector(host=self.host,port=self.port,livebox_ip=self._livebox_ip)

    def transfer_in(self):
        """

            transfer redis stdin input to slave stdin ( if any )

        :return:
        """
        # read redis queue in
        # incoming_cmd= self.q_in.get(timeout= 1)

        size = len(self._stdin)
        if size > 0:
            # pop next command from redis stdin
            incoming_cmd = self._stdin.popleft()
            if incoming_cmd == self.cmd_exit:
                # exit command
                self.log.debug('adapter for %s received exit command' % self.agent_id)
                self.Terminated = True
            # test id com is in api

            else:
                self._execute_command(incoming_cmd)
                #self.send(incoming_cmd)

    def _execute_command(self,command,**kwargs):
        """

        :param command:
        :param kwargs:
        :return:
        """
        raise NotImplementedError()