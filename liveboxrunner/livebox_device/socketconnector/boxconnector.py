# -*- coding: utf-8 -*-
"""



"""
import time
import re

from telnetlib import   Telnet

from restop.application import ApplicationError

command_success= 'command success'


class LiveboxConnector(object):
    """

    """
    #   ... <key=value> ...
    # key value separated with =   <key = value>
    _key_value_pattern= re.compile("\<([_\w]+)\s*\=(.*)\>")
    # key value separated with :   key : value
    _key_value_pattern_2= re.compile("([_\w]+)\s*\:(.*)")


    def __init__(self,host='127.0.0.1',port=23, user='admin',password='admin1234',
                 livebox_ip='192.168.1.1',livebox_port=23,logger=None):
        """

        :param host:
        :param port:
        """
        self._host= host
        self._port= port
        self.user=user
        self.password=password
        self.livebox_ip=livebox_ip
        self.livebox_port= livebox_port
        self.logger=logger

        #self._connect()
        # try:
        #     self.logger.debug("connecting to boxconnector host:%s, port:%s" % (self._host,self._port))
        #     self._telnet= Telnet(self._host,self._port,timeout=5)
        #     self.logger.debug("boxconnector connected at %s:%d" % (host,port))
        #     # send a dummy ln
        #     self._telnet.write('\n')
        # except Exception, e:
        #     self.logger.debug("cannot connect to boxconnector at %s:%d" % (host,port))
        #     self._telnet= None

        self._logged=False


    def _connect(self):
        """
            connect the socket connector via telnet

        :return:
        """
        try:
            self.logger.debug("connecting to boxconnector host:%s, port:%s" % (self._host, self._port))
            self._telnet = Telnet(self._host, self._port, timeout=5)
            self.logger.debug("boxconnector connected at %s:%d" % (self._host, self._port))
            # send a dummy ln
            self._telnet.write('\n')
            return True
        except Exception, e:
            self.logger.error("cannot connect to boxconnector at %s:%d" % (self._host, self._port))
            self._telnet = None
            return False

    def _write(self,line):
        """

        :param line:
        :return:
        """
        buffer= line.encode('ascii')
        self._telnet.write(buffer)

    def _read(self):
        lines= self._telnet.read_eager()
        return lines

    def _ask(self,command,timeout=5,expect='command success'):
        """
            send
        :param line:
        :return:
        """
        if not self._telnet:
            raise ApplicationError("cannot execute command: socketconnector not connected")
        # check we are logged on the livebox
        if command.strip() not in ['PrintInfo']:
            # we need to be logged
            rc= self.Connect(self.livebox_ip,login=self.user,password=self.password)
            if not self._logged:
                    raise ApplicationError("socket connector cannot log to livebox")

        if not command.endswith('\n'):
            command= command + '\n'
        self._write(command)
        response= self._telnet.read_until(expect,timeout=timeout)
        self.logger.debug(response)
        return response

    def _to_dict(self,lines,sep='='):
        """

        scan each line to extract  <key=value>
        :param lines:
        :return:
        sample:
         [Socket Connector Text Engine]<VisioService=NOK>

        """
        if sep == ':':
            pattern= self._key_value_pattern_2
        else:
            pattern= self._key_value_pattern
        data={}
        if not isinstance(lines,list):
            lines= lines.split("\n")
        for line in lines:
            match= pattern.search(line)
            if match:
                key= match.group(1)
                value= match.group(2)
                data[key]= value.strip()
        return data

    def _exit(self):
        """

        :return:
        """
        self.Disconnect()
        self._telnet.close()



    #
    # public keywords
    #


    def PrintInfo(self):
        """
            Affiche les informations de version de SocketConnector

            eg
                Version    : V1.5.0_FR
                Build      : 19153M
                Build date : 13-December-2016 17:04


        """
        lines= self._ask("PrintInfo\n",timeout=10)
        assert "command success" in lines
        data= self._to_dict(lines,':')
        self._exit()
        return data

    def AddMacAddress(self,mac):
        """
        Ajoute l'addresse MAC donnée en paramètre, à la liste des adresses MAC de la Livebox, si elle est correctement formatée. L'adresse MAC doit avoir le format suivant: XX:XX:XX:XX:XX:XX
        Note: Non disponible sur Livebox 1.1 inventel
        command success: L'adresse MAC a correctement été ajoutée à la liste des adresses MAC de la Livebox.
        command failed: L'adresse MAC n'a pas été ajoutée à la liste des adresses MAC. Exemple: AddMacAddress 00:11:22:33:44:55
        """
        response = self._ask('AddMacAddress %s' % mac, timeout=10)
        if command_success in response:
            return True
        return False


    def BackUpConfigFile(self):
        """
        Effectue une sauvegarde des paramètres de configuration de la Livebox (backup).
        La commande est en cours d'implémentation.
        """
        raise NotImplementedError()

    def Connect(self,ip='192.168.1.1',login='admin' , password='admin'):
        """
        Permet de se connecter à la Livebox. Par défaut l'adresse IP 192.168.1.1
        et les identifiant admin/admin sont utilisés.
        Cette commande une fois exécuté agit sur l'ensemble des clients connectés au serveur en changeant
        l'état de ce dernier.
        command success: Le serveur s'est connecté à la Livebox avec succès.
        command failed: Le serveur n'a pas pu se connecté à la Livebox; Soit les paramètres de la commande de connexion sont erronés, soit le serveur est déjà connecté a une Livebox (dans ce cas il est nécessaire d'utiliser la commande Disconnect)
        """
        #return self._ask("Connect",timeout=(6))
        self._logged=False
        cmd= 'Connect %s %s %s\n' % (ip,login,password)
        self._write(cmd)
        response= self._telnet.read_until('command success',timeout= 5)
        if command_success in response:
            self._logged= True
            self.logger.debug("socketconnector logged with admin")
        else:
            # command failed
            if "Already connected" in response:
                self._logged= True
                self.logger.debug("Already connected")
            else:
                self._logged= False
                self.logger.debug(response)
                self.logger.error('Socket connector cannot log to livebox.')
        return response
        #self._write("Connect %s %s %s\n"% (ip,login,password))
        #lines= self._telnet.read_until('command success',timeout=5)
        #return lines


    def Disconnect(self):
        """
        Déconnecte le serveur de la Livebox sur laquelle il est connecté.
        Cette commande une fois exécuté agit sur l'ensemble des clients connectés au serveur en changeant l'état de ce dernier.
        command success: Le serveur s'est déconnecté de la Livebox.
        command failed: Le serveur est déjà déconnecté.
        """
        response= ""
        if self._logged:
            self._write("Disconnect\n")
            response= self._telnet.read_until(command_success,timeout=3)
        self._logged= False
        return response

    def GetProperties(self,csv=None):
        """
        Récupère l'ensemble des propriétés (nom des informations à capturer associées à leur valeur)
        définies pour le modèle de Livebox.
        Certaine propriété peuvent avoir la valeur null.
        Peut prendre en paramètre un nom de fichier CSV définissant la liste des propriétés à capturer.
        Le point de départ des noms relatifs est le répertoire contenant l'exécutable.
         interne Orange Labs
         Copyright Orange Labs 2009-2016 FT/RD/MAPS/09/01/175 Version : 1.2.101
        19 juillet 2013 SocketConnector: Guide de l'utilisateur 11 / 17

        """
        response= self._ask('GetProperties',timeout=60)
        #response=  self._telnet.read_until('command success',timeout=60)
        data= self._to_dict(response)
        self._exit()
        return data


    def GetProperty(self,name):
        """
           Récupère la valeur pour l'information spécifiée en paramètre.
        command success: Le serveur a récupéré l'information demandée.
        command failed: Le serveur n'a pas pu récupérer l'information; Cette information est peut être inexistante sur le modèle de Livebox connecté au serveur.
        """
        response = self._ask('GetProperty %s' % name, timeout=10)
        # response=  self._telnet.read_until('command success',timeout=60)
        data = self._to_dict(response)
        self._exit()
        return data

    def Help(self,command):
        """
           Renvoie une aide succincte sur le fonctionnement du serveur. Prend en paramètre le nom d'une commande.
        command success: L'aide succincte sur la commande est correctement renvoyée. command failed: Le serveur est dans l'incapacité de donner de l'aide sur la commande. La commande n'est pas implémentée dans cette version.
        """
        response = self._ask('PhoneRingTest', timeout=10)
        self._exit()
        return response

    def PhoneRingTest(self):
        """
         Lance un test de sonnerie du téléphone connecté a la Livebox.
        Seuls certains types de Livebox supportent cette fonctionnalité.
        command success: Le téléphone connecté à la Livebox est sensé sonner; décrochez le combiné.
        command failed: La Livebox connectée n'implémente pas la fonctionnalité PhoneRingingTest, ou bien le serveur est déconnecté de la Livebox.
        """
        response = self._ask('PhoneRingTest', timeout=10)
        self._exit()
        if command_success in response:
            return True
        return False


    def Reboot(self):
        """
             Redémarre la Livebox connectée au serveur.
        La connexion à la Livebox sera perdue.
        """
        response = self._ask('Reboot' , timeout=10)
        self._exit()
        if command_success in response:
            return True
        return False


    def RemoveMacAddress(self,mac):
        """
         Supprime l'adresse MAC adresse spécifiée en paramètre, de la liste des adresses MAC de la Livebox.
        command success: L'adresse MAC a été supprimée de la liste.
        command failed: L'adresse MAC n'a pas été supprimée, ou bien la livebox ne supporte pas cette fonctionnalité.
        """
        response = self._ask('RemoveMacAddress %s' % mac, timeout=10)
        self._exit()
        if command_success in response:
            return True
        return False

    def Reset(self):
        """
             Remise en configuration usine de la Livebox connecté au serveur
        La connexion à la Livebox sera perdue
        command success: La Livebox va redémarrer en configuration usine. command failed: La fonctionnalité n'est pas supportée par la Livebox.
        """
        response = self._ask('Reset' , timeout=10)
        self._exit()
        if command_success in response:
            return True
        return False

    def RestoreConfigFile(self):
        """
         Restauration d'une configuration sur la Livebox, à partir d'un fichier (cf. BackUpConfigFile).
        La commande est en cours d'implémentation.
        command success: Le fichier de configuration a correctement été transmis à la Livebox.
        command failed: La commande n'est pas supportée par la Livebox.
        """
        raise NotImplementedError()


    def SaveInFlash(self):
        """
           Rend les modifications de configuration efféctués sur la Livebox permanentes. command success: La configuration a été rendue permanente.
        command failed: Cette fonctionnalité n'est pas supportée par la Livebox actuellement connecté.
        """
        response = self._ask('SaveInFlash' , timeout=10)
        self._exit()
        if command_success in response:
            return True
        return False

    def SetInternetAccount(self,login,password,timeout=1000):
        """
           Permet de modifier le couple login, mot de passe de connexion Internet.
        command success: Le login et le mot de passe de connexion ont été pris en compte par la Livebox.
            SetAccountAndWai tForConnection
           Permet de modifier le couple login, mot de passe de connexion Internet.
           Un paramètre timeout (entier en ms) qui borne le temps maximum de cette commande.
           command success: Le login et le mot de passe de connexion ont été pris en compte par la Livebox. Cette commande force l'authentification PPP.
        """
        response = self._ask('SetInternetAccount %s %s' % (login,password), timeout=10)
        self._exit()
        if command_success in response:
            return True
        return False


    def ReinitConnection(self):
        """
             Réinitialise la connexion PPP sur la Livebox.

        """
        response = self._ask('ReinitConnection' , timeout=10)
        self._exit()
        if command_success in response:
            return True
        return False

    def SetInternetAccess(self,mode):
        """
         Permet de modifier le type d'accès (ADSL, FTTH, 3G).
        command success: La Livebox est configurée avec le nouveau mode de connection. Cette commande provoque un redémarrage de la Livebox. La reconnection est gérée par le SocketConnector.
        """
        response = self._ask('SetInternetAccess %s' % mode, timeout=10)
        self._exit()
        if command_success in response:
            return True
        return False


    def SwitchAutoConf(self,mode="ON"):
        """
              Permet d'activer ou de désactiver la configuration automatique du port jaune (TV) de la Livebox
              connectée au serveur en lui passant en paramètre la chaine "ON" ou "OFF"
        La commande est uniquement supportée par certains types de Livebox.
           interne Orange Labs
         Copyright Orange Labs 2009-2016 FT/RD/MAPS/09/01/175 Version : 1.2.101
        19 juillet 2013 SocketConnector: Guide de l'utilisateur 12 / 17
               command success: Le service de configuration automatique a été configuré selon le mode demandé.
        """
        response = self._ask('SwitchAutoConf %s' % mode, timeout=10)
        self._exit()
        if command_success in response:
            return True
        return False

    def SwitchDHCP(self,mode="ON"):
        """
           Permet d'activer ou de désactiver le serveur DHCP de la Livebox connectée au serveur en lui
           passant en paramètre la chaine "ON" ou "OFF"
        Si le serveur utilise DHCP, la connexion à la Livebox peut être perdue lors de la désactivation
        du service DCHP.
        command success: Le service DHCP a été configuré selon le mode demandé.
       """
        response = self._ask('SwitchDHCP %s' % mode, timeout=10)
        self._exit()
        if command_success in response:
            return True
        return False

    def SwitchInternet(self,mode="ON"):
        """
             Permet d'activer ou de désactiver le service Internet de la Livebox connectée au serveur en
             lui passant en paramètre la chaine "ON" ou "OFF"
        command success: Le service Internet a été configuré selon le mode demandé.
        """
        response = self._ask('SwitchInternet %s' % mode, timeout=10)
        self._exit()
        if command_success in response:
            return True
        return False

    def SwitchTV(self,mode="ON"):
        """
         Permet d'activer ou de désactiver le service TV de la Livebox connectée au serveur en lui passant
         en paramètre la chaine "ON" ou "OFF"
        Cette commande affecte l'état de l'autoConf du port jaune (TV) pour les Livebox offrant la
        fonctionnalité, en forçant sa valeur à OFF.
        command success: Le service TV a été configuré selon le mode demandé.
        """
        response = self._ask('SwitchTV %s' % mode, timeout=10)
        self._exit()
        if command_success in response:
            return True
        return False


    def SwitchVisio(self,mode="ON"):
        """
           Permet d'activer ou de désactiver le service Visio de la Livebox connectée au serveur en lui
           passant en paramètre la chaine "ON" ou "OFF"
        Uniquement supporté par certains types de Livebox.
        command success: Le service Visio a été configuré selon le mode demandé.
        """
        response = self._ask('SwitchVisio %s' % mode, timeout=10)
        self._exit()
        if command_success in response:
            return True
        return False

    def SwitchWifi(self,mode="ON"):
        """
             Permet d'activer ou de désactiver le service Wifi de la Livebox connectée au serveur en lui
             passant en paramètre la chaine "ON" ou "OFF"
        command success: Le service Wifi a été configuré selon le mode demandé.
        """
        response = self._ask('SwitchWifi %s' % mode, timeout=10)
        self._exit()
        if command_success in response:
            return True
        return False



    def SetWifiParameters(self,ssid=None,channel=0,filter='OFF'):
        """
         Permet de modifier les paramètres de la configuration WiFi (2.4GHz et 5GHz si présent)
        command success: Le service Wifi a été configuré comme demandé.
        Exemples:
        SetWifiParameters wifiSSID=livebox-1234 wifiChannel=3
        SetWifiParameters wifiMacFilterMode=OFF
        SetWifiParameters wifiMacFilterMode=PERMIT

        """

    def SetWifiParameters_24GHz(self):
        """
           Permet de modifier les paramètres de la configuration WiFi 2.4GHz seule.
           command success: Le service Wifi a été configuré comme demandé.
        Exemples:
        SetWifiParameters_24GHz wifiSSID=livebox-1234 wifiChannel=3
        SetWifiParameters_24GHz wifiMacFilterMode=OFF
        SetWifiParameters_24GHz wifiMacFilterMode=PERMIT
        """

    def SetWifiParameters_5GHz(self):
        """
           Permet de modifier les paramètres de la configuration WiFi 5GHz seule.
           command success: Le service Wifi a été configuré comme demandé.
           Exemples:
        SetWifiParameters_5GHz wifiSSID=livebox-1234 wifiChannel=3
        SetWifiParameters_5GHz wifiMacFilterMode=OFF
        SetWifiParameters_5GHz wifiMacFilterMode=PERMIT
        """

    def UpgradeFirmware(self):
        """
             Envoie une demande de mise à jour du firmware vers la plateforme de mise à jour.
        La connexion à la Livebox sera perdue si la une version plus récente du firmware est trouvée.
        command success: La Livebox va tenter de mettre à jour son firmware.
        """
        response = self._ask('UpgradeFirmware', timeout=10)
        self._exit()
        if command_success in response:
            return True
        return False



    def SetEnabledTVForwardingMode(self,mode="FullRouted"):
        """
         Permet de modifier la configuration TV. Paramètre:
        FullRouted
        HalfBridged
        Bridged
        (LBv1.2 & LBV2) Passage en "fullrouté": possibilité de brancher un décodeur ou un pc sur n'importe
           quel port de la Livebox
        (LBV2 uniquement) TV sur port jaune et vert uniquement
           (correspond à l'offre multi service TV sur LBV2)
        (LBv1.2 uniquement) TV sur port jaune uniquement
           (correspond à l'offre multi service TV sur LBv1.2)
        Exemple:
          interne Orange Labs
         Copyright Orange Labs 2009-2016 FT/RD/MAPS/09/01/175 Version : 1.2.101
        19 juillet 2013 SocketConnector: Guide de l'utilisateur 13 / 17
        SetEnabledTVForwardingMode FullRouted
        """
        assert mode.lower() in ['fullrouted,halfbridged','bridged']
        response = self._ask('SetEnabledTVForwardingMode %s' % mode, timeout=10)
        self._exit()
        if command_success in response:
            return True
        return False



    def StartWiFiPairing(self):
        """
        Lance un appairage WiFi WPS (2.4GHz et 5GHz si présent)
        command success: L’appairage a été lancé comme demandé.
        """
        response = self._ask('StartWiFiPairing', timeout=60)
        self._exit()
        if command_success in response:
            return True
        return False

    def StartWiFiPairing_24GHz(self):
        """
        Lance un appairage WiFi WPS 2.4GHz seulement.
        command success: L’appairage a été lancé comme demandé.
        """
        response = self._ask('StartWiFiPairing_24GHz', timeout=60)
        self._exit()
        if command_success in response:
            return True
        return False

    def StartWiFiPairing_5GHz(self):
        """
        Lance un appairage WiFi WPS 5GHz seulement.
        command success: L’appairage a été lancé comme demandé.
        """
        response = self._ask('StartWiFiPairing_5GHz', timeout=60)
        self._exit()
        if command_success in response:
            return True
        return False

    def StartDECTCatIQPairing(self):
        """
        Lance un appairage sur la base DECT Cat-Iq
        command success: L’appairage à été lancé comme demandé.
        """
        response = self._ask('StartWiFiPairing', timeout=60)
        self._exit()
        if command_success in response:
            return True
        return False

    def ResetFullDECTCatIq(self):
        """
        Désappaire tous les devices de la base DECT CatIq.
        Efface la liste des contacts, le journal d’appel et positionne le code PIN
        à sa valeur par défaut (0000).
        """
        response = self._ask('StartWiFiPairing', timeout=60)
        self._exit()
        if command_success in response:
            return True
        return False

    def SetDECTPin(self,code='0000'):
        """
        Permet de configurer le code PIN lié à la base DECT
        exemple : SetDECTPin 1234
        """
        response = self._ask('SetDECTPin %s' % code, timeout=10)
        self._exit()
        if command_success in response:
            return True
        return False

    def SwitchDECTRadio(self,mode="ON"):
        """
        Permet d'activer ou de désactiver L’antenne DECT en lui passant en paramètre
        la chaine "ON" ou "OFF"
        Uniquement supporté par certains types de Livebox.
        command success: L’antenne DECT a été configuré selon le mode demandé.
        """
        response = self._ask('SwitchDECTRadio %s' % mode, timeout=10)
        self._exit()
        if command_success in response:
            return True
        return False

    def GetHosts(self,interface='UNKNOWN',status=None):
        """
        Liste les devices connus.
        Les paramètres permettent de filtrer la réponse :
        Interfacetype (optionnel) : ETHENET, WIFI, USB, FXS, DECT, CATIQ ou UNKNOWN
        Status (optionnel, à utiliser après InterfaceType) : CONNECTED, NOT_CONNECTED
         Exemples:
        GetHosts
        GetHosts USB
        GetHosts USB CONNECTED
        GetHosts USB NOT_CONNECTED
        """
        assert interface.upper() in ['ETHERNET','WIFI','USB','DECT','CATIQ','UNKNOWN'], \
            'bad interface name: %s' % interface
        if status:
            assert status.upper() in ['CONNECTED','NOT_CONNECTED'] , \
                'bad status name: %s ' % status
        cmd = 'GetHosts'
        if not interface == 'UNKNOWN':
              cmd = cmd + ' ' + interface
        if status:
            cmd = cmd + ' ' + status
        response = self._ask(cmd, timeout=10)
        self._exit()
        if command_success in response:
            return response
        return response