Create a new device runner
==========================



duplicate samples
-----------------

git clone wbackend

copy applications/samples newdevice

cd newdevice

in models.py
------------

### change DEVICE_NAME

    DEVICE_NAME=  "newdevice_agents" 


### change class name
 
class Samples  to class NewdeviceAgents



in config.cfg
-------------

### change RESTOP_HUB_NAME

    RESTOP_HUB_NAME= 'newdevice_agents'
    
 
in application.py
-----------------

### change blueprint_name='restop_sample'

    blueprint_name='restop_newdevice'
    
    
in resources.py
----------------

### change class DeviceResource doc string

    class DeviceResource(AgentResource):
        """
            Newdevice : my brand new device
        ""
    

in tests/test_model.py
----------------------

change 
    from applications.samples.models import Samples
    from applications.samples.resources import Resource_samples
to
    from newdevice.models import NewdeviceAgents
    from newdevice.resources import DeviceResource
    


change
    sample= Samples.create(name='sample_1',session=1)
    
to
    sample= NewdeviceAgents.create(name='sample_1',session=1)

change
    autodoc = Resource_samples._autodoc()
to
    autodoc = DeviceResource._autodoc()
    

in tests/demo.py
----------------

change 
    user= "sample_1"
to

    user= "newdevice"


    
create a device in platform.json
--------------------------------

add a profile

    profiles:
         newdevice:
            # my new device
            collection: newdevice_agents
           
add a device alias


    devices:
        newdevice:
            profile: newdevice
            


Test your samples clone
========================

start a master

start your newdevice hub
    dev newdevicerunner
    python starter.py


launch demo.py

