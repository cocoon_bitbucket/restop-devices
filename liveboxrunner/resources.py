__author__ = 'cocoon'

from restop.application import ApplicationError
from restop.tools import ScanMethods
from models import DEVICE_NAME
from agent import Agent
from restop.plugins.resources import RootResource
from restop_platform.resources import AgentResource
from restop_platform.journal import Journal
from restop_stats.controllers import StatScheduler

class Resource_Root(RootResource):
    """

    """
    collection= 'root'



class DeviceResource(AgentResource):
    """
        LiveboxRunner : the Orange Livebox

    """
    collection= DEVICE_NAME
    protected= ''

    _AgentClass= Agent


    @classmethod
    def _autodoc(cls,**kwargs):
        """

            override _autodoc to add methods from Livebox connector
            generate autodocumentation

        :param kwargs:
        :return:
        """
        info = ScanMethods()
        info.add_docstring(cls)
        info.add_op_methods(cls)
        if cls._AgentClass:
            # ask agent to complete autodoc
            cls._AgentClass._autodoc(info)

        return info.result


    def configure_session(self,session_id,members,configuration):
        """
        :param session_id:
        :param members:
        :param configuration:
        :return:
        """
        return configuration



    def op_feedback_scheduler(self,device='LB'):
        """
            reserved keyword used by the stb to send stats result
        """
        """
           call back for stats scheduler

            the device performs a curl POST on

            http://<internal_ip>/restop/upload/livebox_agents/<device>/feedback_scheduler

            the master redirects here with

            localhost:5005/restop/api/v1/tvbox_agents/<device>/feedback_feedback_schduler

        """
        database = self.model.database
        journal = Journal(database)
        journal.write("hit feedback_scheduler url")
        print "receive stats for device %s" % device
        if self.request.content_type == 'application/json':
            data= self.request.json
        else:
            data = self.request.get_data()
        filename = "./stats-%s.txt" % device
        journal.write("stats length: %d , file is %s" % (len(data), filename))
        with open(filename, 'w') as fh:
            fh.write(data)
        data = data.split('\n')
        #scheduler = StatSchedulerClient.push_to_graphite(data, device=device, configfile='collectors.ini')
        #return ""
        if data:
            journal.write('feedback_scheduler: send data to graphite (len: %d)' % len(data))
            try:
                nb_metrics = StatScheduler.push_to_graphite(data, device=device, configfile='collectors.ini')
            except Exception, e:
                journal.write('error on feedback_scheduler,%s' % str(e))
                nb_metrics = 0
            journal.write("number of metrics sent to graphite: %d" % nb_metrics)
        else:
            journal.write("feedback_scheduler: no data to push to graphite")
        return "OK"
