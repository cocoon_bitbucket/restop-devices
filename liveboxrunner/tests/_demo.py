# -*- coding: utf-8 -*-

__author__ = 'cocoon'

import time

import os
from restop_client import Pilot


user= "livebox"


master_url= "http://localhost:5000/restop/api/v1"

def test_fetch_restop_interface():
    """



    :return:
    """
    # to test localy
    #phonehub_url = "http://localhost:5001/restop/api/v1"
    #os.environ['NO_PROXY'] = 'localhost'

    p= Pilot()

    data= p.fetch_restop_interface(url=master_url)


    return data


def test_samples_basic():

    p =Pilot()



    p.setup_pilot("demo", "demo_qualif", master_url)


    try:

        res= p.open_Session(user)


        try:
            res= p.raise_error(user)
        except:
            pass

        try:
            res= p.application_error(user)
        except:
            pass

        try:
            res = p.not_implemented(user)
        except:
            pass

        res= p.dummy(user)

        # read alarm on samples
        time.sleep(2)

        res = p.read(user)
        #assert res.startswith('Signal')

        res= p.ping(user)
        time.sleep(2)

        res= p.read(user)
        assert res=='pong\n'

    except RuntimeError,e:
        print "interrupted: %s" % e.message

    except Exception,e:
        print e.message
    finally:
        r= p.close_session()

    return







if __name__=="__main__":

    test_fetch_restop_interface()
    test_samples_basic()


    print "Done."
