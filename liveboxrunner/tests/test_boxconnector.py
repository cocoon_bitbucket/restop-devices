import time
from wbackend.logger import SimpleLogger
from liveboxrunner.livebox_device.socketconnector.boxconnector import LiveboxConnector


#
# start liveboxrunner.livebox_device.draft.fake_livebox_connector
#


boxconnector_host= '192.168.1.21'
boxconnector_port= 23

#boxconnector_host= '192.168.99.100'
#boxconnector_port= 23

livebox_host= "192.168.1.1"
livebox_user= "admin"
livebox_password= "admin1234"



def logger():
    log= SimpleLogger(queue_key='agent:id:1')
    return log


def boxconnector():
    """

    :return:
    """
    bc= LiveboxConnector(host=boxconnector_host,port=boxconnector_port,logger=logger())
    if bc._telnet is None:
        raise RuntimeError("cannot connect to socket connector at  %s=%d" % (boxconnector_host,boxconnector_port))
    return bc

def connected_boxconnector():
    """

    :return:
    """
    bc = boxconnector()
    rc= bc._telnet.read_until('\n',timeout=3)
    rc= bc.Connect(livebox_host,livebox_user,livebox_password)

    return bc


def test_boxconnector():
    """

    :return:
    """

    bc= boxconnector()
    #bc= LiveboxConnector(host='192.168.99.100',port=8023,logger=logger())

    try:

        rc= bc._telnet.read_until('\n',timeout=3)



        rc= bc.Connect('192.168.1.1','admin','admin1234')
        print rc

        rc= bc.PrintInfo()
        print rc
        #rc= bc.GetProperties()


        rc =bc.Disconnect()
        print rc

    finally:

        bc._exit()
        pass

    return



def test_PrintInfo():
    """

        [Socket Connector Text Engine]
        SocketConnector
          Version    : V1.5.0_FR
          Build      : 19153M
          Build date : 13-December-2016 17:04
        Based on BoxConnector FR_G2R14C1 (build 19145, build date 2016-11-17_10h55)
        [Socket Connector Text Engine]command success

    :return:
    """
    bc= None
    try:
        bc= boxconnector()
        data= bc.PrintInfo()
        print data
        assert 'Version' in data.keys()

        rc=bc.Disconnect()

    finally:
        if bc:
            bc._exit()
    return




def test_GetProperties():
    """
    :return:
    """
    bc= None
    try:
        bc= connected_boxconnector()
        data= bc.GetProperties()
        print data
        assert 'TV_Status' in data.keys()

        rc=bc.Disconnect()

    finally:
        if bc:
            bc._exit()
    return


def test_GetProperty():
    """

    :return:
    """
    bc= None
    try:
        #bc= connected_boxconnector()
        bc = boxconnector()
        data= bc.GetProperty('TV_Status')
        print data
        assert 'TV_Status' in data.keys()

        rc=bc.Disconnect()

    finally:
        if bc:
            bc._exit()
    return


def test_SwitchTV():
    """

    :return:
    """
    bc= None
    try:
        bc= connected_boxconnector()
        rc= bc.SwitchTV(mode='ON')
        print rc
        assert rc == False


        rc=bc.Disconnect()

    finally:
        if bc:
            bc._exit()
    return

def test_GetHosts():
    """

    :return:
    """
    bc= None
    try:
        bc= connected_boxconnector()
        rc= bc.GetHosts()
        print rc

        rc=bc.Disconnect()

    finally:
        if bc:
            bc._exit()
    return



def test_endurance():

    for i in xrange( 1,300 ):

        print("iteration: %d" % i)

        test_PrintInfo()
        test_GetProperty()

        time.sleep(1)

    return

if __name__=="__main__":


    # test_boxconnector()
    # # test_GetHosts()
    # # test_SwitchTV()
    # test_GetProperty()
    # test_PrintInfo()
    # test_GetProperties()

    test_endurance()

    print 'Done.'





