#
# test telnet connection on livebox
#

import time
from telnetlib import   Telnet
from jinja2 import Template

from walrus import Database
from restop_adapters.telnet_adapter import TelnetAdapter
from restop_adapters.client import ClientAdapter
from restop_components.stats_scheduler.crontab.cronjob import CronStatScheduler

# to start a fake livebox with a telnet server at 192.168.99.100 port:23
#     cd mock-stb/mock-livebox/docker
#     docker-compose up &

telnet_ip= "192.168.99.100"
#telnet_ip= "192.168.1.1"

telnet_port= 23

telnet_user= "root"
telnet_password= "sah"

agent_id= "agent:id:1"

#
# testing stats schedsuler
#

#default_metrics= ['df', 'cpu','meminfo','loadavg']
default_metrics= ['df', 'cpu','loadavg']

job_commands= [
    dict(name='df',cmd='df'),
    dict(name='meminfo', cmd= 'cat /proc/meminfo')
]

original_script_pattern='''\
#!/bin/ash
while true; do
  {% for job in jobs %}echo ">->" \`date -u +"%Y-%m-%dT%H:%M:%SZ"\` {{ job.name }};{{ job.cmd }};echo ">-<"
  {% endfor %}
  sleep {{ delay }}
done
'''

script_pattern='''\
#!/bin/ash
while true; do
  {% for job in jobs %}echo ">->" `date -u +"%Y-%m-%dT%H:%M:%SZ"` {{ job.name }};{{ job.cmd }};echo ">-<"
  {% endfor %}
  sleep {{ delay }}
done
'''

def connection():
    """

    :return:
    """
    client = Telnet(host=telnet_ip, port=telnet_port)

    rc = client.read_until('login:', timeout=10)
    #assert rc
    print rc

    client.write(b"%s\n" % telnet_user)
    rc = client.read_until('assword:', timeout=5)
    #assert rc
    print rc

    client.write(b'%s\n' % telnet_password)

    time.sleep(2)
    rc = client.read_until('#', timeout=5)
    #assert rc
    print rc

    time.sleep(1)
    return client


def test_direct_telnet():
    """

    :return:
    """
    client= Telnet(host=telnet_ip,port=telnet_port)

    rc = client.read_until('login:', timeout=10)
    assert rc
    print rc


    client.write(b"%s\n" % telnet_user)
    rc = client.read_until('assword:', timeout=5)
    assert rc
    print rc

    client.write(b'%s\n' % telnet_password)


    time.sleep(2)
    rc= client.read_until('#',timeout=5)
    assert rc
    print rc
    #@rc = client.read_until('\n', timeout=5)
    #rc = client.read_until('\n', timeout=5)

    time.sleep(1)
    rc= client.close()
    time.sleep(1)
    return




def telnet_escape(line):
    """
        prefix characters like ( ` " ) with back slash to avoid shell interpretation

    :param line:
    :return:
    """
    line= line.replace('"' , '\\"')
    line= line.replace('`', '\\`')
    return line


def trace_scheduler_command_lines():
    """

    :return:
    """
    sequence= [
        "rm -f /tmp/trace-scheduler",

    ]

    t = Template(script_pattern)
    command_lines = t.render(delay=5, jobs=job_commands)

    script_path = '/tmp/trace-scheduler'
    # r= self.target_client.send('echo "#!/bin/ash" > %s' % script_path)
    for l in command_lines.split('\n'):
        line = telnet_escape(l)
        #line= l
        line = 'echo  "%s" >> %s' % (line, script_path)
        sequence.append(line)

    print sequence
    return sequence


def test_build_scheduler_script():
    """

        create trace-scheduler command file directly with telenetlib client


    :return:
    """
    sequence= trace_scheduler_command_lines()

    client=connection()

    for line in sequence:
        print line
        buffer= (line + '\n').encode('ascii')
        client.write(buffer)

    time.sleep(1)


    client.close()

    return


def db_connection():
    """


    :return:
    """
    db = Database(host='localhost', port=6379, db=0)
    db.flushall()
    return db


def test_build_scheduler_script_2():
    """

        create trace-scheduler command file
        via the telnetadapter


    :return:
    """
    sequence= trace_scheduler_command_lines()

    db= db_connection()

    adapter = TelnetAdapter(agent_id,host=telnet_ip,user=telnet_user,password=telnet_password,port=telnet_port,
                           redis_db=db)
    adapter.start()
    client= ClientAdapter(agent_id,redis_db=db)

    for line in sequence:
        print line
        line = telnet_escape(line)
        client.send(line)

        #buffer= (line + '\n').encode('ascii')
        #client.write(buffer)

    time.sleep(1)

    client.exit()

    time.sleep(3)

    return



if __name__ == "__main__":


    #test_direct_telnet()

    test_build_scheduler_script()

    #test_build_scheduler_script_2()



    print "Done."



