simulate a livebox
==================


simulate the telnet port
------------------------

    telnet_user: root
    telnet_password: sah
    telnet_port: 23


an alpine busybox for testing scheduler


simulate the box connector
--------------------------

    http_user: admin                                                                                            
    http_password: admin1234
    
    livebox_connector_ip: localhost
    livebox_connector_port: 8023

an alpine-java image with box connector



simulate the web ui interface
-----------------------------

    http_user: admin
    http_password: admin1234
    
    http_port: 80
    https_port: 443



usage:
=====

    cd mock-stb/mock_livebox/docker/mocklivebox
    docker-compose up
    
test fake livebox telnet server
-------------------------------

    telnet 192.168.99.100 23
    
test fake socket_connector
--------------------------

    telnet 192.168.99.100 8023

