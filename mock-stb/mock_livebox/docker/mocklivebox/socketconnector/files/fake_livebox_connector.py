"Testing - Accept a single connection"
import SocketServer
from telnetsrvlib import TelnetHandler
import logging



fixtures= dict(

    PRINFO= """\
[Socket Connector Text Engine]<info>
[Socket Connector Text Engine]command success
""" ,

    CONNECT= """\
[Socket Connector Text Engine]please wait as far as the connection is ready... [Socket Connector Text Engine]connect on liveBox:Livebox SAGEM 3.10
[Socket Connector Text Engine]using firmware:2600EA.2601E2
[Socket Connector Text Engine]<connected>
[Socket Connector Text Engine]command success
""",

    DISCONNECT="""\
[Socket Connector Text Engine]<disconnected>
[Socket Connector Text Engine]command success
""",


)







class TNS(SocketServer.TCPServer):
    allow_reuse_address = True


class TNH(TelnetHandler):
    """

    """
    def cmdECHO(self, params):
        """ [<arg> ...]
        Echo parameters
        Echo command line parameters back to user, one per line.
        """
        self.writeline("Parameters:")
        for item in params:
            self.writeline("\t%s" % item)

    def handle_unknown_command(self, cmd, params):
        """
        """
        self.write("received an Unknown command '%s'\n" % cmd)


    def cmdCONNECT(self,params):
        """"

        """
        logging.debug('received command Connect %s' % params)
        response="""\
[Socket Connector Text Engine]please wait as far as the connection is ready... [Socket Connector Text Engine]connect on liveBox:Livebox SAGEM 3.10
[Socket Connector Text Engine]using firmware:2600EA.2601E2
[Socket Connector Text Engine]<connected>
[Socket Connector Text Engine]command success
"""
        self.write(response)

    def cmdDISCONNECT(self, params):
        """"

        """
        logging.debug('received command Disconnect %s' % params)
        response="""\
[Socket Connector Text Engine]<disconnected>
[Socket Connector Text Engine]command success
"""
        self.write(response)


    def cmdPRINTINFO(self,params):
        """

        :param params:
        :return:
        """
        logging.debug('received command PrintInfo %s' % params)
        response="""\
[Socket Connector Text Engine]<printinfo>
[Socket Connector Text Engine]command success
"""
        self.write(response)



logging.getLogger('').setLevel(logging.DEBUG)

logging.info("start telnet server")
tns = TNS(("0.0.0.0", 8023), TNH)
tns.serve_forever()
logging.info("leave telnet server")