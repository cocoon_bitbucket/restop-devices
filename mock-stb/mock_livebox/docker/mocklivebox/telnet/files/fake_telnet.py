
import  subprocess
import SocketServer



from telnetsrvlib import TelnetHandler

import logging



"Testing - Accept a single connection"
class TNS(SocketServer.TCPServer):
    allow_reuse_address = True

class TNH(TelnetHandler):
    """


    """
    authNeedUser = True
    authNeedPass = True


    # def cmdECHO(self, params):
    #     """ [<arg> ...]
    #     Echo parameters
    #     Echo command line parameters back to user, one per line.
    #     """
    #     self.writeline("Parameters:")
    #     for item in params:
    #         self.writeline("\t%s" % item)


    def authCallback(self,user,password):
        """

        :param user:
        :param password:
        :return:
        """
        self.write('Connected\n')


    def handle_unknown_command(self, cmd, params=None):
        """
        """
        #command = "%s %s" % (cmd,str(* params))
        #out= check_output()
        #args= []
        #args.append(cmd)
        #args.extend(params)
        #self.write("received command '%s'\n" % cmd)
        try:
            output = subprocess.check_output(cmd,shell=True,stderr=subprocess.STDOUT)
            self.write(output)
        except Exception ,e:
            logging.error(str(e)+'\n')
            self.write(str(e)+'\n')




logging.getLogger('').setLevel(logging.DEBUG)

tns = TNS(("0.0.0.0", 23), TNH)
tns.serve_forever()

# vim: set syntax=python ai showmatch:
