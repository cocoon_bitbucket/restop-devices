from restop_tests.mockdevice.console import ThreadedMockServer

import os
ardom_filename_default= "./stb/fixture/ardom_sample.html"
ardom_filename_pattern= "./stb/fixture/ardom_sample_%s.html"


#ardom_callback_url = "%s/restop/upload/tvbox_agents/%s/feedback_ardom"

ardom_callback_param_file= "/flash/Resources/resources/test_params"


class MockConsole(ThreadedMockServer):
    """



    """

    def handle_command(self,cmd):
        """

        :param cmd:
        :return:
        """
        lines=""
        if cmd.startswith('curl '):
            # intercept curl command to add --insecure option
            cmd= "curl --insecure " + cmd[5:]
            lines= self._shell(cmd)

        return lines




    def send_ardom(self):
        """

        :return:
        """
        try:
            # retrieve current channel
            channel= self.dashboard_get('channel')
            filename= ardom_filename_pattern % channel
            os.stat(filename)
        except Exception ,e:
            filename= ardom_filename_default

        self.log.debug("send ardom file: %s" % filename)

        # read call back url
        try:
            with open(ardom_callback_param_file,"rb") as fh:
                content= fh.read()
        except Exception ,e:
            self.log.error("fail to read ardom parma file: %s" % str(e))
            return

        self.log.debug("call back url is : %s" % str(content))

        ardom_callback_url= content.replace("https://","http://")

        cmd_line = "curl -i -X POST --data-binary @%s %s" % (
            filename, ardom_callback_url)
        self.send(cmd_line)
        return