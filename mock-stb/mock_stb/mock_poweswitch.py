import flask
from flask import Blueprint,request,make_response,Response,abort
from flask import jsonify
from stb.keycodes import key_codes,operations

ALL_METHODS= ['GET', 'POST','PUT','PATCH','DELETE','HEAD','OPTIONS']
powerswitch= Blueprint('powerswitch','powerswitch')



@powerswitch.route('/set.cmd', methods=['GET'])
def set_cmd():
    """
        emulate power switch

        /set.cmd?user=admin+pass=12345678+cmd=setpower+p61=1

        :return

        p61=1

    """
    args= {}
    switchs= {}

    if '+' in request.query_string:
        parts= request.query_string.split('+')
        for arg in parts:
            if '=' in arg:
                name,value=arg.split('=')
                args[str(name)]= value


    user=args.get('user','admin')
    password = args.get('pass', '12345678')
    cmd= args.get('cmd')

    for channel in ['p61','p62','p63','p64']:
        if args.get(channel,None):
            switchs[channel]= request.args.get(channel)

    # if ' ' in cmd:
    #     channels= cmd.split(' ')
    #     for channel in channels[1:]:
    #         name,value= channel.split('=')
    #         switchs[str(name)]= value



    current_app= flask.current_app
    #console= current_app.config['console']

    if cmd.startswith("setpower"):
        # set power on
        print "power switch: command=%s " % cmd
        #console.log.debug("remote control received: remote control: key=%s , mode=%s" % (_key,_mode))

        response= [ "%s=%s" % (port ,value) for port,value in switchs.iteritems()]
        response= " ".join(response)
        return response

    #return "not implemented operation: %s" % cmd

