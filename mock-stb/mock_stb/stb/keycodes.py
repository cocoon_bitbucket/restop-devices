# -*- coding: utf-8 -*-

raw_codes= """\
512
0

513
1

514
2

515
3

516
4

517
5

518
6

519
7

520
8

521
9

158
BACK

223
CANCEL

352
OK

174
QUIT
During VOD playing : same as Back key.
358
INFO

139
MENU
Shortcut to the Main Menu (STBU) / Welcome Screen (Newbox)
393
VOD
Shortcut to the "à la demande/catalogue VOD" application
365
GUIDE
Shortcut to the "TV/programme TV" application
395
LIST
On Live, displays the channel list
402
CHANNEL+

403
CHANNEL-

113
MUTE

115
VOL+

114
VOL-

159
FORWARD

168
REWIND

407
NEXT
During VOD playing : forward 5 minutes
412
PREVIOUS
During VOD playing: rewind 5 minutes
164
PLAY/PAUSE

166
STOP
During VOD playing : same as Back key.
167
RECORD

103
UP

105
LEFT

106
RIGHT

108
DOWN

398
RED BUTTON

399
GREEN BUTTON
Shortcut to the "TV/enregistreur TV" application
400
YELLOW BUTTON

401
BLUE BUTTON
"""

key_codes= {}
codes= raw_codes.split('\n')
while len(codes):

    code= codes.pop(0)
    name= codes.pop(0)
    comment= codes.pop(0)
    key_codes[code]= name

pass

operations= {
    '01': "remote_control",
    '10': "state",
    '09': "live_channel",
    '06': "watch_vod",
    '07': "display_vod_information"

}