"""


    inject influxdb events to an influxdb store


    subscribe to redis pubsub channel: "influxdb.events" and write events received  to influxdb database



"""
import threading
import json
import redis
import requests
from core import default_dbname,default_channel


class Injector(object):
    """


    """
    headers = {
        'Content-type': 'application/octet-stream',
        'Accept': 'text/plain'
    }

    def __init__(self,influxdb_url="http://localhost:8086",dbname='graphite'):
        """

        :param influxdb_url:
        :param dbname:
        :param channel:
        """
        self.influxdb_url= influxdb_url
        self.dbname= dbname
        self.session= requests.session()
        self.session.headers= self.headers

    def make_influxdb_event_line(self,msg):
        """
            compute event line form msg
        :param msg: dict ( title,text,tags,timestamp)  timestamp is a float representing seconds
        :return:  string: payload for send event
        """
        # timestamp with precision = ms
        msg["timestamp"] = str(int(msg["timestamp"] * 1000))
        line = 'events title="%(title)s",text="%(text)s",tags="%(tags)s" %(timestamp)s\n' % msg
        return line


    def send(self,line):
        """
            send event lineto _influxdb
        :param line: string eg
        :return:
        """
        url= self.influxdb_url + "/write"
        params = (
            ('db', self.dbname),
            ('precision', "ms")   # precision is milli-second
        )
        response = self.session.post(url, data=line, params=params)
        print(response.text)
        assert response.status_code == 204
        return


    def write(self,msg):
        """
            write an event message to the _influxdb

        :param msg: dict
        :return:
        """
        line = self.make_influxdb_event_line(msg)
        self.send(line)



class Worker(object):
    """
        minimal worker

    """
    def write(self,item):
        """

        :param item:
        :return:
        """
        print(item['channel'], ":", item['data'])



class Listener(threading.Thread):
    """
        a redis Pusub listener

    """
    def __init__(self, redis_db, channels=default_channel):
        threading.Thread.__init__(self)
        self.redis = redis_db
        self.pubsub = self.redis.pubsub()
        self.pubsub.subscribe(channels)

        self.worker= None

    def set_worker(self, worker):
        """

        :param injector: instance of  class with write(msg) method
        :return:
        """
        self.worker = worker

    def work(self, item):

        if self.worker :
            print(item['channel'], ":", item['data'])
        else:
            print("no worker defined: use listener.set_worker(worker)")

    def run(self):
        for item in self.pubsub.listen():
            if item["type"] == "message":
                if item['data'] == '"KILL"':
                    self.pubsub.unsubscribe()
                    print(self, "unsubscribed and finished")
                    break
                else:
                    self.work(item["data"])



class Subscriber(Listener):
    """

    """


    def work(self,item):
        """

            send event message to _influxdb

        :param item: string: a json representation of a event dict (title,text,tag,timestamp)
        :return:
        """
        try:
            msg= json.loads(item)
        except Exception as e:
            # cannot serialize item
            self.log("infludb subscriber: %s" % str(e))
            msg= None

        if msg:
            self.log("subscriber worker write message: %s" % msg)
            try:
                self.worker.write(msg)
            except Exception as e:
                self.log(" ... error:%s" % str(e))

    def log(self,message):
        """
            publish message on _influxdb.logs
        :param message: string
        :return:
        """
        self.redis.publish("_influxdb.logs",message)
        #print(message)




if __name__=="__main__":
    """

    """
    import time

    redis_db= redis.StrictRedis()

    injector= Injector("http://192.168.99.100:8086",dbname="telegraf")

    subscriber= Subscriber(redis_db)
    subscriber.set_worker(injector)


    # publish some message
    for i in range(5):
        #
        msg= dict(title="event %d" %i , text="new sample event", tags="some,tags,T%s" % str(i),timestamp=time.time())
        redis_db.publish(default_channel,json.dumps(msg))
        time.sleep(1)



    # invalid msg
    msg = dict(title="event %d" % 99, text="bad sample event", tags="some,tags,T%s" % str(99))
    redis_db.publish(default_channel, json.dumps(msg))

    redis_db.publish(default_channel, json.dumps("KILL"))


    subscriber.start()

    # for msg in subscriber.pubsub.listen():
    #     print(msg)



    time.sleep(5)











