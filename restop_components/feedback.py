
"""



"""

from restop_adapters.client import ClientAdapter


class FeedBackSender(ClientAdapter):
    """

        send curl command to stb to send file

        ( from stb to test station)

    """

    def get_by_curl(self,filename,url):
        """

        :param filename:
        :param url:
        :return:
        """
        cmd_line = "curl -i -X POST --data-binary @%s %s" % (filename,url)

        self.log.debug("send command to stb serial: %s" % cmd_line)
        r= self.send(cmd_line)

        return r

    def get_by_redis(self,filename,redis_key,redis_host,redis_port=6379):
        """

        :param filename:
        :param redis_key:
        :return:
        """

        cmd_line = 'while read line; do echo -e "rpush %s \\"${line}\\"" | nc %s %s; done  < %s >/dev/null' % (
            redis_key, redis_host, redis_port, filename
        )
        self.log.debug("send command to stb serial: %s" % cmd_line)
        r = self.send(cmd_line)

        return r




