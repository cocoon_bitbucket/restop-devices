
import os
import datetime
import time
import tarfile

import logging
log=logging.getLogger('figaro')



def datetime_from_timestamp(timestamp):
    """

    :param timestamp: float or string represents time in second
    :return:
    """
    timestamp= float(timestamp)
    date = datetime.datetime.fromtimestamp(timestamp)
    return date


def datetime_from_figaro_date(self, data, format="[%Y/%m/%d - %H:%M:%S]"):
    """

    [2016/03/15 - 22:43:08]

    datetime.datetime.strptime(s, "%d/%m/%Y").timestamp()

    :param data: string , the date string from figaro logs
    :return:
    """
    ts = datetime.datetime.strptime(data, format)
    return ts


def datetime_from_figaro_RebootCounterTimestamp(self, data, format="[%Y-%m-%dT%H:%M:%SZ]"):
    """

    [0001-01-01T00:00:00Z]


    datetime.datetime.strptime(s, "%d/%m/%Y").timestamp()

    :param data: string , the date string from figaro logs for RebootCounterTimestamp
    :return:
    """
    ts = datetime.datetime.strptime(data, format)
    return ts


def is_numeric(value):
    """

    :param value:
    :return:
    """
    try:
        dummy = float(value)
    except Exception, e:
        return False
    return True





class WhiteBoard(dict):
    """

    """

    @property
    def current_file(self):
        last= self.get('files')
        if last:
            return last[-1]
        else:
            return []


    @property
    def current_device_info(self):
        last= self.get('DeviceInfo')
        if last:
            return last[-1]
        else:
            return []

    @property
    def line_count(self):
        return self.get('line_count',0)

    def incr_line(self):
        count= self.line_count
        count +=1
        self['line_count'] = count


class FigaroParser(object):
    """

    """
    root= 'figaro'

    registrar ={}
    white_board=WhiteBoard()

    def __init__(self,session=0,graphite_host="localhost",graphite_port=2003):
        """

        :param session:
        """
        self.session= 0
        self.handlers= {}

        self.graphite_host= graphite_host
        self.graphite_port= graphite_port

        #
        # create handlers
        #
        self.handlers['default']= FigaroHandler(self)
        for collection,handler_class in self.registrar.iteritems():
            self.handlers[collection]= handler_class(self)


        # current line count
        self.white_board['line_count']= 0

        # list of all line kinds
        self.white_board['collections'] = set()

        # trace DeviceInfo lines
        self.white_board['DeviceInfo']= []

        # trace files
        self.white_board['files']= []





    def parse_timestamp(self,data):
        """

        [2016/03/15 - 22:43:08]

        datetime.datetime.strptime(s, "%d/%m/%Y").timestamp()

        :param data:
        :return:
        """
        ts = datetime.datetime.strptime(data, "[%Y/%m/%d - %H:%M:%S]")
        return ts

    def parse_file(self,filename,path=None):
        """

        :param filename:
        :return:
        """
        if isinstance(filename,basestring):
            # a filename
            fh= file(filename,'r')
            path= filename
        else:
            # a file descriptor
            fh =filename
            path= path or fh

        log.info("open file %s" % path)

        self.white_board['line_count'] = 0
        self.white_board['files'].append(path)
        while 1:
            self.white_board.incr_line()
            line = fh.readline()
            if not line:
                break
            line = line.strip()
            if line:
                parser.feed(line, self.white_board.line_count)

        return self.white_board.line_count

    def parse_tar(self,filename):
        """

        :param filename:
        :return:
        """
        assert tarfile.is_tarfile(filename)
        tar = tarfile.open(filename, 'r')
        tar_path= tar.fileobj.filename
        for member_info in tar.getmembers():
            #
            name= member_info.path
            path= os.path.join(tar_path,name)
            f = tar.extractfile(member_info)
            self.parse_file(f,path)
        return

    def parse_directory(self,directory):
        """


        :param directory:
        :return:
        """
        d = os.listdir(directory)
        for f in sorted(d):
            if f.startswith('.'):
                continue
            path= os.path.join(directory,f)
            if os.path.isdir(path):
                # it is a directory
                self.parse_directory(path)
            else:
                tar= False
                try:
                    if tarfile.is_tarfile(path):
                       tar= True
                except IOError,e:
                    if e.errno == 21:
                        # it is a directory
                        raise
                if tar:
                    self.parse_tar(path)
                else:
                    # it is a file
                    self.parse_file(path)
        return



    def handler(self,collection):
        """

            select custom handler for the line
        :param collection:
        :return:
        """
        # simple select
        handler = self.handlers.get(collection, None)
        if not handler:
            # try to find handler by tag
            for candidate in self.handlers.values():
                if collection.upper() in candidate.tags:
                    return candidate
            return self.handlers['default']
        return handler

    def split_parts(self,line):
        """
            [2016/03/15 - 22:43:08], WIFI, Interface = wl0, SyncRate = 288.5 Mbps
        :param line:
        :return:
        """
        parts= line.strip().split(',',2)
        assert len(parts)== 3, "invalid line: %s" % (line)

        if parts[0].upper() == "DEVICEINFO":
            # first line detected
            self.handle_first(line)
            timestamp= 0
            collection=""
            body=""

        else:
            timestamp= self.parse_timestamp(parts[0].strip())
            collection= parts[1].strip()

            body= parts[2].strip()

        return timestamp,collection,body


    def feed(self,line,count):
        """

        :return:
        """
        # split source line
        timestamp,collection,body= self.split_parts(line)

        if timestamp:
            self.white_board['collections'].add(collection)
            handler= self.handler(collection)
            handler.handle(timestamp,collection,body)


    def handle_first(self,line):
        """

            DeviceInfo
            Sagemcom Livebox 4
            LK15015DP990272
            40:F2:01:8C:8A:70
            SG40_sip-fr-2.14.1.1_7.21.1.0
            86.242.92.123
            2016-03-15 22:43:08

        :return:
        """
        #parts= line.split(',')
        parts=[part.strip() for part in line.split(',')]
        self.white_board['DeviceInfo'].append(parts)
        log.info("handle device: %s " % self.namespace)
        #print "========================================================================="
        #print parts

    @property
    def namespace(self):
        """
            device namespace for graphing

            DeviceInfo, Sagemcom Livebox 4, LK15015DP990272, 40:F2:01:8C:8A:70,
            SG40_sip-fr-2.14.1.1_7.21.1.0, 86.242.92.123
        :return:
        """
        device_info = self.white_board.current_device_info
        info={}
        if device_info:
            info= dict(
                device=device_info[1].replace(" ",'-'),
                serial=device_info[2],
                mac=device_info[3],
                version=device_info[4],
                ip=device_info[5],
            )
            info['root']= self.root
            namespace= "%(root)s.%(device)s.%(serial)s" % info
        else:
            namespace= None

        return namespace



class FigaroHandler(object):
    """

    """
    collection= 'default'
    tags= []

    @classmethod
    def register(cls):
        FigaroParser.registrar[cls.collection]= cls


    def __init__(self,parser):
        """

        :param parser:
        """
        self.parser= parser


    def log_line(self,collection):
        """

        :return:
        """
        count= self.parser.white_board.line_count
        msg= "=== %s ===========  %s  ==========" % (count,collection)
        log.debug(msg)

    def parse_attributes(self, data):
        """

        parse lines like
             MemTotal = 998460, MemFree = 199216, Buffers = 503764, Cached = 81020
        :param data:
        :return:
        """
        results = {}
        attributes = data.split(',')

        for attr in attributes:
            if "=" in attr:
                parts = attr.split('=')
                key = parts[0].strip()
                value = parts[1].strip()
                results[key] = value
            else:
                #
                pass
        return results


    def handle(self, timestamp, collection, body):
        """

        :param timestamp:
        :param collection:
        :param body:
        :return:
        """
        self.log_line(collection)
        print timestamp,collection
        attributes = self.parse_attributes(body)
        print attributes

    def graphite_handler(self,timestamp,collection,body):
        """

        :param timestamp:
        :param collection:
        :param body:
        :return:
        """
        attributes = self.parse_attributes(body)
        namespace = self.parser.namespace
        for name, value in attributes.iteritems():
            path = "%s.%s" % (namespace, name)
            self.push_to_graphite(path, value, timestamp.strftime('%s'))
        return


    def push_to_graphite(self,path,value,timestamp):
        """

        :param path:
        :param value:
        :param timestamp:
        :return:
        """
        graphite_host= self.parser.graphite_host
        graphite_port= self.parser.graphite_port

        if not is_numeric(value):
            log.warning("Non numeric value: [%s] for key: %s ==> skipped" % (value, path))
            return
        else:
            log.debug("push to carbon: %s %s %s" % (path,value,datetime_from_timestamp(timestamp)))
        cmd = 'echo "%s %s %s" | nc %s %s' % (path,value,timestamp,graphite_host,graphite_port)
        #print cmd
        log.debug(cmd)
        #print "%s %s %s" % (path, value, timestamp)
        os.system(cmd)
        return

class WifiHandler(FigaroHandler):
    """

        TODO:
        Sagemcom-Livebox-4.LK15015DP990272.wifi0.RebootCounterTimestamp 0001-01-01T00:00:00Z 1458080889
        Sagemcom-Livebox-4.LK16050DP990068.eth6.MACAddress 00:26:86:F0:36:61 1458164307

    """
    collection= 'WIFI'
    tags= ['WIFI','WI-FI']


    def handle(self, timestamp, collection, body):
        """

        :param timestamp:
        :param collection:
        :param body:
        :return:
        """
        self.log_line(collection)

        attributes = self.parse_attributes(body)

        if 'Interface' in attributes:
            interface= attributes.pop('Interface')

            # print attributes
            namespace = "%s.%s" % (self.parser.namespace,interface)
            for name, value in attributes.iteritems():
                path = "%s.%s" % (namespace, name)
                if ' ' in value:
                    value= value.split()[0]
                if not value:
                    value='0'
                self.push_to_graphite( path, value, timestamp.strftime('%s'))
        else:

            #raise RuntimeError('WIFI attributes without Interface field' )
            print attributes

        return
WifiHandler.register()

class WanHandler(FigaroHandler):
    """

    """
    collection= "WAN"

    def handle(self, timestamp, collection, body):
        """

        :param timestamp:
        :param collection:
        :param body:
        :return:
        """
        self.log_line(collection)

        #count= self.parser.white_board.line_count
        #msg= "=== %s ===========  %s  ==========" % (count,collection)
        #log.debug(msg)

        self.graphite_handler(timestamp,collection,body)

WanHandler.register()

class MeminfonHandler(FigaroHandler):
    """

    """
    collection = "MEMINFO"

    def handle(self, timestamp, collection, body):
        """

        :param timestamp:
        :param collection:
        :param body:
        :return:
        """
        self.log_line(collection)
        self.graphite_handler(timestamp,collection,body)

        return
MeminfonHandler.register()



class ProcessHandler(FigaroHandler):
    """

    """
    collection = "MEMINFO"


    def handle(self, timestamp, collection, body):
        """

        :param timestamp:
        :param collection:
        :param body:
        :return:
        """
        self.log_line(collection)
        self.graphite_handler(timestamp, collection, body)

ProcessHandler.register()


class DeviceHandler(FigaroHandler):
    """

    """
    collection = "DEVICE"

DeviceHandler.register()

class CpuHandler(FigaroHandler):
    """

    """
    collection = "CPU"

    def handle(self, timestamp, collection, body):
        """

        :param timestamp:
        :param collection:
        :param body:
        :return:
        """
        self.log_line(collection)
        self.graphite_handler(timestamp,collection,body)

        return
CpuHandler.register()



# FigaroParser.registrar.update({
#
#     'WIFI': WifiHandler,
#     'WAN': WanHandler,
#     'MEMINFO': MeminfonHandler,
#     'PROCESS': ProcessHandler,
#     #'DEVICE': DeviceHandler,
#     #'CPU': CpuHandler,
#
# })


if __name__== "__main__":


    logging.basicConfig(level=logging.DEBUG)

    log.info("BEGIN")

    ROOT= "./tests/samples"
    DIRECTORY = "./tests/samples/export_RECEIVED_lb4figaro_TACSIs_201603160000"
    CHUNK = "./tests/samples/export_RECEIVED_lb4figaro_TACSIs_201603160000/40F2018C8A70_1458081788"

    parser= FigaroParser()

    #parser.parse_directory(ROOT)
    parser.parse_file(CHUNK)


    namespace = parser.namespace


    print "collections: %s" % str(list(parser.white_board['collections']))

    last_device= parser.white_board.current_device_info
    last_file= parser.white_board.current_file


    log.info("DONE")
    print "Done."