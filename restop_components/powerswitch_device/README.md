powerswitch
===========

overview
--------
a powerswitch is a device to control outlets (power on/off)

this package implements a client/server solution to control a powerswitch

currently supports 2 models:
* energenie
* ippower


server
------
the server receives command via redis pubsub

to power_on an outlet (1 to 4)

send json command string:  [  101 , switch_on , {} ] on channel powerswitch/energenie/1
101 is the command_id

to power_off an outlet 

send json command string [ 101, switch_off , {} ] on channel powerswitch/energenie/1
 
 
 
we can wait response on channel -/powerswitch/energenie/ with a command_id = 101
 








IPPOWER
=======
model: 9258xx
-------------
see: http://www.aviosys.com/9258xx.html

4 power ports 


Default Login: admin
Default Password: 12345678

command
-------
http://ipaddress:port/ user=admin+pass=12345678+command



switch on port 1
-----------------
http://192.168.1.3/set.cmd?cmd=setpower+p61=1

switch off port 1
-----------------
http://192.168.1.3/set.cmd?cmd=setpower+p61=0

* port 1= 61 
* port 2= 62
* port 3= 63
* port 4= 64

read port status
----------------
http://192.168.1.3/set.cmd?cmd=getpower

Read current value
------------------
1a, 1b, 2a, 2b, 3a, 3b, 4a, 4b
- http://ip:port/set.cmd?user=admin+pass=12345678+cmd=getcurrent


command with login
------------------

http://ip:port/set.cmd?user=admin+pass=12345678+cmd=getschedule+power=xx,


ENERGENIE
=========

see powerswitch_device/devices/energenie.py