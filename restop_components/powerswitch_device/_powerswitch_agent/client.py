"""

    powerswitch client   a pubsub client to powerswitch server


"""
import redis
import random
import json


class RedisPublisher(object):
    """

    """

    def __init__(self,pool,name,root="powerswitch"):
        """

        :param pool: instance of redis ConnectionPool
        :param name: string eg ernergenie, powerswitch
        :param root_name:
        """
        self.pool=pool
        self.name= name
        self.root= root
        self.cnx= redis.Redis(connection_pool=pool)
        self.pubsub= self.cnx.pubsub()


    def channel(self, channel_name):
        """

        :param channel_name:
        :return:
        """
        return "%s/%s/%s" % (self.root, self.name, channel_name)

    def publish(self, channel_name, message):
        """

        :param channel_name:
        :param message:
        :return:
        """
        return self.cnx.publish(self.channel(channel_name), message)

    @classmethod
    def get_pool(cls,host="localhost",port=6379, db=0 ):
        return redis.ConnectionPool(host=host, port=port, db=db)



class PowerswitchClient(RedisPublisher):
    """


    """
    def expect(self,order,channel="stdout",timeout=3):
     """

        wait a response of type "OK $order $comment
     :param order:
     :return: a response dictionary ( code ,order, message)
     """
     # wait for a message of the corresponding order
     fqdn_channel= self.channel(channel)
     self.pubsub.subscribe(fqdn_channel)
     done= False
     response= dict(code="KO",order="0",message="unknown")
     while not done:
         # receive a pusub message ( type, data , channel)
         msg= self.pubsub.get_message(ignore_subscribe_messages=True,timeout=1)
         # if msg:
         #     content= msg['data']
         #     # content is string   $code $order $message
         #     parts= content.split(" ")
         #     if parts:
         #         response["code"]= parts.popleft()
         #         if parts:
         #             response["order"]= parts.popleft()
         #             if parts:
         #                 response["message"]= parts.popleft()
         #if response["order"]=
         done= True


    def switch_on(self,outlet=0):
     """
        send a message "ON 3243 " to powerswitch server on channel powerswitch/energenie/$outlet
     :param outlet: int 1 to 4
     :return:
     """
     order= random.randint(1,65535)
     message= "ON %d" % order
     self.publish(str(outlet),message)

     # wait for response  a dictionary code order message
     response = self.expect(order,channel=str(outlet))

     return True




