__author__ = 'cocoon'
"""


        an http proxy to ip power device



"""
import json
import requests
from requests.exceptions import ConnectionError

from restop.application import ApplicationError



headers= { 'Content-type': "application/json"}

defaults= dict( user='admin',password= '12345678',accept='text/html')


class PowerSwitchRack(object):
    """
        an Ip Power rack with 4 ports


    """
    def __init__(self,base_url, **parameters):
        """

        """
        if not base_url.startswith('http://'):
            base_url = 'http://' + base_url
        self.base_url= base_url

        self.user= parameters.get('user',defaults['user'])
        self.password= parameters.get('password',defaults['password'])

        self._channel= 0

        self.rs = requests.session()
        self.rs.headers.update({
            'Accept': defaults['accept'],
        })

        return

    def bind_channel(self,channel):
        """
            restrict the instance to a specific channel

        :param channel:
        :return:
        """
        self._channel= channel

    def port_url(self):
        """

        :param collection:
        :return:
        """
        url= self.base_url + "/set.cmd?user=%s+pass=%s" % (self.user,self.password)
        return url

    def switch_on(self,channel=None):
        """

            switch on port 1
            -----------------
            http://192.168.1.3/set.cmd?cmd=setpower+p61=1

        :param port:
        :return:
        """
        channel= channel or self._channel
        if channel:
            url= self.port_url() + "+cmd=setpower+p6%d=1" % int(channel)
            response= self.get(url)
            return True
        else:
            raise ApplicationError('no powerswitch channel specified')


    def switch_off(self, channel=None):
        """

            switch on port 1
            -----------------
            http://192.168.1.3/set.cmd?cmd=setpower+p61=1

            Output: p61=1

        :param port:
        :return:
        """
        channel = channel or self._channel
        if channel:
            url = self.port_url() + "+cmd=setpower+p6%d=0" % int(channel)
            response= self.get(url)
            return True
        else:
            raise ApplicationError('no powerswitch channel specified')

    def get_current(self):
        """

            get current
            -----------------
            http://192.168.1.3/set.cmd?cmd=getcurrent

        :param port:
        :return:
        """
        url= self.port_url() + "+cmd=getcurrent"
        response= self.get(url)
        return response.text




    def get(self,url,data=None,headers= None , access_token=None,**kwargs):
        data= data or {}
        headers= headers or {}
        if data:
            data= json.dumps(data)
            headers.update( {'Content-type': "application/json"})
        try:
            response=self.rs.get(url,data=data,headers=headers,**kwargs)
        except ConnectionError:
            raise ApplicationError("cannot reach powerswitch at %s" % url)
        if response.status_code == 200:
            # OK
            return response
        else:
            # KO
            raise ApplicationError("powerswitch error %s" % response.status_code)





if __name__=='__main__':


    pw= PowerSwitchRack('localhost:8080')

    response= pw.swith_on(1)

    print 'Done'