from core import PowerSwitchRack

import time
import requests
from requests.exceptions import ConnectionError

defaults= dict( password= '1',accept='text/html')


class PowerEnergenie(object):
    """
        control of power switch energenie EG-PM2
        login :

    """

    def __init__(self,base_url, **parameters):
        """

        :param base_url: string complete url eg http://192.168.1.60:80
        :param parameters: dict    password
        """
        if not base_url.startswith('http://'):
            base_url = 'http://' + base_url
        self.base_url= base_url
        self.password= parameters.get('password',defaults['password'])
        self._channel= 0
        self.rs= requests

        return

    def bind_channel(self, channel):
        """
            restrict the instance to a specific channel

        :param channel:
        :return:
        """
        self._channel = channel


    def login(self):
        """
        login

            POST http://$ip/login.html?pw=$password


            :return:
        """
        url = self.base_url + "/login.html"
        #data= dict(pw=1)
        data= "pw=%s" % self.password
        response = self.post(url,data=data)
        return response

    def logout(self):
        """
         POST http://$ip/login.html?pw=''
        :return:
        """
        url = self.base_url + "/login.html"
        #data= dict( pw=None)
        data= "pw="
        response = self.post(url,data=data)
        return response

    def switch_on(self, channel=None):
        """
            switch on port 1
            -----------------
            POST http://$ip?cte1=1&cte2=0

        :param port:
        :return:
        """
        self.login()
        channel = channel or self._channel
        if channel:
            url = self.base_url + "?cte%d=1" % int(channel)
            response = self.post(url)
            self.logout()
            return True
        else:
            self.logout()
            raise RuntimeError('no powerswitch channel specified')

    def switch_off(self, channel=None):
        """

            switch on port 1
            -----------------
            http://192.168.1.3/set.cmd?cmd=setpower+p61=1

            Output: p61=1

        :param port:
        :return:
        """
        self.login()
        channel = channel or self._channel
        if channel:
            url = self.base_url + "?cte%d=0" % int(channel)
            response = self.post(url)
            self.logout()
            return True
        else:
            self.logout()
            raise RuntimeError('no powerswitch channel specified')


    def post(self,url,data=None,headers= None , access_token=None,**kwargs):
        data= data or {}
        headers= headers or {}
        if data:
            #data= json.dumps(data)
            headers.update( {'Content-type': "application/json",'accept':"application/json"},)
        try:
            response=self.rs.post(url,data=data,headers=headers,**kwargs)
        except ConnectionError as error:
            raise RuntimeError("cannot reach powerswitch at %s" % url)
        if response.status_code == 200:
            # OK
            return response
        else:
            # KO
            raise RuntimeError("powerswitch error %s" % response.status_code)




if __name__=='__main__':


    parameters = dict(password='1')

    pw= PowerEnergenie('192.168.1.30:80',**parameters)

    # r= pw.logout()
    # r= pw.login()

    r= pw.switch_on(channel=1)
    time.sleep(0.5)
    r = pw.switch_on(channel=4)

    time.sleep(2)
    r = pw.switch_off(channel=1)
    time.sleep(0.5)
    r = pw.switch_off(channel=4)



    #r = pw.logout()

    #response= pw.switch_on(1)

    print('Done')