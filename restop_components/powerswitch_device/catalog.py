
from restop.application import ApplicationError
from restop_platform.models import Device,Profile
from models import Platform,DEVICE_NAME


from powerswitch_agent.energenie import PowerEnergenie
from powerswitch_agent.core import PowerSwitchRack

models= [
    "powerswitch",
    "energenie"
]



# class PowerSwitchCatalog(object):
#     """
#
#
#     """
#
#     @classmethod
#     def get(cls, name, channel=0, platform_name='default'):
#         """
#
#         :return:
#         """
#
#         platform = Platform.get(Platform.name == platform_name)
#         # list profile with collection== powerswitch
#         profiles = list(Profile.query(Profile.collection == DEVICE_NAME))
#         if not profiles:
#             # build profiles and device for this collection
#             platform._set_powerswitch()
#         try:
#             # get device object
#             device = Device.get(Device.name == name)
#         except ValueError:
#             raise ApplicationError('no such device: %s' % name)
#
#         # get a power
#         configuration = device.get_configuration()
#
#         return cls(name, configuration=configuration, channel=channel)





def get_powerswitch_handler(model,ip,channel=None,**parameters):
    """


    :param model:
    :param parameters:
    :return:
    """
    handler= None
    if not model in models:
        raise RuntimeError("unknown powerswitch model: %s" % model)

    if model.lower() == "energenie":
        handler= PowerEnergenie(ip, **parameters)
    else:
        handler= PowerSwitchRack(ip,**parameters)

    # bind handler to a channel
    if channel is not None:
        handler.bind_channel(channel)
    return handler



if __name__== "__main__":


    import time

    def test_energenie():


        parameters = dict(password='1')

        pw = get_powerswitch_handler("energenie",ip="192.168.1.30:80",channel=2, **parameters)

        pw.switch_on()
        time.sleep(1)
        pw.switch_off()

        return


    def test_powerswitch():


        parameters = dict( user="admin" , password='12345678')

        pw = get_powerswitch_handler("powerswitch",ip="192.168.1.60:80",channel=2, **parameters)

        pw.switch_on()
        time.sleep(1)
        pw.switch_off()

        return





    test_energenie()
    #test_powerswitch()
    print("Done")