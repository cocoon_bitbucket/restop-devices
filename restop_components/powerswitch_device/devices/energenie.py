"""

    control a powerswitch of type energenie




"""
import time
import json
import requests
from requests.exceptions import ConnectionError
#from bs4 import BeautifulSoup as bs

# for server
from restop_components.rpublish.core import short_channel
from restop_components.rpublish.core import Request,Response,short_channel
from restop_components.rpublish.listnener import RedisPubsubListener, Connector


import re
import logging

ROOT= "powerswitch"
NAME= "energenie"
PORT= 80
PASSWORD= "1"


defaults= dict( password= PASSWORD,accept='text/html')



class StatusPageObject(object):
    """

        Page object for the main html page of the energenie web interface

        GET http://$ip/energenie.html   ( once logged )



    """
    sockstates_pattern= r'var sockstates = \[(.*?)\]\;'



    def __init__(self,content):
        """

        :param content:
        """
        self.content= content
        return

    def outlet_status(self):
        """

            search for:  var sockstates = [1,0,0,1];
            in html content

        :return: list of integers
        """
        status=[]
        logging.debug("energenie status page search for %s" % self.sockstates_pattern)
        r= re.compile(self.sockstates_pattern)
        m= r.search(self.content)
        if m:
            status_string= m.group(1)
            logging.debug("found status string: %s" % status_string)
            try:
                parts= status_string.split(",")
                status= [ int(part) for part in parts]
            except Exception as err:
                logging.debug("fail to extract status from %s" % status_string)
        else:
            # not found
            pass
        return status




#
#  powerswitch energenie server
#


class EnergenieConnector(Connector):
    """

    """
    def __init__(self,**parameters):
        """

        :param parameters: dict ( ip= , port= password= )
        """
        super(EnergenieConnector,self).__init__(**parameters)

        #self.parameters=parameters
        # if not base_url.startswith('http://'):
        #     base_url = 'http://' + base_url

        ip= parameters.get("ip")
        port= parameters.get("port",PORT)

        self.base_url= "http://%s:%d" % ( ip,port)
        self.password=  str(parameters.get("password",PASSWORD))
        self._channel= 0
        self.rs= requests



    def handle_message(self,message):
        """
            handle message on channels powerswitch/energenie/*

        :return: object: instance of Response
        """
        channel = short_channel(message["channel"])
        data= message["data"]
        logging.debug("*********** Connector handle message: %s" % message)

        # emit response on channel stdout
        m= Request.from_json(data)
        command_id= m.command_id
        command= m.command.lower()

        # default response
        response = Response(command, response="KO on %s" % command, command_id=command_id)

        if channel in ["1","2","3","4"]:
            # receive message on channel powerswitch/energenie/1 to powerswitch/energenie/4
            outlet= int(channel)
            if command == "switch_on":
                r= self.switch_on(outlet)
                response = Response(command, response="OK", command_id=command_id)
            elif command == "switch_off":
                r= self.switch_off(outlet)
                response = Response(command, response="OK", command_id=command_id)
            else:
                # not implemented
                response = Response(command,response="KO bad keyword: %s for channel %s" % (command,channel),command_id=command_id)
        else:
            # receive message on generic channel  powerswitch/energenie
            if command == "status":
                status= self.status()
                response= Response(command,response=status,command_id=command_id)
            else:
                response = Response(command, response="KO unknown keyword: %s" % command, command_id=command_id)

        return response.json , "response"


    def login(self):
        """
        login

            POST http://$ip/login.html?pw=$password


            :return:
        """
        url = self.base_url + "/login.html"
        #data= dict(pw=1)
        data= "pw=%s" % self.password
        response = self.post(url,data=data)
        return response

    def logout(self):
        """
         POST http://$ip/login.html?pw=''
        :return:
        """
        url = self.base_url + "/login.html"
        #data= dict( pw=None)
        data= "pw="
        response = self.post(url,data=data)
        return response

    def switch_on(self, channel=None):
        """
            switch on port 1
            -----------------
            POST http://$ip?cte1=1&cte2=0

        :param port:
        :return:
        """
        self.login()
        channel = channel or self._channel
        if channel:
            url = self.base_url + "?cte%d=1" % int(channel)
            response = self.post(url)
            self.logout()
            return True
        else:
            self.logout()
            raise RuntimeError('no powerswitch channel specified')

    def switch_off(self, channel=None):
        """

            switch on port 1
            -----------------
            http://192.168.1.3/set.cmd?cmd=setpower+p61=1

            Output: p61=1

        :param port:
        :return:
        """
        self.login()
        channel = channel or self._channel
        if channel:
            url = self.base_url + "?cte%d=0" % int(channel)
            response = self.post(url)
            self.logout()
            return True
        else:
            self.logout()
            raise RuntimeError('no powerswitch channel specified')


    def status(self):
        """

            get outlet status
            -----------------
            GET http://192.168.1.30/energenie.html

        :param port:
        :return: list of outlet status, 0=OFF, 1= ON
        """
        outlets=[]
        self.login()
        url = self.base_url + "/energenie.html"
        response = self.rs.get(url)
        self.logout()
        if response.status_code == 200:
            page= StatusPageObject(response.text)
            outlets= page.outlet_status()
        else:
            # bad response return empty outlets
            pass
        return outlets



    def post(self,url,data=None,headers= None , access_token=None,**kwargs):
        data= data or {}
        headers= headers or {}
        if data:
            #data= json.dumps(data)
            headers.update( {'Content-type': "application/json",'accept':"application/json"},)
        try:
            response=self.rs.post(url,data=data,headers=headers,**kwargs)
        except ConnectionError as error:
            raise RuntimeError("cannot reach powerswitch at %s" % url)
        if response.status_code == 200:
            # OK
            return response
        else:
            # KO
            raise RuntimeError("powerswitch error %s" % response.status_code)





class EnergenieServer(RedisPubsubListener):
    """

        listen on
          * powerswitch/$name/stdin
          * powerswitch/$name/$outlet

        log to

            * powerswitch/$name/$outlet/stdout
            * powerswitch/$name/stdout

            * logs/powerswitch

    """



def get_energenie_server(redis_pool, ip , password= PASSWORD,  port= PORT, root= ROOT,name=NAME, user= None):

    server = EnergenieServer(redis_pool, root= root , name= name)

    connector = EnergenieConnector(ip= ip, port=port, password=password)
    server.set_connector(connector)
    return server



if __name__== "__main__":

    """



    """
    logging.basicConfig(level=logging.DEBUG)

    IP= "192.168.1.30"
    PORT= 80
    PASSWORD= "1"



    parameters = dict( ip= IP, port=PORT, password=PASSWORD)

    pw= EnergenieConnector(**parameters)

    # r= pw.logout()
    # r= pw.login()

    r= pw.switch_on(channel=1)
    time.sleep(0.5)
    r = pw.switch_on(channel=4)

    r= pw.status()


    time.sleep(2)
    r = pw.switch_off(channel=1)
    time.sleep(0.5)
    r = pw.switch_off(channel=4)



    #r = pw.logout()

    #response= pw.switch_on(1)

    print('Done')