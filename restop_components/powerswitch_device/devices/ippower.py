
import time
import json
# for server
from restop_components.rpublish.core import short_channel
from restop_components.rpublish.core import Request,Response,short_channel
from restop_components.rpublish.listnener import RedisPubsubListener, Connector
# for device
import requests
from requests.exceptions import ConnectionError
import logging

ROOT= "powerswitch"
NAME= "ippower"
PORT= 80
USER= "admin"
PASSWORD= "12345678"


defaults= dict( user=USER,  password= PASSWORD, port= PORT, accept='text/html')


#
#  powerswitch energenie server
#


class IppowerConnector(Connector):
    """

    """
    def __init__(self,**parameters):
        """

        :param parameters: dict ( ip= , port= password= )
        """
        super(IppowerConnector,self).__init__(**parameters)

        #self.parameters=parameters
        # if not base_url.startswith('http://'):
        #     base_url = 'http://' + base_url

        ip= parameters.get("ip")
        port= parameters.get("port",PORT)

        self.base_url= "http://%s:%d" % ( ip,port)
        self.password=  str(parameters.get("password",PASSWORD))

        self._channel= 0

        self.user = parameters.get('user', USER)

        self.rs = requests.session()
        self.rs.headers.update({
            'Accept': defaults['accept'],
        })



    def handle_message(self,message):
        """
            handle message on channels powerswitch/energenie/*

        :return:
        """
        channel = short_channel(message["channel"])
        data= message["data"]
        logging.debug("*********** sampleConnector handle message: %s" % message)

        # emit response on channel stdout
        m= Request.from_json(data)
        command_id= m.command_id

        # alaways ok response
        response= Response(m.command,response="OK",command_id=command_id)

        return response.json , "response"


    """
        an Ip Power rack with 4 ports


    """

    def bind_channel(self,channel):
        """
            restrict the instance to a specific channel

        :param channel:
        :return:
        """
        self._channel= channel

    def port_url(self):
        """

        :param collection:
        :return:
        """
        url= self.base_url + "/set.cmd?user=%s+pass=%s" % (self.user,self.password)
        return url

    def switch_on(self,channel=None):
        """

            switch on port 1
            -----------------
            http://192.168.1.3/set.cmd?cmd=setpower+p61=1

        :param port:
        :return:
        """
        channel= channel or self._channel
        if channel:
            url= self.port_url() + "+cmd=setpower+p6%d=1" % int(channel)
            response= self.get(url)
            return True
        else:
            raise RuntimeError('no powerswitch channel specified')


    def switch_off(self, channel=None):
        """

            switch on port 1
            -----------------
            http://192.168.1.3/set.cmd?cmd=setpower+p61=1

            Output: p61=1

        :param port:
        :return:
        """
        channel = channel or self._channel
        if channel:
            url = self.port_url() + "+cmd=setpower+p6%d=0" % int(channel)
            response= self.get(url)
            return True
        else:
            raise RuntimeError('no powerswitch channel specified')

    def get_current(self):
        """

            get current
            -----------------
            http://192.168.1.3/set.cmd?cmd=getcurrent

        :param port:
        :return:
        """
        url= self.port_url() + "+cmd=getcurrent"
        response= self.get(url)
        return response.text




    def get(self,url,data=None,headers= None , access_token=None,timeout=3,**kwargs):
        data= data or {}
        headers= headers or {}
        if data:
            data= json.dumps(data)
            headers.update( {'Content-type': "application/json"})
        try:
            response=self.rs.get(url,data=data,headers=headers,timeout=timeout,**kwargs)
        except ConnectionError:
            raise RuntimeError("cannot reach powerswitch at %s" % url)
        if response.status_code == 200:
            # OK
            return response
        else:
            # KO
            raise RuntimeError("powerswitch error %s" % response.status_code)






class IppowerServer(RedisPubsubListener):
    """

        listen on
          * powerswitch/$name/stdin
          * powerswitch/$name/$outlet

        log to

            * powerswitch/$name/$outlet/stdout
            * powerswitch/$name/stdout

            * logs/powerswitch

    """



def get_ippower_server(redis_pool, ip , user= USER, password= PASSWORD,  port= PORT, root= ROOT,name=NAME):

    server = IppowerServer(redis_pool, root= root , name= name)

    connector = IppowerConnector(ip= ip, port=port,user= user, password=password)
    server.set_connector(connector)
    return server



if __name__== "__main__":

    """



    """
    IP= "192.168.1.60"
    PORT= 80
    USER= "admin"
    PASSWORD= "12345678"



    parameters = dict( ip= IP, port=PORT,user=USER, password=PASSWORD)

    pw= IppowerConnector(**parameters)

    # r= pw.logout()
    # r= pw.login()

    r= pw.switch_on(channel=1)
    time.sleep(0.5)
    r = pw.switch_on(channel=4)

    time.sleep(2)
    r = pw.switch_off(channel=1)
    time.sleep(0.5)
    r = pw.switch_off(channel=4)



    #r = pw.logout()

    #response= pw.switch_on(1)

    print('Done')