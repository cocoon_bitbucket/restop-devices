#!/bin/bash

# This script toggles a socket on
# "energenie LAN programmable power connector"

typeset -a connectors=( 192.168.1.30 )
typeset -a passwords=( 1 )
typeset toggle="" state_dir="/var/tmp" lock_file="toggle_lamp"
typeset -i i=0 socket=1

typeset -r connectors passwords state_dir

while [ -e ${lock_file} ]; do
    if [ $i -gt 10 ]; then
        logger -p user.error -t `basename $0` -s -- "Could not execute - lock file detected."
        echo "Please contact administrator if problem exists for longer time." >&2
        exit 3
    fi
    i=`expr $i + 1`
    sleep 2
done

touch $lock_file

################# FUNCTIONS ###################

usage() {
    cat << EOF
You called ${0} with unsupported option(s).
Usage: ${0} [1|2|3|4] <on|off>
Numbers 1 to 4 stands for the socket number. If no socket is given, it will
toggle socket 1 per default.
Please try again.
EOF
}

get_states() {
# get states of sockets
    if [ $# -ne 1 ]; then
        return 1
    else
        srv=$1
    fi
    states=( $(curl -f http://${srv}/status.html 2>/dev/null | sed -r "s/(.*)((ctl.*)([0|1]),([0|1]),([0|1]),([0|1]))(.*)/\4 \5 \6 \7/") )
}

toggle() {
    local server="" str_state=""
    local -i i=0 state sckt

    if [ $# -ne 3 ]; then
        return 1
    fi

    while [ $# -gt 0 ]; do
        case $1 in
            1|2|3|4)
                sckt=${1}
                shift
                ;;
            "on")
                str_state="${1}"
                state=1
                shift
                ;;
            "off")
                str_state="${1}"
                state=0
                shift
                ;;
            *)
                server="${1}"
                shift
                ;;
        esac
    done

    # poll status and toggle only if needed
    get_states ${server}
    if [ ${state} -ne ${states[$( expr ${sckt} - 1 )]} ]; then
        curl -f -d "ctl${sckt}=${state}" http://${server}/ &>/dev/null
        logger -p user.info -t `basename $0` -- "state of ${server} socket ${sckt} toggled ${str_state} by ${LOGNAME}"
    fi
}

persist() {
# for cron job use only
# saves state of sockets

    local state_file
    local -i i=0 j=0
    while [ ${i} -lt ${#connectors[*]} ]; do
        state_file=${state_dir}/${connectors[$i]}

        if (curl -f -d "pw=${passwords[$i]}" http://${connectors[$i]}/login.html 2>/dev/null | grep -q 'status.html'); then
            logger -p user.info -t `basename $0` -- "Save states of ${connectors[$i]} to file ${state_file}"
            # get states
            get_states ${connectors[$i]}
            echo "SavedStates=( ${states[*]} )" > ${state_file}

            j=0
            while [ $j -lt ${#states[*]} ]; do
                j=`expr ${j} + 1`
                toggle ${j} off ${connectors[$i]}
                sleep 1
            done

            curl -f http://${connectors[$i]}/login.html &>/dev/null
            logger -p user.info -t `basename $0` -- "States saved and all sockets switched off"
        else
            logger -p user.error -t `basename $0` -s -- "Login to ${connectors[$i]} failed."
        fi

        i=`expr ${i} + 1`
        typeset +r state_file
    done
}

recover() {
# recovers states from state file

    local state_file new_state
    local -a SavedStates
    local -i i=0 j=0

    i=0
    while [ ${i} -lt ${#connectors[*]} ]; do
        typeset -r state_file=${state_dir}/${connectors[$i]}

        if [ -r ${state_file} ]; then

            source ${state_file}
            if (curl -f -d "pw=${passwords[$i]}" http://${connectors[$i]}/login.html 2>/dev/null | grep -q 'status.html'); then

                logger -p user.info -t `basename $0` -- "Restore socket states from ${state_file} to ${connectors[$i]}"
                j=0
                while [ ${j} -lt ${#SavedStates[*]} ]; do
                    if [ ${SavedStates[$j]} -eq 0 ]; then
                        new_state="off"
                    else
                        new_state="on"
                    fi
                    j=`expr ${j} + 1`
                    toggle ${j} ${new_state} ${connectors[$i]}
                    sleep 1
                done

                curl -f http://${connectors[$i]}/login.html &>/dev/null
                logger -p user.info -t `basename $0` -- "Socket states restored and switched on if needed."
            else
                logger -p user.error -t `basename $0` -s -- "Login to ${connectors[$i]} failed."
            fi
            rm ${state_file}

        else
            logger -p user.error -t `basename $0` -s -- "Could not read file ${state_file}"
        fi

        i=`expr ${i} + 1`
    done
}

common() {
# common mode

local -i i=0

while  [ ${i} -lt ${#connectors[*]} ]; do
    state_file=${state_dir}/${connectors[$i]}
    if [ -e ${state_file} ]; then
        # state file exists -> do not toggle life, change in state file only
        if [ ${new_state} = "on" ]; then
            new_state=1
        elif [ ${new_state} = "off" ]; then
            new_state=0
        fi
        socket=`expr ${socket} - 1`

        source $state_file
        if [ ${SavedStates[${socket}]} -ne ${new_state} ]; then
            SavedStates[${socket}]=$new_state
            echo "SavedStates=( ${SavedStates[*]} )" > ${state_file}
            logger -p user.info -t `basename $0` -- "Toggled state of socket ${socket} to ${new_state} in state file by ${LOGNAME}"
        fi

    else
        if (curl -f -d "pw=${passwords[$i]}" http://${connectors[$i]}/login.html 2>/dev/null | grep -q 'status.html'); then
            toggle ${socket} ${new_state} ${connectors[$i]}
#            curl -f -d "ctl${socket}=${new_state}" http://${connectors[$i]}/ &>/dev/null
            curl -f http://${connectors[$i]}/login.html &>/dev/null
        else
            logger -p user.error -t `basename $0` -s -- "Login to ${connectors[$i]} failed."
        fi
    fi
    i=$( expr $i + 1 )
done

}
############# END FUNCTIONS ##################

typeset -r curl_bin="$(which curl | head -n 1)"

if [ -z "${curl_bin}" ]; then
    echo "Tool curl not found. Please install it."
    exit 1
fi

if [ $# -lt 1 ]; then
    echo "No action provided. What should I do?"
    exit 1
fi

while [ $# -ge 1 ]; do
    case ${1} in
        "on"|"off")
            new_state=${1}
            mode="common"
            ;;
        1|2|3|4)
            socket=${1}
            mode="common"
            ;;
        "-r"|"--recover")
            mode="recover"
            ;;
        "-s"|"--save")
            mode="save"
            ;;
        *)
            usage
            rm $lock_file && exit 2
            ;;
    esac

    shift
done

case ${mode} in
    "recover")
        recover
        ;;
    "save")
        persist
        ;;
    "common")
        common
        ;;
esac

rm $lock_file && exit 0 || exit 1