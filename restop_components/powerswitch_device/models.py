DEVICE_NAME= "powerswitch_agents"

from walrus import *
from wbackend.model import Model
from wbackend.graph import Graph

from restop_platform.models import Platform as BasePlatform
from restop_platform.models import Device,Profile
from restop_platform.models import AgentModel


class PowerSwitchAgents(AgentModel):
    """


    """
    collection= DEVICE_NAME


class Platform(BasePlatform):
    """


    """

    def _set_powerswitch(self):
        """


        :return:
        """
        # setup links
        graph = Graph(self.database, namespace='links')


        # set powerswitch profiles
        # setup profiles
        profiles_data= self.data['profiles']
        for profile_name,profile_data in profiles_data.iteritems():
            collection = profile_data['collection']
            # create profiles for collection powerswitch
            if collection == DEVICE_NAME:
                self.collections.add(collection)
                d= Profile.create(name=profile_name,collection=collection,data=profile_data)


        # setup devices
        devices_data = self.data['devices']
        for device_name, device_data in devices_data.iteritems():
            # print device_name, devices_data
            profile_name = device_data.get('profile')
            try:
                profile = Profile.get(Profile.name == profile_name)
                if profile.collection == DEVICE_NAME:
                    d = Device.create(name=device_name,
                                      profile=profile_name,
                                      data=device_data)

                    # add link: device has_profile profile
                    graph.store(d.get_hash_id(), 'has_profile', profile.get_hash_id())
            except ValueError:
                # profile is not a powerswitch_agents profile
                pass

        return



