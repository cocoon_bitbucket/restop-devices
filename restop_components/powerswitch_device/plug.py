from walrus import *
from wbackend.model import Model

from restop.application import ApplicationError
from restop_platform.models import Device,Profile

from models import Platform,DEVICE_NAME


class PowerswitchPlug(object):
    """

        a plug for powerswitch



        :parameter: agent instance of restop_components.some_agents.PipeAgent

    """

    def __init__(self,name,configuration,channel=0):
        """


        :param name:
        :param configuration:
        """
        self.name=name
        self.configuration=configuration

        user= self.configuration.get('user','admin')
        password= self.configuration.get('password',123456)
        host= self.configuration['peer_ip']
        port= self.configuration.get('peer_port',None)


        if port:
            url= "%s:%s"% (host,port)
        else:
            url =host

        self.rack= PowerSwitchRack(url,user=user,password=password)
        if channel:
            self.rack.bind_channel(channel)
        return


    @classmethod
    def get(cls,name,channel=0,platform_name='default'):
        """

        :return:
        """

        platform = Platform.get(Platform.name == platform_name)
        # list profile with collection== powerswitch
        profiles = list(Profile.query(Profile.collection == DEVICE_NAME))
        if not profiles:
            # build profiles and device for this collection
            platform._set_powerswitch()
        try:
            # get device object
            device = Device.get(Device.name == name)
        except ValueError:
            raise ApplicationError('no such device: %s' % name)

        # get a power
        configuration = device.get_configuration()

        return cls(name,configuration=configuration,channel=channel)

    def switch_on(self,channel=0):
        """

        :return:
        """
        return self.rack.switch_on(channel)

    def switch_off(self,channel=0):
        """

        :param channel:
        :return:
        """
        return self.rack.switch_off(channel)
