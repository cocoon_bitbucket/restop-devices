"""
powerswitch client

a universal client for powerswitch  rpublish server


    usage:

        client = PowerswitchClient( redis_pool,name)

        r= client.switch_on(outlet=1)
        r= client.switch_off(outlet= 2 )





"""
from restop_components.rpublish.publisher import RedisPublisher

ROOT= "powerswitch"



class PowerswitchClient(RedisPublisher):
    """


    """

    def switch_on(self,outlet):
        """

        :param outlet: int or string : the slot number
        :return:
        """
        msg_id = self.send("switch_on", channel=str(outlet))
        #
        response = self.wait_response(msg_id, channel="response")
        return response

    def switch_off(self,outlet):
        """

        :param outlet:
        :return:
        """
        msg_id = self.send("switch_off", channel=str(outlet))
        #
        response = self.wait_response(msg_id, channel="response")
        return response


    def status(self):
        """

        :param outlet:
        :return:
        """
        msg_id = self.send("status")
        #
        response = self.wait_response(msg_id, channel="response")
        return response


if __name__== "__main__":
    """


    """

