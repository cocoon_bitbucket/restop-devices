"""

    get a server for a specific powerswitch ( energenie, ippower )


    2 models :

    * energenie  parameters: ip  [ password='1' , port=80]
    * ipposer    parameters: ip  [ user='admin', password="12345678' , port= 80 ]



    usage:

    server = get_powerswitch_server( redis_pool , name, ip , **parameters
    server.start()




"""
from restop.application import ApplicationError
from restop_components.powerswitch_device.devices.energenie import get_energenie_server
from restop_components.powerswitch_device.devices.ippower import get_ippower_server

POWERSWITCH_CATALOG= ("energenie", "ippower")






def get_powerswitch_server(redis_pool, name, ip ,model=None,**parameters):
    """



    :param redis_pool:
    :param name: string the name of powerswitch model [ energenie, ippower ]
    :param ip: string  ip of the device
    :param parameters: dict other parameters ( user, password ,port )
    :return:
    """
    model = model or name
    server= None


    if model not in POWERSWITCH_CATALOG:
        raise ApplicationError("not supported powerswitch model [%s], must be in %s" %
                               (model,POWERSWITCH_CATALOG))

    if model == "energenie":
        # energenie
        server= get_energenie_server(redis_pool,ip, **parameters)
    else:
        # ippower by default
        server= get_ippower_server(redis_pool,ip, ** parameters)

    return server