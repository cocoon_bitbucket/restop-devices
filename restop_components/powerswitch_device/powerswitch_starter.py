"""


    starts the powerswitch redis pubsub servers


    read platform.yaml
        search for devices section to find device with "powerswitch" profile

        for each of the device in this profile, start a server
        availables servers are
        * ippower
        * energenie


    platform.yaml structure


    profiles:
         # profiles for sub devices
        powerswitch:
            collection: powerswitch_agents

    devices:


          # a sample device using powerswith energenie port 1
          sample:
            profile: sample
            powerswitch_device: energenie
            powersiwtch_port: 1

          # sub devices
          energenie:
              profile: powerswitch
              peer_ip: 192.168.1.30
              peer_port: 80
              model: EG-PM2-LAN
              number_of_ports: 4
              password: 1

          ippower:
              profile: powerswitch
              peer_ip: 192.168.1.60
              peer_port: 80
              model: IP-POWER
              number_of_ports: 4
              user: admin
              password: 12345678



"""
from restop_platform import Dashboard
from restop_platform.journal import Journal

from restop_platform.models import *
from restop_components.powerswitch_device.powerswitch_server import get_powerswitch_server


import logging


POWERSWITCH_PROFILE= "powerswitch"

#POWERSWITCH_PROFILE= "NONE"



class PowerswitchStarter(object):
    """



    """
    def __init__(self,platform_name="default"):
        """



        :param platform_name:
        """
        self.platform= Platform.get(Platform.name=='default')

        self.redis_pool= self.platform.database.connection_pool
        self.devices= {}
        self.search_devices()



    def search_devices(self):
        """
            search in platorm.yml the devices with 'powerswitch' as profile

        :return:
        """
        # search devices with 'powerswitch' profile
        devices= self.platform.data["devices"]
        for name in devices:
            parameters= devices[name]
            profile= parameters.get("profile",None)
            if profile:
                if profile == POWERSWITCH_PROFILE:
                    # this a powerswitch device
                    self.devices[name] = parameters
                    logging.info("Powerswitch found: %s  %s" % (name,parameters))
        return self.devices

    def start(self):
        """
            start the powerswitch in self.devices
        :return:
        """
        if not self.devices:
            # no powerswitch device declared on platform
            logging.debug("no powerswitch devices declared in platform with profile: %s" % POWERSWITCH_PROFILE)
            return False


        for name in self.devices:
            parameters= self.devices[name]
            # map the config
            ip= parameters.get("peer_ip")
            port= parameters.get("peer_port",None)
            user= parameters.get("user",None)
            password= parameters.get("password",None)

            server= get_powerswitch_server(self.redis_pool,name,ip,port=port,user=user,password=password)
            channels= server.channel("*")
            server.start()
            logging.debug("start powerswitch server for : %s listening on pubsub channels: %s" % (name,channels))
        return True


    def get_device_powerswitch(self, device_alias):
        """
            retrieve the powerswitch alias of the device

        :param alias: string alias ot the device , eg sample_A, alice ...
        :return:
        """
        # find the device
        powerswitch_alias= None
        device= Device.get(Device.name == device_alias)
        if "powerswitch_device" in device.data:
            # found directly on device parameters
            powerswitch_alias= device.data["powerswitch_device"]
        else:
            # search in profile
            profile= device.get_profile()
            powerswitch_alias = profile.data["powerswitch_device"]
        return powerswitch_alias

