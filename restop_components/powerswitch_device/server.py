"""

    powerswitch server:  a pubub server for a powerswtich


    a server to handle an electrical power switch (via http )


    the server is started with host,port and configuration

    * listen to redis pusub channels:

    ** powerswitch/$name/$outlet    eg powerswitch/energenie/2
            accept messages: ON / OFF

    ** powerswitch/$name/stdin
        accept messages
         * PING $id    return message "OK $id" on powerswitch/$name/stdlog
         * CLOSE $id   return message "OK $id" on powerswitch/$name/stdlog


    * emit logs on channels
        * powerswitch/$name/$outlet/stdout  eg powerswitch/energenie/1/stdlog
           * respond OK or KO /reason
        * logs/powerswitch
            log all powerswitch operation eg powerswitch/energenie/1/stlog ON
                                             powerswitch/energenie/stdlog CLOSE

    examples :

        check the server is on
        1) send message "PING $id" to channel powerswitch/$name
        2) wait for message "OK $id on channel powerswitch/$name/stdlog

        switch on the outlet number 1 on device named energenie

        1) send a pubsub message "ON $id" to channel powerswitch/energenie/1
        2) wait response on channel powerswitch/energie/1/stdlog
           the message should be  OK $id or KO $id $reason

        get the status of outlets:

        1) send a message "status $id" to powerswitch/$name/stdin
        2) wait response  "OK $id 1=1 2=0 3=0 4=0" on powersiwtch/$name/stdlog


"""

import redis



class RedisPubsubListener(object):
    """

    """
    def __init__(self,pool,root="root",name="name"):
        """

            Listener will listen on  root/name/*
        :param redis_db:
        """
        self.root = root
        self.name = name
        self.cnx = redis.Redis(connection_pool=pool)
        self.pubsub = self.cnx.pubsub()
        self.channels= []
        self.subscribe()
        self.connector= None
        self.done = False

    def subscribe(self):
        """

        :return:
        """
        self.channels= ["%s/%s/*" % (self.root,self.name)]
        for channel in self.channels:
            self.pubsub.psubscribe(channel)


    @classmethod
    def get_pool(cls,host="localhost",port=6379, db=0 ):
        return redis.ConnectionPool(host=host, port=port, db=db)



    def log(self,message,channel="stdlog"):
        """
            log to channel /root/name/stdlog by default
        :param message:
        :return:
        """
        fqdn_channel= "%s/%s/%s" % (self.root,self.name,channel)
        self.cnx.publish(fqdn_channel,message)

    def check(self):
        """

        :return:
        """
        self.cnx.ping()

    def run(self):
        """

        :return:
        """
        # start relay_stdout thread
        self.log("MANAGER START LISTENING")
        #self.stdout_thread.start()

        self.done= False
        while not self.done:
            item= self.pubsub.get_message(timeout=5)
            if item:
                self.work(item)
            # else:
            #     print("no item")

        self.pubsub.unsubscribe()
        self.log("MANAGER unsubscribed and finished")

    def start(self):
        """

            launch the run thread
        :return:
        """


    def work(self, item):
        """

        :param item:
        :return:
        """
        # pprint(item)
        if item["type"] == "message":
            # messages
            self.log("Message is: %s" % item["data"])

        elif item["type"].endswith("subscribe"):
            # assume a subscribe message
            self.log("Subscription confirmation %s to channel %s" % (item["type"], item["channel"]))



class PowerSwitchServer(RedisPubsubListener):
    """

        listen on
          * powerswitch/$name/stdin
          * powerswitch/$name/$outlet

        log to

            * powerswitch/$name/$outlet/stdout
            * powerswitch/$name/stdout

            * logs/powerswitch



    """
    def configure(self,ip,port=0,**parameters):
        """

            configure the powerswwitch
        :param name:
        :param ip:
        :param port:
        :param parameters:
        :return:
        """
        self.ip = ip
        self.port = port or 80
        self.parameters = parameters

    def work(self, item):
        """

        :param item:
        :return:
        """
        # pprint(item)
        if item["type"] == "message":
            # messages
            self.log("Message is: %s" % item["data"])

            # analyse channel
            if item["channel"].channel.endswith("stdin"):
                # this is an internal command on channel $root/$name/stdin
                return self.handle_stdin(item["message"])
            else:
                return self.handle_command(item["channel"],item["message"])


        elif item["type"].endswith("subscribe"):
            # assume a subscribe message
            self.log("Subscription confirmation %s to channel %s" % (item["type"], item["channel"]))


    def handle_stdin(self,message):
        """

        :param message: string    line "PING 12"  , "CLOSE 25"
        :return:
        """
        return "OK 0 no_message"


    def handle_command(self,channel,message):
        """


        :param channel: string like $root/$name/$outlet
        :param message: string like "ON 24"
        :return:
        """



if __name__ == "__main__":


    REDIS_HOST = "localhost"
    REDIS_DB = 0
    REDIS_PORT = 6379

    P_HOST= "192.168.1.30"
    P_PORT= 80
    P_PASSWORD= "1"


    pool = RedisPubsubListener.get_pool(host=REDIS_HOST, port=REDIS_PORT, db=REDIS_DB)

    l= PowerSwitchServer(pool,root="powerswitch",name="energenie")

    l.configure(ip=P_HOST,port=P_PORT,password=P_PASSWORD)

    l.run()


    print("Done.")
