import time
import json


from restop_components.rpublish.core import Request,Response

from restop_components.powerswitch_device.devices.energenie import get_energenie_server
from restop_components.powerswitch_device.powerswitch_client import PowerswitchClient



ROOT= "powerswitch"
NAME= "energenie"

REDIS_HOST = "localhost"
REDIS_DB = 0
REDIS_PORT = 6379


P_HOST = "192.168.1.30"
#P_PORT = 80
#P_PASSWORD = "1"



def test_connector_handle_message():
    """



    :return:
    """
    pool = get_redis_pool(host=REDIS_HOST, port=REDIS_PORT, db=REDIS_DB)
    server = get_energenie_server(pool, ip=P_HOST, name=NAME)


    request=  Request("switch_on",{'outlet':1})
    # simulate a redis pubsub message
    rmsg= dict( channel="powerswitch/energenie/1" , data= request.json )

    server.connector.handle_message(rmsg)

    request = Request("switch_off", {'outlet': 1})
    # simulate a redis pubsub message
    rmsg = dict(channel="powerswitch/energenie/1", data=request.json)

    server.connector.handle_message(rmsg)



    return


def test_publisher():


    pool = get_redis_pool(host=REDIS_HOST, port=REDIS_PORT, db=REDIS_DB)

    #
    # observers
    #
    watcher = get_observer(pool, NAME, root=ROOT)
    watcher.start()
    back_watcher = get_back_observer(pool, NAME, root=ROOT)
    back_watcher.start()

    #
    # server
    #
    server = get_energenie_server(pool,ip=P_HOST,name=NAME)
    server.start()


    #l = SampleClient(pool, root=ROOT, name=NAME)
    l= PowerswitchClient(pool,name=NAME)
    l.publish("stdin",json.dumps("PING"))

    #l.publish("stdin","my message")
    #l.publish("1/stdin","message to outlet 1")


    msg_id= l.send("switch_on",channel="1")
    #
    response= l.wait_response(msg_id,channel="response")
    print( response)


    time.sleep(5)



    server.done =True
    server.join()


    return








if __name__ == "__main__":


    from restop_components.rpublish import get_redis_pool
    from restop_components.rpublish.core import get_observer, get_back_observer

    import logging
    logging.basicConfig(level=logging.DEBUG)


    test_connector_handle_message()
    test_publisher()

    print("Done.")
