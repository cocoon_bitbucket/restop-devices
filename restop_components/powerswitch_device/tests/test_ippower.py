import time
import json


from restop_components.powerswitch_device.devices.ippower import get_ippower_server
from restop_components.powerswitch_device.powerswitch_client import PowerswitchClient



ROOT= "powerswitch"
NAME= "ippower"

REDIS_HOST = "localhost"
REDIS_DB = 0
REDIS_PORT = 6379


P_HOST = "192.168.1.60"
P_PORT = 80
P_USER = "admin"
P_PASSWORD = "12345678"


def test_publisher():


    pool = get_redis_pool(host=REDIS_HOST, port=REDIS_PORT, db=REDIS_DB)

    #
    # observers
    #
    watcher = get_observer(pool, NAME, root=ROOT)
    watcher.start()
    back_watcher = get_back_observer(pool, NAME, root=ROOT)
    back_watcher.start()

    #
    # server
    #
    server = get_ippower_server(pool,ip=P_HOST,user=NAME,name=NAME)
    server.start()


    #l = SampleClient(pool, root=ROOT, name=NAME)
    l= PowerswitchClient(pool,name=NAME)
    l.publish("stdin",json.dumps("PING"))

    #l.publish("stdin","my message")
    #l.publish("1/stdin","message to outlet 1")


    msg_id= l.send("swith_on",channel="1")
    #
    response= l.wait_response(msg_id,channel="response")
    print( response)


    time.sleep(5)



    server.done =True
    server.join()


    return



if __name__ == "__main__":


    from restop_components.rpublish import get_redis_pool
    from restop_components.rpublish.core import get_observer, get_back_observer

    import logging
    logging.basicConfig(level=logging.DEBUG)

    test_publisher()

    print("Done.")
