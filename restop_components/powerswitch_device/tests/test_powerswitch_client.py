"""





"""
import time
from restop_components.rpublish import get_redis_pool
from restop_components.powerswitch_device.powerswitch_server import get_powerswitch_server
from restop_components.powerswitch_device.powerswitch_client import PowerswitchClient
import logging

REDIS_HOST="localhost"
REDIS_PORT=6379
REDIS_DB=0



ENERGENIE_MODEL= "energenie"
ENERGENIE_IP= "192.168.1.30"
ENERGENIE_PASSWORD= "1"


IPPOWER_MODEL= "ippower"
IPPOWER_IP= "192.168.1.60"
IPPOWER_USER= "admin"
IPPOWER_PASSWORD= "12345678"



def test_powerswitch_energenie():
    """

    :return:
    """

    redis_pool=get_redis_pool(REDIS_HOST,REDIS_PORT,REDIS_DB)

    # setup the server
    server= get_powerswitch_server(redis_pool,
                    ENERGENIE_MODEL,ENERGENIE_IP, password= ENERGENIE_PASSWORD)
    server.start()

    # setup the client

    client= PowerswitchClient(redis_pool, "energenie")


    client.switch_on(1)
    time.sleep(0.5)
    client.switch_on(4)

    time.sleep(1)
    client.switch_off(1)
    time.sleep(0.5)
    client.switch_off(4)


    time.sleep(1)


    server.done=True
    server.join()

    return


def test_powerswitch_ippower():
    """

    :return:
    """

    redis_pool = get_redis_pool(REDIS_HOST, REDIS_PORT, REDIS_DB)

    # setup the server
    server = get_powerswitch_server(redis_pool,
                    IPPOWER_MODEL, IPPOWER_IP, user=IPPOWER_USER,password=IPPOWER_PASSWORD)
    server.start()

    # setup the client

    client = PowerswitchClient(redis_pool, "ippower")

    client.switch_on(1)

    return


if __name__=="__main__":

    logging.basicConfig(level=logging.DEBUG)

    test_powerswitch_energenie()

    #test_powerswitch_ippower()



    print("done")

