
from yaml import load
from wbackend.model import Model,Database
from restop_platform.models import *

from restop_components.powerswitch_device.powerswitch_starter import PowerswitchStarter
from restop_components.powerswitch_device.powerswitch_client import PowerswitchClient
import logging


ROOT= "powerswitch"
NAME= "energenie"

REDIS_HOST = "localhost"
REDIS_DB = 0
REDIS_PORT = 6379

PLATFORM_FILE= "./platform.yaml"

#P_HOST = "192.168.1.30"
#P_PORT = 80
#P_PASSWORD = "1"


def redis_db():
    """

    :return:
    """
    db = Database(
        host= REDIS_HOST,
        port= REDIS_PORT,
        db= REDIS_DB
    )
    Model.bind(database=db, namespace=None)

    return db


def platform():
    """

    :return:
    """
    db = redis_db()
    db.flushdb()
    try:
        p= Platform.get(Platform.name=='default')
    except Exception as e:
        # does not exists create it
        db.flushdb()
        #stream = file('../../tests/platform.yml')
        stream = open(PLATFORM_FILE,"rb")
        data = load(stream)
        print("create platform with data: --[\n%s\n]--" % data)
        p = Platform.create(name='default', data=data)
        p.setup()

    return p



def test_powerswitch_starter():
    """

    :return:
    """
    # build a default platform from sample
    p = platform()

    name= p.name


    pw= PowerswitchStarter(name)
    pw.start()


    return


def test_powerswitch_starter_energenie():
    """

    :return:
    """
    # build a default platform from sample
    p = platform()

    name= p.name

    # start powerswitch servers
    pw= PowerswitchStarter(name)
    pw.start()


    sample_powerswitch_alias= pw.get_device_powerswitch("sample_1")

    #sample= Device.get(Device.name == "sample_1")


    redis_pool= pw.redis_pool

    #client= PowerswitchClient(redis_pool,"energenie")
    client = PowerswitchClient(redis_pool, sample_powerswitch_alias)

    client.switch_on(2)
    time.sleep(1)
    client.switch_off(2)

    client.switch_on(1)
    time.sleep(1)
    client.switch_off(1)



    return







if __name__=="__main__":
    """


    """
    logging.basicConfig(level=logging.DEBUG)

    test_powerswitch_starter_energenie()
    #test_powerswitch_starter()

    print("Done.")

