
import os
import time
import threading
import json
import redis
from pprint import pprint
import logging
logger= logging.getLogger()




def get_redis_pool(host="localhost", port=6379, db=0):
    return redis.ConnectionPool(host=host, port=port, db=db)



def fqdn_channel(root,name,channel=None):
    """
    :param root: string
    :param name: string
    :param channel: string
    :return:
    """
    if channel:
        ch= "%s/%s/%s" %(root,name,channel)
    else:
        ch= "%s/%s" % (root,name)
    return ch


def fqdn_back_channel(fqdn_channel):
    """
        return the channel precede by -/  the return back channel

    :param root: string
    :param name: string
    :param channel: string
    :return:
    """
    return "-/%s" % fqdn_channel


def short_channel(fqdn_channel):
    """

        remove the 2 first part of the channel
        eg    root/name/1/stdin   =>  1/stdin

    :param fqdn_channel:
    :return:
    """
    short= fqdn_channel
    if "/" in fqdn_channel:
        try:
            parts= fqdn_channel.split("/")
            if parts[0] == "-":
                # filter the back channel mark : "-"
                parts.pop(0)
            parts= parts[2:]
            short= "/".join(parts)
        except Exception as err:
            pass
    return short


#
#  observers
#


def observer_handler(message):
    """

    :return:
    """
    #pprint(message)
    logger.debug(";)observer see: %s" % message)


def get_observer( pool , name, root="root" ,handler=observer_handler):
    """
        observer subscribe to $root/$name/*
        and print all received message

        usage:
            o= get_observer( redis_pool, "name", root= "root
            o.start()

    :param r:
    :return:
    """
    cnx = redis.Redis(connection_pool=pool)
    pubsub = cnx.pubsub()
    channel= fqdn_channel(root,name,"*")
    pubsub.psubscribe(channel)

    def _observer():
        """
        :return:
        """
        logger.debug("observer started on channels: %s" % channel)
        for message in pubsub.listen():
            handler(message)
        logger.debug("observer exited for channels: %s" % channel)
        pubsub.punsubscribe()

    observer = threading.Thread(target=_observer, args=())
    observer.daemon = True
    return observer


def get_back_observer( pool , name, root="root" ,handler=observer_handler):
    """
        observer subscribe to -/$root/$name/*
        and print all received message

        usage:
            o= get_back_observer( redis_pool, "name", root= "root
            o.start()

    :param r:
    :return:
    """
    cnx = redis.Redis(connection_pool=pool)
    pubsub = cnx.pubsub()
    channel= fqdn_channel(root,name,"*")
    back_channel= fqdn_back_channel(channel)
    pubsub.psubscribe(back_channel)

    def _observer():
        """
        :return:
        """
        logger.debug("back_observer started on channels: %s" % back_channel)
        for message in pubsub.listen():
            handler(message)
        logger.debug("back_observer exited for channels: %s" % back_channel)
        pubsub.punsubscribe()

    observer = threading.Thread(target=_observer, args=())
    observer.daemon = True
    return observer



def get_timestamp():
    """

    :return:
    """
    t= time.time()
    return t



#
#   Request / Response objects
#

class Request(object):
    """
        a message componsed by
        a command id
        a command name
        and parameters

    """
    def __init__(self,command,arguments=None,command_id=None):
        """

        :param command:
        :param arguments:
        :param command_id:
        """
        self.command= command
        self.arguments=arguments or {}
        self.command_id= command_id or str(get_timestamp())

    @property
    def data(self):
        """

        :return:
        """
        return [ self.command_id,self.command,self.arguments]

    @property
    def json(self):
        """

        :return: a json representation of the message
        """
        return json.dumps(self.data)


    @classmethod
    def from_json(cls,text):
        """

        :param text:
        :return:
        """
        data = json.loads(text)
        #        command , args , command_id
        m = cls( data[1], data[2],str(data[0]))
        return m



class Response(object):
    """
        a message componsed by
        a command id
        a command name
        and parameters

    """
    def __init__(self,command,response=None,command_id=None):
        """

        :param command:
        :param arguments:
        :param command_id:
        """
        self.command= command
        self.response = response or ""
        self.command_id= command_id or str(get_timestamp())

    @property
    def data(self):
        """

        :return:
        """
        return [ self.command_id,self.command,self.response]

    @property
    def json(self):
        """

        :return: a json representation of the message
        """
        return json.dumps(self.data)


    @classmethod
    def from_json(cls,text):
        """

        :param text:
        :return:
        """
        data = json.loads(text)
        #        command , args , command_id
        m = cls( data[1], data[2],str(data[0]))
        return m



