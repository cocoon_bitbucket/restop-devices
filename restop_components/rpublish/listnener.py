"""

    a pubub server for a $root/$name channel


    a server to handle an electrical power switch (via http )

    the server is started with host,port and configuration

    * listen to redis pusub channels:
        $root/$name

    * emit stdlog, stdout stderr response on
        -/$root/$name/<stdlog|response|stdout...>


    ** powerswitch/$name/$outlet    eg powerswitch/energenie/2
            accept messages: ON / OFF

    #
    # samples
    #
    ** powerswitch/$name/stdin
        accept messages
         * PING $id    return message "OK $id" on powerswitch/$name/stdlog
         * CLOSE $id   return message "OK $id" on powerswitch/$name/stdlog


    * emit logs on channels
        * -/$root/$name/stdlog  eg -/powerswitch/energenie/stdlog

    * emit response on stdout:
        * response -/powerswitch/energenie/response


    examples :

        check the server is on
        1) send message "PING $id" to channel powerswitch/$name
        2) wait for message "OK $id on channel -/powerswitch/$name/response

        switch on the outlet number 1 on device named energenie

        1) send a pubsub message "ON $id" to channel powerswitch/energenie/1
        2) wait response on channel -/powerswitch/energie/response
           the message should be  OK $id or KO $id $reason

        get the status of outlets:

        1) send a message "status $id" to powerswitch/$name
        2) wait response  "OK $id 1=1 2=0 3=0 4=0" on -/powersiwtch/$name/response


"""
import threading
import redis
from core import get_redis_pool,fqdn_channel,fqdn_back_channel,short_channel
from core import Request,Response
import logging
logger= logging.getLogger()



class Connector(object):
    """



    """
    def __init__(self,**parameters):
        """

        :param parameters:
        """
        self.parameters=parameters

    def open(self,message=None):
        return True

    def close(self,message=None):
        return True

    def handle_message(self,message):
        """

        :param message: dict a redis message  type,channel,data
        :return:
        """
        pass


class RedisPubsubListener(threading.Thread):
    """

        * listen on $root/$name/*
        * emit on  -/$kind/$root/$name/*
            $kind can be stdlog , stdout, stderr, response


    """
    def __init__(self,pool,root="root",name="name"):
        """

            Listener will listen on  root/name/*
        :param redis_db:
        """
        threading.Thread.__init__(self)
        self.root = root
        self.name = name
        self.cnx = redis.Redis(connection_pool=pool)
        self.pubsub = self.cnx.pubsub()
        self.channels= []
        self.subscribe()
        self.connector= None
        self.daemon= True
        self.done = False

    def subscribe(self):
        """
            subscribe to root/name/*
        :return:
        """
        self.channels= [fqdn_channel(self.root,self.name,"*")]
        for channel in self.channels:
            self.pubsub.psubscribe(channel)

    def channel(self,short_name=""):
        return fqdn_channel(self.root,self.name,short_name)


    def log(self,message,channel="stdlog"):
        """
            log to channel $root/$name/stdlog by default
        :param message:
        :return:
        """

        log_channel= fqdn_back_channel(self.channel("stdlog"))
        self.cnx.publish(log_channel,message)

    def check(self):
        """
            check redis connection
        :return:
        """
        return self.cnx.ping()

    def run(self):
        """

        :return:
        """
        # start relay_stdout thread
        self.log("MANAGER START LISTENING")
        logger.debug("MANAGER START LISTENING on channels: %s" % str(self.channels))


        self.done= False
        while not self.done:
            item= self.pubsub.get_message(timeout=5)
            if item:
                self.work(item)
            # else:
            #     print("no item")

        self.pubsub.punsubscribe()
        self.log("MANAGER unsubscribed and finished")
        logger.debug("MANAGER unsubscribed and finished on channels: %s" % str(self.channels))


    def handle_subscription(self,rmessage):
        """

        :param rmessage: dict a redis message ( type,channel,data)
        :return:
        """
        self.log("Subscription confirmation %s to channel %s" % (rmessage["type"], rmessage["channel"]))


    def work(self, item):
        """

        :param item:
        :return:
        """
        # pprint(item)
        if item["type"].endswith("message") :
            # messages  ( message or pmessage
            #self.log("Message is: %s" % item["data"])
            # root message to connector
            if self.connector:
                response,return_channel= self.connector.handle_message(item)
                if response:
                    # emit a response on the return channel
                    response_channel= fqdn_back_channel(self.channel(return_channel))
                    self.cnx.publish(response_channel,response)


        elif item["type"].endswith("subscribe"):
            # assume a subscribe message  (subscribe or psubscribe
            self.handle_subscription(item)


    def set_connector(self,connector):
        """

        :param connector: an object with a handle_message method
        :return:
        """
        self.connector=connector

#
#   samples
#

class SampleConnector(Connector):
    """

    """

    def handle_message(self,message):
        """

        :param message: dict a redis message type,channel,data
        :return:
        """
        channel = short_channel(message["channel"])
        data= message["data"]
        print("*********** sampleConnector handle message: %s" % message)

        # emit response on channel stdout
        m= Request.from_json(data)
        command_id= m.command_id

        # alaways ok response
        response= Response(m.command,response="OK",command_id=command_id)

        return response.json , "response"


class SampleServer(RedisPubsubListener):
    """

        listen on
          * powerswitch/$name/stdin
          * powerswitch/$name/$outlet

        log to

            * powerswitch/$name/$outlet/stdout
            * powerswitch/$name/stdout

            * logs/powerswitch



    """
    # def log(self,message,channel="stdlog"):
    #     ch= fqdn_channel(self.root, self.name, channel)
    #     print("%s, %s" % (ch, message))
    #     self.cnx.publish( ch, message)



if __name__ == "__main__":


    REDIS_HOST = "localhost"
    REDIS_DB = 0
    REDIS_PORT = 6379

    P_HOST= "192.168.1.30"
    P_PORT= 80
    P_PASSWORD= "1"




    pool = get_redis_pool(host=REDIS_HOST, port=REDIS_PORT, db=REDIS_DB)

    l= SampleServer(pool,root="powerswitch",name="energenie")

    connector = SampleConnector(ip=P_HOST, port=P_PORT, password=P_PASSWORD)
    l.set_connector(connector)

    l.run()


    print("Done.")
