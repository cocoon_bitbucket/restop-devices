"""

    powerswitch client   a pubsub client to powerswitch server


"""
import time
import random
import redis

from core import fqdn_channel,fqdn_back_channel,short_channel
from core import Request, Response



class RedisPublisher(object):
    """

    """

    def __init__(self,pool,name,root="powerswitch"):
        """

        :param pool: instance of redis ConnectionPool
        :param name: string eg ernergenie, powerswitch
        :param root_name:
        """
        self.pool=pool
        self.name= name
        self.root= root
        self.cnx= redis.Redis(connection_pool=pool)
        self.pubsub= self.cnx.pubsub()
        # subscribe to back_channel  -/root/name/*
        self.subscribe()


    def subscribe(self):
        """

        :return:
        """
        back_channel = fqdn_back_channel(self.channel("*"))
        self.pubsub.psubscribe(back_channel)


    def channel(self, short_channel):
        """

        :param channel_name:
        :return:
        """
        return fqdn_channel(self.root, self.name, short_channel)

    def back_channel(self,fqdn_channel):
        """

        :return:
        """
        return fqdn_back_channel(fqdn_channel)


    def publish(self, channel_name, message):
        """
            publish a message to fqdnchannel  $root/$name/$channel
        :param channel_name:
        :param message:
        :return:
        """
        return self.cnx.publish(self.channel(channel_name), message)


    def send(self,command,arguments=None,channel="request",command_id=None):
        """
            send a request

        :param command: string command name
        :param arguments: dict arguments
        :param channel: string short channel name
        :param command_id: string command identifier
        :return:
        """
        arguments=arguments or {}
        assert isinstance(arguments,dict)
        m= Request(command, arguments=arguments, command_id=command_id)
        msg_id= m.command_id
        #channel = self.channel(channel)
        r= self.publish(channel , m.json )

        return msg_id

    def wait_response(self,command_id,channel="response",timeout=3):
        """

        :param command_id:
        :return:
        """
        # wait for a message of the corresponding order on back channel -/root/name/response
        end_time = time.time() + timeout
        done = False
        response = dict(code="KO", order="0", message="unknown")
        while not done:
            # receive a pusub message ( type, data , channel)
            #response = Response("wait_response","Bad response",command_id=command_id)
            rmsg = self.pubsub.get_message(ignore_subscribe_messages=True, timeout=1)
            if rmsg:
                rchannel= short_channel(rmsg["channel"])
                if rchannel== channel:
                    # we got a response
                    try:
                        received= Response.from_json(rmsg["data"])
                        if received.command_id == command_id:
                            # we got the expected response
                            response["code"] ="OK"
                            response["message"] = received.response
                            response["order"]= command_id

                    except Exception as err:
                        done= True
            if time.time() >= end_time:
                done = True

        return response
#
#   samples
#




class SampleClient(RedisPublisher):
    """


    """
    def expect(self,order,channel="stdout",timeout=3):
        """

        wait a response of type "OK $order $comment
        :param order:
        :return: a response dictionary ( code ,order, message)
        """
        # wait for a message of the corresponding order
        fqdn_channel= self.channel(channel)
        self.pubsub.subscribe(fqdn_channel)
        done= False
        response= dict(code="KO",order="0",message="unknown")
        while not done:
            # receive a pusub message ( type, data , channel)
            rmsg= self.pubsub.get_message(ignore_subscribe_messages=True,timeout=1)
            channel = short_channel(rmsg["channel"])
            if channel == "response":
             # find matchin command id
             try:
                 msg= Response.from_json(rmsg["data"])
                 if msg.command_id == order:
                     # this is the response we look for
                     return msg.data
             except Exception as err:
                # bad message format
                break
            done= True
        return response




    def switch_on(self,outlet=0):
     """
        send a message "ON 3243 " to powerswitch server on channel powerswitch/energenie/$outlet
     :param outlet: int 1 to 4
     :return:
     """
     order= random.randint(1,65535)
     message= "ON %d" % order
     self.publish(str(outlet),message)

     # wait for response  a dictionary code order message
     response = self.expect(order,channel=str(outlet))

     return True


if __name__ == "__main__" :

    print("done")


