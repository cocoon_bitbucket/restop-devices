import time
import json
from restop_components.rpublish import get_redis_pool


from restop_components.rpublish.core import fqdn_channel,fqdn_back_channel,short_channel, Request
from restop_components.rpublish.core import get_observer, get_back_observer
from restop_components.rpublish.listnener import SampleConnector,SampleServer
from restop_components.rpublish.publisher import SampleClient


ROOT= "powerswitch"
NAME= "energenie"

REDIS_HOST = "localhost"
REDIS_DB = 0
REDIS_PORT = 6379


P_HOST = "192.168.1.30"
P_PORT = 80
P_PASSWORD = "1"


def redis_pool():
    return get_redis_pool(host=REDIS_HOST, port=REDIS_PORT, db=REDIS_DB)


# def observer(pool):
#     o= get_observer(pool,NAME,root=ROOT)
#     return o

def listener(pool):

    l = SampleServer(pool, root= ROOT, name= NAME)

    connector = SampleConnector(ip=P_HOST, port=P_PORT, password=P_PASSWORD)
    l.set_connector(connector)
    return l



def test_core():


    # fqdn channels
    fqdn= fqdn_channel("root","name","channel")
    assert fqdn == "root/name/channel"

    fqdn = fqdn_channel("root", "name")
    assert fqdn == "root/name"


    # fqdn back channels
    fqdn = fqdn_back_channel("root/name/stdlog")
    assert fqdn == "-/root/name/stdlog"

    fqdn = fqdn_back_channel("root/name/1/stdlog")
    assert fqdn == "-/root/name/1/stdlog"





    # short channels
    short= short_channel("root/name/channel")
    assert short == "channel"

    short = short_channel("root/name")
    assert short == ""

    short = short_channel("root")
    assert short == "root"

    short = short_channel("root/name/1/stdin")
    assert short == "1/stdin"

    # short back channels
    short = short_channel("-/root/name/1/stdin")
    assert short == "1/stdin"





    return


def test_message():


    m = Request("switch_on", {"outlet":0})

    data= m.data

    assert isinstance(data[0],basestring)
    assert data[1] == "switch_on"
    assert data[2] ==  {"outlet":0}


    json_message= m.json

    assert json_message.endswith('switch_on", {"outlet": 0}]')



    return



def test_listener():

    pool= redis_pool()
    l= listener(pool)

    #l.run()
    l.start()

    return


def test_publisher():


    pool = redis_pool()

    watcher = get_observer(pool, NAME, root=ROOT)
    watcher.start()

    back_watcher = get_back_observer(pool, NAME, root=ROOT)
    back_watcher.start()

    server = listener(pool)
    server.start()


    l = SampleClient(pool, root=ROOT, name=NAME)
    l.publish("stdin",json.dumps("PING"))

    #l.publish("stdin","my message")
    #l.publish("1/stdin","message to outlet 1")


    msg_id= l.send("swith_on",channel="1")
    #
    response= l.wait_response(msg_id,channel="response")
    print( response)


    time.sleep(5)



    server.done =True
    server.join()


    return



if __name__ == "__main__":

    import logging
    logging.basicConfig(level=logging.DEBUG)

    test_core()
    test_message()

    #test_listener()

    test_publisher()

    print("Done.")
