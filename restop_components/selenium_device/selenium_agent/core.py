import time
import json
from restop_adapters.client import ClientAdapter
from selenium_adapter import ThreadedSeleniumAdapter as AgentAdapter


class SeleniumAgent(object):
    """

    """
    def __init__(self,agent_model,**kwargs):
        """

        :param agent_model:
        :param kwargs:
        """
        self.model= agent_model
        self.agent_id= self.model.get_hash_id()
        self.parameters= kwargs

        self.redis_db= self.model.database

        self.selenium= ClientAdapter(self.agent_id,redis_db=self.redis_db)

    def _start(self):
        """
            start the slave process term.py

        :return:
        """
        url= self.parameters.get('url',None)

        adapter = AgentAdapter(self.agent_id, url, redis_db=self.model.database)
        adapter.start()
        return True


    def _stop(self):
        """
            stop the fake terminal

        :return:
        """
        self.selenium.exit()
        time.sleep(2)

        return True


    def _execute(self, alias, method,**kwargs):
        """
            push serialized method to queue

        :param alias:
        :param function_name:
        :param args:
        :param kwargs:
        :return:
        """
        #cmd_login = dict(alias='livebox', method='title_should_be', kwargs={'title': 'Login Page'})
        # agent.selenium.send(json.dumps(cmd_login))
        command= dict(alias=alias,method=method,kwargs=kwargs)
        self.selenium.send(json.dumps(command))

    # mapping for the agent method ( click , wait_for_exists , press_home ... )
    def __getattr__(self, method):
        """
            wrapper to dynamic methods
        :param function_name:
        :return:
        """
        def wrapper(agent_id='-', **kwargs):
            """

            """
            return self._execute(agent_id, method, **kwargs)

        return wrapper
