
import time
import json
import threading


from selenium.webdriver.common.desired_capabilities import DesiredCapabilities
import Selenium2Library


from restop_adapters.adapter import Adapter as AgentAdapter



default_parameters= {

    #'url': "http://192.168.1.23:7272",
    'url': "http://192.168.1.152:7272",
    'browser': 'firefox',
    'remote_url': "http://192.168.99.100:4444/wd/hub",
    'desired_capabilities':DesiredCapabilities.FIREFOX
}


class SeleniumAdapter(AgentAdapter):
    """

        adapt to a selenium2Library


        read command of the form

        { alias= , method= , args= , kwargs= }

    """

    def __init__(self, agent_id, command_line=None, log=None, redis_db=None):
        """

        :param agent_id:
        :param command_line:
        :param shell:
        :param log:
        :param redis_db:
        """
        super(SeleniumAdapter,self).__init__(agent_id,command_line,log=log,redis_db=redis_db)

        self.command_line= command_line or default_parameters['url']

        return


    def run_process(self,fullcmd=None,shell=None):
        """
            launch a selenium session
        """
        url= fullcmd or self.command_line

        #self.trace("Popen " + fullcmd  )

        # create a selenium2library session -> proc
        self.proc= Selenium2Library.Selenium2Library(timeout=3)
        if self.proc:
            #d= Hash(self.database,dashboard_pid)
            self._pid[self.agent_id]= -1

        # open a browser
        self.log.debug('selenium adapter: opening a browser for url: [%s]' % url)
        rc= self.proc.open_browser(url,
                             browser= default_parameters['browser'],
                             alias= self.agent_id,
                             remote_url= default_parameters['remote_url'],
                             desired_capabilities=default_parameters['desired_capabilities'] ,
                             ff_profile_dir=None)
        self.log.debug('selenium adapter: browser is open')
        time.sleep(1)
        self.started = None


        return self.started


    def shutdown_process(self):
        """
            close browser
        """

        # close browser
        self.log.debug('selenium adapter: closing browser')
        #self.proc.close_window()
        self.proc.close_browser()
        # reset pid
        self._pid[self.agent_id]= 0
        return

    def send(self, cmd):
        """
            send command to selenium session

        :param cmd:
        :return:
        """
        self.log.debug("selenium adapter: send command to selenium session: [%s]" % cmd)
        command_parameters= json.loads(cmd)

        #self._write_stdin(cmd)
        method_name= command_parameters['method']
        args=  command_parameters.get('args',[])
        kwargs= command_parameters.get('kwargs',{})
        alias= command_parameters.get('alias',self.agent_id)

        method= getattr(self.proc,method_name,None)
        if method:
            try:
                result= method(*args,**kwargs)
            except Exception ,e:
                self.log.error('selenium adapter: %s' % str(e))
                result= None
            self.log.debug('selenium adapter: result is : %s' % str(result))
        else:
            # no such selenium method
            self.log.error('selenium adapter: no such method: %s' % method_name)
            result=False
        # write selenium response to stdout
        response= json.dumps(result)
        self._stdout.append(response)
        return


    def _write_stdin(self, cmd):
        """
            send command to selenium session
        """
        raise NotImplemented


    def run_once(self):
        """

        :return:
        """
        self.keep_alive()

        # transfer slave stdout input to redis <stdout> ( if any )
        #self.transfer_out()
        #  transfer redis <stdin> input to slave stdin ( if any )
        self.transfer_in()


    def read(self,wait=False,log=True):
        """
            read from selenium session

        """
        line= self._read_stdout(wait=wait)
        #if log:
        #    self.log(line)
        return line

    def _read_stdout(self,wait=False):
        """
            not applicable
        :param wait:
        :return:
        """
        line= None
        return line





class ThreadedSeleniumAdapter(SeleniumAdapter,threading.Thread):
    """
        a threading version of adapter

    """
    def __init__(self,agent_id,command_line=None,**kwargs):
        """

        """
        threading.Thread.__init__(self)
        SeleniumAdapter.__init__(self,agent_id,command_line,**kwargs)
        self.Terminated=False
        self.setDaemon(True)



