
import os
from flask.config import Config
from restop.client import RestopClient
from restop_platform import Dashboard
from restop_platform.models import Database ,Model
from restop_platform.models import Platform,Apidoc
from application import start_server

from models import DEVICE_NAME

from resources import DeviceResource

import logging
log= logging.getLogger(DEVICE_NAME)
log.setLevel(logging.DEBUG)

#logging.basicConfig()

root_path= dir_path = os.path.dirname(os.path.realpath(__file__))

platform_name= 'default'


# get config from local config.cfg
config= Config(root_path)
config.from_pyfile('config.cfg')
master_url= config['RESTOP_MASTER_URL']
hub_name= config['RESTOP_HUB_NAME']

# create client to master
master= RestopClient(base_url=master_url)

# fetch platform data from master
url= master.url_operation(collection='platforms',item_id=1,operation='get_config')
log.debug('fetch platform data from master at %s' % url)
response= master.post(url)
if response.status_code == 200:
    platform_data= response.json()
else:
    raise RuntimeError('cannot fetch platform at %s' % url)

# fetch hub name for self
url= master.url_operation(collection='platforms',item_id=1,operation='get_hub')
log.debug('fetch hub data from master at %s' % url)
response= master.post(url,data= { 'name': hub_name})
if response.status_code == 200:
    hub_data= response.json()
else:
    raise RuntimeError('cannot fetch hub data from master at %s' % url)


redis_host= hub_data['redis_host']
redis_port= hub_data['redis_port']
redis_db= hub_data['redis_db']

hub_host= hub_data['host']
hub_port= hub_data['port']


# ...
log.debug('set redis database and bind model')
db = Database(host=redis_host, port=redis_port, db=redis_db)
Model.bind(database=db, namespace=None)
dashboard= Dashboard(db)
dashboard.main['hub_ip']= hub_host
dashboard.main['hub_port']= hub_port
dashboard.main['hub_name']= hub_name


# get existing platform instance
try:
    p = Platform.get(Platform.name == platform_name)
except Exception, e:
    # does not exists create it
    log.debug('create platform')
    db.flushdb()
    p = Platform.create(name=platform_name, data=platform_data)
    #p.setup()
p = Platform.get(Platform.name == platform_name)

# get local api doc and send it to master for update
log.debug('setup autodoc')
doc= DeviceResource._autodoc()
data={'operations':[],'collection_operations':[],'collections':[DEVICE_NAME]}
for operation in doc['item_methods'].keys():
    op = operation.replace('op_','')
    data['operations'].append(op)
for operation in doc['collection_methods'].keys():
    op = operation.replace('op_col_','')
    data['collection_operations'].append(op)

# store local apidoc
log.debug('create or update local apidoc')
api = Apidoc.create_or_update(platform_name=platform_name,
                    session_collection='sessions',
                    collections=data['collections'],
                    collection_operations=list(data['collection_operations']),
                    operations=list(data['operations'])
                    )

# update master
url= master.url_item(collection='apidoc',item_id='samples')
log.debug('update master apidoc at %s' % url)
response= master.post(url,data= data)
if response.status_code == 200:
    pass
else:
    raise RuntimeError('cannot update doc to master')

log.debug('start hub server')
if hub_host== 'localhost':
    # publish on all interface instead of local host
    hub_host= '0.0.0.0'
start_server(host=hub_host, port=hub_port, debug=True, threaded=True)

print 'Done'