import time
import json
from restop_components.selenium_device.selenium_agent.selenium_adapter import SeleniumAdapter,ThreadedSeleniumAdapter
from restop_adapters.client import ClientAdapter

from walrus import Database


agent_id= "agent:id:1"


cmd_maximize= dict( alias='livebox', method='maximize_browser_window' )
cmd_login= dict(alias='livebox', method='title_should_be', kwargs= {'title':'Login Page'} )

def get_db():
    """



    :return:
    """
    db=Database()
    db.flushall()

    return db


def test_1(db):
    """


    :param db:
    :return:
    """
    client= ClientAdapter(agent_id)

    adapter= SeleniumAdapter(agent_id,redis_db=db)


    adapter.run_process()

    client.send(json.dumps(cmd_maximize))
    time.sleep(2)
    adapter.transfer_in()
    response= client.read()

    client.send(json.dumps(cmd_login))
    time.sleep(2)
    adapter.transfer_in()
    response= client.read()




    adapter.shutdown_process()



    return


def test_threaded_adapter(db):
    """

    :param db:
    :return:
    """
    client= ClientAdapter(agent_id)

    adapter= ThreadedSeleniumAdapter(agent_id,redis_db=db)
    adapter.start()


    client.send(json.dumps(cmd_maximize))
    time.sleep(3)
    response= client.read()

    client.send(json.dumps(cmd_login))
    time.sleep(2)
    response= client.read()

    client.exit()



    return





if __name__=="__main__":


    #test_1(get_db())

    test_threaded_adapter(get_db())

    print "Done"













