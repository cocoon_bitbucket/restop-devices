import time
import json


from walrus import Database
from restop_components.selenium_device.models import AgentModel,Model
from restop_components.selenium_device.selenium_agent import SeleniumAgent


#agent_id= "agent:id:1"

#url= "http://192.168.1.23:7272"
url= "http://192.168.1.152:7272"

cmd_maximize= dict( alias='livebox', method='maximize_browser_window' )
cmd_login= dict(alias='livebox', method='title_should_be', kwargs= {'title':'Login Page'} )

def get_db():
    """



    :return:
    """
    db=Database()
    db.flushall()

    Model.bind(db)

    return db



def test_agent(db):
    """

    :param db:
    :return:
    """
    agent_model= AgentModel.create(name='livebox',session=1)

    agent= SeleniumAgent(agent_model,url=url)
    agent._start()

    agent.selenium.send(json.dumps(cmd_maximize))
    time.sleep(3)
    response= agent.selenium.read()

    agent.selenium.send(json.dumps(cmd_login))
    time.sleep(2)
    response= agent.selenium.read()

    agent.title_should_be('alias',title='Login Page')
    time.sleep(2)
    response = agent.selenium.read()

    agent.bad_keyword('alias',title='Login Page')
    time.sleep(2)
    response = agent.selenium.read()

    agent._stop()

    return





if __name__=="__main__":


    test_agent(get_db())

    print "Done"


