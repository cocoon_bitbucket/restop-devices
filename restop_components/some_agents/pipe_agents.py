
import os
import time

import datetime

from wbackend.logger import SimpleLogger as Logger
from restop.application import ApplicationResponse,ApplicationError,CriticalError
from restop_platform import Dashboard
from restop_platform.models import Platform
from restop_adapters.client import ClientAdapter
from restop_adapters.serial_port_adapter import SerialportThreadedAdapter


from restop_adapters.telnet_adapter import TelnetAdapter

from restop_components.stats_scheduler import  StatSchedulerClient
from restop_components.stats_scheduler import  TelnetScheduler

from restop_graph import TimestampConverter
from restop_graph.influxdb import Publisher as EventPublisher


from restop_components.stats_scheduler import StatSchedulerClient
#from restop_components.stats_scheduler import StatScheduler
from restop_components.stats_scheduler import CronStatScheduler as StatScheduler





# callback address for stats scheduler 1: station_peer_address, 2:collection 3:device (tv)
#scheduler_feedback_url_pattern = 'https://%s/restop/upload/%s/%s/feedback_scheduler'
# eg: https://192.168.99.100/restop/upload/tvbox_agents/tv/feedback_scheduler


class BasePipeAgent(object):
    """

        a base for agents who communicates with redis queues


            defines communication operation

            start
            stop

            send( line )
            watch()
            expect (line)
            sync()

            scheduler_init( metrics )
            scheduler_sync()
            scheduler_close()

            publish_event(title,text,tags)


    """
    collection = '_basepipeagents'

    # callback address for stats scheduler 1: station_peer_address, 2:collection 3:device (tv)
    scheduler_feedback_url_pattern = 'https://%s/restop/upload/%s/%s/feedback_scheduler'
    # eg: https://192.168.99.100/restop/upload/tvbox_agents/tv/feedback_scheduler


    _default_parameters= dict (
        external_url= 'localhost',
        grafana_url= "localhost:3000/dashboard/db/restop" ,
        graphite_address= "localhost:8000",

        default_metrics= ['df', 'cpu','meminfo','loadavg'],
    )

    def __init__(self, agent_model, **kwargs):
        """

        :param agent_model:
        :param kwargs:
        """
        self.model = agent_model
        self.agent_id = self.model.get_hash_id()
        self.kwargs = kwargs

        self.redis_db = self.model.database
        self.dashboard= Dashboard(self.redis_db)
        self.log = Logger(self.model.logs.key, self.redis_db)

        self.state = {}

        self.log.info('%s: initialize' % self.agent_id)

        self.session_id = self.model.session
        self.alias = self.model.name
        self.device_data = self.model.parameters

        self._event_publisher= EventPublisher(redis_db=self.redis_db)

        self._client=None
        self._scheduler= None
        self._setup()

    def _setup(self):
        """

        """
        self._setup_platform()
        self._setup_graph()
        self._setup_client()
        self._setup_scheduler()

    def _setup_platform(self):
        """
        """
        # set platform_info
        self._platform = Platform.get(Platform.name == 'default')
        self._platform_info = self._platform.get_info()
        self._station_peer_address = self._platform_info['peer_address']

    def _setup_graph(self):
        """

        """
        # configure graph
        self._external_url = self._platform_info.get('external_url', self._default_parameters['external_url'])
        self.grafana_url = self._platform_info.get('grafana_url', self._default_parameters['grafana_url'])
        self.graphite_ip = self._platform_info.get('graphite_ip', self._station_peer_address)
        self.graphite_port= self._platform_info.get('graphite_port', 2003 )
        self.graphite_api_port= self._platform_info.get('graphite_api_port', 8000 )

    def _setup_client(self):
        """

        :return:
        """
        # start a client to access serial redis queues
        self._client = ClientAdapter(self.agent_id, log=self.log, redis_db=self.redis_db)

    def _setup_scheduler(self):
        """

        :return:
        """
        # start a stat scheduler client
        self._scheduler_key = "scheduler-%s" % self.agent_id
        self._scheduler = StatSchedulerClient(self._scheduler_key, log=self.log, redis_db=self.redis_db)


    ##
    ##  public interface
    ##

    #
    # control interface
    #
    def start(self):
        """
            start the agent
        """
        return self.model.make_response(True)

    def stop(self):
        """
            stop the agent
        """
        return self.model.make_response(True)

    #
    # client interface
    #
    def send(self, cmd):
        """

            send a command to the device
        :param cmd: string
        """
        r = self._client.send(cmd)
        return r

    def watch(self,timeout=5):
        """
            watch device output for a duration of <timeout>
        :param timeout: integer ,number of seconds to wait for
        :return:
        """
        lines= self._client.expect('Never Catch This',timeout=timeout)
        # add lines to log
        self.model.logs.extend(lines)
        return "OK"

    def expect(self,pattern,timeout=10,cancel_on=None, regex='no', **kwargs ):
        """
            read device output to find the pattern <pattern>

        :param pattern: string ,the pattern to search for
        :param timeout: integer, seconds to wait ( default is 5s)
        :return: string; OK ,FAILED or CANCELED
        """
        if str(regex).lower() in ['no','false','-1']:
            regex= False
        else:
            regex= True

        r = self._client.expect(pattern, timeout=timeout, regex=regex,cancel_on=cancel_on,**kwargs)
        last_line= r[-1]
        # add lines to log
        self.model.logs.extend(r)

        if '=== found' in last_line:
            return "OK"

        elif '=== timeout' in last_line:

            message = 'FAILED'

        elif '=== canceled' in last_line:
            message = 'CANCELED'
        else:
            message = 'FAILED ???'

        response= self.model.make_response(message,500)
        raise ApplicationError(**response)

    def _sync(self,timeout=10):
        """
            low level sync
        :param timeout:
        :return:
        """
        t0 = time.time()

        self.send('\n')
        sync_message = "echo SYNCHRO TAG %s" % str(t0)
        self.send(sync_message)
        time.sleep(2)
        r = self._client.expect("^SYNCHRO TAG %s" % str(t0), timeout=timeout)
        return r


    def sync(self,timeout=10):
        """
            flush the device output of the device
        :param timeout: integer, number of seconds to wait for (default is 10s)
        :return:
        """
        r= self._sync(timeout=timeout)
        last_line= r[-1]
        self.model.logs.extend(r)
        try:
            assert "=== found " in last_line , "serial_sync: pattern not found"
        except Exception as e:
            return last_line
        else:
            # ok
            return last_line


    #
    # Event publisher interface
    #
    def publish_event(self,title,text="",tags=""):
        """

        :param title:
        :param text:
        :param tags:
        :return:
        """
        if not tags:
            tags="%s" % self.alias   # eg livebox
        if not text:
            text= "%s:%s" % (self.alias,title) # eg "livebox:my_event"
        msg= self._event_publisher.event_message(title,text=text,tags=tags,timestamp=time.time())
        self._event_publisher.publish_event(msg)
        return msg


    #
    # scheduler interface
    #
    def scheduler_init(self, metrics=None):
        """
            start the stats scheduler  ( start collecting metrics)

        :param metrics: list of wishes metrics eg ['df', 'cpu','meminfo','loadavg']
        if metrics is None: select all the metrics
        """
        metrics = metrics or self._default_parameters['default_metrics']
        try:
            self._scheduler.check_metrics(metrics)
        except KeyError as e:
            message = "unknown metric: %s" % e.message
            self.log.error(message)
            response = self.model.make_response(message, 500)
            raise ApplicationError(**response)
        self._scheduler.init_metrics(metrics)
        message = 'sync scheduler sent'
        self.log.debug(message)
        return message

    def scheduler_sync(self):
        """
            send the collected metrics to graphite

        """
        """
            sync the trace:
                stop scheduler
                curl trace file
                rm trace file
                restart scheduler

        :return:
        """
        self._scheduler.send('!sync')
        time.sleep(15)
        message = 'sync scheduler sent'
        self.log.debug(message)
        return message

    def scheduler_close(self, clear=False,wait=True):
        """
            stop the scheduler and send the collected metrics to graphite
        """
        self.log.debug('close scheduler')

        started = self.model.created
        timestamp = int(TimestampConverter.datetime_to_epoq(started)) * 1000
        logs = ['see result on grafana:',
                ' %s?from=%d' % (self.grafana_url, timestamp)]
        print("consult graph at %s" % logs[1])
        self.model.logs.extend(logs)

        # send close order
        self._scheduler.send('!close')
        rc= True
        if wait:
            rc= self._scheduler.wait_for_status('closed')
        return rc



class SerialAgent(BasePipeAgent):
    """

       a base for agents who communicates with serial port via redis queues


            defines communication operation

            start
            stop

            send( line )
            watch()
            expect (line)
            sync()

            scheduler_init( metrics )
            scheduler_sync()
            scheduler_close()

    """
    collection = '_serialagents'


    _default_parameters= dict (
        external_url= 'localhost',
        grafana_url= "localhost:3000/dashboard/db/restop" ,
        graphite_address= "localhost:8000",
        default_metrics= ['df', 'cpu','meminfo','loadavg'],

        scheduler_feedback_url='https//localhost:5000/restop/upload/tvbox_agents/tv/feedback_scheduler'
    )

    def start(self):
        """
            start the loop proxy between redis queues and tv serial port

        :return:
        """
        rc= self._start_serial()
        rc &= self._start_scheduler()
        return rc

    def _start_serial(self):
        """
        :return:
        """
        # start the loop proxy between redis queue and serial port
        serial_command_line = self.device_data['serial_port']
        # self._serial_proxy=  SerialController(self.alias,command_line=serial_command_line)
        self._serial_proxy = SerialportThreadedAdapter(self.agent_id, command_line=serial_command_line, log=self.log,
                                              redis_db=self.redis_db)
        try:
            r = self._serial_proxy.run_process()
            r2 = self._serial_proxy.start()
        except Exception as e:
            self.model.logs.append(
                'error: %s cannot start serial server with %s' % (self.agent_id, serial_command_line))
            # TODO: LT remove pass ( only for debug)
            #raise e
            pass

        self.log.debug("%s serial server started with %s" % (self.agent_id, serial_command_line))
        return True

    def _start_scheduler(self):
        """

        :return:
        """
        # start a stat scheduler server
        scheduler_key = "scheduler-%s" % self.agent_id

        with_curl = self.model.parameters.get('with_curl', True)
        if with_curl:
            # scheduler_feedback_url = 'https://%s/feedback/stats' % self._platform_info['peer_address']
            scheduler_feedback_url = self.scheduler_feedback_url_pattern % (
                self._platform_info['peer_address'],
                self.collection,
                self.alias
            )
            # eg: https://192.168.99.100/restop/upload/tvbox_agents/tv/feedback_scheduler
            self.log.debug("stats scheduler callback url set to [%s]" % scheduler_feedback_url)
        else:
            scheduler_feedback_url=None
            self.log.debug("stats scheduler in console mode")
        try:
            scheduler = StatScheduler(scheduler_key, self.agent_id, alias=self.alias,
                                      feedback_url=scheduler_feedback_url, redis_db=self.redis_db,
                                      with_curl=with_curl,
                                      log=self.log)
            scheduler.start()
        except Exception as e:
            self.model.logs.append('error: cannot start scheduler server')
            raise e
        self.log.info("%s scheduler server started" % self.agent_id)

        return True

    def stop(self):
        """
            stop the agent
        :return:
        """
        rc= self._stop_scheduler()
        rc &= self._stop_serial()
        return rc

    def _stop_serial(self):
        """
        :return:
        """
        # tell the serial client to send exit signal
        self._client.exit()

        # wait the proxy get the exit signal and exit
        time.sleep(2)
        return True

    def _stop_scheduler(self):
        """

        :return:
        """
        # stop the stat scheduler
        self._scheduler.exit()
        time.sleep(2)
        return True


class TelnetAgent(BasePipeAgent):
    """

       a base for agents who communicates with telnet via redis queues


            defines communication operation

            start
            stop

            send( line )
            watch()
            expect (line)
            sync()

            scheduler_init( metrics )
            scheduler_sync()
            scheduler_close()

    """
    collection = '_telnetagents'


    _default_parameters= dict (
        external_url= 'localhost',
        grafana_url= "localhost:3000/dashboard/db/restop" ,
        graphite_address= "localhost:8000",
        default_metrics= ['df', 'cpu','meminfo','loadavg'],
        telnet_user= 'root',
        telnet_password= 'sah',
        telnet_port= '23',
        peer_ip= "localhost",
        with_curl= False,
        scheduler_feedback_url='https//localhost:5000/restop/upload/livebox_agents/tv/feedback_scheduler'
    )

    def _setup(self):
        """
            TelnetAgent setup

            from restop_components.stats_scheduler import  StatSchedulerClient
            client= StatSchedulerClient(agent_id,log=log,redis_db=db)


        """
        self._setup_platform()
        self._setup_graph()

        self._setup_scheduler()
        self._setup_client()



    def _setup_client(self):
        """

        :return:
        """
        # start a client to access serial redis queues
        self._client = ClientAdapter(self.agent_id, log=self.log, redis_db=self.redis_db)
        #self._client= self._scheduler

    def _setup_scheduler(self):
        """

        :return:
        """
        # start a stat scheduler client
        self._scheduler_key = "scheduler-%s" % self.agent_id
        self._scheduler = StatSchedulerClient(self._scheduler_key, log=self.log, redis_db=self.redis_db)



    def start(self):
        """
            TelnetAgent start

            start the loop proxy between redis queues and telnet

            from restop_components.stats_scheduler import  TelnetScheduler
            ts= TelnetScheduler(
                scheduler_id,redis_db=db,log=log,
                telnet=telnet,
                metric=metric,
                graphite=graphite)
            ts.start()

        :return:
        """
        rc = self._start_scheduler()
        rc &= self._start_telnet()
        return rc

    def _start_telnet(self):
        """

            launch a telnet process

             telnet -l user host port

        :return:
        """


        user=self.device_data.get('telnet_user', self._default_parameters['telnet_user'])
        password = self.device_data.get('telnet_password', self._default_parameters['telnet_password'])
        host = self.device_data.get('peer_ip', self._default_parameters['peer_ip'])
        port = self.device_data.get('telnet_port', self._default_parameters['telnet_port'])


        telnet_proxy= TelnetAdapter(
            self.agent_id, log= self.log, redis_db= self.redis_db,
            user=user,password=password,host=host,port=port
            )
        # scheduler_key= self._client.agent_id
        # telnet_proxy= TelnetScheduler(
        #     scheduler_key,log=self.log,redis_db=self.redis_db,
        #     telnet=telnet,
        #     )
        try:
            # start the telnet adapter
            telnet_proxy.start()
        except Exception, e:
            self.model.logs.append(
                'error: %s cannot start telnet  with host %s' % (self.agent_id, host), exc_info=True)
            raise e
        #self._client.wait_for_status('started')
        self.log.debug("%s server telnet with host:%s, port:%d" % (self.agent_id, host, port))

        return True

    def _start_scheduler(self):
        """

        :return:
        """
        telnet= dict(
            user=self.device_data.get('telnet_user', self._default_parameters['telnet_user']),
            password = self.device_data.get('telnet_password', self._default_parameters['telnet_password']),
            host = self.device_data.get('peer_ip', self._default_parameters['peer_ip']),
            port = self.device_data.get('telnet_port', self._default_parameters['telnet_port'])
        )
        metric= dict(
            period= 10,
            sync= 60,
            name= self.alias,
            collector_config_file= 'collectors.ini',
            collector_config= {},
            metric= []
        )
        graphite= dict(
            host= self.graphite_ip,
            port= self.graphite_port
        )


        with_curl = self.model.parameters.get('with_curl', True)
        if with_curl:
            # scheduler_feedback_url = 'https://%s/feedback/stats' % self._platform_info['peer_address']
            scheduler_feedback_url = 'http://%s/restop/upload/%s/%s/feedback_scheduler' % (
                self._platform_info['peer_address'],
                self.collection,
                self.alias
            )
            # eg: https://192.168.99.100/restop/upload/tvbox_agents/tv/feedback_scheduler
            self.log.debug("stats scheduler callback url set to [%s]" % scheduler_feedback_url)
        else:
            scheduler_feedback_url = None
            self.log.debug("stats scheduler in console mode")


        scheduler_key= self._scheduler.agent_id
        scheduler_proxy= TelnetScheduler(
            scheduler_key,log=self.log,redis_db=self.redis_db,
            telnet=telnet,
            metric=metric,
            graphite=graphite,
            with_curl=with_curl,
            feedback_url=scheduler_feedback_url,
            )
        try:
            # start the telnet adapter
            scheduler_proxy.start()
        except Exception, e:
            self.model.logs.append(
                'error: %s cannot start telnet  with host %s' % (self.agent_id, telnet['host']), exc_info=True)
            raise e
        self._scheduler.wait_for_status('started')
        self.log.debug("%s server telnet with host:%s, port:%d" % (self.agent_id, telnet['host'], telnet['port']))

        return True

    def stop(self):
        """
            stop the agent
        :return:
        """
        rc= self._stop_scheduler()
        rc &= self._stop_telnet()
        return rc

    def _stop_telnet(self):
        """
        :return:
        """
        # tell the serial client to send exit signal
        self.log.info("ask for exiting telnet adapter")
        self._client.exit()

        # wait the proxy get the exit signal and exit
        #time.sleep(2)
        return True

    def _stop_scheduler(self):
        """

        :return:
        """
        # stop the stat scheduler
        self._scheduler.exit()
        self.log.info("ask for exiting scheduler adapter")
        #time.sleep(2)
        return True