import time
import json

from walrus import Database
from restop.application import ApplicationError
from restop_graph import TimestampConverter
from restop_queues.client import QueueClient

from restop_stats.models import StatSchedulerQueue
from restop_stats.controllers import StatScheduler


from pipe_agents import BasePipeAgent


class UsbAgent(BasePipeAgent):
    """

       a base for agents who communicates with serial port via redis queues

            use usbqueserver

            defines communication operation

            start
            stop

            send( line )
            watch()
            expect (line)
            sync()

            scheduler_init( metrics )
            scheduler_sync()
            scheduler_close()

    """
    collection = 'bstvboxagents'


    _default_parameters= dict (
        external_url= 'localhost',
        grafana_url= "localhost:3000/dashboard/db/restop" ,
        graphite_address= "localhost:8000",
        default_metrics= ['df', 'cpu','meminfo','loadavg'],

        scheduler_feedback_url='https//localhost:5000/restop/upload/tvbox_agents/tv/feedback_scheduler'
    )

    def _setup_client(self):
        """
            setup usb client
        :return:
        """
        # start a client to access serial redis queues
        #self._client = ClientAdapter(self.agent_id, log=self.log, redis_db=self.redis_db)

        queue_server_host= self._station_peer_address

        queue_hub_redis = Database(host=queue_server_host, port=6379, db=0)
        self._serial_queue_name = self.model.parameters['serial_port']

        hub_queue_key = 'queuehub:id:default'
        self.queue_hub_client = QueueClient(hub_queue_key, redis_db=queue_hub_redis)

        self._client= QueueClient('queue:id:%s' % self._serial_queue_name, redis_db=queue_hub_redis)
        self._serial_client= self._client

    def _setup_scheduler(self):
        """

        :return:
        """
        # start a stat scheduler client
        #self._scheduler_key = "scheduler-%s" % self._client.agent_id
        self._scheduler_key = "statschedulerqueue:id:%s" % self.alias
        self._scheduler = QueueClient(self._scheduler_key,redis_db=self.redis_db)

    def start(self):
        """
            start the loop proxy between redis queues and tv serial port

        :return:
        """
        rc= self._start_serial()
        rc &= self._start_scheduler()
        return rc

    def _start_serial(self):
        """
            ask the usbqueuehub to start a usbqueue server and get a usbqueue client

        :return:
        """
        try:
            # ask the usb hub client to start a server for the usb queue (usb0)
            self.queue_hub_client.send('start_adapter', dict(name=self._serial_queue_name, run_mode='thread'))
            time.sleep(4)
            status,queue_key = self.queue_hub_client.receive()
            if status:
                self.model.logs.append("queue started: %s" % queue_key)
            else:
                self.model.logs.append("fail to start queue: %s" % self._serial_queue_name)
                raise RuntimeError(queue_key)
        except Exception as e:
            self.model.logs.append(
                'error: %s cannot start serial server for %s' % (self.agent_id, self._serial_queue_name))
            raise e

        self.log.debug("%s serial server started with for %s" % (self.agent_id, self._serial_queue_name))
        return True


    def _start_scheduler(self):
        """

        :return:
        """
        # start a stat scheduler server
        #scheduler_key = "scheduler-%s" % self.agent_id
        with_curl = self.model.parameters.get('with_curl', True)
        if with_curl:
            # scheduler_feedback_url = 'https://%s/feedback/stats' % self._platform_info['peer_address']
            scheduler_feedback_url = self.scheduler_feedback_url_pattern % (
                self._platform_info['peer_address'],
                self.collection,
                self.alias
            )
            # eg: https://192.168.99.100/restop/upload/tvbox_agents/tv/feedback_scheduler
            self.log.debug("stats scheduler callback url set to [%s]" % scheduler_feedback_url)
        else:
            scheduler_feedback_url=None
            self.log.debug("stats scheduler in console mode")

        # create scheduler queue model
        #parameters= dict(feedback_url=scheduler_feedback_url,with_curl=with_curl)

        scheduler_model= StatSchedulerQueue.create(
            name=self.alias,
            feedback_url=scheduler_feedback_url,
            with_curl=with_curl
        )
        scheduler_model.clear()
        scheduler_model.stdlog.append('clear queues')
        # create scheduler controller
        scheduler= StatScheduler(scheduler_model)

        # bind scheduler to usb queue client
        scheduler.bind_connector(self._serial_client)

        try:
            # scheduler = StatScheduler(scheduler_key, self._client.agent_id, alias=self.alias,
            #                           feedback_url=scheduler_feedback_url, redis_db=self.redis_db,
            #                           with_curl=with_curl,
            #                           log=self.log)
            scheduler.start()
        except Exception as e:
            self.model.logs.append('error: cannot start scheduler server')
            raise e
        self.log.info("%s scheduler server started" % self.agent_id)

        self._scheduler_server= scheduler

        return True

    def stop(self):
        """
            stop the agent
        :return:
        """
        rc= self._stop_scheduler()
        rc &= self._stop_serial()
        return rc

    def _stop_serial(self):
        """
        :return:
        """
        # tell the serial client to send exit signal
        self._client.exit()


        # wait the proxy get the exit signal and exit
        time.sleep(2)
        return True

    def _stop_scheduler(self):
        """

        :return:
        """
        # stop the stat scheduler
        self._scheduler.exit()
        time.sleep(2)
        return True

        #
        # client interface
        #

    def send(self, cmd):
        """

            send a command to the device
        :param cmd: string
        """
        if not cmd.endswith('\n'):
            cmd= cmd + b'\n'
        r = self._client.write(cmd)
        return r


    #
    # scheduler interface
    #
    def scheduler_init(self, metrics=None):
        """
            start the stats scheduler  ( start collecting metrics)

        :param metrics: list of wishes metrics eg ['df', 'cpu','meminfo','loadavg']
        if metrics is None: select all the metrics
        """
        metrics = metrics or self._default_parameters['default_metrics']
        try:
            StatScheduler.check_metrics(metrics)
        except KeyError as e:
            message = "unknown metric: %s" % e.message
            self.log.error(message)
            response = self.model.make_response(message, 500)
            raise ApplicationError(**response)

        #self._scheduler.init_metrics(metrics)
        if isinstance(metrics,(list,tuple)):
            metrics= " ".join(metrics)
        kwargs=dict(metrics=metrics)
        self._scheduler.send('job_init',kwargs)
        message = 'init scheduler sent'
        self.log.debug(message)
        return message


    def scheduler_sync(self):
        """
            send the collected metrics to graphite

        """
        """
            sync the trace:
                stop scheduler
                curl trace file
                rm trace file
                restart scheduler

        :return:
        """
        self._scheduler.send('job_sync',"")
        time.sleep(15)
        message = 'sync scheduler sent'
        self.log.debug(message)
        return message


    def scheduler_close(self, clear=False, wait=True):
        """
            stop the scheduler and send the collected metrics to graphite
        """
        self.log.debug('close scheduler')

        started = self.model.created
        timestamp = int(TimestampConverter.datetime_to_epoq(started)) * 1000
        logs = ['see result on grafana:',
                ' %s?from=%d' % (self.grafana_url, timestamp)]
        print("consult graph at %s" % logs[1])
        self.model.logs.extend(logs)

        # send close order
        self._scheduler.send('job_close',"")
        rc = True
        if wait:
            rc = self._scheduler.wait_for_status('closed')
        return rc
