
import sys
import time

import threading
from restop_adapters.adapter import Adapter
from restop_adapters.client import  ClientAdapter
from restop_graph import TraceParser,TraceDriver

import logging
default_logger= logging.getLogger('statscheduler')




class BaseStatScheduler(Adapter):
    """

        StatsScheduler is a queue process ( agent_id )

            send scheduled jobs to a target queue

        jobs and periodicity are described in jobs parameters

        scheduler status
            created
            started
                running
                stopped
                    collecting
                    collected
                stopped
                closed
                running
            terminated

    """
    trace_filename= "/tmp/trace-scheduler.txt"
    trace_scriptname= "/tmp/trace-scheduler"

    def __init__(self, agent_id, target_id, alias='device',feedback_url= None,jobs=None, period= 5,
                 redis_db=None,log=None,
                 with_curl=True,config=None,
                 **redis_kwargs):
        """

        :param agent_id: string eg scheduler:id:1
        :param target_id: string agent_id of the target queue to send commands   eg tvbox:id:1
        :param alias: string , name of the device metric
        :param feedback_url: string ( url for with_curl = True
        :param jobs: list
        :param: redis_db: object
        :param: log: logger
        :param: config : dict , configuration of the graphite server eg { host:localhost, port:2300)
        """
        super(BaseStatScheduler,self).__init__( agent_id,command_line='',redis_db=redis_db,log=log,
                                                 with_curl=with_curl, **redis_kwargs)

        self.target_id= target_id
        self.alias=alias
        self.feedback_url = feedback_url

        jobs = jobs or []
        self.jobs=jobs
        self.period= period
        self.with_curl= with_curl
        self.config= config or {}

        #self.trace_filename= "/tmp/trace-scheduler.txt"
        self.trace_driver= TraceDriver(self.trace_filename)
        self.target_client= ClientAdapter(self.target_id,redis_db=self.database,log=self.log)

        self.t0=time.time()
        self.t1=self.t0
        self.t2=self.t0

        #self.check()
        self.metrics= ""

        self.scheduler_on= False
        self.set_status('created')
        self.log.debug("scheduler %s created: scheduler_on is %s" % (self.agent_id,self.scheduler_on))
        return


    def check(self):
        """

            check jobs are referenced

        :return:
        """
        for job in self.jobs:
            command_lines = self.trace_driver.gen_command_lines(job)

        return True


    def run_once(self):
        """

        """
        self.keep_alive()

        #
        # interpret incoming command !start, !dump_trace ,!read_trace ...
        #
        #
        #
        size= len(self._stdin)
        if size > 0:
            incoming_cmd= self._stdin.popleft()
        else:
            incoming_cmd= None

        if incoming_cmd:
            if incoming_cmd.startswith('!'):
                # a special command command_name,arg  !start !sync !read_trace !dump_trace
                command_line= incoming_cmd[1:].strip()
                if ' ' in command_line:
                    # command like: !start df meminfo
                    command_name,command_parameters= command_line.split(' ',1)
                else:
                    command_name= command_line
                    command_parameters= ''

                # find a job_<command_name> method
                try:
                    method= getattr(self,'job_%s' % command_name)
                    if command_parameters:
                        r= method(command_parameters)
                    else:
                        r= method()
                except AttributeError:
                    # no built in job method
                    # send command to target queue
                    self.send_job(command_name)

            elif incoming_cmd == self.cmd_exit:
                # exit condition, stop slave process and exit main loop
                self.job_stop()
                self.log.debug("scheduler [%s] : exit" % self.agent_id)
                time.sleep(0.1)
                self.Terminated = True
            else:
                # send brute command to target queue
                self.target_client.send(incoming_cmd)

        # schedule a job
        if self.scheduler_on :
            jobs =self.next_scheduled_jobs()
            if jobs:
                for job in jobs:
                    self.send_job(job)
        return True


    def next_scheduled_jobs(self):
        """


        :return:
        """
        jobs= None
        self.t2 = time.time()
        if (self.t2 - self.t1) > self.period:
            # schedule jobs
            #jobs= ['df_H','ps']
            jobs= self.jobs
            self.t1 = self.t2
        else:
            # no jobs to send
            pass
        return jobs

    def send_job(self,command_name):
        """
            send a job to target queue
        :return:
        """
        command= TraceDriver.get_collector_command(command_name)
        command_lines = self.trace_driver.gen_command_lines(command_name,command)
        command_string = ";".join(command_lines)
        r = self.target_client.send(command_string)
        return r


    def run(self):
        """
            main loop

            read from /agents/?/stdin
            if not empty :
                send to slave stdin

            read from slave output
                if not empty :
                    push to /agents/?/stdout

        :return:
        """
        self.log.debug("starting scheduler [%s]" % self.agent_id)
        self.set_status('started')
        #self.log.debug("starting process for agent: %s" % self.agent_id)

        # reset queues
        self.keep_alive()

        #for queue in self._queues.values():
        #    self.reset_queue(queue)

        # launch slave process
        #self.run_process()
        self.Terminated = False

        while not self.Terminated:
            # infinite loop  till 'exit' received via command line
            self.run_once()
            time.sleep(0.1)

        # kill slave process and reset keep_alive
        #self.shutdown_process()
        self.set_status('terminated')
        self.keep_alive(clear=True)
        sys.exit(0)


    def get_by_curl(self,filename,url):
        """
            send curl command to stb

        :param filename:
        :param url:
        :return:
        """
        cmd_line = "curl -i -X POST --data-binary @%s %s" % (filename,url)

        #self.log.debug("send command to stb serial: %s" % cmd_line)
        r= self.target_client.send(cmd_line)

        return r

    def get_by_redis(self, filename, redis_key, redis_host, redis_port=6379):
        """

        :param filename:
        :param redis_key:
        :return:
        """

        cmd_line = 'while read line; do echo -e "rpush %s \\"${line}\\"" | nc %s %s; done  < %s >/dev/null' % (
            redis_key, redis_host, redis_port, filename
        )
        self.log.debug("send command to stb serial: %s" % cmd_line)
        r = self.target_client.send(cmd_line)

        return r

    def get_by_console(self):
        """

            send command to dump the /tmp/scheduler file to console

            read console stdout and put it in scheduler stdout

        :param filename:
        :param redis_db:
        :return:
        """
        # send dump command to busybox
        rc= self._job_dump_trace()
        # wait for the busybox writing to stdout
        time.sleep(15)
        # read busybox stdout and transfer to scheduler stdout
        data= self._job_read_trace()

        if data:
            self.log.debug('get_by_console: send data to graphite (len: %d)' % len(data))
            try:
                sc= StatSchedulerClient(self.agent_id,redis_db=self.redis_db,log=self.log)
                #nb_metrics = StatSchedulerClient.push_to_graphite(data, device=self.alias, configfile='collectors.ini')
                nb_metrics = sc.push_to_graphite_2(data, device=self.alias, configfile='collectors.ini',config=self.config)
            except Exception,e:
                self.log.error('get_by_console,%s' % str(e))
                nb_metrics=0
            self.log.debug("number of metrics sent to graphite: %d" % nb_metrics)
        else:
            self.log.warning("get_by_console: no data to push to graphite")
        return True




    def job_init(self,metrics=""):
        """

        :metrics: string metrics names separated with space eg: df cpu
        :return:
        """
        metrics= metrics.strip()
        # save metrics for restart
        self.metrics= metrics
        self.log.debug("start the scheduler with metrics: %s" % metrics)

        # parse metric list
        if ' ' in  metrics:
            jobs= metrics.split(' ')
        else:
            jobs= [metrics]
        self.jobs= [ job.strip() for job in jobs]

        # send command to re-init scheduler file
        self.job_raz()
        time.sleep(5)
        # command_lines=self.trace_driver.gen_start_trace()
        # command_string = ";".join(command_lines)
        # self.target_client.send(command_string)
        # time.sleep(5)

        self.job_start()
        self.log.debug("scheduler initialized")
        return True

    def job_raz(self):
        """
            send command to erase the scheduler file
        :return:
        """
        command_lines = self.trace_driver.gen_start_trace()
        command_string = ";".join(command_lines)
        self.target_client.send(command_string)
        self.log.debug("scheduler reset")
        return True


    def job_sync(self,restart=True):
        """
            synchronize target queue ( empty it )
        :return:
        """
        # curl -i -X POST -F data=@/tmp/trace-scheduler-1.txt https://192.168.1.21/test
        # while read line; do echo -e "rpush logs \"${line}\"" | nc 192.168.1.21 6379; done  < trace-scheduler-1.txt >/dev/null
        #redis_line= 'while read line; do echo -e "rpush logs \"${line}\"" | nc 192.168.1.21 6379; done  < trace-scheduler-1.txt >/dev/null'

        #trace_filename= '/tmp/trace-scheduler-1.txt'
        #feedback_url = 'https://192.168.1.21/feedback/stats/tv'

        # redis_line = 'while read line; do echo -e "rpush %s \\"${line}\\"" | nc %s %s; done  < %s >/dev/null' % (
        #     redis_log_key, redis_host,redis_port,trace_filename
        # )
        #
        # log.debug("sync scheduler file:%s , redis_key:%s" % (trace_filename,redis_log_key))

        # stop scheduling
        self.job_stop()

        if self.with_curl:
            # send curl command
            self.log.debug("send stats via curl from %s to %s" % (self.trace_filename, self.feedback_url))
            r= self.get_by_curl(self.trace_filename,self.feedback_url)
            time.sleep(15)
        else:
            # no curl on platform get via serial port
            self.log.debug("send stats via console from %s to queue %s" % (self.trace_filename, self.target_id))
            rc= self.get_by_console()
            #self.log.error('cannot send stat: no other methods than curl yet implemented')

        if restart:
            self.log.debug("restart scheduler")
            self.job_raz()
            self.job_start()
        return

    def job_stop(self):
        """
            stop the scheduler
        :return:
        """
        self.scheduler_on = False
        self.set_status('stopped')
        self.log.debug("scheduler stopped")
        time.sleep(6)
        return True

    def job_start(self):
        """
            start the scheduler
        :return:
        """
        self.scheduler_on = True
        self.set_status('running')


    def job_close(self,clear=False):
        self.job_sync(restart=False)
        self.set_status('closed')
        if clear:
            command_lines = self.trace_driver.gen_remove_trace()
            command_string = ";".join(command_lines)
            self.target_client.send(command_string)
            self.log.debug("scheduler file cleared")
        return True

    def _job_dump_trace(self):
        """
            send command to target queue to dump trace file


        :return:
        """
        # stop scheduler function
        #self.scheduler_on = False
        #self.set_status('stopped')
        #self.job_stop()
        # send dump trace file to target client
        command_lines= self.trace_driver.gen_dump_command()
        command_string= ";".join(command_lines)
        self.target_client.send(command_string)


    def _job_read_trace(self):
        """
            read lines from target queue and store them to self stdout queue
        :return:
        """
        stop = False
        top= False
        data=[]
        self.set_status('collecting')
        self.log.debug('collect trace-scheduler data')
        while ( not stop):
            # read line from target queue
            line= self.target_client.read()
            if not line:
                self.log.debug('collect  done')
                break
            if line.startswith('>-['):
                # start of trace file detected
                top= True
                self.log.debug('start of trace detected')
            elif line.startswith('>-]'):
                top = False
                self.log.debug('end of trace detected')
                data.append(line)
            if top:
                # store line to self stdout queue
                data.append(line)
            else:
                # store line to log
                self._logs.append(line)
        self.set_status('collected')
        self.log.debug('collect trace-scheduler done')
        return data




class StatScheduler(BaseStatScheduler,threading.Thread):
    """
        a threading version of adapter

    """
    def __init__(self, agent_id, target_id, alias='device',feedback_url=None,jobs=None, period= 5,
                 redis_db=None,log=None,with_curl=True,config=None, **redis_kwargs):
    #def __init__(self,agent_id,command_line,**kwargs):
        """

        """
        threading.Thread.__init__(self)
        BaseStatScheduler.__init__(self,agent_id,target_id=target_id, alias=alias,feedback_url=feedback_url,
                         jobs=jobs,period=period,redis_db=redis_db,log=log,with_curl=with_curl,config=config, **redis_kwargs)
        self.Terminated=False
        self.setDaemon(True)


class StatSchedulerClient(ClientAdapter):
    """



    """

    def check_metrics(self,metrics=None):
        """

            check metrics are referenced in catalog

        :return:
        """
        metrics= metrics or []
        for metric in metrics:
            trace_driver = TraceDriver('dummy')
            command = trace_driver.get_collector_command(metric)
        return True

    def init_metrics(self,metrics=None,wait=True):
        """
            send  !start command to StatsScheduler
        :param metrics: list , eg  ['df','meminfo' ]
        :return:
        """
        if self.check_metrics(metrics):
            # collect commands
            metrics= metrics or []
            command_lines=[]
            if metrics:
                start_line= "!init %s" % ' '.join(metrics)
                # send start command
                self.send(start_line)
                if wait:
                    self.wait_for_status('running')
                return True
            else:
                raise RuntimeError("No metrics scpecified")




    @classmethod
    def push_to_graphite(self,content,device='TV',configfile='collectors.ini'):
        """

        :content: string  see format in restop_graph.busybox_stats.TraceParser
        :return:
        """
        parser= TraceParser(content,device=device,configfile=configfile)
        parser.push_all_to_graphite()
        return len(parser._commands)


    def push_to_graphite_2(self,content,device='TV',configfile='collectors.ini',config=None):
        """

        :content: string  see format in restop_graph.busybox_stats.TraceParser
        :return:
        """
        config= config or {}
        nb_metrics= 0
        parser= TraceParser(content,device=device,configfile=configfile,config=config,log=self.log)
        if parser.push_all_to_graphite():
                nb_metrics= len(parser._commands)
        return nb_metrics




    def parse_trace(self):
        """

            read and parse stdout queue

        :return:
        """
        content=[]
        while 1:
            line=self.read()
            if not line:
                break
            content.append(line)
        parser= TraceParser(content)

        return parser