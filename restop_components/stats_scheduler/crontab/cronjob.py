import time
import threading

from jinja2 import Template
from restop_graph import TraceParser,TraceDriver
from restop_components.stats_scheduler import BaseStatScheduler


jobs_pattern= '''\
cat << EOF > /tmp/trace-scheduler
#!/bin/ash
while true; do
  {% for job in jobs %}echo ">->" \`date -u +"%Y-%m-%dT%H:%M:%SZ"\` {{ job.name }};{{ job.cmd }};echo ">-<"
  {% endfor %}
  sleep {{ delay }}
done
EOF
'''

script_pattern='''\
#!/bin/ash
while true; do
  {% for job in jobs %}echo ">->" \`date -u +"%Y-%m-%dT%H:%M:%SZ"\` {{ job.name }};{{ job.cmd }};echo ">-<"
  {% endfor %}
  sleep {{ delay }}
done
'''


# chmod +x /tmp/trace-scheduler
# /tmp/trace-scheduler > /tmp/trace-scheduler.txt

def shell_escape(line):
    """
        prefix characters like ( ` " ) with back slash to avoid shell interpretation

    :param line:
    :return:
    """
    line= line.replace('"' , '\\"')
    #line= line.replace('`', '\\`')
    return line



#
# console version
#


class BaseCronStatScheduler(BaseStatScheduler):
    """

        inject a while shell to perform stats


    """
    def send_job(self, command_name):
        """
            send a job to target queue every period
        :return:
        """
        #command = TraceDriver.get_collector_command(command_name)
        #command_lines = self.trace_driver.gen_command_lines(command_name, command)
        #command_string = ";".join(command_lines)
        #r = self.target_client.send(command_string)
        #return r
        return None


    def job_init(self,metrics=""):
        """

        :metrics: string metrics names separated with space eg: df cpu
        :return:
        """
        metrics= metrics.strip()
        # save metrics for restart
        self.metrics= metrics
        self.log.debug("start the scheduler with metrics: %s" % metrics)

        # parse metric list
        if ' ' in  metrics:
            jobs= metrics.split(' ')
        else:
            jobs= [metrics]
        self.jobs= [ job.strip() for job in jobs]

        # send command to re-init scheduler file
        self.job_raz()
        jobs= []
        for name in self.jobs:
            cmd = TraceDriver.get_collector_command(name)
            jobs.append(dict(name=name,cmd=cmd))

        t = Template(script_pattern)
        command_lines= t.render(delay=5,jobs=jobs)

        script_path= '/tmp/trace-scheduler'
        #r= self.target_client.send('echo "#!/bin/ash" > %s' % script_path)
        self.target_client.send("rm -f %s" % script_path)
        for l in command_lines.split('\n'):
            line = shell_escape(l)
            line = 'echo  "%s" >> %s' % (line, script_path)
            r = self.target_client.send(line)
        #r = self.target_client.send("chmod +x /tmp/trace-scheduler")

        time.sleep(5)
        self.job_raz()
        self.job_start()

        self.log.debug("scheduler initialized")
        return True

    def job_raz(self):
        """
            send command to erase the scheduler file
        :return:
        """
        command_lines = self.trace_driver.gen_start_trace()
        command_string = ";".join(command_lines)
        self.target_client.send(command_string)
        self.log.debug("scheduler reset")
        return True


    def job_sync(self,restart=True):
        """
            synchronize target queue ( empty it )
        :return:
        """
        # curl -i -X POST -F data=@/tmp/trace-scheduler-1.txt https://192.168.1.21/test
        # while read line; do echo -e "rpush logs \"${line}\"" | nc 192.168.1.21 6379; done  < trace-scheduler-1.txt >/dev/null
        #redis_line= 'while read line; do echo -e "rpush logs \"${line}\"" | nc 192.168.1.21 6379; done  < trace-scheduler-1.txt >/dev/null'

        #trace_filename= '/tmp/trace-scheduler-1.txt'
        #feedback_url = 'https://192.168.1.21/feedback/stats/tv'

        # redis_line = 'while read line; do echo -e "rpush %s \\"${line}\\"" | nc %s %s; done  < %s >/dev/null' % (
        #     redis_log_key, redis_host,redis_port,trace_filename
        # )
        #
        # log.debug("sync scheduler file:%s , redis_key:%s" % (trace_filename,redis_log_key))

        # stop scheduling
        self.job_stop()

        if self.with_curl:
            # send curl command
            self.log.debug("send stats via curl from %s to %s" % (self.trace_filename, self.feedback_url))
            r= self.get_by_curl(self.trace_filename,self.feedback_url)
            time.sleep(15)
        else:
            # no curl on platform get via serial port
            self.log.debug("send stats via console from %s to queue %s" % (self.trace_filename, self.target_id))
            rc= self.get_by_console()
            time.sleep(15)
            #self.log.error('cannot send stat: no other methods than curl yet implemented')

        if restart:
            self.log.debug("restart scheduler")
            self.job_raz()
            self.job_start()
        return

    def job_stop(self):
        """
            stop the scheduler
        :return:
        """
        self.scheduler_on = False
        self.set_status('stopping...')
        self.log.debug("scheduler stopping...")
        self.target_client.send('killall ash')
        time.sleep(6)
        self.set_status('stopped')
        self.log.debug("scheduler stopped")
        return True

    def job_start(self):
        """
            start the scheduler

        :return:
        """
        self.scheduler_on = True
        r= self.target_client.send('ash /tmp/trace-scheduler >> %s &' % self.trace_filename)
        self.set_status('running')
        self.log.debug("scheduler started")
        return r


    # def job_restart(self):
    #     """
    #
    #         /tmp/trace-scheduler > /tmp/trace-scheduler.txt
    #
    #     :return:
    #     """
    #     r = self.target_client.send('ash /tmp/trace-scheduler > %s' % (self.trace_filename))
    #     self.scheduler_on = True
    #     self.set_status('running')
    #     return True

    def job_close(self,clear=False):
        self.job_sync(restart=False)

        if clear:
            self.job_raz()
            # command_lines = self.trace_driver.gen_remove_trace()
            # command_string = ";".join(command_lines)
            # self.target_client.send(command_string)
            self.log.debug("scheduler file cleared")
        self.set_status('closed')
        return True



class CronStatScheduler(BaseCronStatScheduler, threading.Thread):
    """
        a threading version of adapter

    """

    def __init__(self, agent_id, target_id, alias='device', feedback_url=None, jobs=None, period=5,
                 redis_db=None, log=None, with_curl=True, **redis_kwargs):
        # def __init__(self,agent_id,command_line,**kwargs):
        """

        """
        threading.Thread.__init__(self)
        BaseCronStatScheduler.__init__(self, agent_id, target_id=target_id, alias=alias, feedback_url=feedback_url,
                                   jobs=jobs, period=period, redis_db=redis_db, log=log, with_curl=with_curl,
                                   **redis_kwargs)
        self.Terminated = False
        self.setDaemon(True)


# #
# # telnet version
# #
#
# class BaseTelnetCronStatScheduler(BaseCronStatScheduler):
#     """
#
#         a specific crontab init for telnet
#     """
#
#     def job_init(self, metrics=""):
#         """
#
#         :metrics: string metrics names separated with space eg: df cpu
#         :return:
#         """
#         metrics = metrics.strip()
#         # save metrics for restart
#         self.metrics = metrics
#         self.log.debug("start the scheduler with metrics: %s" % metrics)
#
#         # parse metric list
#         if ' ' in metrics:
#             jobs = metrics.split(' ')
#         else:
#             jobs = [metrics]
#         self.jobs = [job.strip() for job in jobs]
#
#         # send command to re-init scheduler file
#         self.job_raz()
#         jobs = []
#         for name in self.jobs:
#             cmd = TraceDriver.get_collector_command(name)
#             jobs.append(dict(name=name, cmd=cmd))
#
#         t = Template(script_pattern)
#         command_lines = t.render(delay=5, jobs=jobs)
#
#         script_path = '/tmp/trace-scheduler'
#         # r= self.target_client.send('echo "#!/bin/ash" > %s' % script_path)
#         for l in command_lines.split('\n'):
#             line = shell_escape(l)
#             line = 'echo  "%s" >> %s' % (line, script_path)
#             r = self.target_client.send(line)
#         # r = self.target_client.send("chmod +x /tmp/trace-scheduler")
#
#         time.sleep(5)
#         self.job_start()
#
#         self.log.debug("scheduler initialized")
#         return True
#
#
#
# class TelnetCronStatScheduler(BaseTelnetCronStatScheduler, threading.Thread):
#     """
#         a threading version of adapter
#
#     """
#
#     def __init__(self, agent_id, target_id, alias='device', feedback_url=None, jobs=None, period=5,
#                  redis_db=None, log=None, with_curl=True, **redis_kwargs):
#         # def __init__(self,agent_id,command_line,**kwargs):
#         """
#
#         """
#         threading.Thread.__init__(self)
#         BaseTelnetCronStatScheduler.__init__(self, agent_id, target_id=target_id, alias=alias, feedback_url=feedback_url,
#                                    jobs=jobs, period=period, redis_db=redis_db, log=log, with_curl=with_curl,
#                                    **redis_kwargs)
#         self.Terminated = False
#         self.setDaemon(True)

