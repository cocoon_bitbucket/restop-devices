
from jinja2 import Template


header= '#!/bin/ash'

pattern='''\
while true; do
  {% for job in jobs %}echo ">->" `date -u +"%Y-%m-%dT%H:%M:%SZ"` {{ job.name }};{{ job.cmd }};echo ">-<"
  {% endfor %}
  sleep {{ delay }}
done
'''

#
# script_content= '''\
# while true; do
#   echo ">->" `date -u +"%Y-%m-%dT%H:%M:%SZ"` df;df;echo ">-<"
#   echo ">->" `date -u +"%Y-%m-%dT%H:%M:%SZ"` cpu;cat /proc/stat | grep cpu;echo ">-<"
#   echo ">->" `date -u +"%Y-%m-%dT%H:%M:%SZ"` meminfo;cat /proc/meminfo;echo ">-<"
#   echo ">->" `date -u +"%Y-%m-%dT%H:%M:%SZ"` loadavg;cat /proc/loadavg;echo ">-<"
#   sleep 10
# done
# '''


script= '/tmp/trace-scheduler'

jobs= [
    {'name':'df'  ,'cmd':'df'},
    {'name':'cpu', 'cmd': 'cat /proc/stat | grep cpu' },
    {'name':'meminfo', 'cmd': 'cat /proc/meminfo'},
    {'name':'loadavg' ,'cmd':'cat /proc/loadavg'}
]


def shell_escape(line):
    """

    :param line:
    :return:
    """
    line= line.replace('"' , '\\"')
    line= line.replace('`', '\\`')
    return line


t = Template(pattern)
command_lines = t.render(delay=5, jobs=jobs)

print 'echo "#!/bin/ash" > %s' % script
for l in command_lines.split('\n'):
    line = shell_escape(l)
    line= 'echo  "%s" >> %s' % (line ,script)
    print line

#
#
#
# print 'echo "#!/bin/ash" > %s' % script
# for l in script_content.split('\n'):
#     line = shell_escape(l)
#     line= 'echo  "%s" >> %s' % (line ,script)
#     print line
#

