"""


    a telnet adapter with stats scheduler

    TelnetScheduler

        generic parameters:
           agent_id
           log: a logger


        telnet parameters:
            user,password,host,port
        metrics parameters:
            device: string name of the device ( 'tv', 'lb')
            metrics: list of metrics names
            delay: int time between 2 metrics
            sync: int time betwwen sync with stats backend

            collector_config: string name of collector file ( eg collectors.ini)
            config dict : override collector_config data

        backend parameters (graphite
            graphite_host
            graphite_port


        commands:

            !init metrics
            !sync
            !close
            !exit

            _start()
            _stop()


"""
import sys
import time

import threading
from jinja2 import Template

#from restop_adapters.adapter import Adapter
from restop_adapters.telnet_adapter import TelnetBaseAdapter as Adapter
from restop_adapters.client import  ClientAdapter
from restop_graph import TraceParser,TraceDriver

import logging
default_logger= logging.getLogger('statscheduler')


script_pattern='''\
#!/bin/ash
while true; do
  {% for job in jobs %}echo ">->" \`date -u +"%Y-%m-%dT%H:%M:%SZ"\` {{ job.name }};{{ job.cmd }};echo ">-<"
  {% endfor %}
  sleep {{ delay }}
done
'''


# chmod +x /tmp/trace-scheduler
# /tmp/trace-scheduler > /tmp/trace-scheduler.txt

def shell_escape(line):
    """
        prefix characters like ( ` " ) with back slash to avoid shell interpretation

    :param line:
    :return:
    """
    line= line.replace('"' , '\\"')
    #line= line.replace('`', '\\`')
    return line



class BaseTelnetScheduler(Adapter):
    """

    """
    trace_filename = "/tmp/trace-scheduler.txt"
    trace_scriptname = "/tmp/trace-scheduler"


    def __init__(self,
                 agent_id,
                 redis_db,
                 log,
                 telnet,
                 metric,
                 graphite=None,
                 with_curl= False,
                 feedback_url= 'localhost://feedback_url',
                 **kwargs
                 ):
            """

            :param agent_id: string eg scheduler:id:1
            :param redis_db: object
            :param log: object , logger
            :param telnet: dict telnet parameters ( host,port,user,password)
            :param metric: dict metric parameters ( metrics,period,sync,name,collector,config)
            :param: redis_db: object
            :param: config : dict , configuration of the graphite server eg { host:localhost, port:2300)
            """

            self.agent_id= agent_id
            self.redis_db= redis_db
            self._telnet=telnet

            super(BaseTelnetScheduler, self).__init__(
                    agent_id,
                    host= self._telnet.get('host','127.0.0.1'),
                    port= self._telnet.get('port',23),
                    user= self._telnet.get('user','root'),
                    password= self._telnet.get('password','sah'),
                    log=log,
                    redis_db=redis_db,
                    **kwargs)

            self.with_curl= with_curl
            self.feedback_url= feedback_url

            self._metric=metric

            self.period= self._metric.get('period',10)
            self.sync_period= self._metric.get('sync',60)
            self.collector_config_file = self._metric.get('collector','collectors.ini')
            self.collector_config= self._metric.get('config',{})
            self.alias= self._metric.get('name','device')
            self.jobs = self._metric.get('metrics', [])

            self._graphite= graphite or {}
            if 'host' in self._graphite :
                self.collector_config['host']=self._graphite['host']
            if 'port' in self._graphite :
                self.collector_config['port']=self._graphite['port']

            #self.graphite_host= self._graphite.get('host','127.0.0.1')
            #self.graphite_port = self._graphite.get('port', 2003)


            self.trace_driver= TraceDriver(self.trace_filename)

            self.t0=time.time()
            self.t1=self.t0
            self.t2=self.t0

            self.scheduler_on= False
            self.set_status('created')
            self.log.debug("scheduler %s created: scheduler_on is %s" % (self.agent_id,self.scheduler_on))


            self.freeze= False

            return


    def check(self):
        """

            check jobs are referenced

        :return:
        """
        for job in self.jobs:
            command_lines = self.trace_driver.gen_command_lines(job)

        return True


    def transfer_in(self):
        """

        """
        #
        # interpret incoming command !start, !dump_trace ,!read_trace ...
        #
        size= len(self._stdin)
        if size > 0:
            incoming_cmd= self._stdin.popleft()
        else:
            incoming_cmd= None

        if incoming_cmd:
            if incoming_cmd.startswith('!'):
                # a special command command_name,arg  !start !sync !read_trace !dump_trace
                command_line= incoming_cmd[1:].strip()
                if ' ' in command_line:
                    # command like: !start df meminfo
                    command_name,command_parameters= command_line.split(' ',1)
                else:
                    command_name= command_line
                    command_parameters= ''

                # find a job_<command_name> method
                try:
                    method= getattr(self,'job_%s' % command_name)
                    if command_parameters:
                        r= method(command_parameters)
                    else:
                        r= method()
                except AttributeError:
                    # no built in job method
                    # send command to target queue
                    self.send_job(command_name)

            elif incoming_cmd == self.cmd_exit:
                # exit condition, stop slave process and exit main loop
                self.job_exit()
                # self.job_close()
                # self.log.debug("scheduler [%s] : exit" % self.agent_id)
                # time.sleep(0.1)
                # self.Terminated = True
            else:
                # send brute command to target queue
                self.send(incoming_cmd)

        return True


    def next_scheduled_sync(self):
        """


        :return:
        """
        need_sync= False
        self.t2 = time.time()
        if (self.t2 - self.t1) > self.sync_period:
            # schedule a synchronization
            need_sync= True
            self.t1 = self.t2
        else:
            # no synchro
            pass
        return need_sync

    def send_job(self,command_name):
        """
            send a job to target queue
        :return:
        """
        command= TraceDriver.get_collector_command(command_name)
        command_lines = self.trace_driver.gen_command_lines(command_name,command)
        command_string = ";".join(command_lines)
        r = self.send(command_string)
        return r

    def run_once(self):
        """

        :return:
        """
        self.keep_alive()

        #schedule a synchronization
        if self.scheduler_on :
            need_sync =self.next_scheduled_sync()
            if need_sync:
                    self.log.debug("auto scheduler job_sync period:%d" % self.sync_period)
                    self.job_sync()
                    self.log.debug("auto scheduler job_sync done.")
                    #pass

        if not self.freeze:
            # transfer telnet stdout input to redis <stdout> ( if any )
            self.transfer_out()
            #  transfer redis <stdin> input to telnet stdin ( if any )
            self.transfer_in()
        else:
            time.sleep(1)

    def run(self):
        """
            main loop

            read from /agents/?/stdin
            if not empty :
                send to slave stdin

            read from slave output
                if not empty :
                    push to /agents/?/stdout

        :return:
        """
        self.log.debug("starting scheduler [%s]" % self.agent_id)
        self.set_status('started')
        #self.log.debug("starting process for agent: %s" % self.agent_id)

        # reset queues
        self.keep_alive()

        #for queue in self._queues.values():
        #    self.reset_queue(queue)

        # launch slave process
        self.run_process()
        self.Terminated = False

        while not self.Terminated:
            # infinite loop  till 'exit' received via command line
            self.run_once()
            time.sleep(0.1)

        # kill slave process and reset keep_alive
        #self.shutdown_process()
        self.set_status('terminated')
        self.keep_alive(clear=True)
        sys.exit(0)


    def get_by_curl(self,filename,url):
        """
            send curl command to stb

        :param filename:
        :param url:
        :return:
        """
        cmd_line = "curl -i -X POST --data-binary @%s %s" % (filename,url)

        #self.log.debug("send command to stb serial: %s" % cmd_line)
        r= self.send(cmd_line)

        return r

    def _flush(self):
        """
            flush telnet stdout to queue
        :return:
        """
        while  1:
            count= self.transfer_out()
            if count <= 0:
                break
        return


    def get_by_console(self):
        """

            send command to dump the /tmp/scheduler file to console

            read console stdout and put it in scheduler stdout

        :param filename:
        :param redis_db:
        :return:
        """
        # stop input/output
        self.freeze= True

        # send dump command to busybox  (cat /tmp/trace-scheduler.txt)
        rc= self._job_dump_trace()

        # wait for the busybox writing to stdout
        time.sleep(5)
        # read busybox stdout and transfer to scheduler stdout
        data= self._job_read_trace()
        if data:
            self.log.debug('get_by_console: send data to graphite (len: %d)' % len(data))
            nb_metrics = 0
            try:
                parser= TraceParser(data,
                                            device= self.alias,
                                            configfile=  self.collector_config_file,
                                            config= self.collector_config,
                                            log=self.log)
                if parser.push_all_to_graphite():
                        nb_metrics= len(parser._commands)

            except Exception,e:
                self.log.error('get_by_console,%s' % str(e))
                nb_metrics=0
            self.log.debug("number of metrics sent to graphite: %d" % nb_metrics)
        else:
            self.log.warning("get_by_console: no data to push to graphite")

        # reset sync counter
        self.t1 = time.time()
        # restart input/output
        self.freeze= False
        return True

    def push_to_graphite_2(self,content,device='TV',configfile='collectors.ini',config=None):
        """

        :content: string  see format in restop_graph.busybox_stats.TraceParser
        :return:
        """
        config= config or {}
        nb_metrics= 0
        parser= TraceParser(content,device=device,configfile=configfile,config=config,log=self.log)
        if parser.push_all_to_graphite():
                nb_metrics= len(parser._commands)
        return nb_metrics


    def job_init(self,metrics=""):
        """

        :metrics: string metrics names separated with space eg: df cpu
        :return:
        """
        metrics= metrics.strip()
        # save metrics for restart
        self.metrics= metrics
        self.log.debug("start the scheduler with metrics: %s" % metrics)

        # parse metric list
        if ' ' in  metrics:
            jobs= metrics.split(' ')
        elif ',' in metrics:
            jobs = metrics.split(',')
        else:
            jobs= [metrics]
        self.jobs= [ job.strip() for job in jobs]

        # send command to re-init scheduler file
        #self.job_raz()
        jobs= []
        for name in self.jobs:
            cmd = TraceDriver.get_collector_command(name)
            jobs.append(dict(name=name,cmd=cmd))

        t = Template(script_pattern)
        command_lines= t.render(delay=self.period,jobs=jobs)

        # remove script
        self.send("rm -f %s" % self.trace_scriptname)
        # build new script
        for l in command_lines.split('\n'):
            line = shell_escape(l)
            line = 'echo  "%s" >> %s' % (line, self.trace_scriptname)
            r = self.send(line)
        # start script


        #time.sleep(5)
        self.job_raz()
        self.job_start()

        self.log.debug("scheduler initialized")
        return True

    def _start_script(self):
        """
            start the remote script (trace-scheduler)

            ash /tmp/trace-scheduler >> /tmp/trace-scheduler.txt &

        :return:
        """
        cmd= "ash %s >> %s &" % (self.trace_scriptname,self.trace_filename)
        self.send(cmd)

    def _stop_script(self):
        """

            stop the remote script (trace-scheduler)

            killall ash

        :return:
        """
        cmd= "killall ash"
        self.send(cmd)


    def job_raz(self):
        """
            send command to erase the scheduler file
        :return:
        """
        command_lines = self.trace_driver.gen_start_trace()
        command_string = ";".join(command_lines)
        self.send(command_string)
        self.log.debug("scheduler reset")
        return True


    def job_sync(self,restart=True):
        """
            synchronize target queue ( empty it )
        :return:
        """
        # curl -i -X POST -F data=@/tmp/trace-scheduler-1.txt https://192.168.1.21/test
        # while read line; do echo -e "rpush logs \"${line}\"" | nc 192.168.1.21 6379; done  < trace-scheduler-1.txt >/dev/null
        #redis_line= 'while read line; do echo -e "rpush logs \"${line}\"" | nc 192.168.1.21 6379; done  < trace-scheduler-1.txt >/dev/null'

        #trace_filename= '/tmp/trace-scheduler-1.txt'
        #feedback_url = 'https://192.168.1.21/feedback/stats/tv'

        # redis_line = 'while read line; do echo -e "rpush %s \\"${line}\\"" | nc %s %s; done  < %s >/dev/null' % (
        #     redis_log_key, redis_host,redis_port,trace_filename
        # )
        #
        # log.debug("sync scheduler file:%s , redis_key:%s" % (trace_filename,redis_log_key))

        # stop scheduling
        self.job_stop()

        if self.with_curl:
            # send curl command
            self.log.debug("send stats via curl from %s to %s" % (self.trace_filename, self.feedback_url))
            r= self.get_by_curl(self.trace_filename,self.feedback_url)
            time.sleep(15)
        else:
            # no curl on platform get via serial port
            self.log.debug("send stats via console from %s to queue %s" % (self.trace_filename, self.agent_id))
            rc= self.get_by_console()
            #self.log.error('cannot send stat: no other methods than curl yet implemented')

        if restart:
            self.log.debug("restart scheduler")
            self.job_raz()
            self.job_start()
        return

    def job_stop(self):
        """
            stop the scheduler
        :return:
        """
        self._stop_script()
        self.scheduler_on = False
        self.set_status('stopped')
        self.log.debug("scheduler stopped")
        time.sleep(1)
        return True

    def job_start(self):
        """
            start the scheduler
        :return:
        """
        self.scheduler_on = True
        self.set_status('running')
        self._start_script()


    def job_close(self,clear=False):
        """

        :param clear:
        :return:
        """
        self.log.debug("close scheduler")
        status= self.get_status()
        if status not in ['started','closed']:
            self.job_sync(restart=False)
            self.set_status('closed')
            if clear:
                command_lines = self.trace_driver.gen_remove_trace()
                command_string = ";".join(command_lines)
                self.send(command_string)
                self.log.debug("scheduler file cleared")
            self.log.debug("scheduler closed")
        else:
            self.log.debug('scheduler already closed')
        return True


    def job_exit(self):
        """

        :return:
        """
        # exit condition, stop slave process and exit main loop
        self.log.debug("exit scheduler")
        self.job_close()
        self.log.debug("scheduler [%s] : exit" % self.agent_id)
        time.sleep(0.1)
        self.Terminated = True

    def _job_dump_trace(self):
        """
            send command to target queue to dump trace file


        :return:
        """
        # stop scheduler function
        #self.scheduler_on = False
        #self.set_status('stopped')
        #self.job_stop()
        # send dump trace file to target client
        time.sleep(1)
        self._flush()
        command_lines= self.trace_driver.gen_dump_command()
        command_string= ";".join(command_lines)
        self.send(command_string)


    def _job_read_trace(self):
        """
            read lines from target queue and store them to self stdout queue
        :return:
        """
        stop = False
        top= False
        data=[]
        self.set_status('collecting')
        self.log.debug('collect trace-scheduler data')
        while ( not stop):
            # read line from target queue
            line= self.read()
            if not line:
                self.log.debug('collect  done')
                break
            if line.startswith('>-['):
                # start of trace file detected
                top= True
                self.log.debug('start of trace detected')
            elif line.startswith('>-]'):
                top = False
                self.log.debug('end of trace detected')
                data.append(line)
            if top:
                # store line to self stdout queue
                data.append(line)
            else:
                # store line to log
                self._logs.append(line)
        self.set_status('collected')
        self.log.debug('collect trace-scheduler done')
        return data


class ThreadedTelnetScheduler(BaseTelnetScheduler,threading.Thread):
    """
        a threading version of adapter

    """

    def __init__(self,
                 agent_id,
                 redis_db,
                 log,
                 telnet,
                 metric,
                 graphite=None,
                 with_curl=False,
                 feedback_url='localhost:5000//feedback_url',
                 **kwargs
                 ):


    # def __init__(self, agent_id, target_id, alias='device',feedback_url=None,jobs=None, period= 5,
    #              redis_db=None,log=None,with_curl=True,config=None, **redis_kwargs):
    #def __init__(self,agent_id,command_line,**kwargs):
        """

        """
        threading.Thread.__init__(self)
        BaseTelnetScheduler.__init__(self,
                agent_id,
                redis_db=redis_db,
                log=log,
                telnet=telnet,
                metric=metric,
                graphite=graphite,
                with_curl=with_curl,
                feedback_url= feedback_url,
                **kwargs)
        self.Terminated=False
        self.setDaemon(True)



TelnetScheduler= ThreadedTelnetScheduler






if __name__=="__main__":

    from walrus import Database
    from wbackend.logger import SimpleLogger


    db= Database()
    db.flushdb()

    agent_id= 'scheduler:id:1'
    agent_log= 'scheduler:container:logs:scheduler:id:1'

    log= SimpleLogger(agent_log,redis_db=db)
    log.info('log initialized')

    telnet=dict(host="192.168.1.1",port=23,user='root',password='sah')
    metric= dict( name='device', period=10, sync= 60)

    graphite= dict( host='192.168.1.21',port=2003)


    ts= BaseTelnetScheduler(agent_id,redis_db=db,log=log,telnet=telnet,metric=metric,graphite=graphite)

    ts.check()
    ts.run_process()

    ts.job_init(metrics= "df,cpu")

    ts.run_once()

    ts.job_sync()


    ts.job_close()

    print "Done"



