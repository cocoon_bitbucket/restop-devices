import time
from yaml import load
from copy import deepcopy

from wbackend.model import Database,Model
from restop_adapters.client import ClientAdapter
from restop_components.usbcontroller.models import UsbController, UsbQueue
from restop_components.usbcontroller.manager import BaseUsbManager
from restop_components.usbcontroller.client import UsbControllerClient


data= {
    '_loop': dict(device='loop://?logging=debug'),
    'tv': dict(device='/dev/ttyUSB0',bauds=115200)
}

data_loop= {
    '_loop': dict(device='loop://?logging=debug')
}

def get_new_db():
    """

    :return:
    """
    db = Database(host='localhost', port=6379, db=0)
    db.flushdb()
    Model.bind(database=db,namespace=None)
    return db

def test_model():
    """

    """
    db= get_new_db()
    ctrl= UsbController.create(data)

    return


def test_manager():
    """


    :return:
    """
    db= get_new_db()


    manager= BaseUsbManager(name='default',parameters=data_loop)


    for name,data in data_loop.iteritems():
        q= manager.spawn_queue(name)

        text= 'hello  there\n'
        # write to serial queue
        q._stdin.append(text)
        time.sleep(2)
        # read from serial queue
        r= q._stdout.popleft()
        assert r == text
        continue

    return

def test_manager_with_client():
    """


    :return:
    """
    db= get_new_db()


    manager= BaseUsbManager(name='default',parameters=data_loop)

    # start loop queue
    q = manager.spawn_queue('_loop')


    client = UsbControllerClient(name='default',redis_db=db,log=None)

    #loop_client= ClientAdapter("usbqueue:id:_loop",redis_db=db)
    loop_client= client.get_adapter_for_queue('_loop')

    client.send_to_queue('_loop','my text with ! inside')
    time.sleep(2)

    manager.run_once()
    time.sleep(2)

    data= loop_client.read()
    assert data== 'my text with ! inside\n'


    loop_client.send("hello there\n")
    time.sleep(2)
    line= loop_client.read()
    assert line == 'hello there\n'


    # for name,data in data_loop.iteritems():
    #     q= manager.spawn_queue(name)
    #
    #     text= 'hello  there\n'
    #     # write to serial queue
    #     q._stdin.append(text)
    #     time.sleep(2)
    #     # read from serial queue
    #     r= q._stdout.popleft()
    #     assert r == text
    #     continue

    return




if __name__=="__main__":


    #test_model()
    #test_manager()
    test_manager_with_client()


    print "Done"