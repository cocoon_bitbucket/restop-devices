
from walrus import Database
from wbackend.model import Model

from restop_components.powerswitch_device.models import Platform,Device
from restop_components.powerswitch_device import PowerswitchPlug


def test_setup_platform(platform):
    """



    :param platform:
    :return:
    """

    platform._set_powerswitch()

    switch= Device.get(Device.name=='powerswitch')
    conf= switch.get_configuration()

    return


def test_powerswitch_plug(platform):
    """


    :param platform:
    :return:
    """

    plug= PowerswitchPlug.get('powerswitch')
    plug.switch_on('1')
    plug.switch_off(2)

    plug2 = PowerswitchPlug.get('powerswitch',channel=1)
    plug2.switch_on()

    return




if __name__== "__main__":


    from yaml import load


    import logging
    logging.basicConfig(level='DEBUG')


    def get_platform():
        """

        :return:
        """

        # ...
        stream= file('full_platform.yml')
        data = load(stream)

        db = Database(host='localhost', port=6379, db=0)
        db.flushall()

        Model.bind(database=db,namespace=None)

        platform= Platform.create(name='default', data=data)

        return platform

    #
    # tests
    #

    test_setup_platform(get_platform())

    test_powerswitch_plug(get_platform())

    print "Done"