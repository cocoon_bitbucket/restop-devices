
import sys
sys.path.append('../')

import time
from difflib import ndiff

from walrus import Database
from jinja2 import Template

from restop_graph.busybox_stats import TraceDriver,TraceParser
#,TraceHandlers
from restop_components.stats_scheduler import StatScheduler,StatSchedulerClient


from restop_components.stats_scheduler.crontab.cronjob import jobs_pattern
from restop_components.stats_scheduler import BaseCronStatScheduler,CronStatScheduler



default_metrics= ['df', 'cpu','meminfo','loadavg']

redis_db = Database(host='localhost', port=6379, db=0)



# def test_busybox():
#     """
#
#     :return:
#     """
#     # init trace driver
#
#
#     driver = TraceDriver('/tmp/trace')
#
#     lines = driver.gen_start_trace()
#     driver.run_local(lines)
#
#     # list process
#     lines = driver.gen_command_lines('ps', 'ps')
#     driver.run_local(lines)
#
#     # perform average load ( does not work on mac)
#     lines = driver.gen_command_lines('loadavg')
#     driver.run_local(lines)
#
#     # perform df -H
#     lines = driver.gen_command_lines('df_h')
#     driver.run_local(lines)
#
#     # meminfo
#     lines = driver.gen_command_lines('meminfo')
#     driver.run_local(lines)
#
#     # meminfo
#     lines = driver.gen_command_lines('free_m')
#     driver.run_local(lines)
#
#     # dump_trace
#     lines = driver.gen_dump_command()
#     driver.run_local(lines)
#
#     # parse trace
#     p = TraceParser.from_file('/tmp/trace')
#
#     # clean trace_file
#     # lines= driver.gen_remove_trace()
#     # driver.run_local(lines)
#
#
#
#     for timestamp, command, trace in p:
#         print timestamp, command, trace
#
#         if command in TraceHandlers.handlers.keys():  # ['meminfo','free_m']:
#
#             metrics = p.decode(timestamp, command, trace)
#
#
#             # heapq.heapify(p._commands)
#
#             # sort_commands= [heapq.heappop(p._commands) for i in range(len(p._commands))]
#
#     return



def test_trace_driver():
    """

    :return:
    """
    command_name= 'df'
    filename= 'trace-scheduler.txt'

    td= TraceDriver(filename)

    for command_name in default_metrics:

        command = td.get_collector_command(command_name)
        command_lines = td.gen_simple_command_lines(command_name, command)
        command_string = ";".join(command_lines)

        print command_string
    return


def test_scheduler():

    scheduler_key='scheduler:id:1'
    agent_key='tvbox_agents:id:1'

    redis_db.flushall()

    redis_kwargs= dict(host='localhost')

    try:
        scheduler = StatScheduler(scheduler_key, agent_key, redis_db=redis_db)
        scheduler.check()
    except Exception, e:
        raise

    #rc= scheduler.run_once()
    #rc= scheduler.next_scheduled_jobs()

    #rc= scheduler.run()

    try:
        scheduler.start()
    except Exception, e:
        print('cannot start scheduler server %s ' % e)
        raise
    print("%s scheduler server started" % agent_key)


    client= StatSchedulerClient(scheduler_key,redis_db=redis_db)
    client.init_metrics(default_metrics)
    time.sleep(20)

    client.exit()
    time.sleep(10)


    return



def test_scheduler_bad_job():
    """


    :return:
    """


    scheduler_key = 'scheduler:id:1'
    agent_key = 'tvbox_agents:id:1'
    redis_kwargs = dict(host='localhost')

    try:
        scheduler = StatScheduler(scheduler_key, agent_key, redis_db=redis_db)
    except Exception, e:
        raise


    # add an invalid job
    scheduler.jobs.append('invalid_job')


    try:
        scheduler.check()
    except KeyError,e:
        print('cannot start scheduler server %s ' % str(e))


def test_scheduler_by_console():
    """


    :return:
    """
    scheduler_key='scheduler:id:1'
    agent_key='tvbox_agents:id:1'

    config= { 'host':'192.168.1.21','port':2003}
    redis_db.flushall()


    try:
        scheduler = StatScheduler(scheduler_key, agent_key, redis_db=redis_db,with_curl=False,config=config)
        scheduler.check()
    except Exception, e:
        raise

    #rc= scheduler.run_once()
    #rc= scheduler.next_scheduled_jobs()

    #rc= scheduler.run()

    try:
        scheduler.start()
    except Exception, e:
        print('cannot start scheduler server %s ' % e)
        raise
    print("%s scheduler server started" % agent_key)

    client= StatSchedulerClient(scheduler_key,redis_db=redis_db)
    client.init_metrics(default_metrics)

    # simulate output on busybox serial
    with open('samples/trace-scheduler.txt','r') as fh:
        lines=fh.read()
        for line in lines.split('\n'):
            scheduler.target_client.put('stdout',line)

    rc= scheduler.job_sync(restart=False)

    # client= StatSchedulerClient(scheduler_key,redis_db=redis_db)
    # client.init_metrics(default_metrics)
    # time.sleep(20)
    #


    client.exit()
    time.sleep(3)


    return


def test_base_cron_scheduler():

    scheduler_key='scheduler:id:1'
    agent_key='tvbox_agents:id:1'

    redis_db.flushall()

    redis_kwargs= dict(host='localhost')

    try:
        scheduler = BaseCronStatScheduler(scheduler_key, agent_key, redis_db=redis_db)
        scheduler.check()
    except Exception, e:
        raise

    rc=scheduler.job_init(metrics=" ".join(default_metrics))

    rc= scheduler.run_once()
    rc= scheduler.next_scheduled_jobs()

    rc= scheduler.job_sync()

    rc= scheduler.job_close()
    time.sleep(10)


    return


def test_cron_scheduler():

    scheduler_key='scheduler:id:1'
    agent_key='tvbox_agents:id:1'

    redis_db.flushall()

    redis_kwargs= dict(host='localhost')

    try:
        scheduler = CronStatScheduler(scheduler_key, agent_key, redis_db=redis_db)
        scheduler.check()
    except Exception, e:
        raise

    try:
        scheduler.start()
    except Exception, e:
        print('cannot start scheduler server %s ' % e)
        raise
    print("%s scheduler server started" % agent_key)


    client= StatSchedulerClient(scheduler_key,redis_db=redis_db)
    client.init_metrics(default_metrics)
    time.sleep(20)

    client.send('!sync')

    client.send('!close')
    client.exit()
    time.sleep(10)


    return


def test_jobs_pattern():
    """



    :return:
    """

    jobs = []
    for name in default_metrics:
        cmd = TraceDriver.get_collector_command(name)
        jobs.append(dict(name=name, cmd=cmd))

    t = Template(jobs_pattern)
    command_lines = t.render(delay=5, jobs=jobs)

    print command_lines
    result = '''\
cat << EOF > /tmp/trace-scheduler
#!/bin/ash
while true; do
  echo ">->" \`date -u +"%Y-%m-%dT%H:%M:%SZ"\` df;df;echo ">-<"
  echo ">->" \`date -u +"%Y-%m-%dT%H:%M:%SZ"\` cpu;cat /proc/stat | grep cpu;echo ">-<"
  echo ">->" \`date -u +"%Y-%m-%dT%H:%M:%SZ"\` meminfo;cat /proc/meminfo;echo ">-<"
  echo ">->" \`date -u +"%Y-%m-%dT%H:%M:%SZ"\` loadavg;cat /proc/loadavg;echo ">-<"

  sleep 5
done
EOF
'''

    diff = ndiff(result.splitlines(1),command_lines.splitlines(1))

    delta= ''.join(diff)
    print delta
    #assert command_lines == result

    return


if __name__=="__main__":


    import logging
    logging.basicConfig(level=logging.DEBUG)

    #test_trace_driver()

    #test_scheduler()
    #test_scheduler_bad_job()
    test_scheduler_by_console()


    #test_jobs_pattern()

    #test_base_cron_scheduler()
    #test_cron_scheduler()


    print "Done."