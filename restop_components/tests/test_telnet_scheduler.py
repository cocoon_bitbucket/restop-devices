

import time
from restop_components.stats_scheduler.telnet_scheduler import BaseTelnetScheduler, ThreadedTelnetScheduler


from restop_components.stats_scheduler import  TelnetScheduler
from restop_components.stats_scheduler import  StatSchedulerClient


def test_raw_scheduler():
    """


    :return:
    """
    db.flushdb()

    ts= BaseTelnetScheduler(agent_id,redis_db=db,log=log,telnet=telnet,metric=metric,graphite=graphite)

    ts.check()
    ts.run_process()

    ts.job_init(metrics= "df,cpu,meminfo,loadavg")
    ts.run_once()

    time.sleep(11)

    ts.job_sync()

    time.sleep(6)

    ts.job_close()

    ts.job_exit()

    time.sleep(3)

    return



def test_threaded_scheduler():
    """


    :return:
    """
    db.flushdb()

    ts= ThreadedTelnetScheduler(agent_id,redis_db=db,log=log,telnet=telnet,metric=metric,graphite=graphite)
    #ts.run_process()
    ts.start()

    ts.job_init(metrics="df,cpu,loadavg,meminfo")




    time.sleep(60)
    #ts.job_sync()

    time.sleep(3600)


    time.sleep(70)
    ts.job_close()


    time.sleep(3)

    return


def test_full_scheduler():
    """
        test scheduler and client

    :return:
    """
    db.flushdb()


    client= StatSchedulerClient(agent_id,log=log,redis_db=db)

    ts= TelnetScheduler(scheduler_id,redis_db=db,log=log,telnet=telnet,metric=metric,graphite=graphite)
    ts.start()


    client.init_metrics(["df","cpu","loadavg","meminfo"])


    time.sleep(60)

    #time.sleep(120)

    time.sleep(10)

    #client.send(('!close'))

    time.sleep(10)

    client.exit()

    time.sleep(5)

    return





if __name__=="__main__":

    from walrus import Database
    from wbackend.logger import SimpleLogger

    import logging
    logging.basicConfig(level=logging.DEBUG)


    db= Database()
    db.flushall()



    agent_id= 'scheduler:id:1'
    scheduler_id= 'scheduler:id:1'

    #agent_log= 'scheduler:container:logs:scheduler:id:1'
    agent_log = 'agent:container:logs:agent:id:1'

    log= SimpleLogger(agent_log,redis_db=db)
    log.info('log initialized')

    telnet=dict(host="192.168.1.1",port=23,user='root',password='sah')
    metric= dict( name='livebox', period=10, sync= 60)

    graphite= dict( host='192.168.1.21',port=2003)
    #graphite = dict(host='localhost', port=2003)


    #test_raw_scheduler()
    #test_threaded_scheduler()

    test_full_scheduler()

    print "Done"