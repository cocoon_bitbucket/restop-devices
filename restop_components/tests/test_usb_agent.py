import time
from yaml import load
from copy import deepcopy

import requests

from wbackend.model import Database,Model
from restop_platform.models import Platform, AgentModel


from restop_components.some_agents.usb_agent import UsbAgent as Agent


platform_file= './platform_labo.yml'
agent_name = 'stbplay'


HOST='localhost'



def get_new_db():
    """

    :return:
    """
    db = Database(host=HOST, port=6379, db=0)
    db.flushdb()
    Model.bind(database=db,namespace=None)

    try:
        p= Platform.get(Platform.name=='default')
    except Exception,e:
        # does not exists create it
        db.flushdb()
        #stream = file('../../tests/platform.yml')
        stream = file(platform_file)
        data = load(stream)
        p = Platform.create(name='default', data=data)
        #p.setup()

    return db

def get_model(agent_name):

    db= get_new_db()

    platform = Platform.get(Platform.name == 'default')
    agent_parameters= platform.data['devices'][agent_name]
    agent_profile= agent_parameters['profile']
    parameters= deepcopy(platform.data['profiles'][agent_profile])
    parameters.update(agent_parameters)

    tv = AgentModel.create(name=agent_name, session=1, parameters= parameters)
    tv_id= tv.get_id()
    tv = AgentModel.load(tv_id)

    return tv



#
#
#



def test_serial_loop():
    """

        parameters= {
            "profile": "tvbox",
            "serial_port": "loop://?logging=debug",
            "collection": "tvbox_agents",
            "peer_port": "8080",
            "peer_address": "192.168.1.20",
            "id": "1234"
                            }

    :return:
    """

    tv= get_model(agent_name)

    database= tv.database

    agent= Agent(tv)

    # force serial port to loop://?logging=debug
    parameters= agent.model.parameters
    #parameters['serial_port'] = 'loop://?logging=debug'
    agent.model.parameters=parameters
    #agent.model.parameters['serial_port']= 'loop://?logging=debug'
    agent.model.save()

    response= agent.start()

    line= agent._client.readline()
    while line:
        line = agent._client.readline()

    rc= agent._client.write('\n')
    rc= agent._client.write('ls -l /tmp\n')


    line= agent._client.readline()
    while line:
        line = agent._client.readline()


    #log_offset= len(agent.model.logs)
    rc= agent.watch(timeout=5)
    response = agent.model.make_response(rc)
    assert "scheduler server started" in response['logs'][-3]



    #rc= agent._scheduler_server.job_init(metrics='df')


    rc= agent.scheduler_init()
    time.sleep(6)
    rc= agent.scheduler_sync()
    response = agent.model.make_response(rc)
    assert "sync scheduler sent" in response['logs'][-1]

    rc= agent.scheduler_close()
    time.sleep(3)
    response = agent.model.make_response(rc)
    #assert "scheduler stopped" in response['logs'][-1]

    rc= agent.send("hello world")


    rc= agent.sync(timeout=2)
    response = agent.model.make_response(rc)



    response= agent.stop()
    time.sleep(3)



    return


if __name__=="__main__":


    #
    test_serial_loop()

    print "Done."
