# -*- coding: utf-8 -*-

import logging
import subprocess

class StickTvVideoDetection():
    """


    """

    def motionDetection(self):
        logger = logging.getLogger('NewTvTesting.Video')
        if (subprocess.call(["stbt", "run", "NewTvTesting/StbtTestLive.py"])) == 1:
            logger.info(" >> Motion detected")
            return True
        else:
            logger.info(" >> No motion detected")
            return False


    def screenshot(self, file):
        subprocess.call(["stbt", "screenshot", file])