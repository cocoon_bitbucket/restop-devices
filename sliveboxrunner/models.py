

DEVICE_NAME= "slivebox_agents"


from walrus import *
# from wbackend.model import Model
# from wbackend.graph import Graph


from restop_platform.models import AgentModel


default_parameters={

    'with_curl': False,

    'peer_ip': "192.168.1.1",

    # for telnet
    "telnet_port": 23,
    'telnet_user': 'admin',
    'telnet_password': 'admin',
    # for http
    'http_user': 'admin',
    'http_password': "admin",
    'http_port': 80

}


class LiveboxAgents(AgentModel):
    """


    """
    collection= DEVICE_NAME

    boxconnector = IntegerField(default=1)