



import time
from yaml import load
from copy import deepcopy

import requests

from wbackend.model import Database,Model
from restop.application import ApplicationError
from restop_platform.models import Platform

from sliveboxrunner.models import AgentModel
from sliveboxrunner.agent import Agent



platform_file= './platform.yml'
agent_name = 'livebox'


def get_new_db():
    """

    :return:
    """
    db = Database(host='localhost', port=6379, db=0)
    db.flushdb()
    Model.bind(database=db,namespace=None)

    try:
        p= Platform.get(Platform.name=='default')
    except Exception,e:
        # does not exists create it
        db.flushdb()
        #stream = file('../../tests/platform.yml')
        stream = file(platform_file)
        data = load(stream)
        p = Platform.create(name='default', data=data)
        #p.setup()

    return db

def get_model(agent_name):

    db= get_new_db()

    platform = Platform.get(Platform.name == 'default')
    agent_parameters= platform.data['devices'][agent_name]
    agent_profile= agent_parameters['profile']
    parameters= deepcopy(platform.data['profiles'][agent_profile])
    parameters.update(agent_parameters)

    m = AgentModel.create(name=agent_name, session=1, parameters= parameters)
    m_id= m.get_id()
    m = AgentModel.load(m_id)

    return m

def get_agent(agent_name,with_curl=False):
    """

    :param agent_name:
    :return:
    """
    model= get_model(agent_name)
    database= model.database
    agent= Agent(model)

    # force serial port to loop://?logging=debug
    parameters= agent.model.parameters
    parameters['dummy'] = 'dummy'
    agent.model.parameters=parameters
    agent.model.parameters['with_curl']= with_curl
    agent.model.save()

    return agent


def test_basic():
    """
    create an agent and test PrintInfo

    :return:
    """

    agent_name = 'livebox'
    agent= get_agent(agent_name)

    rc= agent.start()

    # send hello
    agent.send('echo "hello"\n')

    response= agent.expect("hello")
    print response
    assert response == "OK"

    rc =agent.stop()
    return


def test_io():
    """
    create an agent and test PrintInfo

    :return:
    """

    agent_name = 'livebox'
    agent= get_agent(agent_name)

    rc= agent.start()

    # send hello
    agent.send('echo "hello"\n')
    response= agent.expect("hello")
    print response
    assert response == "OK"

    response= agent.sync()
    print response
    assert "found" in response

    agent.send("ls -l /tmp\n")
    response=agent.watch(timeout=5)


    rc =agent.stop()
    return








def test_statscheduler():
    """
    create an agent and test PrintInfo

    :return:
    """

    agent_name = 'livebox'
    agent= get_agent(agent_name,with_curl=False)

    rc= agent.start()

    rc=agent.sync()
    #agent.init_metrics(["df", "cpu", "loadavg", "meminfo"])
    agent.scheduler_init(["df", "cpu", "loadavg", "meminfo"])


    time.sleep(60)

    agent.scheduler_close()

    time.sleep(10)
    # client.send(('!close'))
    #time.sleep(10)
    #agent.exit()



    rc =agent.stop()
    return






if __name__=="__main__":


    import logging
    logging.basicConfig(level=logging.DEBUG)

    #
    #test_basic()
    #test_io()
    test_statscheduler()

    print "Done."
