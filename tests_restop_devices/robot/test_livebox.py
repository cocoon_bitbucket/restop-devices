__author__ = 'cocoon'

import time

import os
from restop_client import Pilot


#phonehub_url= "http://localhost:5000/restop/api/v1"
#phonehub_url= "http://192.168.99.100/restop/api/v1"
#phonehub_url= "http://10.179.5.174/restop/api/v1"
phonehub_url= "http://192.168.1.21/restop/api/v1"


user1 = 'livebox'

def test_fetch_restop_interface():
    """



    :return:
    """
    # to test localy
    #phonehub_url = "http://localhost:5001/restop/api/v1"
    #os.environ['NO_PROXY'] = 'localhost'

    p= Pilot()

    data= p.fetch_restop_interface(url=phonehub_url)

    return data





#
#  dummy tvboxrunner
#


def test_basic():

    p =Pilot()

    p.setup_pilot("demo", "demo_qualif", phonehub_url)


    try:

        res= p.open_Session(user1)
        time.sleep(2)


        p.send(user1, cmd=u'echo "hello world"')
        time.sleep(3)
        res = p.watch(user1)



    except RuntimeError as e:
        print("interrupted")

    except Exception as e:
        print(e.message)
    finally:
        r= p.close_session()

    return


def test_boxconnector():

    p =Pilot()

    p.setup_pilot("demo", "demo_qualif", phonehub_url)


    try:

        res= p.open_Session(user1)
        time.sleep(2)

        rc= p.PrintInfo(user1)

        rc= p.GetProperty(user1,name='TV_Status')

        time.sleep(3)

    except RuntimeError as e:
        print("interrupted")

    except Exception as e:
        print(e.message)
    finally:
        r= p.close_session()

    return




def test_scheduler():

    p =Pilot()

    p.setup_pilot("demo", "demo_qualif", phonehub_url)

    try:

        res= p.open_Session(user1)

        res = p.telnet_sync(user1)

        # start scheduler
        res= p.scheduler_init(user1)
        res = p.telnet_watch(user1, timeout=6)

        # collect stats
        res= p.scheduler_sync(user1)
        time.sleep(10)
        res = p.telnet_watch(user1, timeout=6)

        # close scheduler
        res= p.scheduler_close(user1)
        time.sleep(15)
        res= p.telnet_watch(user1,timeout=10)


    except RuntimeError as e:
        print("interrupted")

    except Exception as e:
        print(e.message)
    finally:
        r= p.close_session()

    return



if __name__=="__main__":


    #test_fetch_restop_interface()
    #test_basic()
    test_scheduler()
    #test_boxconnector()


    print("Done")