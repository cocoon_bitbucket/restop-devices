*** Settings ***
Documentation     standard tests
...

# libraries
Library  restop_client


#Suite Setup  init suite
Test Setup     setup pilot     ${platform_name}    ${platform_version}  ${platform_url}
Test Teardown  close session


*** Variables ***
${platform_name}=   demo
${platform_version}=    demo_qualif

#${platform_url}=  http://localhost:5000/restop/api/v1
#${platform_url}=  http://192.168.1.21/restop/api/v1
${platform_url}=  http://192.168.1.26/restop/api/v1


#${pilot_mode}=  dry
${pilot_mode}=  normal



*** Keywords ***

########
#init suite
#	# check platform capablities with test capablities requirements
#	setup pilot 	${platform_name} 	${platform_version}  ${platform_url}
#
#init pilot
#	setup pilot 	${platform_name} 	${platform_version}  ${platform_url}
#	run keyword if   '${pilot_mode}'=='dry'  set pilot dry mode
#
#shutdown pilot
#	close session




Unit Livebox Demo
	[Arguments] 	${lb}
	[Documentation] 	livebox test

	Open session	${lb}
	Scheduler Init  ${lb}


    Telnet Sync    ${lb}


    ${info}=  PrintInfo  ${lb}
    Log Many  ${info}

    ${info}=  GetProperty  ${lb}  name=TV_Status
    Log Many  ${info}


    Telnet send  ${lb}  cmd=ls -l /tmp
    Telnet Watch    ${lb}  timeout=10


    builtin.sleep   60

    Telnet Sync  ${lb}





Unit Livebox Stat Scheduler
	[Arguments] 	${lb}
	[Documentation] 	test stat scheduler on livebox

	Open session	${lb}

    #Telnet Sync    ${lb}

    Scheduler Init  ${lb}
    #Telnet Watch    ${lb}  timeout=10

    builtin.sleep   60

    #Scheduler Sync   ${lb}
    #builtin.sleep   15
    #Telnet Watch    ${lb}  timeout=10


    builtin.sleep   60

    Scheduler Close  ${lb}
    builtin.sleep    15
    #Telnet Watch  ${lb}  timeout=10



Unit Livebox Socket Connector
	[Arguments] 	${lb}
	[Documentation] 	test Socket Connector for livebox

	Open session	${lb}

    builtin.sleep  2

    ${info}=  PrintInfo  ${lb}
    Log Many  ${info}

    ${info}=  GetProperty  ${lb}  name=TV_Status
    Log Many  ${info}



*** Test Cases ***


#lbconnector
#  [Template]  Unit Livebox Socket Connector
#  livebox
#
#
#
#scheduler
#  [Tags]  livebox
#  [Template]  Unit Livebox Stat Scheduler
#  livebox
#
#lbconnector2
#  [Template]  Unit Livebox Socket Connector
#  livebox
#

demo
  Unit Livebox Demo  livebox