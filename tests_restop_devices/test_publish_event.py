__author__ = 'cocoon'

import time

import os
import redis
from restop_client import Pilot

from restop_graph.influxdb.injector import Listener,Worker


#redis_url= "redis://localhost:6379/1"
redis_url= "redis://192.168.1.21:6379"

#phonehub_url= "http://localhost:5000/restop/api/v1"
#phonehub_url= "http://192.168.99.100/restop/api/v1"
#phonehub_url= "http://10.179.5.174/restop/api/v1"
phonehub_url= "http://192.168.1.21/restop/api/v1"
#phonehub_url= "http://192.168.1.26/restop/api/v1"
#phonehub_url= "http://192.168.1.11/restop/api/v1"


user1 = 'slivebox'

def test_fetch_restop_interface():
    """



    :return:
    """
    # to test localy
    #phonehub_url = "http://localhost:5001/restop/api/v1"
    #os.environ['NO_PROXY'] = 'localhost'

    p= Pilot()

    data= p.fetch_restop_interface(url=phonehub_url)

    return data



def test_scheduler():

    p =Pilot()

    p.setup_pilot("demo", "demo_qualif", phonehub_url)

    try:

        res= p.open_Session(user1)

        res = p.telnet_sync(user1)

        # start scheduler
        res= p.scheduler_init(user1, metrics=["df", "meminfo", "cpu","loadavg" ])
        #res = p.init_metrics(user1,metrics=["df", "meminfo", "cpu","loadavg" ])

        res = p.telnet_watch(user1, timeout=6)

        # collect stats
        #res= p.scheduler_sync(user1)
        time.sleep(65)
        #res = p.telnet_watch(user1, timeout=6)

        # close scheduler
        #res= p.scheduler_close(user1)
        #time.sleep(15)
        res= p.telnet_watch(user1,timeout=10)


    except RuntimeError as e:
        print("interrupted")

    except Exception as e:
        print(e.message)
    finally:
        r= p.close_session()


    return


def test_event_publisher():

    p =Pilot()

    p.setup_pilot("demo", "demo_qualif", phonehub_url)


    # start a influxdb.event subscriber
    redis_db= redis.from_url(redis_url)
    subscriber= Listener(redis_db=redis_db)
    worker=Worker()
    subscriber.set_worker(worker)
    subscriber.start()

    try:


        res= p.open_Session(user1)
        time.sleep(2)


        p.publish_event(user1, title="my_open_event")

        time.sleep(5)

        p.publish_event(user1,title="my_close_event")



    except RuntimeError as e:
        print("interrupted")

    except Exception as e:
        print(e.message)
    finally:
        r= p.close_session()


    return






if __name__=="__main__":


    import logging
    logging.basicConfig(level=logging.DEBUG)

    test_fetch_restop_interface()
    #test_scheduler()

    test_event_publisher()

    print("Done")