# -*- coding: utf-8 -*-

__author__ = 'cocoon'

import time

import os
from restop_client import Pilot



#phonehub_url= "http://localhost:5000/restop/api/v1"
#phonehub_url= "http://192.168.99.100:5000/restop/api/v1"
#phonehub_url= "http://10.179.1.246/restop/api/v1"

#phonehub_url= "http://192.168.99.1:5000/restop/api/v1"

phonehub_url= "http://192.168.1.26/restop/api/v1"




def test_fetch_restop_interface():
    """



    :return:
    """
    # to test localy
    #phonehub_url = "http://localhost:5001/restop/api/v1"
    #os.environ['NO_PROXY'] = 'localhost'

    p= Pilot()

    data= p.fetch_restop_interface(url=phonehub_url)

    return data

#
# samples
#
def test_samples_basic():

    p =Pilot()

    p.setup_pilot("demo", "demo_qualif", phonehub_url)


    try:

        res= p.open_Session('sample_1','sample_2')


        res= p.dummy('sample_1')
        res= p.dummy('sample_2')

        # read alarm on samples
        time.sleep(2)

        res = p.read('sample_1')
        #assert res.startswith('Signal')
        res = p.read('sample_2')
        #assert res.startswith('Signal')


        res= p.ping('sample_1')
        time.sleep(2)

        res= p.read('sample_1')
        assert res=='pong\n'

    except RuntimeError,e:
        print "interrupted"

    except Exception,e:
        print e.message
    finally:
        r= p.close_session()

    return


#
#  droydrunner
#
def test_droydrunner_adb_devices():

    p =Pilot()

    p.setup_pilot("demo", "demo_qualif", phonehub_url)


    try:

        res= p.open_Session()

        res=p.adb_devices('droydrunner_agents')


    except RuntimeError,e:
        print "interrupted"

    except Exception,e:
        print e.message
    finally:
        r= p.close_session()

    return


def test_basic_appstick():

    p =Pilot()

    p.setup_pilot("demo", "demo_qualif", phonehub_url)

    user= 'appstick'

    try:

        res= p.open_Session(user)

        # read alarm on samples
        time.sleep(2)

        res= p.press_home(user)
        res= p.press_home(user)

        res= p.start_tv(user)

        time.sleep(2)

    except RuntimeError,e:
        print "interrupted"

    except Exception,e:
        print e.message
    finally:
        r= p.close_session()

    return





def test_droydrunner_call_me():

    p =Pilot()

    p.setup_pilot("demo", "demo_qualif", phonehub_url)


    #user= 'Mobby'
    user= 'Marylin'

    try:

        res= p.open_Session(user)

        # read alarm on samples
        time.sleep(2)

        res= p.press_home(user)
        time.sleep(2)

        res= p.call(user,destination="+33155883628")

        time.sleep(5)

        res= p.hangup(user)

        time.sleep(3)

    except RuntimeError,e:
        print "interrupted"

    except Exception,e:
        print e.message
    finally:
        r= p.close_session()

    return


def test_droydrunner_phones():
    """


    :return:
    """

    #Mobby_number=   '+33784109762'
    #Marylin_number= '+33784109764'


    p =Pilot()
    p.setup_pilot("demo","demo_qualif",phonehub_url)

    try:

        res= p.open_Session('Mobby','Marylin')
        time.sleep(1)


        #res= p.ptf_get_user_data('Mobby')

        #res= p.wait_incoming_call('Marylin')
        #res= p.call_number('Mobby',Marylin_number)

        res= p.call_user('Mobby',userX='Marylin')
        #time.sleep(10)

        res= p.wait_incoming_call('Marylin')
        res= p.answer_call('Marylin')

        #res= p.wait_call_confirmed('Marylin')
        #res= p.wait_call_confirmed('Mobby')

        time.sleep(5)

        res= p.hangup('Mobby')

        time.sleep(3)

        #res= p.wait_call_disconnected('Marylin')
        #res= p.wait_call_disconnected('Mobby')


    except RuntimeError,e:
        print "interrupted: %s" % e.message

    finally:
        p.close_session()


#
#  dummy tvboxrunner
#



def test_tvbox_basic():

    p =Pilot()

    p.setup_pilot("demo", "demo_qualif", phonehub_url)

    user1= 'tv'

    try:

        res= p.open_Session(user1)


        #res= p.dummy(user1)

        # read alarm on samples
        time.sleep(2)

        res= p.serial_expect(user1,pattern= 'feedback/ardom')

        res = p.serial_synch(user1)
        #assert res.startswith('Signal')

        time.sleep(2)

        #res= p.ping(user1)
        #time.sleep(2)

        #res= p.read(user1)
        #assert res=='pong\n'

    except RuntimeError,e:
        print "interrupted"

    except Exception,e:
        print e.message
    finally:
        r= p.close_session()

    return


def test_tvstick_basic():

    p =Pilot()

    p.setup_pilot("demo", "demo_qualif", phonehub_url)

    user= 'stick'

    try:

        res= p.open_Session(user)


        #res= p.dummy(user)

        # read alarm on samples
        time.sleep(2)

        #res= p.serial_expect(user,pattern= 'feedback/ardom')

        res = p.serial_synch(user)
        #assert res.startswith('Signal')

        time.sleep(2)

        #res= p.ping(user1)
        #time.sleep(2)

        #res= p.read(user1)
        #assert res=='pong\n'

    except RuntimeError,e:
        print "interrupted"

    except Exception,e:
        print e.message
    finally:
        r= p.close_session()

    return









# def test_scenario():
#     """
#
#
#     :return:
#     """
#
#
#     p = Pilot()
#     p.setup_pilot("demo", "demo_qualif", phonehub_url)
#
#     res = p.open_Session('tv')
#     time.sleep(1)
#
#     logs = []
#
#     try:
#
#
#         #r = p.stb_send_dump('tv')
#
#         r= p.send_key('tv',key="KEY_CHANNELUP")
#         # time.sleep(1)
#         r=p.send_key('tv',"KEY_INFO")
#         # time.sleep(1)
#         r = p.stb_send_dump('tv')
#         time.sleep(1)
#         r = p.page_get_info_from_live_banner('tv')
#
#
#     except RuntimeError, e:
#         print "interrupted: %s" % e.message
#
#     finally:
#         p.close_session()
#         time.sleep(3)
#
#
#
# def test_first():
#     """
#
#     set([u'PGM_P', u'1', u'RIGHT', u'OK', u'POWER', u'PLAY_PAUSE', u'MENU', u'DOWN', u'2', u'6'])
#     :return:
#     """
#
#
#     p = Pilot()
#     p.setup_pilot("demo", "demo_qualif", phonehub_url)
#
#     res = p.open_Session('tv')
#     time.sleep(1)
#
#     logs = []
#
#     try:
#
#         # dump stb serial
#         #r = p.serial_synch('tv',timeout=1000)
#         #r = p.serial_watch('tv',timeout= 5)
#
#         # start stb scheduler
#         #r= p.scheduler_start('tv')
#
#
#         #
#         # select channel 1
#         #
#         r= p.send_key('tv',key="1")
#         r= p.serial_watch('tv',timeout=1)
#         #time.sleep(2)
#
#         r= p.stb_send_dump('tv')
#         r=p.serial_watch('tv',timeout=1)
#         #time.sleep(2)
#
#         #r= p.page_get_info_from_live_banner('tv')
#         # 'TF1'
#         #print r['channelName']
#         #print r['program']
#
#         r= p.serial_synch('tv')
#
#         # r=p.send_key('tv', key='PGM_P')
#         # time.sleep(1)
#
#         r = p.send_key('tv', key='2')
#         r=p.serial_watch('tv',timeout=1)
#         #time.sleep(5)
#
#         r= p.stb_send_dump('tv')
#         #time.sleep(2)
#         r=p.serial_watch('tv',timeout=2)
#
#         r= p.page_get_info_from_live_banner('tv')
#         # 'FRANCE 2'
#         print r['channelName']
#         print r['program']
#
#
#
#         # r = p.send_key('tv', key='PLAY_PAUSE')
#         # # time.sleep(300)
#         # r = p.send_key('tv', key='PLAY_PAUSE')
#         # # time.sleep(600)
#         # r = p.send_key('tv', key='MENU')
#         # # time.sleep(10)
#
#
#         # dump stb serial
#         r = p.serial_synch('tv')
#
#         # start stb scheduler
#         #stats = p.scheduler_read('tv')
#
#         # dump stb serial
#         #r = p.serial_synch('tv')
#
#
#
#     except RuntimeError, e:
#         print "interrupted: %s" % e.message
#
#     except Exception,e:
#         print  "interrupted: %s" % e.message
#
#     finally:
#         p.close_session()
#         time.sleep(3)
#
#
# def test_second():
#     """
#
#     set([u'PGM_P', u'1', u'RIGHT', u'OK', u'POWER', u'PLAY_PAUSE', u'MENU', u'DOWN', u'2', u'6'])
#     :return:
#     """
#
#
#     p = Pilot()
#     p.setup_pilot("demo", "demo_qualif", phonehub_url)
#
#     res = p.open_Session('tv')
#     time.sleep(1)
#
#     logs = []
#
#     try:
#
#         r = p.serial_watch('tv',timeout= 2)
#
#
#         # menu select
#         r= p.send_key('tv' , key="MENU")
#         r= p.serial_watch('tv',timeout=2)
#
#         # select musique/videos
#         r= p.menu_select('tv',path='4/1/1')
#         r= p.serial_watch('tv',timeout=2)
#
#         r= p.send_key('tv',key="OK")
#         r = p.serial_watch('tv', timeout=5)
#         r = p.send_key('tv', key="MENU")
#         r = p.send_key('tv', key="MENU")
#
#
#         #
#         # select channel 1
#         #
#         r= p.send_key('tv',key="1")
#         r= p.serial_watch('tv',timeout=2)
#
#
#         r= p.stb_send_dump('tv')
#         r=p.serial_watch('tv',timeout=2)
#
#
#         r= p.page_get_info_from_live_banner('tv')
#         # 'TF1'
#         print r['channelName']
#         print r['program']
#
#         r= p.serial_synch('tv')
#
#
#
#         # r = p.send_key('tv', key='PLAY_PAUSE')
#         # # time.sleep(300)
#         # r = p.send_key('tv', key='PLAY_PAUSE')
#         # # time.sleep(600)
#         # r = p.send_key('tv', key='MENU')
#         # # time.sleep(10)
#
#
#         # dump stb serial
#         r = p.serial_synch('tv')
#
#         # start stb scheduler
#         stats = p.scheduler_read('tv')
#
#         # dump stb serial
#         r = p.serial_synch('tv')
#
#
#
#     except RuntimeError, e:
#         print "interrupted: %s" % e.message
#
#     except Exception,e:
#         print  "interrupted: %s" % e.message
#
#     finally:
#         p.close_session()
#         time.sleep(3)


# def test_serial_expect():
#     """
#
#     set([u'PGM_P', u'1', u'RIGHT', u'OK', u'POWER', u'PLAY_PAUSE', u'MENU', u'DOWN', u'2', u'6'])
#     :return:
#     """
#
#
#     p = Pilot()
#     p.setup_pilot("demo", "demo_qualif", phonehub_url)
#
#     res = p.open_Session('tv')
#     time.sleep(1)
#
#     logs = []
#
#     try:
#
#         r = p.serial_watch('tv',timeout= 2)
#
#         r= p.send_key('tv',key='1')
#         time.sleep(2)
#         r = p.send_key('tv', key='3')
#         time.sleep(2)
#
#         r = p.send_key('tv', key='1')
#         time.sleep(2)
#         r = p.send_key('tv', key='3')
#         time.sleep(2)
#
#         r= p.send_key('tv',key='1')
#         time.sleep(2)
#         r = p.send_key('tv', key='3')
#         time.sleep(2)
#
#
#         r= p.send_key('tv',key='1')
#         time.sleep(2)
#
#         # u'root: [1;37m[DEBUG] app.controller> stb event [1] (count: 1)...[0m'
#         r= p.serial_expect('tv',pattern='app.controller> stb event [1]',timeout = 10)
#
#
#     except RuntimeError, e:
#         print "interrupted: %s" % e.message
#
#     except Exception,e:
#         print  "interrupted: %s" % e.message
#
#     finally:
#         p.close_session()
#         time.sleep(3)
#
#     return
#
#
#
# def test_stb_toolbox():
#     """
#
#     set([u'PGM_P', u'1', u'RIGHT', u'OK', u'POWER', u'PLAY_PAUSE', u'MENU', u'DOWN', u'2', u'6'])
#     :return:
#     """
#
#
#     p = Pilot()
#     p.setup_pilot("demo", "demo_qualif", phonehub_url)
#
#     res = p.open_Session('tv')
#     time.sleep(1)
#
#     logs = []
#
#     try:
#
#         r = p.serial_watch('tv',timeout= 2)
#
#         r= p.send_key('tv',key='1')
#         time.sleep(2)
#
#         r= p.send_key('tv',key='2')
#         time.sleep(2)
#
#
#         r = p.send_key('tv', key='OK')
#         time.sleep(2)
#
#
#         r = p.stb_send_dump('tv')
#         #r= p.page_get_info_from_live_banner('tv')
#
#         r= p.stb_toolbox_list('tv')
#         r= p.stb_toolbox_select('tv',text='langue',confirm=True)
#
#         time.sleep(3)
#         r=p.send_key('tv',key='KEY_BACK')
#
#         #r= p.page_get_list('tv')
#         #current= p.page_get_label_from_list_focus('tv')
#         #resume = p.page_find_in_list('tv',t=u'résumé')
#
#
#     except RuntimeError, e:
#         print "interrupted: %s" % e.message
#
#     except Exception,e:
#         print  "interrupted: %s" % e.message
#
#     finally:
#         p.close_session()
#         time.sleep(3)
#
#     return
#
#
# def test_scheduler():
#     """
#
#     set([u'PGM_P', u'1', u'RIGHT', u'OK', u'POWER', u'PLAY_PAUSE', u'MENU', u'DOWN', u'2', u'6'])
#     :return:
#     """
#
#
#     p = Pilot()
#     p.setup_pilot("demo", "demo_qualif", phonehub_url)
#
#     res = p.open_Session('tv')
#     time.sleep(1)
#
#     logs = []
#
#     try:
#
#         #r= p.stb_get_lineup('tv')
#
#
#         # start stb scheduler
#         r= p.scheduler_init('tv')
#
#
#         #r= p.stb_status('tv')
#
#         #
#         # select channel 1
#         #
#         #r= p.send_key('tv',key="2")
#         #r= p.serial_watch('tv',timeout=2)
#         #time.sleep(2)
#
#         #r= p.stb_send_dump('tv')
#         #r=p.serial_watch('tv',timeout=2)
#         #time.sleep(2)
#
#         #r= p.page_get_info_from_live_banner('tv')
#         # 'TF1'
#         #print r['channelName']
#         #print r['program']
#
#         #r= p.serial_synch('tv')
#         time.sleep(20)
#
#         # dump stb serial
#         r = p.serial_synch('tv')
#
#         # start stb scheduler
#         #stats = p.scheduler_read('tv')
#         r= p.scheduler_sync('tv')
#
#         # dump stb serial
#         r = p.serial_synch('tv')
#         time.sleep(15)
#
#
#         r= p.scheduler_close('tv')
#         time.sleep(15)
#
#
#     except RuntimeError, e:
#         print "interrupted: %s" % e.message
#
#     except Exception,e:
#         print  "interrupted: %s" % e.message
#
#     finally:
#         p.close_session()
#         time.sleep(3)
#
#
# def test_get_lineup():
#     """
#
#     set([u'PGM_P', u'1', u'RIGHT', u'OK', u'POWER', u'PLAY_PAUSE', u'MENU', u'DOWN', u'2', u'6'])
#     :return:
#     """
#
#
#     p = Pilot()
#     p.setup_pilot("demo", "demo_qualif", phonehub_url)
#
#     res = p.open_Session('tv')
#     time.sleep(1)
#
#     logs = []
#
#     try:
#
#         # dump stb serial
#         r = p.serial_synch('tv')
#
#         r= p.stb_get_lineup('tv')
#
#         # dump stb serial
#         r = p.serial_synch('tv')
#
#
#         time.sleep(15)
#
#     except RuntimeError, e:
#         print "interrupted: %s" % e.message
#
#     except Exception,e:
#         print  "interrupted: %s" % e.message
#
#     finally:
#         p.close_session()
#         time.sleep(3)
#
#
# def test_stb_info():
#     """
#
#     set([u'PGM_P', u'1', u'RIGHT', u'OK', u'POWER', u'PLAY_PAUSE', u'MENU', u'DOWN', u'2', u'6'])
#     :return:
#     """
#
#
#     p = Pilot()
#     p.setup_pilot("demo", "demo_qualif", phonehub_url)
#
#
#     try:
#
#         res = p.open_Session('tv')
#         time.sleep(1)
#
#         logs = []
#
#         # dump stb serial
#         r = p.serial_synch('tv')
#
#         r= p.stb_info('tv')
#
#         # dump stb serial
#         r = p.serial_synch('tv')
#
#
#         time.sleep(15)
#
#     except RuntimeError, e:
#         print "interrupted: %s" % e.message
#
#     except Exception,e:
#         print  "interrupted: %s" % e.message
#
#     finally:
#         p.close_session()
#         time.sleep(3)



if __name__=="__main__":


    test_fetch_restop_interface()
    #test_upload_platform()

    #test_basic_appstick()

    #test_tvbox_basic()
    #test_tvstick_basic()

    test_samples_basic()



    test_droydrunner_adb_devices()
    test_droydrunner_call_me()
    test_droydrunner_phones()






    print "Done."
