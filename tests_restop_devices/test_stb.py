__author__ = 'cocoon'

import time

import os
from restop_client import Pilot


#phonehub_url= "http://localhost:5000/restop/api/v1"
#phonehub_url= "http://192.168.99.100/restop/api/v1"
#phonehub_url= "http://10.179.5.174/restop/api/v1"
phonehub_url= "http://192.168.1.21/restop/api/v1"


def test_fetch_restop_interface():
    """



    :return:
    """
    # to test localy
    #phonehub_url = "http://localhost:5001/restop/api/v1"
    #os.environ['NO_PROXY'] = 'localhost'

    p= Pilot()

    data= p.fetch_restop_interface(url=phonehub_url)

    return data

def test_tvbox_scheduler():

    p =Pilot()

    p.setup_pilot("demo", "demo_qualif", phonehub_url)

    user1= 'tv'

    try:

        res= p.open_Session(user1)

        res = p.serial_watch(user1, timeout=10)
        #res = p.serial_synch(user1)

        res= p.scheduler_init(user1)

        res = p.serial_watch(user1, timeout=10)

        #time.sleep(11)
        res= p.scheduler_sync(user1)
        res = p.serial_watch(user1, timeout=10)
        #time.sleep(2)

        #res = p.serial_synch(user1)
        #time.sleep(11)

        res= p.scheduler_close(user1)

        #res = p.serial_synch(user1)

        res= p.serial_watch(user1,timeout=10)


    except RuntimeError as e:
        print "interrupted"

    except Exception as e:
        print(e.message)
    finally:
        r= p.close_session()

    return




#
#  dummy tvboxrunner
#


def test_tvbox_basic():

    p =Pilot()

    p.setup_pilot("demo", "demo_qualif", phonehub_url)

    user1= 'tv'

    try:

        res= p.open_Session(user1)


        #res= p.dummy(user1)

        # read alarm on samples
        time.sleep(2)

        #res= p.serial_expect(user1,pattern= 'feedback_ardom')

        #res = p.serial_synch(user1)
        #assert res.startswith('Signal')

        res= p.send_key(user1,key="1")
        time.sleep(5)

        res= p.send_key(user1,key="2")
        time.sleep(2)


        #res= p.stb_send_dump(user1)

        #res= p.serial_watch(user1, timeout=10)

        tv_program = p.page_get_info_from_live_banner(user1)
        print(tv_program)


        res= p.scheduler_init(user1)

        time.sleep(6)

        res= p.scheduler_sync(user1)

        time.sleep(2)

        res = p.serial_synch(user1)

        res= p.scheduler_close(user1)

        res = p.serial_synch(user1)

        res= p.serial_watch(user1,timeout=10)


    except RuntimeError as e:
        print("interrupted")

    except Exception as e:
        print(e.message)
    finally:
        r= p.close_session()

    return



def test_tvbox_ardom_menu():

    p =Pilot()

    p.setup_pilot("demo", "demo_qualif", phonehub_url)

    user1= 'tv'

    try:

        res= p.open_Session(user1)

        res = p.serial_synch(user1)
        # assert res.startswith('Signal')

        res = p.send_key(user1, key="MENU")

        time.sleep(2)
        res = p.serial_synch(user1)

        res = p.stb_send_dump(user1)

        res = p.serial_watch(user1, timeout=10)

        res= p.stb_toolbox_select(user1,text=u'mes videos',confirm= False)
        #res= p.page_action_desk_move_to(user1,text='mes videos')
        time.sleep(5)
        res = p.serial_synch(user1)

        res = p.send_key(user1, key="MENU")

        res= p.serial_watch(user1,timeout=10)




    except RuntimeError as  e:
        print("interrupted")

    except Exception as  e:
        print(e.message)
    finally:
        r = p.close_session()

    return





def test_tvbox_powerswitch():

    p =Pilot()

    p.setup_pilot("demo", "demo_qualif", phonehub_url)

    user1= 'tv'

    try:

        res= p.open_Session(user1)

        res = p.serial_synch(user1)
        # assert res.startswith('Signal')

        res= p.power_on( user1)


        time.sleep(10)

        res= p.power_off(user1)


        time.sleep(5)

    except RuntimeError as  e:
        print("interrupted")

    except Exception as e:
        print(e.message)
    finally:
        r = p.close_session()

    return


def test_polaris():

    p =Pilot()

    p.setup_pilot("demo", "demo_qualif", phonehub_url)

    user1= 'stbplay'

    try:

        res= p.open_Session(user1)


        #res= p.dummy(user1)

        # read alarm on samples
        time.sleep(2)

        #res= p.serial_expect(user1,pattern= 'feedback_ardom')

        #res = p.serial_synch(user1)
        #assert res.startswith('Signal')


        res = p.send_key(user1, key='KEY_BACK')

        res= p.send_key(user1,key="5")
        time.sleep(5)

        res= p.send_key(user1,key="7")
        time.sleep(2)

        res= p.send_key(user1,key='KEY_INFO')
        time.sleep(2)
        #res= p.stb_send_dump(user1)
        #content= p.stb_fetch_dump(user1)

        #res= p.serial_watch(user1, timeout=10)

        tv_program = p.stb_channel_info(user1)
        print(tv_program)
        time.sleep(2)
        res = p.send_key(user1, key='KEY_BACK')



    except RuntimeError as e:
        print("interrupted")

    except Exception as e:
        print(e.message)
    finally:
        r= p.close_session()
    return



def test_phenix_ardom_menu():

    p =Pilot()

    p.setup_pilot("demo", "demo_qualif", phonehub_url)

    user1= 'stbphenix'

    try:

        res= p.open_Session(user1)

        res = p.serial_synch(user1)
        # assert res.startswith('Signal')

        res = p.send_key(user1, key="MENU")

        time.sleep(2)
        res = p.serial_synch(user1)

        res = p.stb_send_dump(user1,mode="1")
        res = p.serial_watch(user1, timeout=10)

        #tv_program = p.stb_channel_info(user1)
        #print(tv_program)

        #res= p.stb_toolbox_select(user1,text=u'mes videos',confirm= False)
        #res= p.page_action_desk_move_to(user1,text='mes videos')
        time.sleep(5)
        res = p.serial_synch(user1)

        res = p.send_key(user1, key="MENU")

        res= p.serial_watch(user1,timeout=10)




    except RuntimeError as  e:
        print("interrupted")

    except Exception as  e:
        print(e.message)
    finally:
        r = p.close_session()

    return




if __name__=="__main__":


    test_fetch_restop_interface()

    #test_polaris()
    #test_tvbox_scheduler()

    #test_tvbox_basic()

    #test_tvbox_ardom_menu()

    #test_tvbox_powerswitch()


    test_phenix_ardom_menu()

    print("Done")
