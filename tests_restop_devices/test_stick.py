__author__ = 'cocoon'

import time

import os
from restop_client import Pilot



phonehub_url= "http://localhost:5000/restop/api/v1"
#phonehub_url= "http://192.168.99.100/restop/api/v1"
#phonehub_url= "http://10.179.5.174/restop/api/v1"



def test_fetch_restop_interface():
    """



    :return:
    """
    # to test localy
    #phonehub_url = "http://localhost:5001/restop/api/v1"
    #os.environ['NO_PROXY'] = 'localhost'

    p= Pilot()

    data= p.fetch_restop_interface(url=phonehub_url)

    return data





#
#  dummy tvboxrunner
#


def test_tvbox_basic():

    p =Pilot()

    p.setup_pilot("demo", "demo_qualif", phonehub_url)

    user1= 'stick'

    try:

        res= p.open_Session(user1)

        # read alarm on samples
        time.sleep(2)


        res= p.scheduler_init(user1)

        time.sleep(6)

        res = p.serial_synch(user1)
        res= p.scheduler_sync(user1)

        time.sleep(6)

        res = p.serial_synch(user1)

        res= p.scheduler_close(user1)
        time.sleep(15)

        res = p.serial_synch(user1)

        res= p.serial_watch(user1,timeout=10)


    except RuntimeError,e:
        print "interrupted"

    except Exception,e:
        print e.message
    finally:
        r= p.close_session()

    return




if __name__=="__main__":


    test_fetch_restop_interface()
    test_tvbox_basic()




    print "Done"