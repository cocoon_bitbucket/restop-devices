_author__ = 'cocoon'

import time

import os
from restop_client import Pilot


phonehub_url= "http://localhost:5000/restop/api/v1"
#phonehub_url= "http://192.168.99.100/restop/api/v1"
#phonehub_url= "http://10.179.5.174/restop/api/v1"
#phonehub_url= "http://192.168.1.21/restop/api/v1"


user1= "alice"
user2= "bob"


def test_fetch_restop_interface():
    """



    :return:
    """
    # to test localy
    #phonehub_url = "http://localhost:5001/restop/api/v1"
    #os.environ['NO_PROXY'] = 'localhost'

    p= Pilot()

    data= p.fetch_restop_interface(url=phonehub_url)

    return data

def test_ping():

    p =Pilot()

    p.setup_pilot("demo", "demo_qualif", phonehub_url)


    try:

        res= p.open_Session(user1,user2)

        time.sleep(5)


        #res= p.ping(user1)
        #res= p.ping(user2)

        r = p.send( "alice",cmd= "d")
        r= p.expect("alice",pattern= "Dump complete")

        #r= p.failed("alice")
        #r = p.activate_redirection_to_user("bob", userB="alice", redirect_kind="CFB", format="universal")
        #r=  p.cancel_redirection("bob",redirect_kind="CFB")

        #r = p.call_feature_access_code("bob", fac="+CFA")
        #r= p.call_feature_access_code("bob",fac="+CFA",userX="alice",format="ext")

        #r= p.call_destination("alice",destination="wrong_Extension")


        r= p.call_user("alice",userX="bob",format="ext")
        # r= p.call("alice",destination="sip:1002@192.168.1.51")


        #time.sleep(4)

        r= p.wait_incoming_call("bob",timeout=20)
        r= p.answer_call("bob",code=200,timeout=60)

        r= p.check_call("alice")

        r= p.get_last_received_request("bob", sip_method= "INVITE")
        r= p.get_last_received_sip_request("bob",sip_method= "INVITE")

        r= p.check_incoming_call("bob",display_name="Extension 1001")


        #r= p.transfer_to_user("bob",userX="alice",format="universal")
        #r = p.transfer_to_destination("bob", destination="oms_service")


        #oms_service

        print("wait 5s ...")
        time.sleep(5)




    except RuntimeError as e:
        print("interrupted")

    except Exception as e:
        print(e.message)
    finally:
        r= p.close_session()

    return



if __name__=="__main__":


    test_fetch_restop_interface()

    test_ping()


    print("Done")
