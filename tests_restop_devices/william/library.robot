*** Settings ***
Documentation     Library for test
...

# libraries
Library    restop_client
Library    Collections
Library    String
#Library    HttpLibrary.HTTP


*** Keywords ***
Change to channel
    [Arguments]   ${device}     ${channel}        ${wait_before_verify} 	${channel_number} 	${ignore_error}		${stay_on_channel}
    Log to console  change to channel ${channel}
    serial synch  ${device}
    send_key  ${device}  key=${channel}
    builtin.sleep  ${wait_before_verify}
    stb_send_dump  ${device}
    serial synch  ${device}
    ${info}=  page_get_info_from_live_banner  ${device}
    ${channelInfo}=  Get From Dictionary 	${info} 	lcn
    ${channelNumberString}=  Convert to String  ${channelInfo}
    ${returnStatus} 	${value} = 	Run Keyword And Ignore Error 	Should Be Equal    ${channelNumberString}    ${channel_number}
    Run Keyword If  '${returnStatus}' == 'PASS'  Log to console  Touche ${channel} => ${channel_number} : OK  ELSE  Log to console  Touche ${channel} FAIL: ${channelInfo}
    Run Keyword If  '${returnStatus}' == 'PASS' or ${ignore_error} == True  Log to console  Attente ${stay_on_channel} secondes  ELSE  Fail  Error on Change to channel ${channel_number}
    Run Keyword If  '${returnStatus}' == 'PASS' or ${ignore_error} == True  builtin.sleep  ${stay_on_channel}
    [Return]    ${returnStatus}

Send key and check context
    [Arguments]   ${device}     ${key}        ${wait_before_verify} 	${status} 	${ignore_error}		${stay_on_channel}
    Log to console  send key ${key}
    serial synch  ${device}
    send_key  ${device}  key=${key}
    builtin.sleep  ${wait_before_verify}
    serial synch  ${device}
    ${statusInfo}=  stb status  ${device}
    ${channelInfo}=  Get From Dictionary 	${statusInfo} 	osdContext
    ${returnStatus} 	${value} = 	Run Keyword And Ignore Error 	Should Be Equal    ${channelInfo}    ${status}
    Run Keyword If  '${returnStatus}' == 'PASS'  Log to console  Touche ${key} => ${status} : OK  ELSE  Log to console  Touche ${key} FAIL: ${channelInfo}
    Run Keyword If  '${returnStatus}' == 'PASS' or ${ignore_error} == True  Log to console  Attente ${stay_on_channel} secondes  ELSE  Fail  Error on Send key and check context ${status}
    Run Keyword If  '${returnStatus}' == 'PASS' or ${ignore_error} == True  builtin.sleep  ${stay_on_channel}
    [Return]    ${returnStatus}

Get status and check
	[Arguments]   ${device}     ${status}        ${value_to_check}  ${ignore_error}
	${statusInfo}=  stb status  ${device}
    ${value}=  Get From Dictionary 	${statusInfo} 	${value_to_check}
	${returnStatus} 	${result} = 	Run Keyword And Ignore Error 	Should Be Equal    ${value}    ${status}
	Run Keyword If  '${returnStatus}' == 'PASS'  Log to console  status ${status} => ${value} : OK  ELSE  Log to console  status ${status} FAIL: ${value}
    Run Keyword If  '${returnStatus}' != 'PASS' and ${ignore_error} == False  Fail  Error on Get status and check ${status}
 	[Return]    ${returnStatus}

#Change to channel play
#    [Arguments]   ${device}     ${channel}        ${wait_before_verify} 	${channel_number} 	${ignore_error}		${stay_on_channel}
#    Log to console  change to channel ${channel}
#    serial synch  ${device}
#    send_key  ${device}  key=${channel}
#    builtin.sleep  ${wait_before_verify}
#    stb_send_dump  ${device}
#    Create Http Context 	host=192.168.1.24:8080  scheme=http
#    GET  /remoteControl/cmd?operation=01&key=401&mode=1
#    builtin.sleep  5
#    GET  /remoteControl/cmd?operation=01&key=401&mode=2
#    builtin.sleep  10
#    serial synch  ${device}
#    ${info}=  page_get_info_from_live_banner  ${device}
#    ${channelInfo}=  Get From Dictionary 	${info} 	lcn
#    ${channelNumberString}=  Convert to String  ${channelInfo}
#    ${returnStatus} 	${value} = 	Run Keyword And Ignore Error 	Should Be Equal    ${channelNumberString}    ${channel_number}
#    Run Keyword If  '${returnStatus}' == 'PASS'  Log to console  Touche ${channel} => ${channel_number} : OK  ELSE  Log to console  Touche ${channel} FAIL: ${channelInfo}
#    Run Keyword If  '${returnStatus}' == 'PASS' or ${ignore_error} == True  Log to console  Attente ${stay_on_channel} secondes  ELSE  Fail  Error on Change to channel ${channel_number}
#    Run Keyword If  '${returnStatus}' == 'PASS' or ${ignore_error} == True  builtin.sleep  ${stay_on_channel}
#    [Return]    ${returnStatus}
