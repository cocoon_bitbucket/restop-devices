*** Settings ***
Documentation     standard tests
...

# libraries
Library    restop_client
Library    Collections
Library    String
Resource   library.robot


#Suite Setup  init suite
Suite Setup    fetch restop interface  ${platform_url}
Test Setup     setup pilot     ${platform_name}    ${platform_version}  ${platform_url}
Test Teardown  close session


*** Variables ***
${platform_name}=   demo
${platform_version}=    demo_qualif

${platform_url}=  http://192.168.1.21/restop/api/v1

${pilot_mode}=  normal



*** Keywords ***
########
#init suite
#	# check platform capablities with test capablities requirements
#	setup pilot 	${platform_name} 	${platform_version}  ${platform_url}
#
#init pilot
#	setup pilot 	${platform_name} 	${platform_version}  ${platform_url}
#	run keyword if   '${pilot_mode}'=='dry'  set pilot dry mode
#
#shutdown pilot
#	close session


############### units and macros



Unit first
    [Arguments] 	${tv}
    [Documentation]  Parcours STB

    Open session	${tv}

    serial synch  ${tv}

    scheduler init  ${tv}

    Log to console  \nDebut du test

    #LOOP
    : FOR    ${INDEX}    IN RANGE    1    3
    \    #Power
    \    Send key and check context  device=${tv}  key=POWER  wait_before_verify=120  status=HOMEPAGE  ignore_error=True  stay_on_channel=0
    \
    \    # P+ (2 mins)
    \    ${returnStatus}=  Send key and check context  device=${tv}  key=KEY_CHANNELUP  wait_before_verify=120  status=LIVE  ignore_error=True  stay_on_channel=0
    \    Run Keyword If  '${returnStatus}' == 'PASS'  Exit For Loop  ELSE  Log to console  can't start live try again
    \    Run Keyword If  '${INDEX}' == 3  Fail  error on starting STB
    Log to console  Live ok

    #TF1 (10 mins)
    change to channel  device=${tv}  channel=1  wait_before_verify=2  channel_number=1  ignore_error=False  stay_on_channel=600

    # FR2 via P+ (10 mins)
    change to channel  device=${tv}  channel=KEY_CHANNELUP  wait_before_verify=2  channel_number=2  ignore_error=False  stay_on_channel=600

    scheduler sync  ${tv}

    # Timeshift
    serial synch  ${tv}
    Log to console  Timeshift pendant 5 mins
    send_key  ${tv}  key=PLAY_PAUSE
    builtin.sleep  2
    ${status}=  stb status  ${tv}
    Log to console  ${status}
    ${ts_on}=  Get From Dictionary 	${status} 	timeShiftingState
    ${play_state}=  Get From Dictionary 	${status} 	playedMediaState
    Should Be Equal  '${play_state}'  'PAUSE'
    Should Be Equal  '${ts_on}'  '1'
    Log to console  Timeshift pause pour 5min ...
    builtin.sleep  300

    scheduler sync  ${tv}

    #play timeshift
    Log to console  Lecture différé pendant 10min
    send_key  ${tv}  key=PLAY_PAUSE
    builtin.sleep  2
    ${status}=  stb status  ${tv}
    Log to console  ${status}
    ${ts_on}=  Get From Dictionary 	${status} 	timeShiftingState
    ${play_state}=  Get From Dictionary 	${status} 	playedMediaState
    Should Be Equal  '${play_state}'  'PLAY'
    Should Be Equal  '${ts_on}'  '1'
    Log to console  Lecture différé en cours pour 10min ...
    builtin.sleep  600

    scheduler sync  ${tv}

    # Menu and exit timeshift
    serial synch  ${tv}
    Log to console  => Menu
    send_key  ${tv}  key=MENU
    builtin.sleep  1
    send_key  ${tv}  key=OK
    builtin.sleep  1
    Get status and check  device=${tv}  status=HOMEPAGE  value_to_check=osdContext  ignore_error=False
    Log to console  Menu : OK

    #Menu Vod
    serial synch  ${tv}
    Log to console  => Menu VOD
    menu_select  ${tv}  path=1/0/0
    send_key  ${tv}  key=OK
    builtin.sleep  1
    Get status and check  device=${tv}  status=VOD  value_to_check=osdContext  ignore_error=False
    Log to console  Menu VOD : OK

    scheduler sync  ${tv}

    #Menu Vod : Browser
    serial synch  ${tv}
    Log to console  => Menu VOD Browse
    menu_select  ${tv}  path=2/0/0
    send_key  ${tv}  key=OK
    builtin.sleep  1

    #open jaquette
    serial synch  ${tv}
    Log to console  open first video info VOD
    send_key  ${tv}  key=OK
    builtin.sleep  1
#    #pour aller jusqu'à la limite du démarrage
#    Log to console  start VOD video
#    send_key  ${tv}  key=OK
#    #check if a popup price appear
#    ${status}  ${result}=  Run Keyword And Ignore Error  serial expect  ${tv}  pattern=NxClient_P_GetDisplayStatus  timeout=15
#    Log to console  ${result}
#    serial synch  ${tv}
#    builtin.sleep  5
#    #if valid menu appear valid it
#    Run Keyword If  ${status} == FAIL  Log to console  valid price VOD
#    Run Keyword If  ${status} == PASS  send_key  ${tv}  key=OK
#    builtin.sleep  10

    scheduler sync  ${tv}

    # Menu
    Send key and check context  device=${tv}  key=MENU  wait_before_verify=2  status=HOMEPAGE  ignore_error=False  stay_on_channel=0

    # P+
    Send key and check context  device=${tv}  key=KEY_CHANNELUP  wait_before_verify=2  status=LIVE  ignore_error=False  stay_on_channel=0

    # TF1 (10 mins)
    change to channel  device=${tv}  channel=1  wait_before_verify=2  channel_number=1  ignore_error=False  stay_on_channel=600

    scheduler sync  ${tv}

    #Replay list
    serial synch  ${tv}
    Log to console  => replay
    send_key  ${tv}  key=OK
    builtin.sleep  1
    stb_send_dump  ${tv}
    builtin.sleep  2
    ${list}=  stb_toolbox_list  ${tv}
    Log to console  ${list}
    # open replay context
    stb_toolbox_select  ${tv}  text=à la demande  confirm=True
    builtin.sleep  2
    #check if Replay menu
    Get status and check  device=${tv}  status=isf  value_to_check=osdContext  ignore_error=False
    Log to console  à la une ok : OK

    scheduler sync  ${tv}

    # select a la une DOWN ok
    serial synch  ${tv}
    Log to console  => à la une
    send_key  ${tv}  key=DOWN
    builtin.sleep  1
    send_key  ${tv}  key=OK
    builtin.sleep  1
    Log to console  à la une ok : OK

    # episode dans episode groupe DOWN, OK
    serial synch  ${tv}
    Log to console  => episode
    send_key  ${tv}  key=DOWN
    builtin.sleep  1
    send_key  ${tv}  key=OK
    builtin.sleep  1
    Log to console  select episode: OK

    scheduler sync  ${tv}

    # lancer lecture 10 min du film
    serial synch  ${tv}
    Log to console  => start episode
    send_key  ${tv}  key=OK
    builtin.sleep  1
    Get status and check  device=${tv}  status=AdvPlayer  value_to_check=osdContext  ignore_error=False
    Log to console  start episode for 10min: OK
    builtin.sleep  600

    scheduler sync  ${tv}

    # quitter le replay
    # Menu
    Send key and check context  device=${tv}  key=MENU  wait_before_verify=2  status=HOMEPAGE  ignore_error=False  stay_on_channel=0

    scheduler sync  ${tv}

    # P+ (2 mins)
    Send key and check context  device=${tv}  key=KEY_CHANNELUP  wait_before_verify=2  status=LIVE  ignore_error=False  stay_on_channel=120

    #ouvrir M6 puis un episode et lire 10min
    # M6
    change to channel  device=${tv}  channel=6  wait_before_verify=2  channel_number=6  ignore_error=False  stay_on_channel=600

    scheduler sync  ${tv}

    #Replay menu
    serial synch  ${tv}
    Log to console  => open list
    send_key  ${tv}  key=OK
    builtin.sleep  1
    Log to console  => open replay
    stb_send_dump  ${tv}
    builtin.sleep  2
    ${list}=  stb_toolbox_list  ${tv}
    Log to console  ${list}
    # open replay context
    stb_toolbox_select  ${tv}  text=à la demande  confirm=True
    builtin.sleep  2
    #check if Replay menu
    Get status and check  device=${tv}  status=6PLAY  value_to_check=osdContext  ignore_error=False
    serial synch  ${tv}
    Log to console  replay : OK

    scheduler sync  ${tv}

    #right /down / ok / ok
    serial synch  ${tv}
    Log to console  => serie
    send_key  ${tv}  key=RIGHT
    builtin.sleep  1
    send_key  ${tv}  key=OK
    builtin.sleep  1
    Log to console  => select video replay
    send_key  ${tv}  key=DOWN
    builtin.sleep  1
    send_key  ${tv}  key=OK
    builtin.sleep  1
    Log to console  => start video replay
    send_key  ${tv}  key=OK
    builtin.sleep  1
    #check if Replay menu
    Get status and check  device=${tv}  status=6PLAY  value_to_check=osdContext  ignore_error=False
    serial synch  ${tv}
    Log to console  start replay video for 10min: OK
    builtin.sleep  600

    scheduler sync  ${tv}

    # quitter le replay
    # Menu
    Send key and check context  device=${tv}  key=MENU  wait_before_verify=2  status=HOMEPAGE  ignore_error=False  stay_on_channel=0

    # P+ (2 mins)
    Send key and check context  device=${tv}  key=KEY_CHANNELUP  wait_before_verify=2  status=LIVE  ignore_error=False  stay_on_channel=120

    # ouvrir FR2 pluzz puis 1 episode pendant 10min
    # FR2
    change to channel  device=${tv}  channel=2  wait_before_verify=2  channel_number=2  ignore_error=False  stay_on_channel=600

    scheduler sync  ${tv}

    # Replay menu
    serial synch  ${tv}
    Log to console  => open list
    send_key  ${tv}  key=OK
    builtin.sleep  1
    Log to console  => open replay
    stb_send_dump  ${tv}
    builtin.sleep  2
    ${list}=  stb_toolbox_list  ${tv}
    Log to console  ${list}
    # open replay context
    stb_toolbox_select  ${tv}  text=à la demande  confirm=True
    builtin.sleep  2
    #check if Replay menu
    Get status and check  device=${tv}  status=isf  value_to_check=osdContext  ignore_error=False
    serial synch  ${tv}
    Log to console  replay : OK

    scheduler sync  ${tv}

    #right / ok / ok
    serial synch  ${tv}
    Log to console  => first video
    send_key  ${tv}  key=RIGHT
    builtin.sleep  2
    send_key  ${tv}  key=OK
    builtin.sleep  2
    Log to console  => start video replay
    send_key  ${tv}  key=OK
    builtin.sleep  5
    #check if Replay menu
    Get status and check  device=${tv}  status=AdvPlayer  value_to_check=osdContext  ignore_error=False
    serial synch  ${tv}
    Log to console  start replay video for 10min: OK
    builtin.sleep  600

    scheduler sync  ${tv}

    # quitter le replay
    # Menu
    Send key and check context  device=${tv}  key=MENU  wait_before_verify=2  status=HOMEPAGE  ignore_error=False  stay_on_channel=0

    # P+
    Send key and check context  device=${tv}  key=KEY_CHANNELUP  wait_before_verify=2  status=LIVE  ignore_error=False  stay_on_channel=120

    #Retour sur le live pendant 15min
    Log to console  Live for 15min
    builtin.sleep  900

    scheduler sync  ${tv}

    #ouvrir le guide, Ce soir, Navigation puis choisir 20 chaine plus bas
    # Menu
    Send key and check context  device=${tv}  key=MENU  wait_before_verify=2  status=HOMEPAGE  ignore_error=False  stay_on_channel=0

    scheduler sync  ${tv}

    # Programme TV
    serial synch  ${tv}
    Log to console  => Menu Programme TV
    menu_select  ${tv}  path=0/1/0
    builtin.sleep  1
    send_key  ${tv}  key=OK
    builtin.sleep  1
    Get status and check  device=${tv}  status=EPG  value_to_check=osdContext  ignore_error=False
    Log to console  Menu EPG : OK

    #navigate in ce soir
    #ok then 20 down
    #TODO:voir si on peut vérifier si on a changé de focus
    serial synch  ${tv}
    Log to console  => open ce soir
    send_key  ${tv}  key=OK
    builtin.sleep  1
    #LOOP
    : FOR    ${INDEX}    IN RANGE    1    20
    \   send_key  ${tv}  key=DOWN
    \   builtin.sleep  2
    serial synch  ${tv}
    Log to console  navigation ce soir EPG: OK

    scheduler sync  ${tv}

    #Menu
    Send key and check context  device=${tv}  key=MENU  wait_before_verify=2  status=HOMEPAGE  ignore_error=False  stay_on_channel=0

    # enregistreur TV
    serial synch  ${tv}
    Log to console  => Menu enregistreur TV
    menu_select  ${tv}  path=0/2/1
    builtin.sleep  1
    send_key  ${tv}  key=OK
    builtin.sleep  5
    Get status and check  device=${tv}  status=PVR  value_to_check=osdContext  ignore_error=False
    Log to console  Menu PVR : OK

    scheduler sync  ${tv}

    #ouvrir le premier enregistrement pendant 20min
    serial synch  ${tv}
    Log to console  => first video
    send_key  ${tv}  key=OK
    builtin.sleep  1
    send_key  ${tv}  key=OK
    builtin.sleep  1
    Log to console  => start first video
    send_key  ${tv}  key=OK
    builtin.sleep  5
    ${status}=  stb status  ${tv}
    Log to console  ${status}
    #check if Replay menu
    ${playedMediaType}=  Get From Dictionary 	${status} 	playedMediaType
    Should Be Equal    ${playedMediaType}    PVR
    ${playedMediaState}=  Get From Dictionary 	${status} 	playedMediaState
    Should Be Equal    ${playedMediaState}    PLAY
    serial synch  ${tv}
    Log to console  start video for 20min: OK
    builtin.sleep  1200

    scheduler sync  ${tv}

    #retour au menu, au live puis mise en veille
    # Menu
    Send key and check context  device=${tv}  key=MENU  wait_before_verify=2  status=HOMEPAGE  ignore_error=False  stay_on_channel=0

    scheduler sync  ${tv}

    # P+ (2 mins)
    Send key and check context  device=${tv}  key=KEY_CHANNELUP  wait_before_verify=2  status=LIVE  ignore_error=False  stay_on_channel=120

    #Power
    Log to console  Touche Power
    Send key and check context  device=${tv}  key=POWER  wait_before_verify=2  status=MAIN_PROCESS  ignore_error=False  stay_on_channel=0

    scheduler sync  ${tv}

End test
    [Arguments] 	${tv}
    [Documentation]  fin parcours STB

    scheduler close  ${tv}
    serial synch  ${tv}
    close session

*** Test Cases ***


first
  Unit first  stbplay
  [Teardown]  End test  stbplay