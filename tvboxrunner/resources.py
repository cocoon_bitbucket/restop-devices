
__author__ = 'cocoon'

import io
from restop.application import ApplicationError
from models import DEVICE_NAME
#from models import *
from agent import Agent
from restop.plugins.resources import RootResource, Resource
#from restop.tools import ScanMethods

from restop_platform.journal import Journal
from restop_platform.resources import AgentResource
from restop_components.stats_scheduler import StatSchedulerClient

class Resource_Root(RootResource):
    """

    """
    collection= 'root'


class DeviceResource(AgentResource):
    """
        TVBOXRUNNER : the Orange Set Top Box

    """
    collection= DEVICE_NAME
    protected= ''

    _AgentClass= Agent


    def configure_session(self,session_id,members,configuration):
        """
        :param session_id:
        :param members:
        :param configuration:
        :return:
        """
        return configuration


    def op_feedback_ardom(self,item,**kwargs):
        """
            reserved keyword used by the stb to send ardom
        """
        """

            call back for ardom

            the stb performs a curl POST on

            https://<internal_ip>/restop/upload/tvbox_agents/<device>/feedback_ardom

            the master redirects here with

            localhost:5005/restop/api/v1/tvbox_agents/<device>/feedback_ardom

        :return:
        """
        database=self.model.database
        journal= Journal(database)
        journal.write("hit feedback_ardom url")
        device=item
        filename = "ardom-%s.html" % device

        if self.request.method== 'POST':
            journal.write("receive an ArDom for device: %s -> %s" % (device, filename))
            if self.request.content_type == 'application/json':
                data = self.request.json
            else:
                data = self.request.get_data()
            journal.write("ArDom length=%d" % len(data))

            with io.open(filename, 'w', encoding='utf8') as f:
                f.write(data)

            # with open(filename, 'w') as fh:
            #     fh.write(data)
            journal.write('ardom file written')
            return ""
        else:
            journal.write("ask for ardom content")
            content= 'ardom file empty'
            try:
                with open(filename,'rb') as fh:
                    content= fh.read()
            except Exception as e:
                content= "cannot read file %s" % filename

            return content

    def op_feedback_scheduler(self,device='TV'):
        """
            reserved keyword used by the stb to send stats result
        """
        """
           call back for stats scheduler

            the stb performs a curl POST on

            https://<internal_ip>/restop/upload/tvbox_agents/<device>/feedback_scheduler

            the master redirects here with

            localhost:5005/restop/api/v1/tvbox_agents/<device>/feedback_feedback_schduler

        """
        database = self.model.database
        journal = Journal(database)
        journal.write("hit feedback_scheduler url")
        print("receive stats for device %s" % device)
        if self.request.content_type == 'application/json':
            data= self.request.json
        else:
            data = self.request.get_data()
        filename = "./stats-%s.txt" % device
        journal.write("stats length: %d , file is %s" % (len(data), filename))
        with open(filename, 'w') as fh:
            fh.write(data)
        data = data.split('\n')
        #scheduler = StatSchedulerClient.push_to_graphite(data, device=device, configfile='collectors.ini')
        #return ""
        if data:
            journal.write('feedback_scheduler: send data to graphite (len: %d)' % len(data))
            try:
                #scheduler= StatSchedulerClient('dummy')
                nb_metrics = StatSchedulerClient.push_to_graphite(data, device=device, configfile='collectors.ini')
            except Exception as e:
                journal.write('error on feedback_scheduler,%s' % str(e))
                nb_metrics = 0
            journal.write("number of metrics sent to graphite: %d" % nb_metrics)
        else:
            journal.write("feedback_scheduler: no data to push to graphite")
        return "OK"