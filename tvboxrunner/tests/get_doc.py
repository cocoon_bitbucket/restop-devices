

from tvboxrunner.resources import DeviceResource, DEVICE_NAME


doc= DeviceResource._autodoc()
data={'operations':[],'collection_operations':[],'collections':[DEVICE_NAME]}
for operation in doc['item_methods'].keys():
    op = operation.replace('op_','')
    data['operations'].append(op)
for operation in doc['collection_methods'].keys():
    op = operation.replace('op_col_','')
    data['collection_operations'].append(op)



