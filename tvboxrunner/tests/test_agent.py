
import time
from yaml import load
from copy import deepcopy

import requests

from wbackend.model import Database,Model
from restop.application import ApplicationError
from restop_platform.models import Platform

from tvboxrunner.models import TvboxAgents as AgentModel

from tvboxrunner.agent import Agent

platform_file= './platform.yml'
agent_name = 'tv'


def get_new_db():
    """

    :return:
    """
    db = Database(host='localhost', port=6379, db=0)
    db.flushdb()
    Model.bind(database=db,namespace=None)

    try:
        p= Platform.get(Platform.name=='default')
    except Exception,e:
        # does not exists create it
        db.flushdb()
        #stream = file('../../tests/platform.yml')
        stream = file(platform_file)
        data = load(stream)
        p = Platform.create(name='default', data=data)
        #p.setup()

    return db

def get_model(agent_name):

    db= get_new_db()

    platform = Platform.get(Platform.name == 'default')
    agent_parameters= platform.data['devices'][agent_name]
    agent_profile= agent_parameters['profile']
    parameters= deepcopy(platform.data['profiles'][agent_profile])
    parameters.update(agent_parameters)

    tv = AgentModel.create(name='tv', session=1, parameters= parameters)
    tv_id= tv.get_id()
    tv = AgentModel.load(tv_id)

    return tv



#
#
#

def test_remote():
    """

        test remote without remote_server: expect requests timeout exception

    :return:
    """
    agent_name = 'tv'

    tv= get_model(agent_name)

    database= tv.database

    agent = Agent(tv)


    try:
        r= agent.send_key('1')
    except requests.exceptions.ConnectTimeout:
        pass
    except requests.exceptions.ConnectionError:
        pass
    except Exception, e:
        print e.message
    try:
        r= agent.stb_send_dump()
    except requests.exceptions.ConnectTimeout:
        pass
    except requests.exceptions.ConnectionError:
        pass
    except Exception, e:
        print e.message

    try:
        r= agent.stb_zap('tv1')
    except requests.exceptions.ConnectTimeout:
        pass
    except requests.exceptions.ConnectionError:
        pass
    except Exception ,e:
        print e.message

    try:
        r= agent.stb_status()
    except requests.exceptions.ConnectTimeout:
        pass
    except requests.exceptions.ConnectionError:
        pass
    except Exception, e:
        print e.message


    return

def test_page():
    """

        test remote without remote_server: expect requests timeout exception

    :return:
    """
    agent_name = 'tv'

    tv= get_model(agent_name)

    database= tv.database

    agent = Agent(tv)


    try:
        r= agent.page
    except Exception , e:
        pass



    return






def test_serial_loop():
    """

        parameters= {
            "profile": "tvbox",
            "serial_port": "loop://?logging=debug",
            "collection": "tvbox_agents",
            "peer_port": "8080",
            "peer_address": "192.168.1.20",
            "id": "1234"
                            }

    :return:
    """
    agent_name = 'tv'

    tv= get_model(agent_name)

    database= tv.database

    # db= get_new_db()
    #
    # # print db.connection_pool.connection_kwargs['host']
    # # print db.connection_pool.connection_kwargs['port']
    # # print db.connection_pool.connection_kwargs['db']
    #
    # platform = Platform.get(Platform.name == 'default')
    # agent_parameters= platform.data['devices'][agent_name]
    # agent_profile= agent_parameters['profile']
    # parameters= deepcopy(platform.data['profiles'][agent_profile])
    # parameters.update(agent_parameters)
    #
    # tv = AgentModel.create(name='tv', session=1, parameters= parameters)
    # tv_id= tv.get_id()
    # tv = AgentModel.load(tv_id)


    agent= Agent(tv)

    # force serial port to loop://?logging=debug
    parameters= agent.model.parameters
    parameters['serial_port'] = 'loop://?logging=debug'
    agent.model.parameters=parameters
    #agent.model.parameters['serial_port']= 'loop://?logging=debug'
    agent.model.save()

    response= agent.start()


    #log_offset= len(agent.model.logs)
    rc= agent.serial_watch(timeout=5)
    response = agent.model.make_response(rc)
    assert "feedback/ardom" in response['logs'][-2]

    rc= agent.scheduler_init()
    time.sleep(6)
    rc= agent.scheduler_sync()
    response = agent.model.make_response(rc)

    rc= agent.scheduler_close()
    response = agent.model.make_response(rc)

    rc= agent.send_cmd("hello world")

    #log_offset = len(agent.model.logs)
    rc= agent.serial_synch(timeout=2)
    response = agent.model.make_response(rc)

    #log_offset = len(agent.model.logs)
    rc = agent.serial_synch(timeout=2)
    response = agent.model.make_response(rc)

    #log_offset = len(agent.model.logs)
    #rc = agent.serial_synch(timeout=2)
    #response = agent.model.make_response(rc)



    response= agent.stop()

    time.sleep(3)

    return


def test_stb_interface():

    agent_name = 'tv'

    tv= get_model(agent_name)

    database= tv.database


    agent= Agent(tv)

    # force serial port to loop://?logging=debug
    parameters= agent.model.parameters
    parameters['serial_port'] = 'loop://?logging=debug'
    agent.model.parameters=parameters
    #agent.model.parameters['serial_port']= 'loop://?logging=debug'
    agent.model.save()

    response= agent.start()


    #
    # test stb_get_lineup
    #
    rc= agent.stb_get_lineup()
    time.sleep(2)
    rc= agent.serial_synch(timeout=5)
    response = agent.model.make_response(rc)
    assert 'curl -i -X POST' in response['logs'][-4]

    #
    # pcb_cli
    #
    command='AdditionalSoftwareVersion'
    rc = agent.stb_pcb_cli(command)

    time.sleep(2)
    rc = agent.serial_synch(timeout=5)
    response = agent.model.make_response(rc)
    assert 'pcb_cli "DeviceInfo.AdditionalSoftwareVersion.?"' in response['logs'][-4]


    try:
        rc = agent.stb_info()
    except ApplicationError:
        pass

    time.sleep(2)
    rc = agent.serial_synch(timeout=5)
    response = agent.model.make_response(rc)
    #assert '' in response['logs'][-4]

    response= agent.stop()

    time.sleep(3)


if __name__=="__main__":


    #
    #test_page()
    #test_remote()
    #test_serial_loop()

    test_stb_interface()

    print "Done."
