#!/usr/bin/env python


# import os
# from robot import libdoc
# import logging
#
#
#
# class_pattern='''
# class Dummy(object):
#     """
#         %s
#     """
#
# '''
#
# method_pattern='''
#     def %s(self, %s **kwargs):
#         """
#         %s
#         """
# '''
#
# class DocGenerator(object):
#     """
#
#
#
#     """
#     filetemp= 'Dummy.py'
#
#     def __init__(self,name,device_resource,version='1.0'):
#         """
#
#
#         :param name:
#         :param device_resource:
#         """
#         self.name=name
#         self.device_resource=device_resource
#         self.version=version
#         self.data= self.device_resource._autodoc()
#         self.outfile= "%s.html" % name
#
#     def setup(self):
#         """
#
#
#         :return:
#         """
#
#         lines= []
#         lines.extend((class_pattern % self.data['doc']).split('\n'))
#
#         for operation, data in self.collection_operations():
#             op_lines = method_pattern % (operation, '', self.docstring_lines(data['docstring']))
#             lines.extend(op_lines.split('\n'))
#
#         for operation,data in self.operations():
#             op_lines= method_pattern %  (operation,'alias,',self.docstring_lines(data['docstring']))
#             lines.extend(op_lines.split('\n'))
#
#         self.lines= lines
#
#         # write Dummy.py file
#         with open(self.filetemp, "wb") as fout:
#             fout.write("\n".join(lines))
#
#         # generate html doc
#         current_dir = os.path.dirname(os.path.abspath(__file__))
#         os.environ['PYTHONPATH'] = current_dir
#         libdoc.libdoc_cli(['--version', self.version, '--name', self.name, self.filetemp, self.outfile])
#
#         return lines
#
#     def collection_operations(self):
#         """
#
#
#         :return:
#         """
#         for operation in sorted(self.data['collection_methods'].keys()):
#             op = operation.replace('op_col', '')
#             yield op,self.data['collection_methods'][operation]
#
#     def operations(self):
#         """
#
#
#         :return:
#         """
#         for operation in sorted(self.data['item_methods'].keys()):
#             op = operation.replace('op_', '')
#             yield op,self.data['item_methods'][operation]
#
#     def docstring_lines(self,data,indent= 8 ):
#         """
#
#         :param data:
#         :param indent:
#         :return:
#         """
#         lines=[]
#         if data:
#             for line in data.split('\n'):
#                 lines.append(" " * indent + line)
#         return "\n".join(lines)




# def gen_doc(name):
#     """
#
#     :param name: string eg Mobile Tvbox
#     :return:
#     """
#     spec_file= "./data/interface_spec_%s.py" % name
#     local_file= "%s.py" % name
#     html_file= "%s.html" % name
#
#
#     origin_mobile= os.path.realpath(os.path.join(current_dir,spec_file))
#     here_mobile= os.path.join(current_dir,local_file)
#
#     # copy original mobile file to here
#     with open (origin_mobile,"rb") as fin:
#         with open(here_mobile,"wb") as fout:
#             fout.write(fin.read())
#
#     # gen doc
#     source = os.path.join(current_dir,"../v-restop/bin/activate")
#     #gen_cmd = "python -m robot.libdoc Mobile.py Mobile.html"
#
#
#     r = commands.getstatusoutput("source %s && python -m robot.libdoc %s %s" % (source,local_file,html_file))
#
#     print r
#
#     return

if __name__=="__main__":

    from restop_apidoc import DocGenerator
    from tvboxrunner.resources import DeviceResource

    import logging
    logging.basicConfig(level=logging.DEBUG)


    doc= DocGenerator('TvboxRunner',DeviceResource,version="1.1")

    lines= doc.setup()
    #print "\n".join(lines)


    # # get local api doc and send it to master for update
    # doc= DeviceResource._autodoc()
    #
    # gen_doc('Mobile')
    # gen_doc('Tvbox')



print "Done"