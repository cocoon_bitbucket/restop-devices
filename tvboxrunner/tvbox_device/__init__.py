__version__='0.1.4'


"""

0.1.4
======

fix  Issue #9
 ------------

stb_toolbox select



0.1.3
=====

fix: Issue #7
-------------

create /flash/Resources/resources/ directory on stb prior to config call back



0.1.2
=====

 fix: Issue #6
 -------------

new keywords to handle toolbox menu:

*  stb_toolbox_list(device)
*  stb_toolbox_select( device, text ,confirm=True )




"""

