

# class BaseAgent(object):
#     """
#
#         tvbox agent base class
#         -----------
#
#         an agent to drive the orange tv set top box
#
#
#
#     """
#     collection= 'tvbox_agents'
#
#
#     def __init__(self,agent_id,backend):
#         """
#
#             get device data from platform backend
#
#             create The Remote Contoler and set it up
#
#         :param agent_id: id of the tv
#         :param backend:
#         :return:
#         """
#         self.agent_id=agent_id
#         self.backend=backend
#         self.state={}
#
#         self.setup()
#
#     #
#     # interface with backup for device and agent data
#     #
#     def _get_agent(self,agent_id):
#         """ get agent_data
#
#             sample:
#                 {
#                     "category": "tvbox_agents",
#                     "parameters": {
#                         "device_id": "tv"
#                     },
#                     "session_id": "1",
#                     "lock_name": "device_id",
#                     "alias": "tv",
#                     "agent_indice": 1
#                 }
#
#
#         :param agent_id: str eg 1 ,2
#         :return:
#         """
#         key= self.backend.item_key(self.collection,agent_id)
#         data=self.backend.item_get_by_key(key)
#         return data
#
#     def _get_device(self,alias,platform=1):
#         """ get device data
#
#             sample:
#                 {
#                     "profile": "tvbox",
#                     "collection": "tvbox_agents",
#                     "proxy": "192.168.1.50:5060",
#                     "peer_address": "192.168.1.12",
#                     "media_dir": "/usr/share/sounds/syprunner",
#                     "peer_port": "8080",
#                     "id": "1234",
#                     "serial_port": "/dev/cu.usbserial-FTVE89ZND"
#                 }
#
#
#         :param alias:
#         :return:
#         """
#         key= 'platforms:%s:devices:%s' % (str(platform),alias)
#         data= self.backend.item_get_by_key(key)
#         return data
#
#
#     def setup(self):
#         """
#             create
#                 a remote: remote controller fot sending ir keys
#                 a serial: serial interface with stb
#         :return:
#         """
#         self.agent_data= self._get_agent(self.agent_id)
#         self.alias= self.agent_data['alias']
#         self.device_data= self._get_device(self.alias)
#
#         remote_control_url= '%s:%s' % (self.device_data['peer_address'],self.device_data['peer_port'])
#
#         return
#
#
#     def start(self):
#         """
#             start agent
#
#         :return:
#         """
#         return True
#
#
#     def stop(self):
#         """
#
#         :return:
#         """
#         return True
#
#     #
#     #   public interface to RemoteRpi  (NewTvTesting.RemoteControl.RpiRemoteControl
#     #
#
#     def send_key(self,key,tempo=None): raise NotImplementedError
#     def send_keys(self,keys): raise NotImplementedError
#     def zap(self,channel): raise NotImplementedError
#     def send_word(self,word): raise NotImplementedError
#     def send_date_hour_min(self,date): raise NotImplementedError
#     def hard_reset(self): raise NotImplementedError
#     def get_stb_status(self): raise NotImplementedError
#     def get_data_model_value(self,key): raise NotImplementedError
#     def set_data_model_value(self,key,value): raise NotImplementedError
#
#
#     # public interface for page: NewTvTesting.ResidentAppPage.WebKitPage
#     # -------------------------
#
#     def page_action_select(self,t,partial=False): raise NotImplementedError
#     def page_action_desk_select(self,**kwargs): raise NotImplementedError
#     def page_find_in_Dialog_box(self,**kwargs): raise NotImplementedError
#     def page_get_info_from_live_banner(self,**kwargs): raise NotImplementedError
#     def page_action_select_in_live_banner(self,**kwargs): raise NotImplementedError
#     def page_get_info_from_epg_focus(self,**kwargs): raise NotImplementedError
#     def page_find_in_page(self,**kwargs): raise NotImplementedError
#     def page_get_info_from_mosaic_focus(self,**kwargs): raise NotImplementedError
#
#
#     # public interface to wording  NewTvTesting.Config.Wording
#     def wording_get(self,tag): raise NotImplementedError
#
#     # interface to TV data
#     def tv_data_get(self,tag): raise NotImplementedError
#
#     # interface to VOD Data
#
#
#
#
# if __name__ == "__main__":
#
#     import time
#     from plugable_backend.backends import get_backend
#
#     Backend= get_backend()
#     bk= Backend("")
#
#     r= bk.item_new('tvbox_agents',1, {'alias':'tv'})
#     r= bk.item_new('platforms:1:devices','tv', { 'peer_address':'localhost','peer_port':5000})
#
#     # root: [DEBUG] app.controller> stb event [VOD] (count: 1)...
#
#     # at open session
#     tv= BaseAgent(1,backend=bk)
#
#
#     # sample usage
#     #'''zapping p+'''
#     tv.send_key(key="KEY_CHANNELUP")
#     time.sleep(15)
#     tv.send_key("KEY_INFO")
#
#     pg= tv.page_get_info_from_live_banner()
#
#     #pg.display()
#
#     tv.send_key("KEY_GUIDE")
#     #self.assertTrue(self.page.actionSelect(Wording.W['epgWeek']))
#
#
#     #epgWeek= tv.wording_get('epgweek')
#     epgWeek = 'maintenant'
#     r= tv.page_action_select(epgWeek)
#
#     assert r == True
