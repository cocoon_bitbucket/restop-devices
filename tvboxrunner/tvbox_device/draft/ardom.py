# -*- coding: utf-8 -*-

import re
from bs4 import BeautifulSoup

#from devices.tvbox_device.ntvt_interface import ProgramInfoItem

import logging
log= logging.getLogger(__name__)



class ListItem(object):

    def __init__(self,text, selected=False, active=True):
        self.text = text
        self.selected = selected
        self.active = active


class ArDomPage(object):
    """


    """
    def __init__(self,data):
        """


        :param data:
        """
        self.soup= BeautifulSoup(data, 'html.parser')
        self.title= self.soup.title.text

    @classmethod
    def from_file(cls,filename="dump.htlm"):
        """

        :param finename:
        :return:
        """
        html_doc= file(filename,'rb').read()
        page= cls(html_doc)
        return page

    def prettify(self):
        """

        :param self:
        :return:
        """

        return self.soup.prettify()

    def findInPage(self, txt, regex=False):

        found = False

        body= self.soup.body
        if regex:
            found = re.search(re.compile(txt), body.text) is not None
        else:
            found= body.text.find(txt) > -1
        return found



class ScreenView(object):
    """



    """
    root_class= ''
    _items= {}

    def __init__(self, soup):
        """

        """
        self._ready=False
        self._status= 'empty'
        self.root = self.find_root(soup)
        self._ready=True
        self._data={}


    @classmethod
    def find_root(cls, soup):
        """

        :return:
        """
        log.debug('search root: <? class="%s ...">' % cls.root_class)
        f = lambda tag: tag.has_attr('class') and tag['class'][0] == cls.root_class
        live_scene = soup.body.find_all(f)
        root= cls._first(live_scene)
        log.debug('found  root: %s' % root)
        return root

    @classmethod
    def _first(cls, result_set, unique=True):
        """

        :param result_set:
        :return:
        """
        assert len(result_set) > 0, "not found"
        if unique:
            assert len(result_set) == 1, "not unique"
        return result_set[0]


    def get_child(self, name):
        """

        :param name:
        :return:
        """
        label = self._items[name]
        log.debug('    search child  <? class="%s ...">' % label)
        f = lambda tag: tag.has_attr('class') and tag['class'][0] == label
        q = self.root.find_all(f)
        child= self._first(q)
        log.debug('    found  child  %s' % child)
        return child



if __name__=="__main__":


    logging.basicConfig(level=logging.DEBUG)

    page= ArDomPage.from_file('dump_sample.html')
    print page.title
    assert page.title == "Orange TV - New UI"


    found= page.findInPage('Multicam')
    assert found

    found= page.findInPage('no such text')
    assert not found

    found= page.findInPage('Multicam',regex=True)
    assert found

    found= page.findInPage('no such text',regex=True)
    assert not found


    print "Done."

