# -*- coding: utf-8 -*-

"""


    def __init__(self, lcn, channelName, program, start, end, length, genre, favorite, ongoingRec, nextProgram, pictoCsa, pictoDefinition, pictoRatio, pictoAudioDesc, pictoContentVersion, pictoDts, pictoDolby, pictoEar, picto3D=None, pictoVO=None, TS_start=None, TS_width=None, TS_cursor=None):
        self.lcn = lcn
        self.channelName = channelName
        self.favorite = favorite
        self.ongoingRec = ongoingRec
        self.program = program
        self.start = start
        self.end = end
        self.length = length
        self.genre = genre
        self.nextProgram = nextProgram
        self.logger = logging.getLogger('NewTvTesting.ProgramInfoItem')
        self.pictoCsa = pictoCsa
        self.pictoDefinition = pictoDefinition
        self.pictoRatio = pictoRatio
        self.pictoAudioDesc = pictoAudioDesc
        self.pictoContentVersion = pictoContentVersion
        self.pictoDts = pictoDts
        self.pictoDolby = pictoDolby
        self.pictoEar = pictoEar
        self.picto3D = picto3D
        self.pictoVO = pictoVO
        self.TS_width = TS_width
        self.TS_start = TS_start
        self.TS_cursor = TS_cursor




"""

#from BeautifulSoup import BeautifulSoup
from bs4 import BeautifulSoup

from slugify import slugify
import re
from ardom import ArDomPage,ScreenView,ListItem

#from devices.tvbox_device.ntvt_interface import ProgramInfoItem

import logging
log= logging.getLogger(__name__)


html_file= "dump_sample.html"



class LiveScene(ScreenView):
    """

    """
    root_class= "programInformation"
    _items= {
        # 'channel': "nameOfChannel",
        # 'clock': "clock",
        # 'start':"start",
        # 'end': "end",
        # 'program':"program",
        # "kind":"kind",
        # "on_demand": "onDemandText",
        # "next": "next",
        # "next_time": "nextTime",

        'lcn': '',
        'channelName': "nameOfChannel",
        'favorite' : 'favorite',
        'ongoingRec': '',
        'program': 'program',
        'start': 'start',
        'end': 'end',
        'length': '',
        'genre': 'kind',
        'nextProgram': 'next',
        #self.logger = logging.getLogger('NewTvTesting.ProgramInfoItem')
        'pictoCsa': 'pictoCsa',
        'pictoDefinition': 'pictoHD',
        'pictoRatio': 'picto16_9',
        'pictoAudioDesc':'pictoAD',
        'pictoContentVersion':'pictoVM',
        'pictoDts': 'pictoDTS',
        'pictoDolby': 'pictoDolby',
        'pictoEar': 'pictoEar',
        'picto3D': '',
        'pictoVO': '',
        'TS_width': 'scrollerBar',
        'TS_start': 'scrollerBar',
        'TS_cursor': 'tsCursor',


    }


    @classmethod
    def find_root(cls,soup):
        """

        :return:
        """
        log.debug('search root:  <? class="live scene ...">')
        f = lambda tag: tag.has_attr('class') and tag['class'][0] == "live" and tag['class'][1] == "scene"
        live_scene = soup.body.find_all(f)
        root= cls._first(live_scene)
        log.debug('found  root:  %s' % root)
        return root


    def feed(self):
        """


        :return:
        """
        for name in ['channelName','program','start','end','genre','nextProgram']:
            tag= self.get_child(name)
            self._data[name]= tag.text

        for name in ['pictoAudioDesc','pictoContentVersion','pictoDolby','pictoDts','pictoEar','favorite']:
            tag = self.get_child(name)
            #print tag
            if 'hidden' in tag['class']:
                self._data[name] = False
            else:
                self._data[name]= True

        for name in ['pictoDefinition','pictoRatio']:
            try:
                tag = self.get_child(name)
                # print tag
                if 'hidden' in tag['class']:
                    self._data[name] = False
                else:
                    self._data[name] = True
            except:
                self._data[name]=False


        for name in ['pictoCsa']:
            tag = self.get_child(name)
            print tag
            self._data[name]= tag['class'][1]

        for name in ['TS_cursor']:
            tag = self.get_child(name)
            # class ="tsCursor" style="left: 66%;" >
            style= tag['style']
            if style:
                r= re.compile('(\d+)')
                match= r.search(style)
                if match:
                    value= match.group(1)
                    self._data[name] = int(value)

        tag= self.get_child('TS_start')
        #<div class ="scrollerBar" style="left: 0%; width: 0%;" >
        style = tag['style']
        if style:
            r = re.compile('(\d+).*?(\d+)')
            match = r.search(style)
            if match:
                left = match.group(1)
                width= match.group(2)
                self._data['TS_start'] = int(left)
                self._data['TS_width'] = int(width)



        return



class ToolBoxLive(ScreenView):
    """

    """
    root_class = 'ToolBoxLive'
    item_list_class= "listItem"



    def list_items(self):
        """

        :return:
        """
        f= lambda tag: tag.has_attr('class') and tag['class'][0] == self.item_list_class
        q= self.root.find_all(f)
        return q

    def list_active_items(self):
        """

        :return:
        """
        items= self.list_items()
        self.active = [ tag for tag in items if not 'separator' in tag['class']]
        return self.active


    def feed(self):
        """


        :return:
        """
        #self.menu= self.list_active_items()
        self.menu=[]
        self.get_items()

        self._ready= True


    def get_active_index(self):
        """

        :return:
        """
        if self._ready:
            for index,tag in enumerate(self.list_active_items()):
                if 'highlight' in tag['class']:
                    return index

    def get_index_of(self,t,regex=False):
        """

        :param t:
        :param regex:
        :return:
        """
        if self._ready:
            for index,tag in enumerate(self.list_active_items()):
                t= slugify(t,to_lower=True)
                text= slugify(tag.text,to_lower=True)
                if t  in text:
                    return index
        return -1

    def get_delta_index_for(self,t ,regex=False):

        active= self.get_active_index()
        found= self.get_index_of(t,regex=regex)
        if found == -1:
            return None
        else:
            delta= found - active
        return delta


    def get_items(self):
        """

        :return:
        """
        self.activeItems=[]
        elements= self.list_active_items()

        indexActive= 0
        for index, element in enumerate(elements):
            if 'highlight' in element['class']:
                self.highlight = index
                self.activeHighlight = indexActive

            if 'selected' in element['class'] or 'checked' in element['class']:
                selected = True
            else:
                selected = False

            if 'inactive' in element['class']:
                active = False
            else:
                active = True
                self.activeItems.append(ListItem(element.text, selected, active))
                indexActive = indexActive +1


            self.menu.append(ListItem(element.text, selected, active))
        return True


    #
    # find in list
    #
    def findInList(self, t, regex = False):
        """

        :param t:
        :param regex:
        :return:
        """
        found = True
        r= self.get_index_of(t)
        if r == -1:
            found = False
        return found


    def getList(self):
        """

        :return:
        """
        # if self._loadPageList():
        #     return self.items
        # else:
        #     return None
        return self.menu

    def getLabelFromListFocus(self):
        if self._ready:
            return self.getList()[self.highlight]
        else:
            return None


    def actionSelect(self,t,partial=False):

        #self.logger.info("  >>   selecting text >" + t + "<")
        #self._loadPageList()
        find = False
        index = 0

        # if partial is True:
        #     while not t in self.activeItems[index].text and index < (len(self.activeItems)-1):
        #         index = index+1
        #
        #     if t in self.activeItems[index].text:
        #         find = True
        # else:
        #     while self.activeItems[index].text!=t and index < (len(self.activeItems)-1):
        #         index = index+1
        #
        #     if self.activeItems[index].text==t:
        #         find = True
        #
        # if find:
        #     gap = index - self.activeHighlight
        #
        #     if gap >= 0:
        #         keyGap = "KEY_DOWN"
        #     else:
        #         keyGap = "KEY_UP"
        #
        #     self.remote.sendKeys([keyGap] * abs(gap))
        #     self.remote.sendKey("KEY_OK")
        #
        #     return True
        #
        # else:
        #     if len(self.driver.find_elements_by_css_selector(".ToolBoxLive"))>0:
        #         maxItems = ConfAR.MAX_ITEMS_TOOLBOX
        #     else:
        #         maxItems = ConfAR.MAX_ITEMS
        #
        #     if len(self.items) == maxItems:
        #
        #         end = False
        #         endText=self.activeItems[-1].text
        #
        #         while not end:
        #
        #             self.remote.sendKeys(["KEY_UP"] * int(self.activeHighlight + 1))
        #
        #             self._loadPageList()
        #             find = False
        #             index = 0
        #
        #             if partial is True:
        #                 while not t in self.activeItems[index].text and index < (len(self.activeItems)-1):
        #                     index=index+1
        #                     if self.activeItems[index].text == endText:
        #                         end = True
        #
        #                 if t in self.activeItems[index].text:
        #                     find = True
        #                     end = True
        #             else:
        #                 while self.activeItems[index].text!=t and index < (len(self.activeItems)-1):
        #                     index=index+1
        #                     if self.activeItems[index].text == endText:
        #                         end = True
        #
        #                 if self.activeItems[index].text==t:
        #                     find = True
        #                     end = True
        #
        #         if find:
        #
        #             gap = index - self.activeHighlight
        #
        #             if gap >= 0:
        #                 keyGap = "KEY_DOWN"
        #             else:
        #                 keyGap = "KEY_UP"
        #
        #             self.remote.sendKeys([keyGap] * abs(gap))
        #             self.remote.sendKey("KEY_OK")
        #
        #             return True
        #
        #         else:
        #             self._detect_timeout()
        #             return False
        #
        #     else:
        #         self._detect_timeout()
        #         return False



logging.basicConfig(level=logging.DEBUG)


page= ArDomPage.from_file(html_file)

print page.prettify()



body=page.soup.body


# html_doc= file(html_file,'rb').read()
#
#
# soup = BeautifulSoup(html_doc, 'html.parser')
#
# print soup.title
#
# print(soup.prettify())


# with open('beautiful_sample.html',"w") as fh:
#     text= soup.prettify()
#     text= text.encode(encoding='utf-8')
#     fh.write(text)


# # select all id
# head= soup.head
# body=soup.body
#
# # for string in soup.body.stripped_strings:
# #     print(repr(string))
#
#
#
# # <div class="listItem highlight" id="live.tlbx.onDemand">&gt; Multicam</div>
#
# #ids= soup.p['id']
# results= soup.find_all(has_id)
# classes= soup.find_all(has_class)
#
# for tag in classes:
#     print tag['class']
#
#
#
# results= soup.find_all(has_ondemand_id)
# results= soup.find_all(has_live_id)
#
#
# #<div class="live scene">
#
# live_scene= soup.body.find_all(catch_live_scene)
# assert len(live_scene) == 1
# live_scene=live_scene[0]
#
# catch_channel_name = live_scene.find_all(catch_channel_name)
# #channel_name= soup.body.find_all(catch_channel_name)


l= LiveScene(page.soup)


print l.get_child('start').text
print l.get_child('end').text
print l.get_child('genre').text


l.feed()




t= ToolBoxLive(page.soup)

items= t.list_items()

# active = [ tag for tag in items if not 'separator' in tag['class']]
#
# for e in active:
#     for s in e.strings:
#         print s

t.feed()
print t.get_active_index()

delta= t.get_delta_index_for('> Multicam')
assert delta == 0

delta= t.get_delta_index_for('no such item')
assert delta == None

delta= t.get_delta_index_for(u'à la demande')
assert delta == 1

delta= t.get_delta_index_for(u'résumé')
assert delta == 2

delta= t.get_delta_index_for(u'chaînes  (non)')
assert delta == 3

delta= t.get_delta_index_for(u'langue (français)')
assert delta == 4

delta= t.get_delta_index_for(u'langue')
assert delta == 4

delta= t.get_delta_index_for(u'français')
assert delta == 4


labels = [
    '> Multicam',
    u'à la demande',
    u'résumé',
    u'chaînes  (non)',
    u'langue (français)'
    ]

for l in labels:
    print slugify(l)


items= t.getList()

r= t.getLabelFromListFocus()

r= t.findInList('résumé')
assert r is True

r= t.findInList('dummy')
assert r is False




print "Done"