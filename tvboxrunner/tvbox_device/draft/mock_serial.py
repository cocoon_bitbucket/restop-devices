


from serial import Serial
import serial
import posix


class MyDriver(object):
    """


    """
    def __init__(self,peer):
        """

        :param port:
        :return:
        """
        self.peer=peer

        self.port= None



    def start(self):
        """

        :return:
        """
        raise NotImplementedError

    def get_data_from_serial(self):
        """

        :return:
        """
        raise NotImplementedError


if __name__=="__main__":



    def best_solution():


        # loop://[?logging={debug|info|warning|error}]

        fake_device= "loop://?logging=debug"

        #ser2= serial.serial_for_url("loop://[?logging={debug|info|warning|error}]")
        ser2= serial.serial_for_url(fake_device)

        ser2.write("this is my sample\n")

        line= ser2.readline()

        assert line == "this is my sample\n"

        return



    def solution_2():

        import os, pty, serial

        master, slave = pty.openpty()
        s_name = os.ttyname(slave)

        ser = serial.Serial(s_name)

        # To Write to the device
        ser.write('Your text')

        # To read from the device
        os.read(master,1000)

        return




    def solution1():
        """

        """
        # see: http://stackoverflow.com/questions/2500420/fake-serial-communication-under-linux

        # driver = MyDriver()  # what I want to test
        # peer = serial.Serial()
        # driver.port.fd, peer.fd = posix.openpty()
        # driver.port._reconfigurePort()
        # peer.setTimeout(timeout=0.1)
        # peer._reconfigurePort()
        # driver.start()
        #
        # # peer.write("something")
        # # driver.get_data_from_serial()




    # starts here
    best_solution()

    print "Done."