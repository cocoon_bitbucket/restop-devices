#!/usr/bin/python
import subprocess

"""

    list serial ports with pyserial

    # list serial ports
    python -m serial.tools.list_ports


    # start a terminal
    python -m serial.tools.miniterm


    # list usb devices from shell
    ls -ltr /dev/*usb*


"""



class SerialSession(object):
    """

        start session:   screen -S STB -d -RR /dev/ttyUSB0 115200

        screen -d -r -S STB -X eval "log off"
        screen -d -r -S STB -X eval "logfile /tmp/mylog.log"
        screen -d -r -S STB -X eval "log on"


    """
    def __init__(self,session_name='STB'):
        """


        :return:
        """
        self.session_name= session_name
        subprocess.call(["screen", "-S",session_name, "-d", "-RR", "/dev/ttyUSB0", "115200"])


    def _send_command(self,command):
        """

        :param command:
        :return:
        """
        subprocess.call(["screen", "-S", self.session_name, "-r", "-d" ,"-X", "eval", 'command' ])


    def log_off(self):
        return self._send_command("log off")

    def log_on(self):
        return self._send_command('log on')

    def set_logfile(self,filename="/tmp/logfile.log"):
        return self._send_command("logfile %s" % filename)

    def quit(self):
        raise NotImplementedError



#subprocess.call(["screen", "-S", "session", "-X", "stuff", "'command here'`echo -ne '\015'`"])