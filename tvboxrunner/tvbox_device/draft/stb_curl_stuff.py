"""

    tools to handle hettp transfers between stb and restop app


    workspaces
    ----------

    /workspace/platform-1/tv/lineup
    /workspace/platform-1/agent-2/scheduler-1.txt




    sample curl commands

    curl -i -X POST -F data=@/tmp/trace-scheduler.txt https://192.168.1.21/agent-1/scheduler-1.txt

    curl -i -X POST -F data=@/flash/Resources/lineup/lineup.json https://192.168.1.21/platforms/1/lineup




"""

import os

#from plugable_backend.workspace_manager import Workspace


# filenmare , url
curl_pattern= "curl -i -X POST -F data=@%s %s"




stb_resources ={
    'lineup': '/flash/Resources/lineup/lineup.json'

}


class HttpFeedBack(object):
    """



    """

    def __init__(self,station_url,workspace):
        """

        :param station_url: str test station bas url: eg https://192.168.1.21
        :param workspace: str eg /workspace/platform-1
        """
        self.station_url= station_url
        self.workspace= workspace


    def curl_command(self,filename,uri):
        """


        :param filename: str eg
        :param uri:
        :return:
        """
        url= os.path.join(self.station_url,uri)
        cmd= curl_pattern % (filename,url)
        return cmd


    def send_command(self,adapter,filename,uri):
        """
            send command to a client adapter

        :param adapter: instance of adapter
        :return:
        """
        cmd= self.curl_command(filename,uri)
        r= adapter.send(cmd)
        return r






if __name__=="__main__":




    fb= HttpFeedBack('https://192.168.1.21','platform-1')

    c= fb.curl_command('/flash/Resources/lineup/lineup/json','platforms/1/lineup')


    print "Done."





