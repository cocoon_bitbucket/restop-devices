import time
from qadapters.busybox_stats import TraceDriver,TraceParser
from qadapters.statscheduler import StatScheduler ,StatSchedulerClient,ThreadedAdapter, ClientAdapter


def test_basic():

    # init trace driver
    driver= TraceDriver('/tmp/trace')

    lines = driver.gen_start_trace()
    driver.run_local(lines)

    # list process
    lines = driver.gen_command_lines('ps', 'ps')
    driver.run_local(lines)

    # perform average load ( does not work on mac)
    lines= driver.gen_command_lines('loadavg')
    driver.run_local(lines)

    # perform df -H
    lines= driver.gen_command_lines('df_h')
    driver.run_local(lines)

    # dump_trace
    lines= driver.gen_dump_command()
    driver.run_local(lines)

    # parse trace
    p= TraceParser.from_file('/tmp/trace')

    # clean trace_file
    #lines= driver.gen_remove_trace()
    #driver.run_local(lines)

    for timestamp, command , trace in p:
        print timestamp , command , trace


    return


def test_tvbox_stats():

    dummy=None

    #dummy= ThreadedAdapter('tvbox:1','sleep 200')
    dummy = ThreadedAdapter('tvbox:1', 'bash')
    dummy.start()
    time.sleep(1)


    # start a scheduler server
    a= StatScheduler(agent_id='scheduler:1', target_id='tvbox:1', jobs=[])
    a.start()
    #a.send('!ps')


    # start a scheduler client
    cl= StatSchedulerClient(agent_id='scheduler:1')

    #cl.send('!ps')

    cl.send('!start')

    time.sleep(15)


    cl.send('!dump_trace')
    time.sleep(10)


    # simulate an entry on dummy
    # if dummy:
    #     dummy.queue('stdout').put('This is a dummy dump trace')
    #     time.sleep(5)


    r= cl.send('!read_trace')
    print r

    time.sleep(5)

    parser= cl.parse_trace()

    for line in parser:
        print line



    cl.exit()
    dummy.send('exit')




    time.sleep(5)
    return

# start
test_basic()
test_tvbox_stats()
print "Done"



