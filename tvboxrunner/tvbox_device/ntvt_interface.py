import os
import sys

import logging
log= logging.getLogger('NewTvTesting')


from restop.application import ApplicationError
#from pyvirtualdisplay import Display
from selenium import webdriver

try:
    import NewTvTesting

except ImportError:
    NewTvTesting= None
    log.error('NewTvTesting is not installed')
    sys.exit(1)


from selenium.webdriver.common.desired_capabilities import DesiredCapabilities
from NewTvTesting.Config import ConfAR, getDeskDataObject
from NewTvTesting.Config import Env, Wording, TvData, VodData
from NewTvTesting.Containers import RecordItem, Status, ProgramInfoItem, ListItem, VodItem, pvrItem,TvEsp,TvItem,ChannelDetail

#from RemoteControl import RpiRemoteControl


from NewTvTesting.Config import Rpi as RpiBase


from NewTvTesting.Config import Rpi

#base= os.path.dirname(os.path.realpath(__file__))
base = os.getcwd()
DUMP_FILENAME= os.path.join(base,"dump.html")
#DUMP_FILENAME= "/Users/cocoon/Documents/projects/restop/applications/tests/dump_sample.html"

# patch NewTvTesting.Config.Rpi
global Rpi
Rpi.DUMP="file://%s" % DUMP_FILENAME

from NewTvTesting.ResidentAppPage import WebKitPage



# class RpiClass(RpiBase):
#     """
#         redefine Rpi to handle DUMP
#     """
#     #DUMP = URL_RPI + "sendKey.php?dumpDom=true&remoteName=newtv&forceIR="+Env.INFRARED
#
#
#     @property
#     def DUMP(self):
#         #return self.URL_RPI + "sendKey.php?dumpDom=true&remoteName=newtv&forceIR="+Env.INFRARED
#         return "file:///./dump.html"
# Rpi= RpiClass()


selenium_host= 'localhost:4444'

class Pager(WebKitPage):
    """

        redefine WebKitPAge : add remote_control argument


    """
    def __init__(self,remote_control,device='tv',selenium_host=selenium_host,http_callback=None):
        """

        :param remote_control: instance of RpiRemoteControl or equivalent
        :return:
        """
        self.device_alias= device
        self.http_callback= http_callback
        self.ardom_filename = "ardom-%s.html" % self.device_alias

        if Env.BROWSER == "firefox":
            self.driver = webdriver.Remote(
                command_executor='http://%s/wd/hub' % selenium_host,
                desired_capabilities=DesiredCapabilities.FIREFOX)

            #self.display = Display(visible=0, size=(1024, 768))
            #self.display.start()
            #self.driver = webdriver.Firefox()
            self.driver.maximize_window()
        else:
            raise ApplicationError("Remote Selenium Server for CHROME is not available")
            #self.driver = webdriver.Chrome()
            #self.driver.maximize_window()
        self.logger = logging.getLogger('NewTvTesting.WebKitPage')
        #self.remote = RpiRemoteControl()
        self.remote=remote_control

    def close(self):
        try:
            self.driver.quit()
        except Exception ,e:
            pass
        #self.display.stop()


    # @classmethod
    # def set_rpi_dump(cls,filename):
    #     """
    #
    #     :param filename: string : eg ardom-tv.html
    #     :return:
    #     """
    #     global Rpi
    #     DUMP_FILENAME= os.path.join(base, filename)
    #     Rpi.DUMP="file://%s" % DUMP_FILENAME


    def load_driver(self):
        """

                some info  to load content into selenium driver
                see: http://stackoverflow.com/questions/24748482/selenium-get-content-for-dynamic-page-including-ajax-objects

                    # Save whole page
                    text = self.driver.page_source

        :return:
        """
        #self.driver.get(Rpi.DUMP)
        filename = os.path.join( os.getcwd(), self.ardom_filename)
        #dom_file= 'file://%s' % filename
        dom_file= "%s/files/%s" % (self.http_callback,self.ardom_filename)
        self.driver.get(dom_file)


    def dummy(self):
        print Rpi.DUMP


if __name__ == "__main__":

    #print Rpi.DUMP
    p =Pager('remote_control')
    p.dummy()

    os.stat(DUMP_FILENAME)
    program_info= p.getInfoFromLiveBanner()



    print "Done."