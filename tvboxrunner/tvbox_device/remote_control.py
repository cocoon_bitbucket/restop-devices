
"""


    stb remote control via http interface


def Yellow(): #Appui sur le bouton jaune
    CommandOutput=os.popen('wget -q -O /dev/null "http://'+IP[0]+':8080/remoteControl/cmd?operation=01&key=400&mode=0"').read()
    if debug==1:
        print(time.strftime("%H:%M:%S"))
        print("   Yellow\n")
    if debug==2:
        print("   Yellow\n")
        print(CommandOutput)

"""
import time
import json
import requests

import logging
default_logger= logging.getLogger('tvremote')
default_logger.setLevel(logging.DEBUG)


remote_codes= {

"VOD":393
,"Guide":365
,"Info":358
,"Liste":395
,"Menu":139
,"Zoom":372
,"PPlus":402
,"PMoins":403
,"Key0":512
,"Key1":513
,"Key2":514
,"Key3":515
,"Key4":516
,"Key5":517
,"Key6":518
,"Key7":519
,"Key8":520
,"Key9":521
,"Power":116
#,"OK":352
,"Quit":174
,"Retour":158
,"Record":167
,"Cancel":223
,"Down":108
,"Up":103
,"Right":106
,"Left":105
,"Mute":113
,"VolDown":114
,"VolUp":115
,"PlayPause":164
,"Stop":166
,"FForward":159
,"FRewind":168
,"Next":407
,"Previous":412
,"Yellow":400
,"Red":398
,"Green":399
,"Blue":401

# add native
,"HOME":  139
,"KEY_MENU":  139
,"UP": 103
,"LEFT":  105
,"RIGHT": 106
,"DOWN": 108
,"OK":    352
,"SOUND_UP": 114
,"SOUND_DOWN": 115
,"PLAY": 164
,"KEY_CHANNELUP": 402
,"KEY_CHANNELDOWN": 403
,"KEY_INFO": 358
,"KEY_GUIDE": 365
,"KEY_0":512
,"KEY_1":513
,"KEY_2":514
,"KEY_3":515
,"KEY_4":516
,"KEY_5":517
,"KEY_6":518
,"KEY_7":519
,"KEY_8":520
,"KEY_9":521
,"KEY_BACK":158
,"ZOOM":372
,"RECORD":167
,
#
# for javascript tests source
#
"0":512,
"1":513,
"2":514,
"3":515,
"4":516,
"5":517,
"6":518,
"7":519,
"8":520,
"9":521,

"PGM_P": 402,
"PGM_M": 403,

#"DOWN":108,
#"UP":103,
#"RIGHT":106,
#"LEFT":105,
#"OK":    352

"POWER": 116,
"MENU":  139,

"PLAY_PAUSE":164,


}

class StbRemoteControl(object):
    """
        An interface to STB  http remote controller


    """
    proxies = {
        "http": None,
        "https": None,
    }

    def __init__(self,base_url, debug_level=2,log=None,**parameters):
        """

        :param base_url: string url of the stb http remote controller


        """
        if not base_url.startswith("http://"):
            base_url="http://" + base_url


        self.base_url= base_url
        self.debug_level = debug_level
        self.parameters=parameters

        self.log=  log or default_logger

        # setup requests session
        self.rs = requests.session()
        self.rs.proxies= self.proxies
        self.rs.trust_env= False

        #self.rs.headers.update({
        #    'Accept': 'application/json',
        #})

        return

    def url_for_key(self,key_name,mode=0):
        """
        /remoteControl/cmd?operation=01&key=400&mode=0

        :param collection:
        :return:
        """
        key_code= remote_codes[key_name]
        url="%s/remoteControl/cmd?operation=01&key=%d&mode=%s" % (self.base_url,key_code,str(mode))
        return url


    def get(self,url,data=None,headers= None , access_token=None,timeout=3,**kwargs):
        data= data or {}
        headers= headers or {}
        if data:
            data= json.dumps(data)
            headers.update( {'Content-type': "application/json"})
        if access_token:
            raise RuntimeError('access_token not implemented')
            #return self.rs.get(url,data=data,headers=headers,auth=Oauth2(access_token) , **kwargs)
        else:
            return self.rs.get(url,data=data,headers=headers,timeout=timeout,**kwargs)



    def send_key(self,key_name): #Appui sur le bouton jaune

        url =self.url_for_key(key_name)

        # send command
        rc= self.get(url,timeout=3)

        if self.debug_level >=0 :
            if self.debug_level>=1:
                print(time.strftime("%H:%M:%S"))
                #print("   Yellow\n")
            if self.debug_level>=2:
                print("GET %s =>%s" % (url,rc.status_code))
        return rc

    def stb_zap(self,epg_id,universe=1):
        """

        :param channel:
        :return:

        GET http://stb_ipaddr:8080/remoteControl/cmd?operation=09&epg_id=IIIIIIIIII&uui=U
        Parameters

        operation : [0-9]{2} - must be set to 09
        epg_id : [0-9]{10} - EPG identifier of the requested channel. If the id length is not equal to 10 characters, '*' must be used as prefix of the epg_id to complete.
        uui : [1,2]{1} - Universe unique identifier
        1 : Orange universe
        2 : Canal universe.



        """
        #raise NotImplemented
        url="%s/remoteControl/cmd?operation=09&epg_id=%s&uui=%s" % (
            self.base_url,epg_id,str(universe))
        # send command
        rc= self.get(url)

        if self.debug_level >=0 :
            if self.debug_level>=1:
                print(time.strftime("%H:%M:%S"))
                #print("   Yellow\n")
            if self.debug_level>=2:
                print("GET %s =>%s" % (url,rc.status_code))
        return rc


    def stb_send_dump(self,mode=0):
        """
            send a dump signal to stb

        :param mode: int
                        mode= 0 =>    press Blue/ wait / release Blue
                        otherwise =>  press ZOOM then press RECORD
        :return:

             root: [DEBUG] devAndRpiTools> ************** Dump requested **************\x1b[0m\r\n',
             root: [DEBUG] devAndRpiTools> Debug config file exists /flash/Resources/resources/test_params\x1b[0m\r\n',
             root: [DEBUG] devAndRpiTools> DUMP sended to https://192.168.1.17/PostArDom.php\r\n',


        :return:
        """
        if mode != 0 :
            start= self.url_for_key("Blue",mode=1)
            stop= self.url_for_key("Blue",mode=2)
            r1= self.get(start)
            time.sleep(5)
            r2= self.get(stop)
        else :
            seq1= self.url_for_key("ZOOM",mode=0)
            seq2= self.url_for_key("RECORD",mode=0)
            self.get(seq1)
            self.get(seq2)

        return True

    def stb_notification(self):
        """
        GET http://stb_ipaddr:8080/remoteControl/notifyEvent


        :return:
        """
        url="%s/remoteControl/notifyEvent" % (self.base_url)
        rc= self.get(url)
        return rc

    def stb_status(self):
        """
        GET http://stb_ipaddr:8080/remoteControl/cmd?operation=10

        :return:
            "result" : {
                "responseCode" : "0",
                "message" : "ok",
                "data" : {
                    "playedMediaType" : "VOD",
                    "playedMediaState" : "PLAY",
                    "playedMediaId" : "SUPERCONDRIW0089555_S_2424VIDEO_1",
                    "playedMediaContextId" : "2424VIDEO",
                    "playedMediaPosition" : "72154",
                    "osdContext" : "VOD",
                    "macAddress" : "80:18:A7:CE:08:4C",
                    "wolSupport" : "0",
                    "friendlyName" : "decodeur TV d'Orange",
                    "activeStandbyState" : "0"
                }
            }

        """
        url="%s/remoteControl/cmd?operation=10" % (self.base_url)

        rc= self.get(url)
        assert rc.status_code == 200
        try:
            result= rc.json()
        except Exception as e:
            # failed
            return {}

        assert result['result']['responseCode'] == "0" ,"command stb_status has failed"
        return result['result']['data']






class KeySender(StbRemoteControl):
    """


    """

    def __getattr__(self, item):
        """

        :param item:
        :return:
        """

        def wrapper():
            self.send_key(item)

        return wrapper



class StbResponse(object):
    """

    "result" : {
                "responseCode" : "0",
                "message" : "ok",
                "data" : {



    """
    def __init__(self,status):
        """

        :param status:
        :return:
        """
        self.status=status

        self.response_code= self.status['responseCode']
        self.message= self.status['message']

        self._data=self.status['data']

    @property
    def data(self):
        if self.response_code == "0" :
            return self._data
        else:
            raise RuntimeError('Bad response code: %s [%s]' % (self.response_code,self.message))


class StbEvent(StbResponse):
    """

        "result":
          {
            "responseCode": "1",
            "message": "event notification",
            "data":
            { "eventType": "OSD_CONTEXT_CHANGED", "service": "LIVE_FALLBACK" }
      }

    """


class StbStatus(StbResponse):
    """
        "result" : {
                "responseCode" : "0",
                "message" : "ok",
                "data" : {
                    "playedMediaType" : "VOD",
                    "playedMediaState" : "PLAY",
                    "playedMediaId" : "SUPERCONDRIW0089555_S_2424VIDEO_1",
                    "playedMediaContextId" : "2424VIDEO",
                    "playedMediaPosition" : "72154",
                    "osdContext" : "VOD",
                    "macAddress" : "80:18:A7:CE:08:4C",
                    "wolSupport" : "0",
                    "friendlyName" : "decodeur TV d'Orange",
                    "activeStandbyState" : "0"
                }

    """

    @property
    def name(self):
        return self.data['friendlyName']
    @property
    def media_type(self):
        return self._data['playedMediaType']
    @property
    def media_state(self):
        return self._data['playedMediaState']
    @property
    def media_id(self):
        return self._data['playedMediaId']
    @property
    def media_position(self):
        return self._data['playedMediaPosition']
    @property
    def osd_context(self):
        return self._data['osdContext']
    @property
    def mac_address(self):
        return self._data['macAddress']
    @property
    def wol_support(self):
        return self._data['wolSupport']
    @property
    def standby_state(self):
        return self._data['activeStandbyState']

if __name__=="__main__":


    stb_base_url= "http://192.168.1.20:8080"

    c= StbRemoteControl( stb_base_url)

    k1= c.url_for_key('VOD')
    assert k1 =='%s/remoteControl/cmd?operation=01&key=393&mode=0' % stb_base_url


    wget= KeySender( stb_base_url)
    wget.OK()


    print('Done')

