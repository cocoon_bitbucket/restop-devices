
"""

    handle set top box menu selection


    to navigate and select an option in the stb menu we need 3 steps

    one on vertical axis ( -1 is UP , 1 ,2 .. is DOWN , 0 is no move )
    second on horizontal axis ( positive is RIGHT , 0 is no move )
    third on vertical axis ( positive is DOWN , 0 is no move


    a menu_path is  then 3 number

    -1 : move UP one time

    1/2/1 : means move DOWN 1, move RIGHT 2 , and move DOWN 1


    the first step of the path has easy names

    recherche: -1
    tv: 0
    vod: 1
    sports: 2
    jeux: 3
    musique: 4
    applis: 5
    orange: 6
    boutique: 7

    so a path like  tv/1/1  means 0/1/1



"""
import time

topography= [
    dict(
        recherche= -1,
        tv= 0,
        vod= 1,
        sports= 2,
        jeux= 3,
        musique= 4,
        applications= 5,
        orange= 6,
        boutique= 7),
    dict(
        programme= 1,
        demande= 1.1,
        canal= 2,
        enregistreur= 1.1,

        netflix= 1,
        videos= 1.1,
        got= 2,
        favoris= 2.1,

        bein= 1,

        multi= 1,
        nouveautes= 1.1,

        deezer= 1,
        clips= 1.1,
        radios= 2,

        web= 1,
        twiter= 1.1,
        mediacenter= 1,
        reglages= 1.1,
        cloud= 2,
        accessibilite= 2.1,
        visiteguidee= 3.1,

        passjeux= 1,
        famillemax= 1.1,
        cinemax= 2,
        famille= 2.1,
        cineserie= 3)
]


class StbMenuHandler(object):
    """


    """
    separator= '/'
    UP= "UP"
    DOWN= "DOWN"
    RIGHT= "RIGHT"
    LEFT= "LEFT"


    def __init__(self,agent,topography=topography,delay=2):
        """

        :param agent: object , instance of tvbox_device.agent.Agent
        :param topography:

        """
        self.agent= agent
        self.topography= topography
        self.delay= int(delay)


    def decode_path(self,path):
        """
            get a path and translate it to a sequence of integers


            sample paths:
            -1
             0/1
             vod/1/1

        :param path: string a sequence a 1 to 3 parts separated by /
        :return: a sequence of integers
        """
        sequence= []

        if not self.separator in path:
            # a path with only one element
            sequence.append(self.decode_first(path))
        else:
            # multiple elements
            parts= path.split(self.separator)
            sequence.append(self.decode_first(parts[0]))
            sequence.extend(self.decode_second(parts[1]))
            if len(parts) > 2:
                sequence.extend(self.decode_second(parts[2]))
        return sequence


    def decode_first(self,part):
        """

        :param part: integer or string in topography
        :return:
        """
        first =0
        try:
            first= int(part)

        except ValueError:
            # not an integer assumes it is a string
            part= self._canonical(part)
            first= self.topography[0][part]
            first= int(first)
        return first


    def decode_second(self, part):
        """

        :param part: integer/float or string in topography
        :return: a sequence
        """
        sequence=[]
        try:
            composite = float(part)
            parts= str(composite).split('.')
            sequence.append( int(parts[0]))
            second= int(parts[1])
            if second:
                sequence.append( int(parts[1]))
        except ValueError:
            # not a float
            try:
                element= int(part)
            except ValueError:
                # not an int , neither a float: assume it a string
                part= self._canonical(part)
                second= self.topography[1][part]

                return self.decode_second(second)

        return sequence


    def _canonical(self,text):
        """
            return a canonical form of text to search in topography

            exclude - and _ , lower , strip

        :param text:
        :return:
        """
        text= text.strip().replace('_','').replace('-','').lower()
        return text


    def _select(self,sequence):
        """

            send sequence of keys to select a stb menu

        :param sequence: list of integers  for Vertical , RIGHT , DOWN
        :return:
        """

        for step,distance in enumerate(sequence):

            if step == 0:
                # first move: -1 is UP positive is DOWN, 0  is no mouvelment
                if distance:
                    if distance < 0:
                        # move UP ( recherche )
                        distance = abs(distance)
                        key= self.UP
                    else :
                        key= self.DOWN
                    # move
                    self.move(key,distance)
                else:
                    # 0: no move
                    continue
            elif step == 1:
                # move horizontaly -> right
                if distance:
                    key= self.RIGHT
                    self.move(key,distance)
            else :  # step = 2
                # move verticaly -> DOWN
                if distance:
                    key= self.DOWN
                    self.move(key,distance)
        return


    def move(self,key,n=1):
        """

        :param key:
        :return:
        """
        for i in xrange(0,n):
            self.agent.send_key(key)
            time.sleep(self.delay)


    def select(self,path):
        """


        :param path:
        :return:
        """
        sequence= self.decode_path(path)
        self._select(sequence)






if __name__=="__main__":


    class DummyAgent(object):
        """

        """
        def send_key(self,key):
            print key

    agent= DummyAgent()


    a= StbMenuHandler(agent,topography,delay=0)


    r= a._canonical(' Media_center ')
    assert r == 'mediacenter'

    paths= ['-1', '0/1.1', '2/3', '2/3/1' , 'tv/programme', 'tv/2/1','jeux/nouveautes' ,'jeux/radios']

    sequences = [[-1],[0, 1, 1],[2, 3],[2, 3, 1],[0, 1],[0, 2, 1],[3, 1, 1],[3, 2]]


    for indice, path in enumerate(paths):
        r= a.decode_path(path)
        assert r == sequences[indice]

        print r

    print('-')
    r= a._select([-1])
    print ('-')
    r = a._select([2,3,1])

    print ('-')
    r = a._select([0,1,1])


    print ('-')
    r = a.select('tv/programme')

    print ('-')
    r = a.select('VOD/favoris')

    print

