# -*- coding: utf-8 -*-

import re
import json
import io
import datetime
from bs4 import BeautifulSoup

#from devices.tvbox_device.ntvt_interface import ProgramInfoItem

import logging
log= logging.getLogger(__name__)


dump_file= "../tests/samples/ardom_sample_1.html"


class ListItem(object):

    def __init__(self,text, selected=False, active=True):
        self.text = text
        self.selected = selected
        self.active = active


class ArDomPage(object):
    """


    """
    def __init__(self,data):
        """


        :param data:
        """
        self.soup= BeautifulSoup(data, 'html.parser')
        self.title= self.soup.title.text

    @classmethod
    def from_file(cls,filename=dump_file):
        """

        :param finename:
        :return:
        """
        html_doc= file(filename,'rb').read()
        page= cls(html_doc)
        return page

    def prettify(self):
        """

        :param self:
        :return:
        """

        return self.soup.prettify()

    def findInPage(self, txt, regex=False):

        found = False

        body= self.soup.body
        if regex:
            found = re.search(re.compile(txt), body.text) is not None
        else:
            found= body.text.find(txt) > -1
        return found

    @classmethod
    def json_to_html(self,filename):
        """


        :return:
        """
        with open(filename,'rb') as fh:
            content= fh.read()
            data= json.loads(content)
            with io.open("page.html", 'w', encoding='utf8') as f:
                f.write(data)





class ScreenView(object):
    """



    """
    root_class= ''
    _items= {}

    def __init__(self, soup):
        """

        """
        self._ready=False
        self._status= 'empty'
        self.root = self.find_root(soup)
        self._ready=True
        self._data={}


    @classmethod
    def find_root(cls, soup):
        """

        :return:
        """
        log.debug('search root: <? class="%s ...">' % cls.root_class)
        f = lambda tag: tag.has_attr('class') and tag['class'][0] == cls.root_class
        live_scene = soup.body.find_all(f)
        root= cls._first(live_scene)
        log.debug('found  root: %s' % root)
        return root

    @classmethod
    def from_file(cls,filename):
        """

        :param filename:
        :return:
        """
        page=ArDomPage.from_file(filename)
        soup= page.soup
        return cls(soup)


    @classmethod
    def _first(cls, result_set, unique=True):
        """

        :param result_set:
        :return:
        """
        assert len(result_set) > 0, "not found"
        if unique:
            assert len(result_set) == 1, "not unique"
        return result_set[0]

    @classmethod
    def _last(cls, result_set, unique=False):
        """

        :param result_set:
        :return:
        """
        assert len(result_set) > 0, "not found"
        if unique:
            assert len(result_set) == 1, "not unique"
        return result_set[-1]

    def get_child(self, name,label=None ,root=None):
        """

        :param name:
        :return:
        """
        root= root or self.root
        label = label or self._items[name]
        log.debug('    search child  <? class="%s ...">' % label)
        f = lambda tag: tag.has_attr('class') and tag['class'][0] == label
        q = root.find_all(f)
        child= self._first(q)
        log.debug('    found  child  %s' % child)
        return child


    def export(self,data=None):
        """
            export data to a json compatible dict

        :param data:
        :return:
        """
        data= data or self._data
        dic={}
        for attr_name,attr_value in data.items():
            if isinstance(attr_value, (basestring, int, bool)):
                dic[attr_name] = attr_value
            elif isinstance(attr_value, datetime.datetime):
                dic[attr_name] = str(attr_value)
            elif isinstance(attr_value, datetime.timedelta):
                dic[attr_name] = str(attr_value)
            elif attr_value is None:
                dic[attr_name] = None
            else:
                pass
        return dic

    def find_element_by_css_class(self, root,classes):
        """

        :param root: a soup element
        :param classes:  list eg ["infoChannel","selected"]
        :return:
        """
        if not isinstance(classes,list):
            classes= [classes]

        def inner(tag):
            if tag.has_attr('class'):
                for index,class_ in enumerate(classes):
                    if not tag['class'][index] == class_:
                        return False
                return True
            else:
                return False
        log.debug('search root:  <? class="%s ...">' % str(classes))
        #f = lambda tag: tag.has_attr('class') and tag['class'][0] == "infoChannel" and tag['class'][1] == "selected"
        elements = root.find_all(inner)
        return elements

if __name__=="__main__":


    logging.basicConfig(level=logging.DEBUG)

    #ArDomPage.json_to_html('../tests/samples/ardom_sample_error.json')

    page= ArDomPage.from_file('../tests/samples/ardom_sample_error.html')
    print page.title
    assert page.title == "Orange TV - New UI"


    found= page.findInPage('Multicam')
    assert found

    found= page.findInPage('no such text')
    assert not found

    found= page.findInPage('Multicam',regex=True)
    assert found

    found= page.findInPage('no such text',regex=True)
    assert not found


    print "Done."

