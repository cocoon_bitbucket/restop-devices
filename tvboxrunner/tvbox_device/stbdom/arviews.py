# -*- coding: utf-8 -*-



from datetime import datetime, timedelta
import re

from slugify import slugify

from ardom import ArDomPage,ScreenView,ListItem


import logging
log= logging.getLogger(__name__)



class LiveBanner(ScreenView):
    """

    """
    root_class = "programInformation"

    _items= {

        'lcn': '',
        'channelName': "nameOfChannel",
        'favorite' : 'favorite',
        'ongoingRec': '',
        'program': 'program',
        'start': 'start',
        'end': 'end',
        'length': '',
        'genre': 'kind',
        'nextProgram': 'next',

        'pictoCsa': 'pictoCsa',
        'pictoDefinition': 'pictoHD',
        'pictoRatio': 'picto16_9',
        'pictoAudioDesc':'pictoAD',
        'pictoContentVersion':'pictoVM',
        'pictoDts': 'pictoDTS',
        'pictoDolby': 'pictoDolby',
        'pictoEar': 'pictoEar',
        'picto3D': '',
        'pictoVO': '',
        'TS_width': 'scrollerBar',
        'TS_start': 'scrollerBar',
        'TS_cursor': 'tsCursor',

    }

    @classmethod
    def find_root(cls,soup):
        """

        :return:
        """
        log.debug('search root:  <? class="%s ...">' % cls.root_class)
        f = lambda tag: tag.has_attr('class') and tag['class'][0] == cls.root_class
        elements = soup.body.find_all(f)
        # if several retains the last one
        root= cls._last(elements)
        log.debug('found  root:  %s' % root)
        return root

    # @classmethod
    # def find_root(cls,soup):
    #     """
    #
    #     :return:
    #     """
    #     log.debug('search root:  <? class="live scene ...">')
    #     f = lambda tag: tag.has_attr('class') and tag['class'][0] == "live" and tag['class'][1] == "scene"
    #     live_scene = soup.body.find_all(f)
    #     root= cls._first(live_scene)
    #     log.debug('found  root:  %s' % root)
    #     return root


    def feed(self):
        """


        :return:
        """
        for name in ['channelName','program','genre','nextProgram']:
            tag= self.get_child(name)
            self._data[name]= tag.text

        for name in ['start','end']:
            tag= self.get_child(name)
            if not tag.text:
                # search for alternatives
                if name == 'start':
                    tag= self.get_child(name,label='clock')
                elif name == 'end':
                    tag= self.get_child(name,label='nextTime')
            self._data[name]= tag.text



        for name in ['pictoAudioDesc','pictoContentVersion','pictoDolby','pictoDts','pictoEar','favorite']:
            tag = self.get_child(name)
            #print tag
            if 'hidden' in tag['class']:
                self._data[name] = False
            else:
                self._data[name]= True

        for name in ['pictoDefinition','pictoRatio']:
            try:
                tag = self.get_child(name)
                # print tag
                if 'hidden' in tag['class']:
                    self._data[name] = False
                else:
                    self._data[name] = True
            except:
                self._data[name]=False

        for name in ['pictoCsa']:
            tag = self.get_child(name)
            #print tag
            self._data[name]= tag['class'][1]

        for name in ['TS_cursor']:
            tag = self.get_child(name)
            # class ="tsCursor" style="left: 66%;" >
            style= tag['style']
            if style:
                r= re.compile('(\d+)')
                match= r.search(style)
                if match:
                    value= match.group(1)
                    self._data[name] = int(value)

        tag= self.get_child('TS_start')
        #<div class ="scrollerBar" style="left: 0%; width: 0%;" >
        style = tag['style']
        if style:
            r = re.compile('(\d+).*?(\d+)')
            match = r.search(style)
            if match:
                left = match.group(1)
                width= match.group(2)
                self._data['TS_start'] = int(left)
                self._data['TS_width'] = int(width)

        # split channel name
        channel_parts= self._data['channelName'].split(u' ')
        num= int(channel_parts[0])
        text= channel_parts[-1]
        self._data['lcn']= num
        self._data['channelName']= text

        # convert start end to datetime
        for tag in ['start','end']:
            text= self._data[tag]
            if ':' in text:
                hour,minutes= text.split(':')
            else:
                hour,minutes= u'0',u'0'
            #hour = element.text.split(":")[0]
            #minutes = element.text.split(":")[1]
            now = datetime.now()
            self._data[tag] = datetime(now.year, now.month, now.day, int(hour), int(minutes))

        if self._data['end'] < self._data['start']:
            self._data['end'] = self._data['end'] + timedelta(days=1)
        self._data['length'] = self._data['end'] - self._data['start']

        return self._data


class LiveBannerPolaris(ScreenView):
    """



    """
    root_class = "infoBanner"

    _items= {

        'lcn': '',
        'channelName': "titleLine",
        'favorite' : 'favIcon',

        'ongoingRec': '',
        'program': 'program',
        'start': 'start',
        'end': 'end',
        'length': '',
        'genre': 'kind',
        'nextProgram': 'next',

        'pictoCsa': 'pictoCsa',
        'pictoDefinition': 'pictoHD',
        'pictoRatio': 'picto16_9',
        'pictoAudioDesc':'pictoAD',
        'pictoContentVersion':'pictoVM',
        'pictoDts': 'pictoDTS',
        'pictoDolby': 'pictoDolby',
        'pictoEar': 'pictoEar',
        'picto3D': '',
        'pictoVO': '',
        'TS_width': 'scrollerBar',
        'TS_start': 'scrollerBar',
        'TS_cursor': 'tsCursor',

    }

    @classmethod
    def find_root(cls,soup):
        """

        :return:
        """
        log.debug('search root:  <? class="%s ...">' % cls.root_class)
        f = lambda tag: tag.has_attr('class') and tag['class'][0] == cls.root_class
        elements = soup.body.find_all(f)
        if len(elements):
            root = cls._first(elements)
            log.debug('found  root:  %s' % root)
        else:
            #find_element_by_css_selector(".infoChannel.selected")
            log.debug('search root:  <? class="infoChannel selected ...">')
            f = lambda tag: tag.has_attr('class') and tag['class'][0] == "infoChannel" and tag['class'][1] == "selected"
            elements = soup.body.find_all(f)
            root= cls._first(elements)
            log.debug('found  root:  %s' % root)
        return root

    def feed(self):
        """


        :return:
        """
        for name in ['channelName']:
            tag = self.get_child(name)
            self._data[name] = tag.text
            if self.get_child('favorite',tag):
                self._data['favorite']= True
            else:
                self._data['favorite']= False


        # ATTENTION Gestion de la récupération du nom de chaine avant/après harmonisation avec la mosaaïque (nécessaire pendant valid sprint 28)
        if '-' in self._data['channelName']:
            alltxt_list = self._data['channelName'].text.split(u'-')
        else:
            alltxt_list = self._data['channelName'].text.split(u'.')

        self._data['lcn']= int(alltxt_list[0])
        self._data['channelName']= alltxt_list[1]

                # # channel name, lcn, favorite
        # element = elements.find_element_by_css_selector(".titleLine")
        # # ATTENTION Gestion de la récupération du nom de chaine avant/après harmonisation avec la mosaaïque (nécessaire pendant valid sprint 28)
        # if '-' in element.text:
        #     alltxt_list = element.text.split(u'-')
        # else:
        #     alltxt_list = element.text.split(u'.')
        #
        # num = int(alltxt_list[0])
        # txt = alltxt_list[1]
        # if len(element.find_elements_by_css_selector(".favIcon")) > 0:
        #     favorite = True
        # else:
        #     favorite = False

class ToolBoxLive(ScreenView):
    """

    """
    root_class = 'ToolBoxLive'
    item_list_class= "listItem"


    def list_items(self):
        """

        :return:
        """
        f= lambda tag: tag.has_attr('class') and tag['class'][0] == self.item_list_class
        q= self.root.find_all(f)
        return q

    def list_active_items(self):
        """

        :return:
        """
        items= self.list_items()
        self.active = [ tag for tag in items if not 'separator' in tag['class']]
        return self.active


    def feed(self):
        """


        :return:
        """
        #self.menu= self.list_active_items()
        self.menu=[]
        self.get_items()

        self._ready= True


    def get_active_index(self):
        """

        :return:
        """
        if self._ready:
            for index,tag in enumerate(self.list_active_items()):
                if 'highlight' in tag['class']:
                    return index

    def get_index_of(self,t,regex=False):
        """

        :param t:
        :param regex:
        :return:
        """
        if self._ready:
            for index,tag in enumerate(self.list_active_items()):
                t= unicode(t.lower())
                t= slugify(t)
                text= tag.text.lower()
                text= slugify(text)
                if t  in text:
                    return index
        return -1

    def get_delta_index_for(self,t ,regex=False):

        active= self.get_active_index()
        found= self.get_index_of(t,regex=regex)
        if found == -1:
            return None
        else:
            delta= found - active
        return delta


    def get_items(self):
        """

        :return:
        """
        self.activeItems=[]
        elements= self.list_active_items()

        indexActive= 0
        for index, element in enumerate(elements):
            if 'highlight' in element['class']:
                self.highlight = index
                self.activeHighlight = indexActive

            if 'selected' in element['class'] or 'checked' in element['class']:
                selected = True
            else:
                selected = False

            if 'inactive' in element['class']:
                active = False
            else:
                active = True
                self.activeItems.append(ListItem(element.text, selected, active))
                indexActive = indexActive +1


            self.menu.append(ListItem(element.text, selected, active))
        return True


    #
    # find in list
    #
    def findInList(self, t, regex = False):
        """

        :param t:
        :param regex:
        :return:
        """
        found = True
        r= self.get_index_of(t)
        if r == -1:
            found = False
        return found


    def getList(self):
        """

        :return:
        """
        # if self._loadPageList():
        #     return self.items
        # else:
        #     return None
        return self.menu

    def getLabelFromListFocus(self):
        if self._ready:
            return self.getList()[self.highlight]
        else:
            return None


    def actionSelect(self,t,partial=False):

        #self.logger.info("  >>   selecting text >" + t + "<")
        #self._loadPageList()
        find = False
        index = 0

        # if partial is True:
        #     while not t in self.activeItems[index].text and index < (len(self.activeItems)-1):
        #         index = index+1
        #
        #     if t in self.activeItems[index].text:
        #         find = True
        # else:
        #     while self.activeItems[index].text!=t and index < (len(self.activeItems)-1):
        #         index = index+1
        #
        #     if self.activeItems[index].text==t:
        #         find = True
        #
        # if find:
        #     gap = index - self.activeHighlight
        #
        #     if gap >= 0:
        #         keyGap = "KEY_DOWN"
        #     else:
        #         keyGap = "KEY_UP"
        #
        #     self.remote.sendKeys([keyGap] * abs(gap))
        #     self.remote.sendKey("KEY_OK")
        #
        #     return True
        #
        # else:
        #     if len(self.driver.find_elements_by_css_selector(".ToolBoxLive"))>0:
        #         maxItems = ConfAR.MAX_ITEMS_TOOLBOX
        #     else:
        #         maxItems = ConfAR.MAX_ITEMS
        #
        #     if len(self.items) == maxItems:
        #
        #         end = False
        #         endText=self.activeItems[-1].text
        #
        #         while not end:
        #
        #             self.remote.sendKeys(["KEY_UP"] * int(self.activeHighlight + 1))
        #
        #             self._loadPageList()
        #             find = False
        #             index = 0
        #
        #             if partial is True:
        #                 while not t in self.activeItems[index].text and index < (len(self.activeItems)-1):
        #                     index=index+1
        #                     if self.activeItems[index].text == endText:
        #                         end = True
        #
        #                 if t in self.activeItems[index].text:
        #                     find = True
        #                     end = True
        #             else:
        #                 while self.activeItems[index].text!=t and index < (len(self.activeItems)-1):
        #                     index=index+1
        #                     if self.activeItems[index].text == endText:
        #                         end = True
        #
        #                 if self.activeItems[index].text==t:
        #                     find = True
        #                     end = True
        #
        #         if find:
        #
        #             gap = index - self.activeHighlight
        #
        #             if gap >= 0:
        #                 keyGap = "KEY_DOWN"
        #             else:
        #                 keyGap = "KEY_UP"
        #
        #             self.remote.sendKeys([keyGap] * abs(gap))
        #             self.remote.sendKey("KEY_OK")
        #
        #             return True
        #
        #         else:
        #             self._detect_timeout()
        #             return False
        #
        #     else:
        #         self._detect_timeout()
        #         return False


if __name__=="__main__":

    html_file = "../tests/samples/ardom_sample_1.html"
    html_file = "../tests/samples/ardom_sample_error.html"


    logging.basicConfig(level=logging.DEBUG)


    page= ArDomPage.from_file(html_file)

    print page.prettify()



    body=page.soup.body




    l= LiveBanner(page.soup)


    print l.get_child('start').text
    print l.get_child('end').text
    print l.get_child('genre').text


    l.feed()




    t= ToolBoxLive(page.soup)

    items= t.list_items()

    # active = [ tag for tag in items if not 'separator' in tag['class']]
    #
    # for e in active:
    #     for s in e.strings:
    #         print s

    t.feed()
    print t.get_active_index()

    delta= t.get_delta_index_for('> Multicam')
    assert delta == 0

    delta= t.get_delta_index_for('no such item')
    assert delta == None

    delta= t.get_delta_index_for(u'à la demande')
    assert delta == 1

    delta= t.get_delta_index_for(u'résumé')
    assert delta == 2

    delta= t.get_delta_index_for(u'chaînes  (non)')
    assert delta == 3

    delta= t.get_delta_index_for(u'langue (français)')
    assert delta == 4

    delta= t.get_delta_index_for(u'langue')
    assert delta == 4

    delta= t.get_delta_index_for(u'français')
    assert delta == 4


    labels = [
        '> Multicam',
        u'à la demande',
        u'résumé',
        u'chaînes  (non)',
        u'langue (français)'
        ]

    for l in labels:
        print slugify(unicode(l))


    items= t.getList()

    r= t.getLabelFromListFocus()

    r= t.findInList(u'résumé')
    assert r is True

    r= t.findInList(u'dummy')
    assert r is False




    print "Done"