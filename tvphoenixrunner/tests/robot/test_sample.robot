*** Settings ***
Documentation     standard tests
...

# libraries
Library  restop_client


#Suite Setup  init suite
Test Setup     setup pilot     ${platform_name}    ${platform_version}  ${platform_url}
Test Teardown  close session


*** Variables ***
${platform_name}=   demo
${platform_version}=    demo_qualif

#${platform_url}=  http://localhost:5000/restop/api/v1
${platform_url}=  http://192.168.1.21/restop/api/v1
#${platform_url}=  http://192.168.1.26/restop/api/v1


#${pilot_mode}=  dry
${pilot_mode}=  normal



*** Keywords ***

########
#init suite
#	# check platform capablities with test capablities requirements
#	setup pilot 	${platform_name} 	${platform_version}  ${platform_url}
#
#init pilot
#	setup pilot 	${platform_name} 	${platform_version}  ${platform_url}
#	run keyword if   '${pilot_mode}'=='dry'  set pilot dry mode
#
#shutdown pilot
#	close session




Unit Sample Demo
	[Arguments] 	${device}
	[Documentation]

	Open session	${device}


    Ping  ${device}
    Builtin.sleep  2

    ${info}=  read  ${device}
    Log  ${info}

    Builtin.sleep  2

    Raise Error  ${device}

*** Test Cases ***


demo
  Unit Sample Demo  sample_1