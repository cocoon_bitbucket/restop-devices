*** Settings ***
Documentation     standard tests
...

# libraries
Library  restop_client


#Suite Setup  init suite
Test Setup     setup pilot     ${platform_name}    ${platform_version}  ${platform_url}
Test Teardown  close session


*** Variables ***
${platform_name}=   demo
${platform_version}=    demo_qualif

#${platform_url}=  http://localhost:5000/restop/api/v1
${platform_url}=  http://192.168.1.21/restop/api/v1
#${platform_url}=  http://192.168.1.26/restop/api/v1


#${pilot_mode}=  dry
${pilot_mode}=  normal



*** Keywords ***

########
#init suite
#	# check platform capablities with test capablities requirements
#	setup pilot 	${platform_name} 	${platform_version}  ${platform_url}
#
#init pilot
#	setup pilot 	${platform_name} 	${platform_version}  ${platform_url}
#	run keyword if   '${pilot_mode}'=='dry'  set pilot dry mode
#
#shutdown pilot
#	close session




Unit stb Demo
	[Arguments] 	${lb}
	[Documentation] 	stb test

	Open session	${lb}
	Publish Event  ${lb} title=open_session
    Serial Synch    ${lb}


    scheduler_init   ${lb}
    #
    #  test live banner
    #
    Send Key  ${lb}  key=1  timeout=5
    Publish Event  ${lb}  title=select TF1
    builtin.sleep   2

    Send Key  ${lb}  key=7  timeout=5
     builtin.sleep   1
    Publish Event  ${lb}  title=select ARTE
    ${info}=  page_get_info_from_live_banner  ${lb}
    log  ${info}

    builtin.sleep   10


    #
    #   test desk
    #
    Publish Event  ${lb}  title=select mes favoris
    Send Key  ${lb}  key=MENU  timeout=5
    builtin.sleep   1
    page_action_desk_select  ${lb}  text=mes favoris
    builtin.sleep   10
    Send Key  ${lb}  key=MENU  timeout=5
    builtin.sleep   1
    Send Key  ${lb}  key=MENU  timeout=5
    builtin.sleep   5


    #
    #   test tool box replay
    #
    Publish Event  ${lb}  title=select replay
    Send Key  ${lb}  key=5  timeout=5
    builtin.sleep   1
    Send Key  ${lb}  key=OK  timeout=5
    builtin.sleep   1
    page_action_tool_select  ${lb}  text=replay
    builtin.sleep   5
    Send Key  ${lb}  key=7  timeout=5
    builtin.sleep   5


    Serial Synch    ${lb}  timeout=15


    Publish Event  ${lb} title=close_session











Unit stbplay Stat Scheduler
	[Arguments] 	${lb}
	[Documentation] 	test stat scheduler on livebox

	Open session	${lb}

    #Telnet Sync    ${lb}

    Scheduler Init  ${lb}
    #Telnet Watch    ${lb}  timeout=10

    builtin.sleep   60

    #Scheduler Sync   ${lb}
    #builtin.sleep   15
    #Telnet Watch    ${lb}  timeout=10


    builtin.sleep   60

    Scheduler Close  ${lb}
    builtin.sleep    15
    #Telnet Watch  ${lb}  timeout=10



*** Test Cases ***


#scheduler
#  [Tags]  livebox
#  [Template]  Unit Livebox Stat Scheduler
#  livebox
#


demo
  Unit stb Demo  stbphenix