
import time
from yaml import load
from copy import deepcopy
import pprint
import requests

from wbackend.model import Database,Model
from restop.application import ApplicationError
from restop_platform.models import Platform

from restop_platform.dashboard import Dashboard
from tvphoenixrunner.models import TvPhoenixAgents as AgentModel

from tvphoenixrunner.agent import Agent

import logging


platform_file= './platform.yml'
agent_name = 'tv'

hub_ip= '192.168.1.55'
hub_port= 8080



def get_new_db():
    """

    :return:
    """
    db = Database(host='redis', port=6379, db=0)
    db.flushdb()
    Model.bind(database=db,namespace=None)

    try:
        p= Platform.get(Platform.name=='default')
    except Exception as e:
        # does not exists create it
        db.flushdb()
        #stream = file('../../tests/platform.yml')
        #stream = file(platform_file)
        stream=open(platform_file)
        data = load(stream)
        p = Platform.create(name='default', data=data)
        #p.setup()

    return db

def get_model(agent_name='tv'):

    db= get_new_db()

    platform = Platform.get(Platform.name == 'default')
    agent_parameters= platform.data['devices'][agent_name]
    agent_profile= agent_parameters['profile']
    parameters= deepcopy(platform.data['profiles'][agent_profile])
    parameters.update(agent_parameters)

    #parameters['remote_url']= "192.168.1.20:8080"

    tv = AgentModel.create(name=agent_name, session=1, parameters= parameters)
    tv_id= tv.get_id()
    tv = AgentModel.load(tv_id)

    return tv



#
#
#

def test_remote():
    """

        test remote without remote_server: expect requests timeout exception

    :return:
    """
    agent_name = 'tv'

    tv= get_model(agent_name)

    database= tv.database

    agent = Agent(tv)


    try:
        r= agent.send_key('1')
    except requests.exceptions.ConnectTimeout:
        pass
    except requests.exceptions.ConnectionError:
        pass
    except Exception as  e:
        print(e.message)

    try:
        r= agent.send_keys(['1','2'])
    except requests.exceptions.ConnectTimeout:
        pass
    except requests.exceptions.ConnectionError:
        pass
    except Exception as  e:
        print(e.message)

    try:
        r= agent.send_long_key('1')
    except requests.exceptions.ConnectTimeout:
        pass
    except requests.exceptions.ConnectionError:
        pass
    except Exception as  e:
        print(e.message)


    try:
        r= agent.stb_channel_info()
    except requests.exceptions.ConnectTimeout:
        pass
    except requests.exceptions.ConnectionError:
        pass
    except Exception as  e:
        print(e.message)

    # try:
    #     r= agent.stb_zap('tv1')
    # except requests.exceptions.ConnectTimeout:
    #     pass
    # except requests.exceptions.ConnectionError:
    #     pass
    # except Exception ,e:
    #     print e.message

    try:
        r= agent.stb_status()
    except requests.exceptions.ConnectTimeout:
        pass
    except requests.exceptions.ConnectionError:
        pass
    except Exception as e:
        print(e.message)


    return

def test_desk():
    """

        test remote without remote_server: expect requests timeout exception

    :return:
    """
    agent_name = 'tv'

    tv= get_model(agent_name)

    database= tv.database

    agent = Agent(tv)


    r= agent.send_key('MENU')

    # video a la demande / mes favoris
    rc= agent.page_action_desk_select("mes favoris")


    time.sleep(10)
    r = agent.send_key('MENU')
    time.sleep(1)
    r = agent.send_key('MENU')

    return


def test_toolbox():
    """




    :return:
    """

    agent_name = 'tv'

    tv= get_model(agent_name)

    database= tv.database

    agent = Agent(tv)

    # select a channel
    r= agent.send_key('5')
    time.sleep(1)


    # trigger the toolbox menu
    r = agent.send_key('OK')

    #
    r=  agent.page_action_tool_select('replay')


    time.sleep(10)
    # leave replay
    r = agent.send_key('7')
    time.sleep(1)

    return





def test_page():
    """

        test remote without remote_server: expect requests timeout exception

    :return:
    """
    agent_name = 'tv'

    tv= get_model(agent_name)

    database= tv.database

    agent = Agent(tv)

    # mock the ardom file
    agent.ardom_filename= 'samples/ardom-tv.html'
    try:
        r= agent.page
        agent.page._auto_fetch = False
        agent.page.ardom_filename = 'samples/ardom-tv.html'
    except Exception as e:
        pass


    rc= agent.page_get_info_from_live_banner()

    return







def test_stb_interface():

    agent_name = 'tv'

    tv= get_model(agent_name)

    database= tv.database


    agent= Agent(tv)

    # force serial port to loop://?logging=debug
    parameters= agent.model.parameters
    parameters['serial_port'] = 'usb0'
    agent.model.parameters=parameters
    #agent.model.parameters['serial_port']= 'loop://?logging=debug'
    agent.model.save()

    response= agent.start()


    #
    # test stb_get_lineup
    #
    rc= agent.stb_get_lineup()
    time.sleep(2)
    rc= agent.serial_synch(timeout=5)
    response = agent.model.make_response(rc)
    assert 'curl -i -X POST' in response['logs'][-4]

    #
    # pcb_cli
    #
    command='AdditionalSoftwareVersion'
    rc = agent.stb_pcb_cli(command)

    time.sleep(2)
    rc = agent.serial_synch(timeout=5)
    response = agent.model.make_response(rc)
    assert 'pcb_cli "DeviceInfo.AdditionalSoftwareVersion.?"' in response['logs'][-4]


    try:
        rc = agent.stb_info()
    except ApplicationError:
        pass

    time.sleep(2)
    rc = agent.serial_synch(timeout=5)
    response = agent.model.make_response(rc)
    #assert '' in response['logs'][-4]

    response= agent.stop()

    time.sleep(3)


if __name__=="__main__":

    logging.basicConfig(level=logging.DEBUG)

    #test_page()

    #test_toolbox()
    test_desk()
    #test_remote()


    print("Done.")
