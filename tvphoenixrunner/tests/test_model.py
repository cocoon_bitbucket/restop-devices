
from yaml import load

from wbackend.model import Database,Model
from restop_platform.models import Platform
from tvphoenixrunner.models import TvPhoenixAgents as AgentModel
from tvphoenixrunner.resources import DeviceResource as AgentResource


platform_file= './platform.yml'

def get_new_db():
    """

    :return:
    """
    db = Database(host='localhost', port=6379, db=0)
    db.flushdb()
    Model.bind(database=db,namespace=None)

    try:
        p= Platform.get(Platform.name=='default')
    except Exception as e:
        # does not exists create it
        db.flushdb()
        #stream = file('../../tests/platform.yml')
        stream = file(platform_file)
        data = load(stream)
        p = Platform.create(name='default', data=data)
        #p.setup()

    return db

def test_model():
    """

    :return:
    """
    db= get_new_db()

    sample= AgentModel.create(name='sample_1',session=1)

    return

def test_apidoc():
    """


    :return:
    """
    autodoc = AgentResource._autodoc()
    return


if __name__=="__main__":

    test_apidoc()
    test_model()

    print("Done.")
