__author__ = 'cocoon'

import time

import os
from restop_client import Pilot


#phonehub_url= "http://localhost:5000/restop/api/v1"
#phonehub_url= "http://192.168.99.100/restop/api/v1"
#phonehub_url= "http://10.179.5.174/restop/api/v1"
phonehub_url= "http://192.168.1.21/restop/api/v1"

user= "stbphenix"


def test_fetch_restop_interface():
    """



    :return:
    """
    # to test localy
    #phonehub_url = "http://localhost:5001/restop/api/v1"
    #os.environ['NO_PROXY'] = 'localhost'

    p= Pilot()

    data= p.fetch_restop_interface(url=phonehub_url)

    return data



def test_scheduler():

    p =Pilot()

    p.setup_pilot("demo", "demo_qualif", phonehub_url)

    user1= user

    try:

        res= p.open_Session(user1)

        res = p.serial_watch(user1, timeout=10)
        #res = p.serial_synch(user1)

        res= p.scheduler_init(user1)

        res = p.serial_watch(user1, timeout=10)

        #time.sleep(11)
        res= p.scheduler_sync(user1)
        res = p.serial_watch(user1, timeout=10)
        #time.sleep(2)

        #res = p.serial_synch(user1)
        #time.sleep(11)

        res= p.scheduler_close(user1)

        #res = p.serial_synch(user1)

        res= p.serial_watch(user1,timeout=10)


    except RuntimeError as e:
        print("interrupted: %s" %e)

    except Exception as e:
        print(e.message)
    finally:
        r= p.close_session()

    return



#
#
#


def test_demo():

    p =Pilot()

    p.setup_pilot("demo", "demo_qualif", phonehub_url)

    user1= user

    try:

        res= p.open_Session(user1)
        res= p.publish_event(user1,title="open_session")


        time.sleep(10)


        res= p.send_key(user1,key="1")
        res = p.publish_event(user1, title="select TF1")
        time.sleep(10)

        res= p.send_key(user1,key="2")
        res = p.publish_event(user1, title="select FR2")
        tv_program = p.page_get_info_from_live_banner(user1)
        print(tv_program)

        res = p.serial_synch(user1)

        res= p.scheduler_init(user1)

        time.sleep(30)


        # select mes favoris
        res = p.publish_event(user1, title="mes favoris")
        r= p.send_key(user1,key='MENU')
        rc= p.page_action_desk_select(user1,text="mes favoris")
        time.sleep(30)
        r = p.send_key(user1,key='MENU')
        time.sleep(1)
        r = p.send_key(user1,key='MENU')



        # select a channel
        res = p.publish_event(user1, title="replay")
        r = p.send_key(user1,key='5')
        time.sleep(1)

        # trigger the toolbox menu
        r = p.send_key(user1,key='OK')
        r = p.page_action_tool_select(user1,text='replay')

        time.sleep(30)
        # leave replay
        r = p.send_key(user1,key='7')
        time.sleep(1)

        time.sleep(20)


        # close scheduler
        res= p.scheduler_close(user1)
        res = p.serial_synch(user1)
        res= p.serial_watch(user1,timeout=10)


    except RuntimeError as e:
        print("interrupted")

    except Exception as e:
        print(e.message)
    finally:
        res = p.publish_event(user1, title="close_session")
        r= p.close_session()

    return





def test_powerswitch():

    p =Pilot()

    p.setup_pilot("demo", "demo_qualif", phonehub_url)

    user1= user

    try:

        res= p.open_Session(user1)

        res = p.serial_synch(user1)
        # assert res.startswith('Signal')

        res= p.power_on( user1)


        time.sleep(10)

        res= p.power_off(user1)


        time.sleep(5)

    except RuntimeError as  e:
        print("interrupted")

    except Exception as e:
        print(e.message)
    finally:
        r = p.close_session()

    return





def test_phenix_ardom_menu():

    p =Pilot()

    p.setup_pilot("demo", "demo_qualif", phonehub_url)

    user1= user

    try:

        res= p.open_Session(user1)

        res = p.serial_synch(user1)
        # assert res.startswith('Signal')



        #time.sleep(2)
        #res = p.serial_synch(user1)

        res = p.send_key(user1, key="5")
        res = p.stb_channel_info(user1)
        res = p.serial_watch(user1, timeout=10)

        #tv_program = p.stb_channel_info(user1)
        #print(tv_program)

        #res= p.stb_toolbox_select(user1,text=u'mes videos',confirm= False)
        #res= p.page_action_desk_move_to(user1,text='mes videos')
        time.sleep(5)
        res = p.serial_synch(user1)

        res = p.send_key(user1, key="4")

        res= p.serial_watch(user1,timeout=10)




    except RuntimeError as  e:
        print("interrupted")

    except Exception as  e:
        print(e.message)
    finally:
        r = p.close_session()

    return



def test_desk():
    """

        test remote without remote_server: expect requests timeout exception

    :return:
    """

    p =Pilot()

    p.setup_pilot("demo", "demo_qualif", phonehub_url)

    user1= user

    try:

        res= p.open_Session(user1)


        r= p.send_key(user1,key='MENU')

        # video a la demande / mes favoris
        rc= p.page_action_desk_select(user1,text="mes favoris")


        time.sleep(10)
        r = p.send_key(user1,key='MENU')
        time.sleep(1)
        r = p.send_key(user1,key='MENU')



    except RuntimeError as  e:
        print("interrupted")

    except Exception as  e:
        print(e.message)
    finally:
        r = p.close_session()

    return



def test_toolbox():
    """




    :return:
    """

    p =Pilot()

    p.setup_pilot("demo", "demo_qualif", phonehub_url)

    user1= user

    try:

        res= p.open_Session(user1)


            # select a channel
        r = p.send_key(user1,key='5')
        time.sleep(1)

        # trigger the toolbox menu
        r = p.send_key(user1,key='OK')

        #
        r = p.page_action_tool_select(user1,text='replay')

        time.sleep(10)
        # leave replay
        r = p.send_key(user1,key='7')
        time.sleep(1)


    except RuntimeError as  e:
        print("interrupted")

    except Exception as  e:
        print(e.message)
    finally:
        r = p.close_session()

    return






if __name__=="__main__":


    test_fetch_restop_interface()


    #test_scheduler()

    test_demo()


    #test_powerswitch()


    #test_phenix_ardom_menu()
    #test_desk()
    #test_toolbox()

    print("Done")
