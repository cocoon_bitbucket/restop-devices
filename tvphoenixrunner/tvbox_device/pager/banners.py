# -*- coding: utf-8 -*-

from uilibtest.mapping.common.common import Text,Image,Csa, Button,ListBoxItem,ListBox
from uilibtest.mapping.common.infobanner.timeline import TimeLine
import logging


class Banner(object):
    """

    """
    fields=[]
    def __init__(self,obj):
        """

        :param obj:
        """
        out=""
        self.data= {}
        for field in self.fields:
            value= getattr(obj,field)

            if isinstance(value, Text):
                out = self.decode_text(value)
            elif isinstance(value, str) or isinstance(value, unicode):
                out = u"\"{val}\"".format(val=value)
            elif isinstance(value,bool):
                out= value
            elif isinstance(value, Csa):
                self.handle_csa(value, self.data)
            elif isinstance(value,Image):
                self.handle_image(field,value,self.data)
            elif isinstance(value,TimeLine):
                self.handle_timeline(value,self.data)

            else:
                out = str(value)
            self.data[field]=out
            continue
        return

    def decode_text(self,value):
        """

        :param text:
        :param out:
        :return:
        """
        try:
            return value.text
        except Exception as e:
            return ""

    def handle_image(self,field,image,out):
        """

        :param image:
        :param out:
        :return:
        """
        try:
            value= getattr(image,"status")
            out[field] = value
        except Exception as e:
            out[field] = None
        return

    def handle_csa(self, csa, out):
        """

        :param timeline:
        :param out:
        :return:
        """
        try:
            status= getattr(csa,"status")
            if status is True:
                out["csa"]= True
                for i in range(0,6):
                    field= "CSA_%d" % i
                    v= getattr(csa,field)
                    if v is not None:
                        out[field] = True
                    else:
                        out[field]= False
            else:
                out['csa']= False
        except Exception as e:
            out['csa'] = False
        return

    def handle_timeline(self,timeline,out):
        """
            timeline Object contains several fields
                - begin_buffer_date Datetime
                - cursor_date Datetime
                - end_buffer_date Datetime
                - error
                - end_date Datetime
                - start_date Datetime
                - tool_tip Text

        :param object:  Timeline instance
        :param out: dict
        :return: None
        """
        for name in ['begin_buffer_date','cursor_date','end_buffer_date','end_date','start_date']:
            try:
                value = getattr(timeline,name)
                if value:
                    out[name]= str(value)
                else:
                    out[name]=""
            except Exception as e:
                pass
        for name in ['tooltip_text']:
            try:
                value = getattr(timeline,name)
                out[name] = self.decode_text(value)
            except Exception as e:
                out[name] = ""
        for name in ['error']:
            try:
                value = getattr(timeline,name)
                out[name] = self.decode_text(value)
            except Exception as e:
                out["name"] = ""


class BannerProgram(Banner):
    """
    self.picto_169 = Image()
        self.picto_audio_desc = Image()
        self.picto_csa = Csa()
        self.picto_dolby = Image()
        self.picto_dts = Image()
        self.picto_ear = Image()
        self.picto_hd = Image()
        self.picto_vm = Image()
        self.picto_startover = Image()
        self.program_time_line = TimeLine()
        self.is_recording = False
        self.progress_ongoing_record = False


    """
    fields= ["program","complement_title","genre","program_time_line","is_recording",
             "picto_169","picto_audio_desc","picto_dolby","picto_dts","picto_ear",
             "picto_hd","picto_vm","picto_startover","picto_csa","progress_ongoing_record"
             ]



class BannerChannel(Banner):
    """



    """
    fields= ("name","lcn","is_favorite")




