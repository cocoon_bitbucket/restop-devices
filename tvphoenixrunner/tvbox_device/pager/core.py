
import uilibtest
import logging


class Pager():
    """

        redefine WebKitPAge : add remote_control argument


    """

    def __init__(self,remote_control,device='tv',http_callback=None):
        """

        :param remote_control: instance of RpiRemoteControl or equivalent
        :return:
        """
        self.device_alias= device
        self.http_callback= http_callback
        self.ardom_filename = "ardom-%s.html" % self.device_alias

        self._auto_fetch= True

        self.logger = logging.getLogger('tvphoenixPager')

        self.remote=remote_control
        self.rc= remote_control


    def close(self):
        """

        :return:
        """
        pass

    def _get_dom(self):
        dom = None

        return dom