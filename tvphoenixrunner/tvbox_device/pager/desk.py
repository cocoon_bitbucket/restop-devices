import datetime

from slugify import slugify

from uilibtest.mapping import Desk
from uilibtest.config import Config
from uilibtest.tools import hookrequester
from uilibtest.tools.hookrequester import get_screen_dump, get_dump_by_object_name, get_dump_wait_for_object
from uilibtest.tools import find_children_by_property, find_children_by_object_name, str2bool
from uilibtest.mapping.desk import (DESK_APPLICATION, TILE_VIEW, DIGITS_BOX, DIGITS_BOX_TEXT,
                       HEADER, HEADER_SHORTCUT_LIST,
                       HEADER_FIBER_IMAGE)

import logging




class Menu(Desk):
    """

    """

    def __init__(self):
        """Constructor"""
        super(Menu,self).__init__()
        self.logger = logging.getLogger('PhoenixTesting')
        self.logger.debug("Menu Desk init")

        self._json_object = None

        # dictionary of topic : path from the desk
        self._topics= {}


    def fill(self, json_object=None):
        """
        .. seealso:: :func:`~src.mapping.common.common.MappingObject.fill`
        """
        if not json_object:
            properties = dict(visible=True)
            json_object = get_dump_wait_for_object(application=DESK_APPLICATION, object_name="Desk",
                                                  timeout=500, raw=False, removeHidden=False,
                                                  **properties)
            if not json_object:
                self.error= "cannot fill menu"
                return
            ##json_object = get_screen_dump(application=DESK_APPLICATION)

        self.set_default(json_object)
        self._json_object= json_object

        # Make sure that desk is visible: check the 'visible' property of TileView
        tile_view = find_children_by_object_name(json_object, TILE_VIEW)
        if tile_view is not None and tile_view["properties"]["visible"] == "true":
            selected_block = find_children_by_property(json_object, "isSelected", "true")
            if selected_block is not None:
                self.selected_block_title = selected_block["properties"]["title"]

        header = find_children_by_object_name(json_object, HEADER)
        if header is not None:
            self.header_focused = bool(header["properties"]["focus"])
            self.header_item_count = int(header["properties"]["item_count"])
            json_image = find_children_by_object_name(header, HEADER_FIBER_IMAGE)
            if json_image is not None:
                self.fiber_image.fill(json_image)
                self.fiber_image_visible = self.fiber_image.status

        options_shortcut_list = find_children_by_object_name(json_object, HEADER_SHORTCUT_LIST)
        if options_shortcut_list is not None and options_shortcut_list["properties"]["visible"] == "true":
            self.header_item_selected = int(options_shortcut_list["properties"]["currentShortcutsIndex"])

        json_digits_box = find_children_by_object_name(json_object, DIGITS_BOX)
        if json_digits_box is not None:
            self.digits_box.fill(json_digits_box)

        return

    def _wait(self):
        """
            wait for Desk to appear
        :return:
        """
        properties =dict(visible=True)

        json_object= get_dump_wait_for_object(application=DESK_APPLICATION, object_name="Desk",
                                     timeout=500, raw=False, removeHidden=False,
                                     **properties)
        return json_object

    def iter_child(self,root):
        """
                iter on children_of_? of an object
        :param root:
        :return:
        """
        try:
            count = int(root['children_count'])
            for i in range(0, count):
                child = root["children_info_%d" % i]['properties']
                yield child
        except Exception as e:
            print(e)
            raise StopIteration

    def iter_current_item_info(self, root):
        """
                iter on children_of_? of an object
        :param root:
        :return:
        """
        try:
            count = int(root['children_count'])
            for i in range(0, count):
                child = root["children_info_%d" % i]['properties']
                yield child
        except Exception as e:
            print(e)
            raise StopIteration



    def iter_block(self):
        """

            iter on each row of the desk menu (ie block)
        :return:
        """

        try:
            tile_view = find_children_by_object_name(self._json_object, TILE_VIEW)

            base = tile_view['properties']['children_info_0']['properties']

            for tile in self.iter_child(base):
                if tile['objectName'] == "Tile":
                    #block = tile['currentItem_info']['properties']
                    #yield block
                    yield tile
        except Exception as e:
            print(e)
            raise StopIteration



    def _get_vhv_coordinates(self):
        """

        :return:
        """
        i= -1
        h=-1
        v=-1
        # Make sure that desk is visible: check the 'visible' property of TileView
        tile_view = find_children_by_object_name(self._json_object, TILE_VIEW)
        if tile_view is not None and tile_view["properties"]["visible"] == "true":
            selected_block = find_children_by_property(self._json_object, "isSelected", "true")
            if selected_block is not None:
                self.selected_block_title = selected_block["properties"]["title"]
                try:
                    di= tile_view['properties']['currentIndex']
                    dh= tile_view['properties']["currentItem_info"]['properties']['_currentColumn']
                    dv= tile_view['properties']["currentItem_info"]['properties']['_currentRow']
                    h = int(dh)
                    v= int(dv)
                    i= int(di)
                except Exception as e:
                    pass
        return i,h,v

    def slug(self,text):
        """

        :param topic:
        :return:
        """
        try:
            text = slugify(text.lower())
        except Exception as e:
            text= ""
        return text

    def iter_topic(self):
        """


        :return:
        """
        index =0
        for block in self.iter_block():
            #print(block['title'])
            #print("      %s" % block['objectName'])
            info = block['currentItem_info']['properties']
            #print(info['title'])

            for item in self.iter_child(block):
                try:
                    text= item['title']
                    x= int(item['columnPosition'])
                    y= int(item['rowPosition'])
                except Exception as e:
                    text=""
                    x=-1
                    y=-1
                if text:
                    path = "%s/%s/%s" % (index,x,y)
                    text = self.slug(text)
                    #print("    text:%s  %s" % (text, path))
                    yield text,path
            index += 1



    def lookup_topic(self, topic):
        """

        :param topi:
        :return:
        """
        if self._topics == {}:
            self._topics["recherche"] = "-1"
            for name,path in self.iter_topic():
                self._topics[name]= path

        try:
            # return the path (0/1/2) corresponding to the topic
            name= self.slug(topic)
            return self._topics[name]
        except KeyError:
            # not topic: return blank path
            return ""



    def decode_path(self,path):
        """

        :param path: string represents a path  1/2/3

            first move is on vertical axe  -1 UP , 1 DOWN
            second mode is on horizontal axe -1 LEFT , +1 RIGHT
            third move in on vertical axe -1 UP +1 DOWN

        :return: a sequence of IP/DOWN/RIGHT/LEFT
        """
        sequence= []
        direction = "vertical"
        if not path:
            return sequence
        parts= path.split("/")
        for index,part in enumerate(parts):
            steps= int(part)
            if steps:
                if direction == 'vertical':
                    if steps > 0:
                        # MOVE DOWN +1
                        step= "DOWN"
                    else:
                        # MoVE UP -1
                        step= "UP"
                else :
                    # direction is horizontal
                    if steps > 0:
                        # Move RIGHT +1
                        step= "RIGHT"
                    else:
                        # Move LEFT -1
                        step= "LEFT"
                move = [step for i in range(abs(steps))]
                sequence.extend(move)
            # next direction
            if direction == "vertical" :
                direction = "horizontal"
            else:
                direction= "vertical"
        return sequence







                # try:
        #     tile_view = find_children_by_object_name(self._json_object, TILE_VIEW)
        #
        #     base = tile_view['properties']['children_info_0']['properties']
        #     count = int(base['children_count'])
        #     for i in range(0,count):
        #         tile = base["children_info_%d" % i ]['properties']
        #
        #         if tile['objectName'] == "Tile" :
        #             info = tile['currentItem_info']['properties']
        #             print(info['title'])
        #             print("      %s" % info['objectName'])




    def _waitObjectVisibleProperty(self, objectname, timeout, visible):
        visible_parameter = "true"
        if not visible:
            visible_parameter = "false"

        # hookresult = hookrequester.wait_for_object(
        #     objectname, timeout, "visible=" + visible_parameter)
        # result = hookresult is not None
        result=True
        return result

    def _wait_visibility(self, timeout=3000, visibility=True):
        """
        Wait the Infobanner visiblity (visible or not based on parameter visibility) during the time pass in parameter.
        If the time out is reached without the expected state of the InfoBanner, method return False.
        @param timeout: in milliseconds. Time to wait for the expected state of the Infobanner.
        @type timeout: int

        @param visibility: If True the method wait that the infobanner becomes visible. If False, wait that the infobanner disapears.
        @type visibility: boolean

        @return: True if the info banner is is the expected state0. False if the Infobanner is not found or if the timeout is reached.
        @rtype: boolean

        """
        if visibility:
            self.logger.debug(">>> Wait Desk becomes visible.")
        else:
            self.logger.debug(">>> Wait that the Desk disapears")
        # result = self._waitObjectVisibleProperty(
        #     objectname=ZAPPING_BANNER, timeout=timeout, visible=visibility)
        # if result:
        #     if visibility:
        #         self.logger.debug(">>> ZappingBanner becomes visible")
        #     else:
        #         self.logger.debug(">>> ZappingBanner disapears")
        # else:
        #     if visibility:
        #         self.logger.debug(
        #             ">>> Timeout reached, ZappingBanner doesn't become visible")
        #     else:
        #         self.logger.debug(
        #
        #         ">>> Timeout reached, ZappingBanner hasn't disapear")
        result= True
        return result

    def _get_info(self):
        """
        Get program display on infobanner only if infobanner is found and visible

        :return: Program display on infobanner. None if infobanner is not visible or not found
        :rtype: ~uiphoenixtester.uicontrols.containers.ProgramInfoItem
        """
        self.logger.info(" >>> Get Desk info")
        result = None
        # visible = self._wait_live_infobanner_visibility()
        # if visible:
        #     info_banner = ZappingBanner.load()
        #     if info_banner:
        #         # result = ProgramInfoItem._buildProgramInfoItemWithInfoBannerProgram(
        #         #    info_banner.info_banner_program, info_banner.info_banner_channel)
        #         print(info_banner.info_banner_program.program.decode())
        #         # print(info_banner.info_banner_channel)
        #         program = BannerProgram(info_banner.info_banner_program)
        #         channel = BannerChannel(info_banner.info_banner_channel)
        #         result = program, channel
        #
        # else:
        #     self.logger.warning(" <<< Zapping banner is not visible")
        #
        # if result:
        #     self.logger.debug("InfoBanner infos:")
        #     self.logger.debug(result[0].data, result[1].data)
        # else:
        #     self.logger.debug(str(result))
        return result


    def _export(self, data=None):
        """
            export data to a json compatible dict

        :param data:
        :return:
        """
        data = data
        dic = {}
        for attr_name, attr_value in data.items():
            if isinstance(attr_value, (basestring, int, bool)):
                dic[attr_name] = attr_value
            elif isinstance(attr_value, datetime.datetime):
                dic[attr_name] = str(attr_value)
            elif isinstance(attr_value, datetime.timedelta):
                dic[attr_name] = str(attr_value)
            elif attr_value is None:
                dic[attr_name] = None
            else:
                pass
        return dic


if __name__ == "__main__":
    """



    """
    config = Config()
    config.stb_ip = "192.168.1.55"


    logging.basicConfig(level=logging.DEBUG)

    s = Menu()

    s._wait()


    # test decode path
    seq= s.decode_path("0/0/0")
    assert seq == []
    seq= s.decode_path("1/3/1")
    assert seq == ['DOWN', 'RIGHT', 'RIGHT', 'RIGHT', 'DOWN']
    seq= s.decode_path("-1")
    assert seq == ['UP']


    s.fill()
    print(s.selected_block_title)
    r = s._get_vhv_coordinates()
    print(r)

    r= list(s.iter_topic())
    print(r)


    path= s.lookup_topic("programme Tv")
    print(path)

    print(s.lookup_topic("les chaines web"))

    #lb = s._get_info()

    print("Done")
