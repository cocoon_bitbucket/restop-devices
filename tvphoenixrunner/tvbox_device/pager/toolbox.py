
from slugify import slugify
from uilibtest.mapping.live import Toolbox as BaseToolbox
from uilibtest.tools import hookrequester
from uilibtest.config import Config


import logging


class Toolbox(BaseToolbox):
    """

    """

    def slug(self, text):
        """

        :param topic:
        :return:
        """
        try:
            text = slugify(text.lower())
        except Exception as e:
            text = ""
        return text

    def lookup_topic(self,slug):
        """


        :return:
        """
        r= self.menu_names.index(self.slug(slug))
        return r

    def path_to_topic(self,topic):
        """

        find the topic then determine the sequence to reach it  ["RIGHT","RIGHT" ]

        if sequence is [] then we already positioned on it

        if sequence is None: cannot find the topic



        :param slug:
        :return:
        """
        sequence= []
        try:
            index= self.lookup_topic(topic)
        except IndexError:
            # cannot find the topic
            return None
        dist = index - self.current_index
        if dist == 0:
            # alrady on it
            return []
        elif dist > 0:
            step= "RIGHT"
        else:
            step= "LEFT"
        sequence = [step for i in range(abs(dist))]

        return sequence




    @property
    def menu_names(self):
        """
            return the list of menu item

        :return:
        """
        #items= [str(self.panels_title_list[i]) for i in range(0,len(s.panels_title_list))]
        #return items

        items= [ self.slug(e.text) for e in self.panels_title_list ]
        print(items)
        return items



if __name__ == "__main__":
    """



    """
    logging.basicConfig(level=logging.DEBUG)

    config = Config()
    config.stb_ip = "192.168.1.55"

    logging.basicConfig(level=logging.DEBUG)

    s = Toolbox()
    s.fill()

    l= s.menu_names


    index= s.lookup_topic("rePlay")

    path = s.path_to_topic('replay')





    print("Done")
