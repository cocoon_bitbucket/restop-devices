# -*- coding: utf-8 -*-

from uilibtest.mapping.live import ZappingBanner,ZAPPING_BANNER
from uilibtest.tools import hookrequester
from uilibtest.config import Config
from uilibtest.mapping.common.common import Text,Image,Button,ListBoxItem,ListBox
from uilibtest.mapping.common.infobanner.timeline import TimeLine
import logging

config= Config()
config.stb_ip= "192.168.1.55"



class Banner(object):
    """

    """
    fields=[]
    def __init__(self,obj):
        """

        :param obj:
        """
        out=""
        self.data= {}
        for field in self.fields:
            value= getattr(obj,field)

            if isinstance(value, Text):
                out = self.decode_text(value)
            elif isinstance(value, str) or isinstance(value, unicode):
                out = u"\"{val}\"".format(val=value)
            elif isinstance(value,TimeLine):
                self.handle_timeline(value,self.data)
            else:
                out = str(value)
            self.data[field]=out
            continue
        return

    def decode_text(self,value):
        """

        :param text:
        :param out:
        :return:
        """
        try:
            return value.text
        except Exception as e:
            return ""


    def handle_timeline(self,timeline,out):
        """
            timeline Object contains several fields
                - begin_buffer_date Datetime
                - cursor_date Datetime
                - end_buffer_date Datetime
                - error
                - end_date Datetime
                - start_date Datetime
                - tool_tip Text

        :param object:  Timeline instance
        :param out: dict
        :return: None
        """
        for name in ['begin_buffer_date','cursor_date','end_buffer_date','end_date','start_date']:
            try:
                value = getattr(timeline,name)
                if value:
                    out[name]= str(value)
                else:
                    out[name]=""
            except Exception as e:
                pass
        for name in ['tool_tip']:
            """
            """
            try:
                value = getattr(timeline,name)
                out[name] = self.decode_text(value)
            except Exception as e:
                out[name] = ""
        for name in ['error']:
            try:
                value = getattr(timeline,name)
                out[name] = self.decode_text(value)
            except Exception as e:
                out["name"] = ""


class BannerProgram(Banner):
    """
    self.picto_169 = Image()
        self.picto_audio_desc = Image()
        self.picto_csa = Csa()
        self.picto_dolby = Image()
        self.picto_dts = Image()
        self.picto_ear = Image()
        self.picto_hd = Image()
        self.picto_vm = Image()
        self.picto_startover = Image()
        self.program_time_line = TimeLine()
        self.is_recording = False
        self.progress_ongoing_record = False


    """
    fields= ["program","complement_title","genre","program_time_line","is_recording"]



class BannerChannel(Banner):
    """



    """
    fields= ("name","lcn","is_favorite")





class Screen(object):
    """
    The Screen class can be used to manipulate, with some \
    predefined methods, the User Interface displayed on the TV screen.

    :Example:

        **Instanciate:**

            .. code-block:: python

                from uiphoenixtester.uicontrols import Screen
                screen = Screen()

        **Some usefull methods:**

            .. code-block:: python

                from uiphoenixtester.uicontrols import Screen

                screen = Screen()
                # Get the screen dump:
                screenDump = screen._get_screen_dump()
                # Select "mes enregistrements" Block in desk
                screen.action_desk_select(u"mes enregistrements")
                # Go to 21th channel through the VZL
                screen.action_select_in_VZLBanner(21)

    """
    def __init__(self):
        """Constructor"""
        self.logger = logging.getLogger('PhoenixTesting')
        self.logger.debug("Screen init")

        self._last_screen_dump = None

    def _waitObjectVisibleProperty(self, objectname, timeout, visible):
        visible_parameter = "true"
        if not visible:
            visible_parameter = "false"

        hookresult = hookrequester.wait_for_object(
            objectname, timeout, "visible=" + visible_parameter)
        result = hookresult is not None
        return result



    def _wait_live_infobanner_visibility(self, timeout=3000, visibility=True):
        """
        Wait the Infobanner visiblity (visible or not based on parameter visibility) during the time pass in parameter.
        If the time out is reached without the expected state of the InfoBanner, method return False.
        @param timeout: in milliseconds. Time to wait for the expected state of the Infobanner.
        @type timeout: int

        @param visibility: If True the method wait that the infobanner becomes visible. If False, wait that the infobanner disapears.
        @type visibility: boolean

        @return: True if the info banner is is the expected state0. False if the Infobanner is not found or if the timeout is reached.
        @rtype: boolean

        """
        if visibility:
            self.logger.debug(">>> Wait that ZappingBanner becomes visible.")
        else:
            self.logger.debug(">>> Wait that the ZappingBanner disapears")
        result = self._waitObjectVisibleProperty(
            objectname=ZAPPING_BANNER, timeout=timeout, visible=visibility)
        if result:
            if visibility:
                self.logger.debug(">>> ZappingBanner becomes visible")
            else:
                self.logger.debug(">>> ZappingBanner disapears")
        else:
            if visibility:
                self.logger.debug(
                    ">>> Timeout reached, ZappingBanner doesn't become visible")
            else:
                self.logger.debug(
                    ">>> Timeout reached, ZappingBanner hasn't disapear")
        return result


    def get_info_from_livebanner(self):
        """
        Get program display on infobanner only if infobanner is found and visible

        :return: Program display on infobanner. None if infobanner is not visible or not found
        :rtype: ~uiphoenixtester.uicontrols.containers.ProgramInfoItem
        """
        self.logger.info(" >>> Zapping Banner Get program info")
        result = None
        visible = self._wait_live_infobanner_visibility()
        if visible:
            info_banner = ZappingBanner.load()
            if info_banner:
                #result = ProgramInfoItem._buildProgramInfoItemWithInfoBannerProgram(
                #    info_banner.info_banner_program, info_banner.info_banner_channel)
                print(info_banner.info_banner_program.program.decode())
                #print(info_banner.info_banner_channel)
                program= BannerProgram(info_banner.info_banner_program)
                channel= BannerChannel(info_banner.info_banner_channel)
                result= program,channel

        else:
            self.logger.warning(" <<< Zapping banner is not visible")

        if result:
            self.logger.debug("InfoBanner infos:")
            self.logger.debug(result[0].data,result[1].data)
        else:
            self.logger.debug(str(result))
        return result




if __name__ == "__main__":
    """
    
    
    
    """
    logging.basicConfig(level=logging.DEBUG)


    s = Screen()

    lb = s.get_info_from_livebanner()

    print("Done")

