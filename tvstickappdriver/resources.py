# -*- coding: utf-8 -*-
__author__ = 'cocoon'


from models import DEVICE_NAME


#from models import TvstickappAgents
#from agent import Agent
from restop.plugins.resources import RootResource
from droydrunner.resources import DeviceResource as DroydrunnerResource


from agent import Agent


class Resource_Root(RootResource):
    """

    """
    collection= 'root'


#class Resource_samples(Resource):
class DeviceResource(DroydrunnerResource):
    """
        DROYDRUNNER  : an Android Device

    """
    collection= DEVICE_NAME
    protected= ''

    _AgentClass = Agent

    selectors = {
        'launch_tv': {'text': 'la clé TV'}
    }

    galaxy_selectors= {
        'launch_tv': { 'className':"com.sec.android.app.launcher.views.HomeItemView",'text': 'la clé TV' }
    }

    other_selectors= {
        'launch_tv': {'className': 'android.widget.TextView', 'packageName': 'com.sec.android.app.launcher',
                              'text': 'la clé TV'},
        #'launch_tv': {'className': 'android.widget.TextView','text': 'la clé TV'},

    }


    def op_start_tv(self, item, **kwargs):
        """
            start the

        """
        agent_model = self.model.load(item)
        agent = Agent(agent_model)
        agent.model.logs.append('start tv application')
        # click start tv button
        rc= agent.click(** self.selectors['launch_tv'])
        return agent.model.make_response(rc)


    def op_start_channel(self, item, **kwargs):
        """
            start channel ( channel )

        :param item:
        :return:
        """
        agent_model = self.model.load(item)
        agent = Agent(agent_model)


        input = self.request.json
        channel = input['channel']
        start_channel = u'Démarrer la lecture de : '
        channel_name = start_channel + channel

        agent.model.logs.append("start channel %s" % channel_name)

        #'Démarrer la lecture de : ' +
        rc = agent.click_by_child_by_desc('com.orange.lacletv:id/live_tv_channel_listview',
                                          channel_name,
                                          'android.widget.FrameLayout')
        return agent.model.make_response(rc)

    def op_open_vod_menu(self, item, **kwargs):
        """
            open_vod_menu ( channel )

        :param item:
        :param kwargs:
        :return:
        """
        agent_model = self.model.load(item)
        agent = Agent(agent_model)

        input = self.request.json
        channel = input['channel']
        start_channel = u'TV à la demande : '
        channel_name = start_channel + channel

        agent.model.logs.append("open vod menu %s" % channel_name)

        rc = agent.click_by_child_by_desc('com.orange.lacletv:id/catchup_tv_gridview_channel',
                                           channel_name, 'android.widget.FrameLayout')
        return agent.model.make_response(rc)

    def op_open_vod_type(self, item, **kwargs):
        """
            open_vod_type( type )
        :param item:
        :return:
        """
        agent_model = self.model.load(item)
        agent = Agent(agent_model)

        input = self.request.json
        menuName = input['type']
        menuFullName =  menuName + u' : Tout afficher'

        agent.model.logs.append("open vod type %s" % menuFullName)

        rc = agent.click_by_desc('android.widget.TextView', menuFullName)

        return agent.model.make_response(rc)

    def op_scroll_to_vertical(self, item, **kwargs):
        """


        :param item:
        :return:
        """
        agent_model = self.model.load(item)
        agent = Agent(agent_model)

        request = self.request.json
        scroll_class = request['scroll_class']
        word_stop = request['word_stop']

        agent.model.logs.append("scroll to vertical %s %s" % (scroll_class,word_stop) )

        rc = agent.scroll_to_vertical(scroll_class=scroll_class, textContains=word_stop)

        return agent.model.make_response(rc)