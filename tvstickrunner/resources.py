__author__ = 'cocoon'


from restop.application import ApplicationError
from models import DEVICE_NAME
#from models import *
from agent import Agent
from restop.plugins.resources import RootResource
from restop_platform.resources import AgentResource

class Resource_Root(RootResource):
    """

    """
    collection= 'root'

class DeviceResource(AgentResource):
    """
        TVSTICKRUNNER : the Orange Stick TV

    """
    collection= DEVICE_NAME
    protected= ''

    _AgentClass= Agent


    def configure_session(self,session_id,members,configuration):
        """
        :param session_id:
        :param members:
        :param configuration:
        :return:
        """
        return configuration

