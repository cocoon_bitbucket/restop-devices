
import os

from restop.application import ApplicationError
from restop_components.some_agents.pipe_agents import SerialAgent
from restop_components.powerswitch_device import PowerswitchPlug
from restop_components.video_capture.video_detection import StickTvVideoDetection


default_metrics= ['df', 'cpu','meminfo','loadavg']

default_powerswitch= 'powerswitch'

class Agent(SerialAgent):
    """




    """
    collection= 'tvstick_agents'


    def _setup(self):
        """

        :return:
        """
        super(Agent,self)._setup()
        # add video detection
        self._video_detection = StickTvVideoDetection()
        # add powerswitch
        if 'powerswitch_device' in self.model.parameters:
            if 'powerswitch_port' in self.model.parameters:
                self._powerswitch= PowerswitchPlug.get(
                    self.model.parameters['powerswitch_device'],
                    channel= self.model.parameters['powerswitch_port']
                )
            else:
                raise ApplicationError('no powerswitch_port declared in platform for device: %s' % self.alias)
        else:
            self._powerswitch=None

    #
    # interface to serial
    #
    def serial_send(self, cmd):
        """
            send a command to console via serial port
        :param cmd:
        :return:
        """
        return self.send(cmd)

    def serial_watch(self,timeout=5):
        """
            watch serial log for a duration
        :param timeout:
        :return:
        """
        return self.watch(timeout=timeout)

    def serial_expect(self,pattern,timeout=5,cancel_on=None, regex='no', **kwargs ):
        """
            read serial to find the pattern

        :param pattern:
        :param timeout:
        :return:
        """
        return self.expect(pattern,timeout=timeout,cancel_on=cancel_on, regex=regex, **kwargs )


    def serial_synch(self,timeout=10):
        """
            empty the serial log
        :param timeout:
        :return:
        """
        return self.sync(timeout=timeout)


    #
    # interface to video
    #
    def detect_motion(self):
        """
            check if there is a movement
        :return:
        """
        r = self._video_detection.motionDetection()
        return r

    def video_screenshot(self):
        """
            check if there is a movement
        :return: False or encoded image
        """
        r = False
        try:
            os.unlink('stickimage.png')
        except OSError:
            pass
        self._video_detection.screenshot("stickimage.png")
        with open("stickimage.png", "rb") as fh:
            data = fh.read()
            encoded = data.encode("base64")
            os.unlink("stickimage.png")
            # print("flash:\n")
            # print encoded
            # print("----\n")
            return encoded

    #
    #  powerswitch interface
    #
    def power_on(self):
        """

        :return:
        """
        if self._powerswitch:
            return self._powerswitch.switch_on()
        else:
            raise ApplicationError('no powerswitch declared in platform')

    def power_off(self):
        """

        :return:
        """
        if self._powerswitch:
            return self._powerswitch.switch_off(self.model.parameters['powerswitch_port'])
        else:
            raise ApplicationError('no powerswitch declared in platform')
